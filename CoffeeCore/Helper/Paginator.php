<?php

namespace CoffeeCore\Helper;

/**
 * Class Paginator
 * @package CoffeeCore\Helper
 */
class Paginator extends \Voodoo\Paginator
{

    /**
     * Render the paginator in HTML format
     *
     * @param string $aClass
     * @return string
     */
    public function toHtml($aClass = "bts")
    {
        //$list = "<a class=\"{$aClass}\" href=\"{$this->getPrevPageUrl()}\">«{$this->prevTitle}</a> " . PHP_EOL;

        foreach ($this->toArray() as $page) {
            $tagClass = ($page["is_current"]) ? " atual" : "";
            $list .= "<a class=\"{$aClass}{$tagClass}\" href=\"{$page["url"]}\">{$page["label"]}</a>" . PHP_EOL;
        }

        //$list .= "<a class=\"{$aClass}\" href=\"{$this->getNextPageUrl()}\">»{$this->nextTitle}</a>" . PHP_EOL;

        return $list;
    }
} 