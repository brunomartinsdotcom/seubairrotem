<?php

namespace CoffeeCore\Helper;

/**
 * Class Abrevia
 * @package CoffeeCore\Helper
 */
class Abrevia
{
    /**
     * @param $text
     * @param $limit
     * @param string $delimiter
     * @return string
     */
    public function abreviar($text, $limit, $delimiter = '...'){
        $totalCaracteres = 0;
        //Retorna o texto em plain/text
        $text = $this->escapeText($text);
        //Cria um array com todas as palavras do texto
        $vetorPalavras = explode(" ", $text);
        if(strlen($text) <= $limit):
            $delimiter = "";
            $newText = $text;
        else:
            //Começa a criar o novo texto resumido.
            $newText = "";
            //Acrescenta palavra por palavra na string enquanto ela
            //não exceder o tamanho máximo do resumo
            for($i = 0; $i <count($vetorPalavras); $i++):
                $totalCaracteres += strlen(" ".$vetorPalavras[$i]);
                if($totalCaracteres <= $limit)
                    $newText .= ' ' . $vetorPalavras[$i];
                else break;
            endfor;
        endif;
        return $newText . $delimiter;
    }

    /**
     * @param $string
     * @return string
     */
    private function escapeText($string){
        $trans_tbl = get_html_translation_table(HTML_ENTITIES);
        $trans_tbl = array_flip($trans_tbl);
        return trim(strip_tags(strtr($string, $trans_tbl)));
    }
} 