<?php

namespace CoffeeCore\Helper;


/**
 * Class JsonReader
 * @package CoffeeCore\Helper
 */
class JsonReader
{
    private $resourcePath = "res/";

    /**
     * @param string $resourcePath
     */
    public function setResourcePath($resourcePath)
    {
        $this->resourcePath = $resourcePath;
    }

    /**
     * @return string
     */
    public function getResourcePath()
    {
        return $this->resourcePath;
    }

    /**
     * @var string
     */
    private $fileContent = '';

    /**
     * @param null $file
     */
    public function __construct($file = null)
    {
        if (!empty($file)) {
            $this->readFile($file);
        }
    }

    /**
     * @param $file
     * @throws \Exception
     */
    public function readFile($file)
    {
        try {
            $this->fileContent = file_get_contents($this->resourcePath . $file . '.json');
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param bool $returnArrayAssoc
     * @throws \Exception
     * @return mixed
     */
    public function getFileContent($returnArrayAssoc = false)
    {
        try {
            return json_decode($this->fileContent, $returnArrayAssoc);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
