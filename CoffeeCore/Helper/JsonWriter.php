<?php

namespace CoffeeCore\Helper;


/**
 * Class JsonWriter
 * @package CoffeeCore\Helper
 */
class JsonWriter
{
    private $resourcePath = "lib/res/";

    /**
     * @param string $resourcePath
     */
    public function setResourcePath($resourcePath)
    {
        $this->resourcePath = $resourcePath;
    }

    /**
     * @return string
     */
    public function getResourcePath()
    {
        return $this->resourcePath;
    }

    /**
     * @var string
     */
    private $fileContent = '';

    /**
     * @param $file
     * @param $content
     * @throws \Exception
     */
    public function writeFile($file, $content)
    {
        $file = $this->resourcePath . $file . '.json';
        try {
            if (file_exists($file)) {
                unlink($file);
            }

            $fh = fopen($file, 'w+');
            fwrite($fh, $content);
            fclose($fh);

        } catch (\Exception $e) {
            throw $e;
        }
    }
}
