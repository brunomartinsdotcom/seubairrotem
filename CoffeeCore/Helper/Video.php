<?php
namespace CoffeeCore\Helper;

/**
 * Class Video
 * @package CoffeeCore\Helper
 */
class Video
{
    /**
     * @param $url
     * @param $tamanho
     * @return string
     */
    public function identificaVideo($url, $tamanho)
    {
        $url_imagem = parse_url($url);
        if ($url_imagem['host'] == 'www_antigo.youtube.com' || $url_imagem['host'] == 'youtube.com' || $url_imagem['host'] == 'youtu.be') {
            if ($tamanho == 'maior') {
                return '<iframe width="640" height="480" src="http://www_antigo.youtube.com/embed/' . $this->idYouTube($url) . '" frameborder="0" allowfullscreen></iframe>';
            } else if ($tamanho == 'menor') {
                return '<iframe width="300" height="225" src="http://www_antigo.youtube.com/embed/' . $this->idYouTube($url) . '" frameborder="0" allowfullscreen></iframe>';
            }
        } else if ($url_imagem['host'] == 'www_antigo.vimeo.com' || $url_imagem['host'] == 'vimeo.com') {
            if ($tamanho == 'maior') {
                return '<iframe src="http://player.vimeo.com/video/' . $this->idVimeo($url) . '?title=0&amp;byline=0&amp;portrait=0" width="640" height="480" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
            } else if ($tamanho == 'menor') {
                return '<iframe src="http://player.vimeo.com/video/' . $this->idVimeo($url) . '?title=0&amp;byline=0&amp;portrait=0" width="300" height="225" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
            }
        }
        return false;
    }

    /**
     * @param $url
     * @return mixed
     */
    public function video_imagem($url)
    {
        $url_imagem = parse_url($url);
        if ($url_imagem['host'] == 'www_antigo.youtube.com' || $url_imagem['host'] == 'youtube.com') {
            $array = explode("&", $url_imagem['query']);
            return "http://img.youtube.com/vi/" . substr($array[0], 2) . "/0.jpg";
        } else if ($url_imagem['host'] == 'www_antigo.youtu.be' || $url_imagem['host'] == 'youtu.be') {
            $array = explode(".be/", $url);
            return "http://img.youtube.com/vi/" . substr($array[1], 0, 11) . "/0.jpg";
        } else if ($url_imagem['host'] == 'www_antigo.vimeo.com' || $url_imagem['host'] == 'vimeo.com') {
            $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/" . substr($url_imagem['path'], 1) . ".php"));
            return $hash[0]["thumbnail_small"];
        }
        return false;
    }

    /**
     * @param $url
     * @return string
     */
    public function idVimeo($url)
    {
        $var = explode(".com/", $url);
        $final = substr($var[1], 0, 8);
        return $final;
    }

    /**
     * @param $url
     * @return string
     */
    public function idYouTube($url)
    {
        $final = false;
        $url_imagem = parse_url($url);
        if ($url_imagem['host'] == 'www_antigo.youtube.com' || $url_imagem['host'] == 'youtube.com') {
            $var = explode("watch?v=", $url);
            $final = substr($var[1], 0, 11);
        } else if ($url_imagem['host'] == 'youtu.be') {
            $var = explode(".be/", $url);
            $final = substr($var[1], 0, 11);
        }
        return $final;
    }
}
