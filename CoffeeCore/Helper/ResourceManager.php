<?php

namespace CoffeeCore\Helper;


use \Exception;

/**
 * Class ResourceManager
 * @package CoffeeCore
 */
class ResourceManager
{
    /**
     * @var self
     */
    static private $instance;

    /**
     * @return ResourceManager
     */
    protected static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @param $filename
     * @throws Exception
     * @return array
     */
    public static function readFile($filename)
    {
        try {
            $file = ROOT . "res/" . $filename;
            $content = file_get_contents($file);
        } catch (Exception $e) {
            throw new Exception("Error open file: " . $e->getMessage());
        }

        return json_decode($content, true);
    }

    /**
     * @param $file
     * @param $content
     * @throws \Exception
     */
    public static function writeFile($file, $content)
    {
        $file = ROOT . "res/" . $file;
        try {
            if (file_exists($file)) {
                unlink($file);
            }
            echo $file;

            $fh = fopen($file, 'w+');
            fwrite($fh, $content);
            fclose($fh);

        } catch (\Exception $e) {
            throw $e;
        }
    }
}
