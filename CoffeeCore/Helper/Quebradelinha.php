<?php

namespace CoffeeCore\Helper;

/**
 * Class Abrevia
 * @package CoffeeCore\Helper
 */
class Quebradelinha
{
    /**
     * @param $text
     * @param $limit
     * @param string $delimiter
     * @return string
     */
    public function quebradelinha($txt){
		$texto = str_replace("\r","<br />",$txt);
		return $texto;
    }
} 