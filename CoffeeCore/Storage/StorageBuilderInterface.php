<?php

namespace CoffeeCore\Storage;

/**
 * Interface StorageInterface
 * @package CoffeeCore\Storage\ORM
 */
interface StorageBuilderInterface
{
    /**
     * @param null $config
     * @param bool $devMode
     * @return Object
     */
    public static function getConnection($config = null, $devMode = false);

}
