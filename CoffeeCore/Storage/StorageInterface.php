<?php

namespace CoffeeCore\Storage;

use CoffeeCore\Core\Entity;

/**
 * Interface StorageInterface
 * @package CoffeeCore\Storage
 */
interface StorageInterface
{
    /**
     * @param array $criteria
     * @param array $orderBy
     * @param null $limit
     * @param null $offset
     * @return array|\arrayIterator
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null);

    /**
     * @param array $criteria
     * @param array $orderBy
     * @return array|\stdClass
     */
    public function findOneBy(array $criteria, array $orderBy = null);

    /**
     * @param Entity $entity
     * @return void
     */
    public function insert(Entity $entity);

    /**
     * @param Entity $entity
     * @return void
     */
    public function update(Entity $entity);

    /**
     * @param Entity $entity
     * @return void
     */
    public function delete(Entity $entity);
}
