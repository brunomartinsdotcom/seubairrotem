<?php

namespace CoffeeCore\Storage\Doctrine;


use CoffeeCore\Helper\ResourceManager;
use CoffeeCore\Storage\StorageBuilderInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

/**
 * Class Builder
 * @package CoffeeCore\Storage\Doctrine
 */
class Builder implements StorageBuilderInterface
{
    /**
     * @var array
     */
    protected static $connectionConfig = [];

    /**
     * @var bool
     */
    protected static $isDevMode = true;

    /**
     * @var EntityManager
     */
    protected static $entityManager;


    /**
     * @param array $connectionConfig
     * @param bool $devMode
     */
    protected function __construct(array $connectionConfig = [], $devMode = false)
    {
        $this->configs = $connectionConfig['config'];

    }

    /**
     * @return EntityManager
     */
    protected static function getEntityManager()
    {
        $setup  = Setup::createAnnotationMetadataConfiguration([], self::$isDevMode);
        return EntityManager::create(self::$connectionConfig, $setup);
    }

    /**
     * @param $config
     * @param bool $devMode
     * @return EntityManager
     */
    public static function getConnection($config = null, $devMode = false)
    {
        if (!self::$entityManager instanceof EntityManager or !self::$entityManager->isOpen()) {
            if (empty($config) and empty(self::$connectionConfig)) {
                self::$connectionConfig = ResourceManager::readFile('databaseConfig.json');
            }
            self::$entityManager = self::getEntityManager();
        }
        return self::$entityManager;
    }
}
