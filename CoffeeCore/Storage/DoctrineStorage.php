<?php

namespace CoffeeCore\Storage;

use CoffeeCore\Core\Entity;
use CoffeeCore\Storage\Doctrine\Builder;
use Doctrine\ORM\EntityManager;


/**
 * Class DoctrineStorage
 * @package CoffeeCore\Storage
 */
class DoctrineStorage implements StorageInterface
{
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    protected $repository;

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @var EntityManager
     */
    protected $storage;

    /**
     * @param string|Entity $entity
     * @throws \Exception
     */
    public function __construct($entity = null)
    {
        $this->storage = self::orm();

        if (!empty($entity)) {
            if ($entity instanceof Entity) {
                if (!empty($entity::FULL_NAME)) {
                    $this->repository = $this->storage->getRepository($entity::FULL_NAME);
                }
            }
            else {
                try {
                    $this->repository = $this->storage->getRepository($entity);
                } catch (\Exception $e) {
                    throw new \Exception("Entidade não encontrada.");
                }
            }
        }
    }

    public static function orm()
    {
        if (! Builder::getConnection()->isOpen()) {
            return Builder::getConnection()->create(
                Builder::getConnection()->getConnection(),
                Builder::getConnection()->getConfiguration()
            );
        }
        return Builder::getConnection();
    }

    /**
     * @param array $criteria
     * @param array $orderBy
     * @param integer|null $limit
     * @param null $offset
     * @return \ArrayIterator
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        $result = $this->repository->findBy($criteria, $orderBy, $limit, $offset);
        return new \ArrayIterator($result);

    }

    /**
     * @param array $criteria
     * @param array $orderBy
     * @return array|\stdClass
     */
    public function findOneBy(array $criteria, array $orderBy = null)
    {
        return $this->repository->findOneBy($criteria, $orderBy);
    }

    /**
     * @param Entity $entity
     * @return void
     */
    public function update(Entity $entity)
    {
        $this->storage->merge($entity);

    }

    /**
     * @param Entity $entity
     * @return void
     */
    public function delete(Entity $entity)
    {
        $this->storage->remove($entity);
    }

    /**
     * @param Entity $entity
     * @return void
     */
    public function insert(Entity $entity)
    {
        $this->storage->persist($entity);
    }

    /**
     * @param string $sql
     * @return void
     */
    public function nativeQuery($sql)
    {
        $this->storage->createNativeQuery($sql, null);
    }

    /**
     *
     */
    public function flush()
    {
        $this->storage->flush();
    }

    public function __destruct() {
        //$this->orm()->getConnection()->close();
        //$this->orm()->close();
        unset($this->storage);
    }
}
