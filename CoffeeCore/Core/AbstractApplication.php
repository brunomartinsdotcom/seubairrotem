<?php

namespace CoffeeCore\Core;

use Slim\Slim;

/**
 * Class App
 * @package CoffeeCore\Api
 */
abstract class AbstractApplication
{
    /**
     *
     */
    public function __construct(Slim $app)
    {
        $this->setRoutes($app);

        $app->run();
    }

    /**
     * @param Slim $app
     * @return mixed
     */
    abstract protected function setRoutes(Slim $app);
}
