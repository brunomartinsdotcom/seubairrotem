<?php

namespace CoffeeCore\Core;

/**
 * Class AbstractController
 * @package CoffeeCore\Core
 */
abstract class AbstractController
{
    /**
     * @var string
     */
    protected $databaseConnection = '';

}
