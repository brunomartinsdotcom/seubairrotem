<?php

namespace CoffeeCore\Core;

/**
 * Class AbstractController
 * @package Core
 */
/**
 * Class AbstractController
 * @package CoffeeCore\Core
 */
abstract class AbstractPageController
{
    /**
     * @var string
     */
    protected $databaseConnection = '';

    /**
     * @param Renderable $view
     * @param array $dados
     * @param string $outputType
     * @return mixed
     */
    public function render(Renderable $view, $dados = [], $outputType = 'html')
    {
        $view->render($outputType, $dados);
        exit();
    }
}
