<?php


namespace CoffeeCore\Core;

use CoffeeCore\Storage\DoctrineStorage;

/**
 * Class AbstractDao
 * @package CoffeeCore\Core
 */
class AbstractDao
{
    /**
     * @var DoctrineStorage
     */
    protected static $storage;

    public $repository;
    /**
     * @param DoctrineStorage $storage
     */
    public function __construct(DoctrineStorage $storage)
    {
        self::$storage = $storage;
        if (method_exists(self::$storage, 'getRepository')) {
            $this->repository = self::$storage->getRepository();
        }
    }

    /**
     * Creates a new QueryBuilder instance that is prepopulated for this entity name.
     *
     * @param string $alias
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias)
    {
        return $this->_em->createQueryBuilder()
            ->select($alias)
            ->from($this->_entityName, $alias);
    }

    /**
     * @param array $criteria
     * @param array $orderBy
     * @param null $limit
     * @param null $offset
     * @return \arrayIterator
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return self::$storage->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * @param array $criteria
     * @param array $orderBy
     * @return \stdClass
     */
    public function findOneBy(array $criteria, array $orderBy = null)
    {
        return self::$storage->findOneBy($criteria, $orderBy);
    }

    /**
     * @param Entity $entity
     * @return self::$storage
     */
    public function insert(Entity $entity)
    {
        self::$storage->insert($entity);
    }

    /**
     * @param Entity $entity
     * @return void
     */
    public function update(Entity $entity)
    {
        self::$storage->update($entity);
    }

    /**
     * @param Entity $entity
     * @return void
     */
    public function delete(Entity $entity)
    {
        self::$storage->delete($entity);
    }

    /**
     * @return void
     */
    public function flush()
    {
        self::$storage->flush();
    }

    /**
     * @param array $columns
     * @param array $orderBy
     * @param string $search
     * @param int $limit
     * @param int $offset
     * @param array $noSearch
     * @return array
     */
    public function getDataTableData(
        array $columns = [],
        array $orderBy = [
            0,
            'ASC'
        ],
        $search = '',
        $limit = 10,
        $offset = 0,
        array $noSearch = []
    ) {
        $dados = [];

        $numRows = count($this->repository->createQueryBuilder('a')->getQuery()->getArrayResult());

        $countQuery = $this->repository->createQueryBuilder('a');

        foreach ($columns as $k => $column) {
            if (!empty($column)) {
                $where = 'a.'.$column.' LIKE :'.$column;
                if ($k == 0) {
                    $countQuery->where($where);
                } else {
                    $countQuery->orWhere($where);
                }
                $countQuery->setParameter($column, '%'.$search.'%');
            }
        }

        $numFiltereds = count($countQuery->getQuery()->getArrayResult());

        $query = $this->repository->createQueryBuilder('a');

        foreach ($columns as $k => $column) {
            if (!empty($column)) {
                if(in_array($column,$noSearch))
                    continue;
                $where = 'a.'.$column.' LIKE :'.$column;
                if ($k == 0) {
                    $query->where($where);
                } else {
                    $query->orWhere($where);
                }
                $query->setParameter($column, '%'.$search.'%');
            }
        }

        $q = $query->orderBy('a.'.$columns[$orderBy[0]], $orderBy[1])
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery();

        $dbData = new \ArrayIterator($q->getArrayResult());

        while ($dbData->valid()) {
            $line = [];
            foreach ($columns as $k => $column) {
                if (!empty($column)) {
                    $line[] = $dbData->current()[$column];
                } else {
                    $line[] = "";
                }
                unset($k);
            }
            $dados[] = $line;
            $dbData->next();
        }

        return [
            //"draw" => $_POST['draw'],
            "recordsTotal" => $numRows,
            "recordsFiltered" => $numFiltereds,
            "data" => $dados
        ];
    }
}
