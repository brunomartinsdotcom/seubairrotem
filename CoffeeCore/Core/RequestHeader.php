<?php

namespace CoffeeCore\Core;

    /**
     * Class RequestHeader
     * @package CoffeCore\Api
     */
/**
 * Class RequestHeader
 * @package CoffeeCore\Core
 */
class RequestHeader
{
    /**
     * @var AbstractApplication
     */
    private $app;


    /**
     * @param AbstractApplication $app
     */
    protected function __construct(AbstractApplication $app)
    {
        $this->app = $app;
    }

    /**
     * @return String|null
     */
    public function getAuthorization()
    {
        return $this->app->request()->headers("X-Authorization");
    }
}
