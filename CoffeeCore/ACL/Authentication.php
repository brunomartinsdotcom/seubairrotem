<?php

namespace CoffeeCore\ACL;

/**
 * Class Authentication
 * @package CoffeeCore\ACL
 */
class Authentication
{
    /**
     * @var \CoffeeCore\ACL\Authentication
     */
    protected static $instance;

    /**
     * @return \CoffeeCore\ACL\Authentication
     */
    protected function __construct()
    {
        if (empty($_SESSION))
            $_SESSION[session_id()] = [];
    }

    /**
     * @return array
     */
    protected static function startSession()
    {
        if (false === self::$instance instanceof self) {
            self::$instance = new self();
        }
    }

    /**
     * @param $key
     * @param $val
     */
    public static function putInSession($key, $val)
    {
        $_SESSION[session_id()][$key] = serialize($val);
    }

    /**
     * @param $key
     * @return array|mixed
     */
    public static function getFromSession($key)
    {
        if (isset($_SESSION[session_id()][$key]))
            return unserialize($_SESSION[session_id()][$key]);
        else return [];
    }

    public static function endSession()
    {
        session_destroy();
    }
}
