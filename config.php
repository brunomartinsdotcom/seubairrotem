<?php

require_once "bootstrap.php";
require_once "slug.php";
require_once "imagecreatefrombmp.php";

function notEmpty($var) {
    if (isset($var) and !empty($var)) {
        return true;
    }
    return false;
}

$app = new \Slim\Slim(
    [
        'mode' => 'development',
        'debug' => true,
        'templates.path' => ROOT . "src" . DIRECTORY_SEPARATOR,
    ]
);


/** @var \Nette\Mail\SmtpMailer mailer */
$app->mailer = new \Nette\Mail\SmtpMailer(
    [
        'host' => 'rlin14.hpwoc.com',//'smtp.wmbr.co',
        'username' => 'contato@seubairrotem.com.br',//'smtp@wmbr.co',
        'password' => 's3uB41rr0',//'EHrQoCe3ZNroIMMhy87S7w',
        'secure' => 'ssl'
    ]
);

/** @var Doctrine\ORM\EntityManager orm */
$app->orm = $entityManager;
