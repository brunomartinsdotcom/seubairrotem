<?php

namespace Usuarios;

use CoffeeCore\Core\AbstractController;
use CoffeeCore\Helper\Canvas;
use CoffeeCore\Storage\DoctrineStorage;

/**
 * Class Controller
 * @package Subcategoria
 */
class Controller extends AbstractController
{
    /**
     * @var string
     */
    public $pagina = 'usuarios';
    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Entity
     */
    protected $entity;

    /**
     * @return void
     */
    public function getDataTable()
    {
        $this->startConnection();
        $columns = ['id', 'nome', 'usuario', 'email'];

        $search = isset($_POST['search']['value']) ? $_POST['search']['value'] : ""; //$_POST['search']['value'];

        $limit = isset($_POST['length']) ? $_POST['length'] : 10;
        $offset = isset($_POST['start']) ? $_POST['start'] : 0;
        if(isset($_POST['order'])){
            $orderBy = [
                $_POST['order'][0]['column'],
                $_POST['order'][0]['dir']
            ];
        }else{
            $orderBy = [0, 'ASC'];
        }

        $dataTables = $this->model->getDataTableData($columns, $orderBy, $search, $limit, $offset);

        foreach ($dataTables['data'] as $k => &$row) {
            $row[] = "<span class='center'>
					     <button class='btn btn-primary' onclick='javascript:window.location = \"/painel/{$this->pagina}/alterar/{$row[0]}\"'>
                             <i class='icon-pencil'></i>
					     </button>
				     </span>";
            $row[] .= "<span class='center'>
			             <button data-url='/painel/{$this->pagina}/deletar/{$row[0]}' id='deletar' class='btn btn-danger'>
                             <i class='icon-trash'></i>
						 </button>
				     </span>";
            unset($k);
        }

        //header('Content-Type: application/json');
        echo json_encode($dataTables);
    }

    /**
     *
     */
    public function startConnection()
    {
        $this->entity = new \Usuarios\Entity();
        $this->model = new Model(new DoctrineStorage($this->entity));
    }

    /**
     *
     */
    public function gravar()
    {
        if (empty($this->entity) or empty($this->model)) {
            $this->startConnection();
        }

        if (!empty($_POST['id'])) {
            $this->entity = $this->model->findOneBy(['id' => $_POST['id']]);
        }

        $NamePasta = $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "painel/_uploads/usuarios/";

        if($_FILES['foto']['tmp_name'] != ""){
            $tiW = $_POST['tiW'];
            $tiH = $_POST['tiH'];
            $x1 = round($_POST["x1"]);
            $y1 = round($_POST["y1"]);
            $val_resize = $_POST["val_resize"];
            $w = round($_POST["w"]);
            $h = round($_POST["h"]);
            $tamOriginal = $_POST["filedim"];
            $largura = explode('x', $tamOriginal);

            if (!empty($_POST['id'])) {
                if(!empty($_POST['fotoantiga'])) {
                    if (is_file($NamePasta . $_POST['fotoantiga'])){
                        unlink($NamePasta . $_POST['fotoantiga']);
                    }
                }
            }

            //PARAMETROS
            $tempFile = $_FILES['foto']['tmp_name'];
            $nomeFile = $_FILES['foto']['name'];
            $fileName = date("YmdGis") . "_" . nome_arquivo($nomeFile);
            $targetFile = $NamePasta . $fileName;

            move_uploaded_file($tempFile, $targetFile);

            $img = new Canvas();
            $img->load($targetFile);
            $img->save($targetFile);
            //$img = new canvas($NamePasta . $fileName);
            if ($val_resize != "") {
                if ($largura[0] > $val_resize) {
                    $img->resize($val_resize, '', "")->save($targetFile);
                }
            }

            $img->set_crop_coordinates(-$x1, -$y1)->resize($w, $h, "crop")->save($targetFile);
            $img->resize($tiW, $tiH, "")->save($targetFile);

            $this->entity->setFoto($fileName);
        }else{
            if(!empty($_POST['id'])) {
                if(!empty($_POST['excluir_foto'])){
                    if(!empty($_POST['fotoantiga'])) {
                        if (is_file($NamePasta . $_POST['fotoantiga'])){
                            unlink($NamePasta . $_POST['fotoantiga']);
                        }
                    }
                    $this->entity->setFoto('');
                }
            }
        }

        $this->entity->setNome($_POST['nome']);
        $this->entity->setUsuario($_POST['usuario']);

        if (!empty($_POST['senha'])) {
            $this->entity->setSenha($_POST['senha']);
        }

        if (!empty($_POST['email'])) {
            $this->entity->setEmail($_POST['email']);
        }

        if (!empty($_POST['tipo'])) {
            $this->entity->setTipo($_POST['tipo']);
        }

        if (!empty($_POST['paginas'])) {
            $this->entity->setPaginas(implode('|', $_POST['paginas']));
        }

        if (!isset($_POST['id']) or empty($_POST['id'])) {
            DoctrineStorage::orm()->persist($this->entity);
        } else {
            DoctrineStorage::orm()->merge($this->entity);
        }
        $this->model->flush();

        /*echo "
        <script>
            window.location = '/painel/{$this->pagina}';
        </script>
        ";*/
    }

    /**
     * @param int|null $id
     */
    public function deletar($id = null)
    {
        $this->startConnection();
        if (!empty($id)) {
            $this->entity = $this->model->findOneBy(["id"=>$id]);
            $foto = $this->entity->getFoto();

            $output_dir = $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "painel/_uploads/usuarios/";

            if (is_file($output_dir.$foto)) {
                unlink($output_dir.$foto);
            }

            $this->model->delete($this->entity);
            $this->model->flush();
        }

        /*echo "
        <script>
            window.location = '/painel/{$this->pagina}';
        </script>
        ";*/
    }
}
