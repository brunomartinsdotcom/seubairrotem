<?php
include 'inc_head.php';
$tiW = 200;
$tiH = 200;
$action = "Adicionar";
/** @var \Usuarios\Entity $usuario */
if (isset($usuario)) {
    $action = "Editar";
    $dados = [
        'id'        => $usuario->getId(),
        'nome'      => $usuario->getNome(),
        'usuario'   => $usuario->getUsuario(),
        'email'     => $usuario->getEmail(),
        'foto'      => $usuario->getFoto(),
        'tipo'      => $usuario->getTipo(),
        'paginas'   => explode('|', $usuario->getPaginas())
    ];
} else {
    $dados = [
        'id'        => '',
        'nome'      => '',
        'usuario'   => '',
        'email'     => '',
        'foto'      => '',
        'tipo'      => '',
        'paginas'   => []
    ];
}
//$preview = (empty($dados['foto'])) ? "http://www_antigo.placehold.it/200x200":'/_uploads/usuarios/'.$dados['foto'];
$preview = (empty($dados['foto'])) ? "http://www.placehold.it/".$tiW."x".$tiH : $prefix."_uploads/usuarios/".$dados['foto'];
$tipos = [
    [
        'id'=> 1,
        'description' => "Administrador"
    ],
    [
        'id'=> 2,
        'description' => "Usuário"
    ]
];
$jsonReader = new \CoffeeCore\Helper\JsonReader('paginas');
$paginas = $jsonReader->getFileContent(true);
?>
</head>
<body>
    <div class="layout">
    <?php
        include_once 'inc_header.php';
        include_once 'inc_usuario.php';
        include_once 'inc_sidebar.php';
    ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">Usuários do Painel</h3>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="<?=$prefix;?>" class="icon-home" title="Home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li>
                            <a href="<?=$prefix;?>usuarios" title="Usuários do Painel">Usuários do Painel</a><span class="divider">
                                <i class="icon-angle-right"></i>
                            </span>
                        </li>
                        <li class="active"><?=$action;?></li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="content-widgets light-gray">
                    <div class="widget-head black">
                        <h3><?=$action;?> Usuário do Painel</h3>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function(){

                            $(function () {
                                $(".chzn-select").chosen();
                                $(".chzn-select-deselect").chosen({
                                    allow_single_deselect: true
                                });
                            });

                            // valida o formulário
                            $('#usuarios').validate({
                                // define regras para os campos
                                rules: {
                                    nome: {
                                        required: true,
                                        minlength: 4
                                    },
                                    usuario: {
                                        required: true,
                                        minlength: 4
                                    },
									email: {
                                        required:true,
                                        email: true
                                    },
                                    tipo: {
                                        required: true
                                    }
                                    <?php
                                    if (empty($dados['id'])) {
                                        echo ",
                                        senha: {
                                            required: true,
                                            minlength: 6
                                        }";
                                    }
                                    ?>
                                },
                                // define messages para cada campo
                                messages: {
                                    nome:"Informe o nome",
                                    usuario:"Informe o login",
                                    email: {
                                        required: "Informe o e-mail",
                                        email: "Informe um e-mail válido"
                                    }
                                    tipo:"Selecione um tipo",
                                    senha:"Informe a senha"
                                }
                            });
                        });
                    </script>
                    <div class="widget-container">
                        <form name="usuarios" id="usuarios" enctype="multipart/form-data" class="form-horizontal" action="<?=$_SERVER["REQUEST_URI"];?>" method="post">
                        	<!-- CAMPOS OCULTOS PARA CORTE DA IMAGEM -->
                            <input type="hidden" id="tiW" name="tiW" value="<?=$tiW;?>" />
                            <input type="hidden" id="tiH" name="tiH" value="<?=$tiH;?>" />
                            <input type="hidden" id="x1" name="x1" />
                            <input type="hidden" id="y1" name="y1" />
                            <input type="hidden" id="x2" name="x2" />
                            <input type="hidden" id="y2" name="y2" />
                            <input type="hidden" id="val_resize" name="val_resize" />
                            <input type="hidden" id="filesize" name="filesize" />
                            <input type="hidden" id="filetype" name="filetype" />
                            <input type="hidden" id="filedim" name="filedim" />
                            <input type="hidden" id="w" name="w" />
                            <input type="hidden" id="h" name="h" />
                            <input type="hidden" id="ratio" name="ratio" value="<?=($tiW)/760;?>" />
                            <!-- TERMINA OS CAMPOS OCULTOS -->
                            <input name="id" id="id" type="hidden" value="<?=$dados['id'];?>">
                            <!-- .control-group -->
                            <div class="control-group">
                                <label for="nome" class="control-label">Nome</label>
                                <!-- .controls -->
                                <div class="controls">
                                    <input name="nome" id="nome" type="text" maxlength="60" class="span7" value="<?=$dados['nome'];?>">
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                              <!-- .control-group -->
                            <div class="control-group">
                                <label for="email" class="control-label">E-mail</label>
                                <!-- .controls -->
                                <div class="controls">
                                    <input name="email" id="email" type="text" maxlength="120" class="span7" value="<?=$dados['email'];?>">
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                            <!-- .control-group -->
                            <div class="control-group">
                                <label for="foto" class="control-label">Foto<br><em style="font-size: 11px; color:red;">(Tamanho: <?=$tiW;?>x<?=$tiH;?> pixels)</em></label>
                                <!-- .controls -->
                                <div class="controls">
                                    <!-- .fileupload -->
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="error" style="color:#F00;"></div>
                                        <!-- .fileupload-new -->
                                        <div class="fileupload-new thumbnail">
                                            <img id="preview" src="<?=$preview;?>" alt="img"/>
                                            <div class="info"></div>
                                        </div><!-- /.fileupload-new -->
                                        <!-- .fileupload-preview .ileupload-exists .thumbnail -->
                                        <div class="" style="max-width:<?=$tiW;?>px; max-height:<?=$tiH;?>px; line-height:20px;"></div>
                                        <div>
                                            <!-- .btn .btn-file -->
                                            <span class="btn btn-file">
                                                <span class="fileupload-new">Selecione a Foto</span>
                                                <span class="fileupload-exists">Trocar</span>
                                                <input name="fotoantiga" id="fotoantiga" value="<?=$dados['foto'];?>" type="hidden"/>
                                                <input name="foto" id="foto" type="file" onChange="fileSelectHandler('foto', <?=$tiW;?>, <?=$tiH;?>, '<?=($tiW)/($tiH);?>')" />
                                            </span><!-- /.btn /.btn-file -->
                                        </div>
                                    </div><!-- /.fileupload -->
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                            <?php if($dados['foto'] != ""){ ?>
                            <!-- .control-group -->
                            <div class="control-group">
                                <label class="control-label"></label>
                                <!-- .controls -->
                                <div class="controls">
                                    <label for="excluir_foto"><input name="excluir_foto" id="excluir_foto" type="checkbox" value="sim" /> Excluir Foto</label>
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                            <?php } ?>
                            <!-- .control-group -->
                            <div class="control-group">
                                <label for="usuario" class="control-label">Login</label>
                                <!-- .controls -->
                                <div class="controls">
                                    <input name="usuario" id="usuario" type="text" maxlength="60" class="span7" value="<?=$dados['usuario'];?>">
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                            <!-- .control-group -->
                            <div class="control-group">
                                <label for="nome" class="control-label"><?php if(!empty($dados['id'])){?>Nova <?php } ?>Senha</label>
                                <!-- .controls -->
                                <div class="controls">
                                    <input name="senha" id="senha" type="password" maxlength="12" class="span7" />
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                            <!-- .control-group -->
                            <div class="control-group">
                                <label for="target" class="control-label">Tipo</label>
                                <!-- .controls -->
                                <div class="controls">
                                    <select name="tipo" id="tipo">
                                        <option value="">Selecione</option>
                                        <?php
                                        foreach ($tipos as $tipo) {
                                        ?>
                                            <option value="<?=$tipo['id'];?>"
                                                <?php if ($tipo['id'] == $dados['tipo'] and !empty($dados['tipo'])) {
                                                    echo " selected ";}
                                                ?>
                                                >
                                                <?=$tipo['description'];?>
                                            </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                            <!-- .control-group -->
                            <div class="control-group"  id="selectPaginas" style="display:<?php if(empty($dados['id']) or (!empty($dados['id']) and $dados['tipo'] == 1)){ ?>none<?php }else{ ?>block<?php } ?>;">
                                <label class="control-label">Páginas de Acesso</label>
                                <!-- .controls -->
                                <div class="controls">
                                <select name="paginas[]" id="paginas" data-placeholder="Selecione as Páginas" class="chzn-select span7" multiple tabindex="-1">
                                    <option value=""></option>
                                    <?php
                                    foreach ($paginas as $pagina) {
                                        ?>
                                        <option value="<?=$pagina['slug'];?>"
                                            <?php if (in_array($pagina['slug'] , $dados['paginas'])) {
                                                echo " selected ";}?>>
                                            <?=$pagina['pagina'];?>
                                        </option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                            <div class="form-actions">
                                <button type="submit" id="Bt" class="btn btn-primary">Salvar</button>
                                <button type="button" onClick="window.open('<?=$prefix;?>usuarios', '_self');" class="btn right-float">Voltar</button>
                            </div>
                        </form>
                    </div>
                </div>
				<script>
                    $(document).ready(function () {
                        $("#tipo").change(function () {
                            if ($(this).val() == 1) {
                                $("div#selectPaginas").hide();
                            } else if ($(this).val() == 2) {
                                $("div#selectPaginas").show('fade');
                            }
                        })
                    })
                </script>
<?php
include 'inc_footer.php';
