<?php

namespace FormasDePagamento;

use CoffeeCore\Core\AbstractController;
use CoffeeCore\Helper\Canvas;
use CoffeeCore\Storage\DoctrineStorage;

/**
 * Class Controller
 * @package FormasDePagamento
 */
class Controller extends AbstractController
{
    /**
     * @var string
     */
    public $pagina = 'formas-de-pagamento';
    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Entity
     */
    protected $entity;

    /**
     * @return void
     */
    public function getDataTable()
    {
        $this->startConnection();
        $columns = ['id', 'titulo', 'img'];
        $noSearch = ['img'];

        $search = isset($_POST['search']['value']) ? $_POST['search']['value'] : ""; //$_POST['search']['value'];

        $limit = isset($_POST['length']) ? $_POST['length'] : 10;
        $offset = isset($_POST['start']) ? $_POST['start'] : 0;
        if(isset($_POST['order'])){
            $orderBy = [
                $_POST['order'][0]['column'],
                $_POST['order'][0]['dir']
            ];
        }else{
            $orderBy = [0, 'ASC'];
        }

        $dataTables = $this->model->getDataTableData($columns, $orderBy, $search, $limit, $offset);

        foreach ($dataTables['data'] as $k => &$row) {
            $row[2] = "<img src=\"/_uploads/formas-de-pagamento/".$row[2]."\" alt=\"".$row[1]."\" />";
            $row[] = "<span class='center'>
                        <button class='btn btn-primary' onclick='javascript:window.location = \"/painel/{$this->pagina}/alterar/{$row[0]}\"'>
                            <span><i class='icon-pencil'></i></span>
                        </button>
                      </span>";
            $row[] = "<span class='center'>
                        <button data-url='/painel/{$this->pagina}/deletar/{$row[0]}' id='deletar' class='btn btn-danger'>
                            <span><i class='icon-trash'></i></span>
                        </button>
                     </span>";
            unset($k);
        }
        //header('Content-Type: application/json');
        echo json_encode($dataTables);
    }

    /**
     *
     */
    public function startConnection()
    {
        $this->entity = new Entity();
        $this->model = new Model(new DoctrineStorage($this->entity));
    }

    /**
     *
     */
    public function gravar()
    {
        $this->startConnection();

        $this->entity->setTitulo($_POST['titulo']);
        $NamePasta = $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "_uploads/formas-de-pagamento/";
        if($_FILES['img']['tmp_name'] != "") {
            $tiW = $_POST['tiW'];
            $tiH = $_POST['tiH'];
            $x1 = round($_POST["x1"]);
            $y1 = round($_POST["y1"]);
            $val_resize = $_POST["val_resize"];
            $w = round($_POST["w"]);
            $h = round($_POST["h"]);
            $tamOriginal = $_POST["filedim"];
            $largura = explode('x', $tamOriginal);

            if (!empty($_POST['id'])) {
                if (!empty($_POST['imgantiga'])) {
                    if (is_file($NamePasta . $_POST['imgantiga'])) {
                        unlink($NamePasta . $_POST['imgantiga']);
                    }
                }
            }

            //PARAMETROS
            $tempFile = $_FILES['img']['tmp_name'];
            $nomeFile = $_FILES['img']['name'];
            $fileName = date("YmdGis") . "_" . nome_arquivo($nomeFile);
            $targetFile = $NamePasta . $fileName;

            move_uploaded_file($tempFile, $targetFile);

            $image = new Canvas();
            $image->load($targetFile);
            $image->save($targetFile);
            //$img = new canvas($NamePasta . $fileName);
            if ($val_resize != "") {
                if ($largura[0] > $val_resize) {
                    $image->resize($val_resize, '', "")->save($targetFile);
                }
            }

            $image->set_crop_coordinates(-$x1, -$y1)->resize($w, $h, "crop")->save($targetFile);
            $image->resize($tiW, $tiH, "")->save($targetFile);

            $this->entity->setImg($fileName);
        }else{
            $this->entity->setImg($_POST['imgantiga']);
        }

        if (!isset($_POST['id']) or empty($_POST['id'])) {
            $this->model->insert($this->entity);
        } else {
            $this->entity->setId($_POST['id']);
            $this->model->update($this->entity);
        }
        $this->model->flush();
    }

    /**
     * @param int|null $id
     */
    public function deletar($id = null)
    {
        $this->startConnection();
        if (!empty($id)) {
            $this->entity = $this->model->findOneBy(["id"=>$id]);
            $img = $this->entity->getImg();

            $output_dir = $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "_uploads/formas-de-pagamento/";

            if (is_file($output_dir.$img)) {
                unlink($output_dir.$img);
            }

            $this->model->delete($this->entity);
            $this->model->flush();
        }
    }
}
