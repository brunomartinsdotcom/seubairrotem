<?php
namespace FormasDePagamento;


/**
 * FormasDePagamento
 *
 * @Table(name="novo_formas_de_pagamento")
 * @Entity
 */
class Entity implements \CoffeeCore\Core\Entity
{
    /**
     *
     */
    const FULL_NAME = "FormasDePagamento\\Entity";

    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="titulo", type="string", length=100, nullable=false)
     */
    private $titulo;

    /**
     * @var string
     *
     * @Column(name="img", type="string", length=255, nullable=false)
     */
    private $img;

    /**
     * @param integer $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    /**
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param string $img
     */
    public function setImg($img)
    {
        $this->img = $img;
    }

    /**
     * @return string
     */
    public function getImg()
    {
        return $this->img;
    }
}
