<?php
include 'inc_head.php';
$tiW = 60;
$tiH = 38;
$action = "Adicionar";
if (isset($formaPagamento)) {
    $action = "Editar";
    $dados = [
        'id'        => $formaPagamento->getId(),
        'titulo'    => $formaPagamento->getTitulo(),
        'img'       => $formaPagamento->getImg()
    ];
} else {
    $dados = [
        'id'        => '',
        'titulo'    => '',
        'img'       => ''
    ];
}
$preview = (empty($dados['img'])) ? "http://www.placehold.it/".$tiW."x".$tiH : "/_uploads/formas-de-pagamento/".$dados['img'];
?>
</head>
<body>
    <div class="layout">
        <?php
        include_once 'inc_header.php';
        include_once 'inc_usuario.php';
        include_once 'inc_sidebar.php';
        ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">Guia Comercial - Formas de Pagamento</h3>
                        <span class="pull-right top-right-toolbar">
                            <a href="<?=$prefix;?>formas-de-pagamento/novo" class="btn btn-mini btn-success" title="Novo"><i class="icon-plus "></i> Adicionar Novo</a>
                        </span>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="<?=$prefix;?>" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li>Guia Comercial<span class="divider"><i class="icon-angle-right"></i></span></li>
                        <li>
                            <a href="<?=$prefix;?>formas-de-pagamento" title="Formas de Pagamento">Formas de Pagamento</a><span class="divider">
                                <i class="icon-angle-right"></i>
                            </span>
                        </li>
                        <li class="active"><?=$action;?></li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="content-widgets light-gray">
                    <div class="widget-head black">
                        <h3><?=$action;?> Forma de Pagamento</h3>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            // valida o formulário
                            $('#formas-de-pagamento').validate({
                                // define regras para os campos
                                rules: {
                                    titulo: {
                                        required: true
                                    }<?php if (!isset($formaPagamento)) { ?>,
                                    img: {
                                        required: true
                                    }<?php } ?>
                                },
                                // define messages para cada campo
                                messages: {
                                    titulo: "Informe o título para a forma de pagamento"<?php if (!isset($formaPagamento)) { ?>,
                                    img: "Informe a imagem para a forma de pagamento"<?php } ?>
                                }
                            });
                        });
                    </script>
                    <div class="widget-container">
                        <form name="formas-de-pagamento" id="formas-de-pagamento" class="form-horizontal" action="<?=$_SERVER["REQUEST_URI"];?>" method="post" enctype="multipart/form-data">
                            <!-- CAMPOS OCULTOS PARA CORTE DA IMAGEM -->
                            <input type="hidden" id="tiW" name="tiW" value="<?=$tiW;?>" />
                            <input type="hidden" id="tiH" name="tiH" value="<?=$tiH;?>" />
                            <input type="hidden" id="x1" name="x1" />
                            <input type="hidden" id="y1" name="y1" />
                            <input type="hidden" id="x2" name="x2" />
                            <input type="hidden" id="y2" name="y2" />
                            <input type="hidden" id="val_resize" name="val_resize" />
                            <input type="hidden" id="filesize" name="filesize" />
                            <input type="hidden" id="filetype" name="filetype" />
                            <input type="hidden" id="filedim" name="filedim" />
                            <input type="hidden" id="w" name="w" />
                            <input type="hidden" id="h" name="h" />
                            <input type="hidden" id="ratio" name="ratio" value="<?=($tiW)/760;?>" />
                            <!-- TERMINA OS CAMPOS OCULTOS -->
                            <input name="id" id="id" type="hidden" value="<?=$dados['id'];?>">
                            <div class="control-group">
                                <label for="nome" class="control-label">Título</label>
                                <div class="controls">
                                    <input name="titulo" id="titulo" type="text" maxlength="50" class="span7" value="<?=$dados['titulo'];?>">
                                </div>
                            </div>
                            <!-- .control-group -->
                            <div class="control-group">
                                <label for="img" class="control-label">Imagem<br><em style="font-size: 11px; color:red;">(Tamanho: <?=$tiW;?>x<?=$tiH;?> pixels)</em></label>
                                <!-- .controls -->
                                <div class="controls">
                                    <!-- .fileupload -->
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="error" style="color:#F00;"></div>
                                        <!-- .fileupload-new -->
                                        <div class="fileupload-new thumbnail">
                                            <img id="preview" src="<?=$preview;?>" alt="img"/>
                                            <div class="info"></div>
                                        </div><!-- /.fileupload-new -->
                                        <!-- .fileupload-preview .ileupload-exists .thumbnail -->
                                        <div class="" style="max-width:<?=$tiW;?>px; max-height:<?=$tiH;?>px; line-height:20px;"></div>
                                        <div>
                                            <!-- .btn .btn-file -->
                                            <span class="btn btn-file">
                                                <span class="fileupload-new">Selecione a Imagem</span>
                                                <span class="fileupload-exists">Trocar</span>
                                                <input name="imgantiga" id="imgantiga" value="<?=$dados['img'];?>" type="hidden"/>
                                                <input name="img" id="img" type="file" onChange="fileSelectHandler('img', <?=$tiW;?>, <?=$tiH;?>, '<?=($tiW)/($tiH);?>')" />
                                            </span><!-- /.btn /.btn-file -->
                                        </div>
                                    </div><!-- /.fileupload -->
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                            <div class="form-actions">
                                <button type="submit" id="Bt" class="btn btn-primary">Salvar</button>
                                <button type="button" onclick="window.open('<?=$prefix;?>formas-de-pagamento', '_self');" class="btn right-float">Voltar</button>
                            </div>
                        </form>
                    </div>
                </div>
<?php
include 'inc_footer.php';
