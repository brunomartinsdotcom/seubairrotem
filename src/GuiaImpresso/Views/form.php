<?php
include 'inc_head.php';
$tiW = 290;
$tiH = 356;
$action = "Adicionar";
if (isset($guiaimpresso)) {
    $action = "Editar";

    $dados = [
        'id'        => $guiaimpresso->getId(),
        'data'      => $guiaimpresso->getDataBR(),
        'titulo'    => $guiaimpresso->getTitulo(),
        'descricao' => $guiaimpresso->getDescricao(),
        'url'       => $guiaimpresso->getUrl(),
        'capa'      => $guiaimpresso->getCapa(),
    ];
} else {
    $dados = [
        'id'        => '',
        'data'      => date('d/m/Y'),
        'titulo'    => '',
        'descricao' => '',
        'url'       => '',
        'capa'      => ''
    ];
}
$preview = (empty($dados['capa'])) ? "http://www.placehold.it/".$tiW."x".$tiH : "/_uploads/guia-impresso/".$dados['capa'];
?>
<script type="text/javascript" src="<?=$prefix;?>js/mask.js"></script>
<script type="text/javascript" src="<?=$prefix;?>js/bootstrap-datetimepicker.min.js"></script>
</head>
<body>
    <div class="layout">
    <?php
        include_once 'inc_header.php';
        include_once 'inc_usuario.php';
        include_once 'inc_sidebar.php';
    ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">Guia Impresso</h3>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="<?=$prefix;?>" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li>
                            <a href="<?=$prefix;?>guia-impresso" title="Guia Impresso">Guia Impresso</a><span class="divider">
                                <i class="icon-angle-right"></i>
                            </span>
                        </li>
                        <li class="active"><?=$action;?></li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="content-widgets light-gray">
                    <div class="widget-head black">
                        <h3><?=$action;?> Guia Impresso</h3>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function(){
                            //CÓDIGO PARA O CALENDÁRIO
                            $('#divData').datetimepicker({
                                pickTime: false
                            });
                            //MÁSCARA PARA O CAMPO DATA
                            $("#data").mask("99/99/9999");
                            // valida o formulário
                            $('#guiaimpresso').validate({
                                // define regras para os campos
                                rules: {
                                    data: {
                                        required: true
                                    },
                                    titulo: {
                                       required: true
                                    },
                                    url: {
                                        required: true,
                                        minlength: 40
                                    },
                                    capa: {
                                        required: true
                                    }
                                },
                                // define messages para cada campo
                                messages: {
                                    data:"Informe a data do guia impresso",
                                    titulo:"Informe o título do guia impresso",
                                    url: {
                                        required: "Informe a URL do ISSUU",
                                        minlength: "Exemplo: http://issuu.com/yogaiowa/docs/yia-fall-14?e=0/10823449"
                                    },
                                    capa: "Informe a capa do guia impresso"
                                }
                            });
                        });
                    </script>
                    <?php
                        if(isset($departamento)) {
                            $data = date_create($dados['data']); }else{
                        }
                    ?>
                    <div class="widget-container">
                        <form name="guiaimpresso" id="guiaimpresso" enctype="multipart/form-data" class="form-horizontal" action="<?=$_SERVER["REQUEST_URI"];?>" method="post">
                            <!-- CAMPOS OCULTOS PARA CORTE DA IMAGEM -->
                            <input type="hidden" id="tiW" name="tiW" value="<?=$tiW;?>" />
                            <input type="hidden" id="tiH" name="tiH" value="<?=$tiH;?>" />
                            <input type="hidden" id="x1" name="x1" />
                            <input type="hidden" id="y1" name="y1" />
                            <input type="hidden" id="x2" name="x2" />
                            <input type="hidden" id="y2" name="y2" />
                            <input type="hidden" id="val_resize" name="val_resize" />
                            <input type="hidden" id="filesize" name="filesize" />
                            <input type="hidden" id="filetype" name="filetype" />
                            <input type="hidden" id="filedim" name="filedim" />
                            <input type="hidden" id="w" name="w" />
                            <input type="hidden" id="h" name="h" />
                            <input type="hidden" id="ratio" name="ratio" value="<?=($tiW)/760;?>" />
                            <!-- TERMINA OS CAMPOS OCULTOS -->
                            <input name="id" id="id" type="hidden" value="<?=$dados['id'];?>">
                            <!-- .control-group -->
                            <div class="control-group">
                                <label for="data" class="control-label">Data</label>
                                <!-- .controls -->
                                <div class="controls">
                                    <!-- #divData -->
                                    <div id="divData" class="input-append">
                                        <input name="data" id="data" type="text" size="12" maxlength="10" data-format="dd/MM/yyyy" value="<?= $dados['data']; ?>" />
                                        <span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-calendar"></i></span>
                                    </div><!-- /#divData -->
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                            <!-- .control-group -->
                              <!-- .control-group -->
                            <div class="control-group">
                                <label for="titulo" class="control-label">Título</label>
                                <!-- .controls -->
                                <div class="controls">
                                   <input name="titulo" id="titulo" type="text" maxlength="100" class="span7" value="<?= $dados['titulo'] ?>">
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                            <!-- .control-group -->
                            <div class="control-group">
                                <label for="descricao" class="control-label">Descrição</label>
                                <!-- .controls -->
                                <div class="controls">
                                    <input name="descricao" id="descricao" type="text" class="span10" maxlength="200" value="<?=$dados['descricao'];?>">
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                            <!-- .control-group -->
                            <div class="control-group">
                                <label for="url" class="control-label">URL</label>
                                <!-- .controls -->
                                <div class="controls">
                                    <input name="url" id="url" maxlength="255" type="text" value="<?=$dados['url'];?>" placeholder="Exemplo: http://issuu.com/yogaiowa/docs/yia-fall-14?e=0/10823449" class="span10"/>
                                    <!-- Dica ao programador: para video use exemplo.swf, para imagem use exemplo.png -->
                                    <br/><a href="<?=$prefix;?>images/exemplo.swf" target="_blank">Onde encontrar</a>
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                            <!-- .control-group -->
                            <div class="control-group">
                                <label for="capa" class="control-label">Capa<br><em style="font-size: 11px; color:red;">(Tamanho: <?=$tiW;?>x<?=$tiH;?> pixels)</em></label>
                                <!-- .controls -->
                                <div class="controls">
                                    <!-- .fileupload -->
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="error" style="color:#F00;"></div>
                                        <!-- .fileupload-new -->
                                        <div class="fileupload-new thumbnail">
                                            <img id="preview" src="<?=$preview;?>" alt="img"/>
                                            <div class="info"></div>
                                        </div><!-- /.fileupload-new -->
                                        <!-- .fileupload-preview .ileupload-exists .thumbnail -->
                                        <div class="" style="max-width:<?=$tiW;?>px; max-height:<?=$tiH;?>px; line-height:20px;"></div>
                                        <div>
                                            <!-- .btn .btn-file -->
                                            <span class="btn btn-file">
                                                <span class="fileupload-new">Selecione a Capa</span>
                                                <span class="fileupload-exists">Trocar</span>
                                                <input name="capaantiga" id="capaantiga" value="<?=$dados['capa'];?>" type="hidden"/>
                                                <input name="capa" id="capa" type="file" onChange="fileSelectHandler('capa', <?=$tiW;?>, <?=$tiH;?>, '<?=($tiW)/($tiH);?>')" />
                                            </span><!-- /.btn /.btn-file -->
                                        </div>
                                    </div><!-- /.fileupload -->
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                            <div class="form-actions">
                                <button type="submit" id="Bt" class="btn btn-primary">Salvar</button>
                                <button type="button" onClick="window.open('<?=$prefix;?>guia-impresso', '_self');" class="btn right-float">Voltar</button>
                            </div>
                        </form>
                    </div>
                </div>
<?php
include 'inc_footer.php';
