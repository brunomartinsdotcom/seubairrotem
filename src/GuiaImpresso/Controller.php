<?php

namespace GuiaImpresso;

use CoffeeCore\Core\AbstractController;
use CoffeeCore\Helper\Canvas;
use CoffeeCore\Storage\DoctrineStorage;

/**
 * Class Controller
 * @package GuiaImpresso
 */
class Controller extends AbstractController
{
    /**
     * @var string
     */
    public $pagina = 'guia-impresso';
    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Entity
     */
    protected $entity;

    /**
     * @return void
     */
    public function getDataTable()
    {
        $this->startConnection();
        $columns = ['id', 'data', 'titulo'];

        $search = isset($_POST['search']['value']) ? $_POST['search']['value'] : ""; //$_POST['search']['value'];

        $limit = isset($_POST['length']) ? $_POST['length'] : 10;
        $offset = isset($_POST['start']) ? $_POST['start'] : 0;
        if(isset($_POST['order'])){
            $orderBy = [
                $_POST['order'][0]['column'],
                $_POST['order'][0]['dir']
            ];
        }else{
            $orderBy = [0, 'ASC'];
        }

        $dataTables = $this->model->getDataTableData($columns, $orderBy, $search, $limit, $offset);

        foreach ($dataTables['data'] as $k => &$row) {
            $row[1] = $this->dataBR($row[1]);
            $row[] = "<span class='center'>
					     <button class='btn btn-primary' onclick='javascript:window.location = \"/painel/{$this->pagina}/alterar/{$row[0]}\"'>
                             <i class='icon-pencil'></i>
					     </button>
				     </span>";
			$row[] .= "<span class='center'>
			             <button data-url='/painel/{$this->pagina}/deletar/{$row[0]}' id='deletar' class='btn btn-danger'>
                             <i class='icon-trash'></i>
						 </button>
				     </span>";
        }

        //header('Content-Type: application/json');
        echo json_encode($dataTables);
    }

    /**
     *
     */
    public function startConnection()
    {
        $this->entity = new Entity(); //\OGuiaImpresso\Entity();
        $this->model = new Model(new DoctrineStorage($this->entity));
    }

    /**
     * @return string
     */
    public function dataBR($data)
    {
        $vetor=explode("-",$data);
        if ($vetor[2]==""){
            return "";
        }
        else{
            return $vetor[2]."/".$vetor[1]."/".$vetor[0];
        }
    }

    /**
     *
     */
    public function gravar()
    {
        if (empty($this->entity) or empty($this->model)) {
            $this->startConnection();
        }

        if (!empty($_POST['id'])) {
            $this->entity = $this->model->findOneBy(['id' => $_POST['id']]);
        }

        $slug = removeAcentos($_POST['titulo'], '-');

        $data = $_POST['data'];
        $vetor=explode("/",$data);
        if ($vetor[2]==""){
            $data = "";
        } else {
            $data = $vetor[2]."-".$vetor[1]."-".$vetor[0];
        }

        $this->entity->setData($data);
        $this->entity->setTitulo($_POST['titulo']);
        $this->entity->setDescricao($_POST['descricao']);
        $this->entity->setUrl($_POST['url']);
        $this->entity->setSlug($slug);

        $NamePasta = $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "_uploads/guia-impresso/";

        if($_FILES['capa']['tmp_name'] != ""){
            $tiW = $_POST['tiW'];
            $tiH = $_POST['tiH'];
            $x1 = round($_POST["x1"]);
            $y1 = round($_POST["y1"]);
            $val_resize = $_POST["val_resize"];
            $w = round($_POST["w"]);
            $h = round($_POST["h"]);
            $tamOriginal = $_POST["filedim"];
            $largura = explode('x', $tamOriginal);

            if (!empty($_POST['id'])) {
                if(!empty($_POST['capaantiga'])) {
                    if (is_file($NamePasta . $_POST['capaantiga'])){
                        unlink($NamePasta . $_POST['capaantiga']);
                    }
                }
            }

            //PARAMETROS
            $tempFile = $_FILES['capa']['tmp_name'];
            $nomeFile = $_FILES['capa']['name'];
            $fileName = date("YmdGis") . "_" . nome_arquivo($nomeFile);
            $targetFile = $NamePasta . $fileName;

            move_uploaded_file($tempFile, $targetFile);

            $img = new Canvas();
            $img->load($targetFile);
            $img->save($targetFile);
            if ($val_resize != "") {
                if ($largura[0] > $val_resize) {
                    $img->resize($val_resize, '', "")->save($targetFile);
                }
            }

            $img->set_crop_coordinates(-$x1, -$y1)->resize($w, $h, "crop")->save($targetFile);
            $img->resize($tiW, $tiH, "")->save($targetFile);

            $this->entity->setCapa($fileName);
        }

        if (!isset($_POST['id']) or empty($_POST['id'])) {
            DoctrineStorage::orm()->persist($this->entity);
        } else {
            DoctrineStorage::orm()->merge($this->entity);
        }
        $this->model->flush();
    }

    /**
     * @param int|null $id
     */
    public function deletar($id = null)
    {
        $this->startConnection();
        if (!empty($id)) {
            $this->entity = $this->model->findOneBy(["id"=>$id]);
            $capa = $this->entity->getCapa();

            $output_dir = $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "_uploads/guia-impresso/";

            if (is_file($output_dir.$capa)) {
                unlink($output_dir.$capa);
            }

            $this->model->delete($this->entity);
            $this->model->flush();
        }
    }
}
