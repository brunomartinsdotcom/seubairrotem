<?php

namespace GuiaImpresso;

/**
 * Entity
 *
 * @Table(name="novo_guia_impresso")
 * @Entity
 */
class Entity implements \CoffeeCore\Core\Entity
{
    const FULL_NAME = "GuiaImpresso\\Entity";

    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="data", type="string", nullable=false)
     */
    private $data;

    /**
     * @var string
     *
     * @Column(name="titulo", type="string", length=100, nullable=false)
     */
    private $titulo;

    /**
     * @var string
     *
     * @Column(name="descricao", type="string", length=200, nullable=false)
     */

    private $descricao;

    /**
     * @var string
     *
     * @Column(name="url", type="string", length=255, nullable=false)
     */

    private $url;

    /**
     * @var string
     *
     * @Column(name="capa", type="string", length=255, nullable=false)
     */

    private $capa;

    /**
     * @var string
     *
     * @Column(name="slug", type="string", length=100, nullable=false)
     */

    private $slug;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function getDataBR()
    {
        $data = $this->data;
        $vetor=explode("-",$data);
        if ($vetor[2]==""){
            return "";
        }
        else{
            return $vetor[2]."/".$vetor[1]."/".$vetor[0];
        }
    }

    /**
     * @param string $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function setDataUSA($data)
    {
        $this->data = $data;
        $vetor=explode("/",$data);
        if ($vetor[2]==""){
            $this->data = "";
        } else {
            $this->data = $vetor[2]."-".$vetor[1]."-".$vetor[0];
        }
    }

    /**
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param string $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    /**
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param string $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getCapa()
    {
        return $this->capa;
    }

    /**
     * @param string $capa
     */
    public function setCapa($capa)
    {
        $this->capa = $capa;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }
}
