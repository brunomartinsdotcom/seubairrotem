<?php
$pagina = "duvidas";
require_once('inc_head.php');
foreach ($paginas as $k => $page) {
if($page['slug'] == $pagina){
?>
<title><?=$page['titulo']; ?> | <?=$dadosDoSite['titulo'];?> - <?=$dadosDoSite['slogan'];?></title>
<meta name="title" content="<?=$page['titulo']; ?> | <?=$dadosDoSite['titulo'];?> - <?=$dadosDoSite['slogan'];?>"/>
<meta name="description" content="<?=$page['descricao']; ?>"/>
<meta name="keywords" content="<?=$page['palavraschave']; ?>"/>
<!-- Tags Facebook -->
<meta property="og:title" content="<?=$page['titulo']; ?> | <?=$dadosDoSite['titulo'];?> - <?=$dadosDoSite['slogan'];?>"/>
<meta property="og:description" content="<?=$page['descricao']; ?>"/>
<meta property="og:type" content="website"/>
<meta property="og:locale" content="pt_BR"/>
<meta property="og:url" content="<?= $prefixLink; ?>"/>
<meta property="og:image" content="<?= $prefixLink; ?>images/avatar.jpg"/>
<meta property="og:site_name" content="<?=$page['titulo']; ?> | <?=$dadosDoSite['titulo'];?> - <?=$dadosDoSite['slogan'];?>"/>
<?php }} ?>
</head>
<body>
<?php require_once('inc_header.php'); ?>
<div class="categorias-titulo-azul hide-for-medium-down"></div>
<section class="row">
    <div class="">
        <div class="off-canvas-wrap" data-offcanvas>
            <div class="inner-wrap">
                <?php require_once('inc_sidebar.php'); ?>
                <!-- CONTEUDO .large-9 .medium-12 .small-12 -->
                <div class="large-9 medium-12 small-12 columns">
                    <h2 class="titulo-azul">Dúvidas</h2>
                    <div class="box-branco">
                        <dl class="accordion" data-accordion>
                            <?php
                                $i = 0;
                                foreach($duvidas as $duvida){
                                    $i++;
                            ?>
                            <dd class="accordion-navigation">
                                <a href="#panel<?=$i;?>"><?=$duvida->getPergunta();?></a>
                                <div id="panel<?=$i;?>" class="content"><?=$duvida->getResposta();?></div>
                            </dd>
                            <?php } ?>
                        </dl>
                    </div><!-- /.box-branco -->
                    <?php require_once('inc_bannerandfacebook.php'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php require_once('inc_footer.php'); ?>
<script type="text/javascript">
    $(".accordion-navigation a").append('<span></span>');
    $(".accordion-navigation a").click(function(event) {
        $(".accordion-navigation a").removeClass();
        $(this).addClass('active');
    });
</script>
</body>
</html>