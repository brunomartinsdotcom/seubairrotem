    <?php
    require_once('inc_head.php');
    /** @var Usuario\Entity $userAutenticated */
    /** @var Anuncio\Entity $getAnuncio */
    $getAnuncio->incrementView();
    \CoffeeCore\Storage\DoctrineStorage::orm()->merge($getAnuncio);
    \CoffeeCore\Storage\DoctrineStorage::orm()->flush();
    ?>
    <title><?=$getAnuncio->getTitulo();?> | Ofertas Agrícolas</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/elastislide.css" />
    <script src="<?=$prefixLink;?>js/jquery.bxslider.js"></script>
    <script id="img-wrapper-tmpl" type="text/x-jquery-tmpl">
    <div class="rg-image-wrapper">
        {{if itemsCount > 1}}
            <div class="rg-image-nav">
                <a href="#" class="rg-image-nav-prev">Imagem Anterior</a>
                <a href="#" class="rg-image-nav-next">Próxima Imagem</a>
            </div>
        {{/if}}
        <div class="rg-image"></div>
        <div class="rg-loading"></div>
        <div class="rg-caption-wrapper">
            <div class="rg-caption" style="display:none;">
                <p></p>
            </div>
        </div>
    </div>
    </script>
    <?php
    $cidade = \CoffeeCore\Storage\DoctrineStorage::orm()
        ->getRepository(Cidade\Entity::FULL_NAME)
        ->findOneBy(["id" => $getAnuncio->getCidade()])->getNome();
    ?>
    <!-- Tags Facebook -->
    <meta name="title" content="<?=$getAnuncio->getTitulo();?> | Ofertas Agrícolas" />
    <meta property="og:title" content="<?=$getAnuncio->getTitulo();?> | Ofertas Agrícolas" />
    <meta property="og:description" content="Preço a vista: R$ <?=number_format($getAnuncio->getPrecoavista(), 2, ',', '.');?> / <?=$getAnuncio->getUnidade();?> | <?= $cidade . ' - ' . $getAnuncio->getEstado();?>" />
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="pt_BR" />
    <meta property="og:url" content="<?="http://".$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI];?>" />
    <meta property="og:image" content="<?=$prefixLink;?>_uploads/anuncios/tumb-<?=$getAnuncio->getFotocapa();?>" />
    <meta property="og:site_name" content="<?=$getAnuncio->getTitulo();?> | Ofertas Agrícolas" />
</head>
<body>
<?php require_once('inc_header.php'); ?>
<!-- .container -->
<div id="box-respostas" class="reveal-modal" data-reveal></div>
<div class="container row">
	<div class="off-canvas-wrap">
		<div class="inner-wrap">                
			<nav class="tab-bar show-for-medium-down">
				<section class="left-small"><a class="left-off-canvas-toggle menu-icon" ><span></span></a></section>
				<section class="middle tab-bar-section"></section>
			</nav>
			<aside class="left-off-canvas-menu">
				<?php include('inc_sidebar.php'); ?>
			</aside>
			<!-- .main-section -->
			<section class="main-section">
				<!-- CONTEUDO .large-9 .medium-12 .small-12 -->
				<div class="">
					<!-- .large-9 .medium-12 .small-12 -->
					<div class="large-9 medium-12 small-12 columns">
                        <div class="breadcrumb"><a href="<?=$prefixLink;?>classificados/<?=create_slug($getAnuncio->getNomecategoria());?>"><?=$getAnuncio->getNomecategoria();?></a> &nbsp; &gt; &nbsp; <a href="<?=$prefixLink;?>classificados/subcategorias/<?=create_slug($getAnuncio->getNomecategoria());?>/<?=create_slug($getAnuncio->getNomesubcategoria());?>"><?=$getAnuncio->getNomesubcategoria();?></a></div>
						<!-- .box-interno -->
						<article class="box-interno">
                            <!-- .titulo-interno -->
							<h2 class="titulo-interno"><?=$getAnuncio->getTitulo();?><small class="right">(Cod. <?=$getAnuncio->getId();?>)</small></h2>
                            <?php if(isset($flash['info-pergunta']) and !empty($flash['info-pergunta'])){ ?>
                            <div data-alert class="alert-box success radius">
                                <?=$flash['info-pergunta'];?>
                                <a href="#" class="close">&times;</a>
                            </div>
							<?php }	?>
                            <?php if(isset($flash['erro-pergunta']) and !empty($flash['erro-pergunta'])){ ?>
                            <div data-alert class="alert-box alert radius">
                                <?=$flash['erro-pergunta'];?>
                                <a href="#" class="close">&times;</a>
                            </div>
							<?php }	?>
							<!-- .dados-anuncio -->
							<div class="dados-anuncio">
                                <div class="left">
                                	<p class="price-a-vista">Preço a vista: R$ <?=number_format($getAnuncio->getPrecoavista(), 2, ',', '.');?> / <?=$getAnuncio->getUnidade();?></p>
                                	<p class="price-a-prazo">Preço a prazo: R$ <?=number_format($getAnuncio->getPrecoaprazo(), 2, ',', '.');?> / <?=$getAnuncio->getUnidade();?></p>
                                    <p class="empty hide-for-small"></p>
                                    <p><strong><?= $cidade . ' - ' . $getAnuncio->getEstado();?> /
                                        <a href="https://www.google.com.br/maps/place/<?=create_slug($cidade) . '+-+'
                                            . $getAnuncio->getEstado();?>" target="_blank" title="Ver Localização">(ver no mapa)</a></strong></p>
                                </div>
                                <div class="right">
                                	<p><em>
                                        Publicado em <?= $getAnuncio->getDatainclusao()->format("d/m/Y"); ?>
                    				</em></p>
									<?php

                                    $conn = \CoffeeCore\Storage\DoctrineStorage::orm()->getConnection();

                                    $sqlPagamentos = "SELECT fp.titulo FROM anunciopagamento ap LEFT JOIN formapagamento fp
                                            ON ap.formapagamento = fp.id where ap.anuncio = {$getAnuncio->getId()} ";
                                    $sqlEntregas = "SELECT fe.titulo FROM anuncioformaentrega ae LEFT JOIN formaentrega fe
                                            ON ae.formaentrega = fe.id where ae.anuncio = {$getAnuncio->getId()} ";

                                    $pagamentos = $conn->fetchAll($sqlPagamentos);
                                    $entregas = $conn->fetchAll($sqlEntregas);
                                    $pagtos = [];
                                    foreach ($pagamentos as $pagamento) {
                                        $pagtos[] = $pagamento['titulo'];
                                    }
                                    $formas = [];
                                    foreach ($entregas as $entrega) {
                                        $formas[] = $entrega['titulo'];
                                    }
                                    ?>
                                    <p>Pagamento: <?= implode(', ', array_values($pagtos)) ?></p>
                                    <p>Entrega: <?= implode(', ', array_values($formas)) ?></p>
                                    <p><em>Este anúncio já foi visualizado: <?=$getAnuncio->getQuantidadevizualizacao();?> vezes</em></p>
                                </div>
							</div><!-- /.dados_anuncio -->
							<!-- GALERIA -->
                            <div id="rg-gallery" class="rg-gallery">
            					<div class="rg-thumbs">
                					<!-- Elastislide Carousel Thumbnail Viewer -->
                					<div class="es-carousel-wrapper">
                    					<div class="es-nav">
                        					<span class="es-nav-prev">Anterior</span>
                        					<span class="es-nav-next">Próximo</span>
                    					</div>
                    					<div class="es-carousel">
                        					<ul>
                                            	<li><a href="#"><img src="<?=$prefixLink;?>_uploads/anuncios/tumb-<?=$getAnuncio->getFotocapa();?>" data-large="<?=$prefixLink;?>_uploads/anuncios/<?=$getAnuncio->getFotocapa();?>" alt="<?=$getAnuncio->getTitulo();?>" /></a></li>
                                                <?php
                                                $anuncioFotos = \CoffeeCore\Storage\DoctrineStorage::orm()
                                                    ->getRepository(AnuncioFoto\Entity::FULL_NAME)
                                                    ->findBy(['anuncio' => $getAnuncio->getId()]);
                                                /** @var \AnuncioFoto\Entity $foto */
                                                foreach ($anuncioFotos as $foto) {
                                                    ?>
                                                    <li>
                                                        <a href="#">
                                                            <img src="<?=$prefixLink;?>_uploads/anuncios/tumb-<?=$foto->getFoto();?>" data-large="<?=$prefixLink;?>_uploads/anuncios/<?=$foto->getFoto();?>" alt="<?=$getAnuncio->getTitulo();?>" />
                                                        </a>
                                                    </li>
                                                <?php
                                                }
                                                ?>

                                            </ul>
                                        </div>
                                    </div>
                                    <!-- End Elastislide Carousel Thumbnail Viewer -->
                                </div><!-- rg-thumbs -->
                            </div><!-- rg-gallery -->
                            <!-- /GALERIA -->
                            <!-- .compartilhar -->
                            <div class="compartilhar">
                            	<!-- AddThis Button BEGIN -->
                                <div class="addthis_toolbox addthis_default_style ">
                                	<span>Compartilhar:</span>
                                    <a class="addthis_button_facebook_like" title="Curtir - Facebook" fb:like:layout="button_count"></a>
                                    <a class="addthis_button_tweet hide-for-small-only" title="Tweet"></a>
                                    <a class="addthis_button_google_plusone hide-for-small-only" g:plusone:size="medium"></a>
                                    <a class="addthis_counter addthis_pill_style hide-for-small-only"></a>
                                </div>
                                <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4e14ab791f0d6aaf"></script>
                            </div><!-- /.compartilhar -->
                            <!-- .botoes-anuncio -->
                            <ul class="botoes-anuncio">
                            	<li><a href="#perguntas" title="Perguntas"><i class="seta"></i><i class="perguntas"></i>Perguntas</a></li>
                                <li><a href="/usuario/<?=\CoffeeCore\Storage\DoctrineStorage::orm()
                                        ->getRepository(Usuario\Entity::FULL_NAME)
                                        ->findOneBy(['id' => $getAnuncio->getUsuario()])
                                        ->getLogin();?>" title="Anúncios do Vendedor"><i class="seta"></i><i class="anuncios"></i>Anúncios do Vendedor</a></li>
                                <li><a href="#fale-com-o-vendedor" title="Fale com o vendedor"><i class="seta"></i><i class="falecom"></i>Fale com o vendedor</a></li>
                            </ul><!-- /.botoes-anuncio -->

                            <!-- .detalhes-anuncio -->
                            <div class="detalhes-anuncio">
                            	<header>
                                	<h3 style="color: #000000" >Descrição</h3>
                                	<ul class="bts-detalhes-anuncio">
                                		<li><a href="/imprimir-anuncio/<?=$getAnuncio->getId()?>" target="_blank" class="imprimir" title="Imprimir"></a></li>
                                    	<li><a href="#" id="aumenta" class="aumenta-texto" title="Aumenta Texto"></a></li>
                                    	<li><a href="#" id="diminui" class="diminui-texto" title="Diminui Texto"></a></li>
                                	</ul>
                                </header>
                                <p id="texto">
                                	<?php $quebradelinha = new \CoffeeCore\Helper\Quebradelinha(); ?>
                                	<?=$quebradelinha->quebradelinha($getAnuncio->getDescricaocompleta());?>
                                </p> 
                            </div><!-- /.detalhes-anuncio -->
                        	<!-- .pergunte-ao-vendedor -->
                            <a name="fale-com-o-vendedor"></a>
                            <div class="pergunte-ao-vendedor">
                            	<header><h2>Fale com o vendedor</h2><span class="arrow-menu"></span></header>
                                <form name="form-pergunta" id="form-pergunta" action="/anuncio-pergunta/<?=$getAnuncio->getId()?>" method="post" enctype="multipart/form-data">
                                    <p>Utilize o formulário abaixo para entrar em contato diretamente com o vendedor do produto ou serviço.</p>
									<?php if (!$intra->isAuthenticated()){ ?>
                                    <label class="cinquentapc">
                                        <input name="nome" id="nome" tabindex="1" maxlength="50" required type="text" placeholder="Nome:" /> *
                                    </label>
                                    <label class="cinquentapc">
                                        <input name="email" id="email" tabindex="2" maxlength="50" required type="text" placeholder="E-mail:" /> *
                                    </label>
                                    <label class="cinquentapc">
                                        <input name="celular" id="celular" tabindex="3" maxlength="15" required type="text" placeholder="Celular: (__) ____-____" /> *
                                    </label>
                                    <label class="cinquentapc">
                                        <input name="telefone" id="telefone" tabindex="4" type="text" maxlength="14" placeholder="Telefone: (__) ____-____" />
                                    </label>
                                    <label class="cinquentapc">
                                        <select required name="estado" id="estado" tabindex="5">
                                            <option value="">Selecione</option>
                                            <?php
                                            $estados = \CoffeeCore\Helper\ResourceManager::readFile("estados.json");
                                            foreach ($estados as $k => $estado) {
                                                echo "<option value='";
                                                echo $estado["name"] . "' > ";
                                                echo $estado["description"] . "</option>";
                                            }
                                            ?>
                                        </select> *
                                    </label>
                                    <label class="cinquentapc">
                                        <select disabled="true" required name="cidade" id="cidade" tabindex="6">
                                            <option value=''>Escolha primeiramente o Estado</option>
                                        </select> *
                                    </label>
                                    <?php }else{ ?>
                                    <input name="usuario" id="usuario" value="<?=$userAutenticated->getId()?>" type="hidden"  />
                                    <?php } ?>
                                    <div class="clearfix"></div>
                                    <label>
                                        <textarea name="pergunta" id="pergunta" required placeholder="Escreva aqui sua pergunta..." tabindex="7"></textarea>
                                    </label>
                                    <label class="right"><button name="bt-envia-pergunta" id="bt-envia-pergunta" type="submit">Enviar</button></label>
                                </form>
                                <br />
                                <div class="clearfix"></div>
                                <a name="perguntas"></a>
                                <h3>Perguntas</h3>
                                <ul class="lista-de-perguntas">
                                    <?php
                                    $perguntas = $conn->fetchAll("SELECT * FROM anuncioperguntas a WHERE  a.anuncio = {$getAnuncio->getId()} order by a.id DESC LIMIT 0, 8");
                                    if (empty($perguntas)) {
                                        echo "<li class=\"text-center\"><br><br>Nenhuma pergunta feita neste anúncio.<br><br><br><br></li>";
                                    } else {
                                        $i = 0;
                                        /** @var $pergunta */
                                        foreach ($perguntas as $pergunta) {
                                            if ($i >= 7) {
                                                break;
                                            }
                                            ?>
                                                <li class="<?= ($i%2==0) ? "cinza" : "";?>">
                                                    <div>
                                                        <time class="col-um">Pergunta:<br/><?= (new DateTime($pergunta['datahorapergunta']))->format("d/m/Y H:i") ; ?></time>
                                                        <span class="col-dois pergunta">
                                                            <?php if($intra->isAuthenticated() and $userAutenticated->getId() == $getAnuncio->getUsuario()){ ?>
                                                            <?php
                                                                if($pergunta['usuario'] != "") {
                                                                    $usuario = \CoffeeCore\Storage\DoctrineStorage::orm()->getRepository(Usuario\Entity::FULL_NAME)->findOneBy(["id" => $pergunta['usuario']]);
                                                                    if ($usuario->getTipopessoa() == 1) {
                                                                        $nomeusuario = $usuario->getNome().' ('.$usuario->getEmail().')';
                                                                    } else {
                                                                        $nomeusuario = $usuario->getNomefantasia().' ('.$usuario->getEmail().')';
                                                                    }
                                                                }else{
                                                                    $nomeusuario = $pergunta['nome'].' ('.$pergunta['email'].')';
                                                                }
                                                            ?>
                                                            <strong><?=$nomeusuario;?></strong>
                                                            <?php } ?>
                                                            <?=$pergunta['pergunta'];?>
                                                        </span>
                                                    </div>
                                                    <?php
                                                    if (!empty($pergunta['resposta'])) {
                                                        ?>
                                                        <div>
                                                            <time class="col-um">Resposta:<br/><?= (new DateTime($pergunta['datahoraresposta']))->format("d/m/Y H:i") ; ?></time>
                                                            <span class="col-dois resposta"><?=$pergunta['resposta'];?></span>
                                                        </div>
                                                    <?php
                                                    } elseif (!empty($userAutenticated) and $getAnuncio->getUsuario() == $userAutenticated->getId()) {
                                                        ?>
                                                        <div class="col-dois resposta">
                                                            <form name="form-resposta" id="form-resposta" action="/anuncio-resposta/<?=$pergunta['id']?>" method="post" enctype="multipart/form-data">
                                                                <label>
                                                                    <textarea name="resposta" id="resposta" required placeholder="Escreva aqui sua resposta"></textarea>
                                                                </label>
                                                                <label class="right"><button name="bt-envia-pergunta" id="bt-envia-pergunta" type="submit">Responder</button></label>
                                                            </form>
                                                            <script>
																$(document).ready(function(){
																	// VALIDA FORM PERGUNTA
																	$('#form-resposta').validate({
																		// define regras para os campos
																		rules: {
																			resposta: {
																				required: true
																			}
																		},
																		// define messages para cada campo
																		messages: {
																			resposta: "Escreva sua resposta"
																		}
																	});
																});
															</script>
                                                        </div>
                                                    <?php
                                                    }
                                                    ?>
                                                </li>
                                        <?php
                                        $i++;
                                        }
                                     }
                                    ?>
                                </ul>
                                <?php
                                if (count($perguntas) > 7 ) {
                                    ?>
                                    <a href="/anuncio-todas-respostas/<?= $getAnuncio->getId(); ?>"
                                       data-reveal-id="box-respostas" data-reveal-ajax="true" class="botao-verde right"
                                       title="Ver todas as perguntas"><i class="seta"></i>Ver todas as perguntas</a>
                                <?php
                                }
                                ?>
                            </div><!-- /.pergunte-ao-vendedor -->
                            <div class="clearfix"></div>
                            <?php
                            $publicidade = \CoffeeCore\Helper\ResourceManager::readFile("publicidade.json");
                            ?>
                            <!-- BANNERS -->
                            <div class="large-6 medium-6 small-12 text-center margin-bottom30 banner columns">
                                <?=$publicidade['interna1']['html'];?>
                                <small></small>
                            </div>
                            <div class="large-6 medium-6 small-12 text-center margin-bottom30 banner columns">
                                <?=$publicidade['interna2']['html'];?>
                                <small></small>
                            </div><!-- /BANNERS -->
                            <!-- .aviso-anuncio -->
                            <div class="aviso-anuncio">
                                <?php
                                	$texto = \CoffeeCore\Storage\DoctrineStorage::orm()->getRepository(\TextoSite\Entity::FULL_NAME)->findOneBy(['local' => 'rodape-do-anuncio']);
									echo str_replace('%titulo%', $getAnuncio->getTitulo(), $texto->getTexto());
                                ?>
                            </div><!-- /.aviso-anuncio -->
                            <!-- .links-help-rodape -->
                            <div class="links-help-rodape">
                            	<a href="/minha-conta/adicionar-anuncio" title="Publicar anúncio Grátis!">Publicar anúncio grátis!</a>
                                <a href="/minha-conta/destacar-anuncio" title="Destacar meu anúncio">Destacar meu anúncio</a>
                                <a href="/duvidas/" title="Precisando de Ajuda?">Precisando de Ajuda?</a>
                            </div><!-- /.links-help-rodape -->
						</article><!-- /.box-interno -->
                        <article class="box-interno">
                        	<?php require_once('inc_ultimos_anuncios.php'); ?>
                        </article><!-- /.box-interno -->
					</div><!-- /.large-9 /.medium-12 /.small-12 -->
				</div><!-- /CONTEUDO /.large-9 /.medium-12 /.small-12 -->
			</section><!-- /.main-section -->
			<a class="exit-off-canvas"></a>
		</div><!--/innerWrapp-->
	</div><!--/offCanvasWrap-->
</div><!-- /.container -->
<?php require_once('inc_footer.php'); ?>
<script type="text/javascript" src="<?=$prefixLink;?>js/jquery.tmpl.min.js"></script>
<script type="text/javascript" src="<?=$prefixLink;?>js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?=$prefixLink;?>js/jquery.elastislide.js"></script>
<script type="text/javascript" src="<?=$prefixLink;?>js/gallery.js"></script>
<script>
$(document).ready(function(){
    // VALIDA FORM PERGUNTA
    $('#form-pergunta').validate({
        // define regras para os campos
        rules: {
            <?php if(!$intra->isAuthenticated()){ ?>
			nome: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            celular: {
                required: true
            },
            estado: {
                required: true
            },
            cidade: {
                required: true
            },<?php } ?>
            pergunta: {
                required: true
            }
        },
        // define messages para cada campo
        messages: {
            <?php if (!$intra->isAuthenticated()){ ?>
			nome		: "Informe o seu nome",
            email		: {
                required:"Informe o seu e-mail",
                email:"E-mail Inválido!"
            },
            celular		: "Informe o seu número de celular",
            estado		: "Informe o Estado onde mora",
            cidade		: "Informe a sua cidade",<?php } ?>
            pergunta	: "Informe sua pergunta"
        }
    });
	<?php if (!$intra->isAuthenticated()){ ?>
    // ATUALIZA CIDADES COM BASE NA TROCA DO ESTADO
    $("#estado").change(function(){
        var uf = $(this).val();
        $.ajax({
            url: "/cidades/" + uf.toLowerCase() ,
            dataType: "json",
            beforeSend : function () {
                if (uf.length > 0) {
                    $("#cidade").html("<option value=''>Selecione</option>");
                } else {
                    $("#cidade").html("<option value=''>Escolha o estado</option>");
                }
                $("#cidade").attr("disabled", true);
            }

        }).done(function(result){
            $.each(result, function (key, value) {
                $("#cidade").append("<option value='"+ value.title +"'>" + value.title + "</option>")
            });
            $("#cidade").attr("disabled", false);
        });
    });
	<?php } ?>

    $('.slider1').bxSlider({
        slideWidth:120,
        minSlides:2,
        maxSlides:5,
        slideMargin:18,
        controls:true,
        infiniteLoop:false
    });

    /* AUMENTA TEXTO */
    $(window).load(function(){
        tamanhoLetra = $('p#texto').css('font-size').substring(0, 2);
        tamanhoLetra++;
        tamanhoLetra++;
        tamanhoMax = 32;
        tamanhoMin = 6;
        $("#aumenta").click(function(){
            if(tamanhoLetra <= tamanhoMax){
                tamanhoLetra++;
                $('p#texto').css({'fontSize': tamanhoLetra});
            }
        });
        $("#diminui").click(function(){
            if(tamanhoLetra >= tamanhoMin){
                tamanhoLetra--;
                $('p#texto').css({'fontSize': tamanhoLetra});
            }
        });
    });
});
</script>
</body>
</html>