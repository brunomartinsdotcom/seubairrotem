<?php
$pagina = "cadastre-se";
require_once('inc_head.php');

foreach ($paginas as $k => $page) {
    if($page['slug'] == $pagina){
?>
<title><?=$page['titulo']; ?> | <?=$dadosDoSite['titulo'];?> - <?=$dadosDoSite['slogan'];?></title>
<meta name="title" content="<?=$page['titulo']; ?> | <?=$dadosDoSite['titulo'];?> - <?=$dadosDoSite['slogan'];?>"/>
<meta name="description" content="<?=$page['descricao']; ?>"/>
<meta name="keywords" content="<?=$page['palavraschave']; ?>"/>
<!-- Tags Facebook -->
<meta property="og:title" content="<?=$page['titulo']; ?> | <?=$dadosDoSite['titulo'];?> - <?=$dadosDoSite['slogan'];?>"/>
<meta property="og:description" content="<?=$page['descricao']; ?>"/>
<meta property="og:type" content="website"/>
<meta property="og:locale" content="pt_BR"/>
<meta property="og:url" content="<?= $prefixLink; ?>"/>
<meta property="og:image" content="<?= $prefixLink; ?>images/avatar.jpg"/>
<meta property="og:site_name" content="<?=$page['titulo']; ?> | <?=$dadosDoSite['titulo'];?> - <?=$dadosDoSite['slogan'];?>"/>
<?php break; }} ?>
</head>
<body>
<?php require_once('inc_header.php'); ?>
<div class="categorias-titulo-azul hide-for-medium-down"></div>
<section class="row">
    <div class="">
        <div class="off-canvas-wrap" data-offcanvas>
            <div class="inner-wrap">
                <?php require_once('inc_sidebar.php'); ?>
                <!-- CONTEUDO .large-9 .medium-12 .small-12 -->
                <div class="large-9 medium-12 small-12 columns">
                    <h2 class="titulo-azul"><?=$page['titulo']; ?></h2>
                    <div class="box-branco">
                        <?php if (isset($flash["error"]) and !empty($flash["error"])){ ?>
                        <div data-alert class="alert-box alert radius">
                            <?=$flash["error"];?>
                            <a href="#" class="close">&times;</a>
                        </div>
                        <?php }else{ ?>
                        <p>Faça seu cadastro em nosso Guia!</p>
                        <p>É rápido, fácil e você ainda pode aproveitar todos os benefícios que só o nosso site tem para lhe oferecer.</p>
                        <p>Para isso, preencha corretamente todos os campos abaixo:</p>
                        <?php } ?>
                        <form name="form-cadastre-se" id="form-cadastre-se" action="<?=$_SERVER["REQUEST_URI"];?>" method="post" enctype="multipart/form-data">
                            <h3>Dados Pessoais</h3>
                            <div class="clear"></div>
                            <p>
                                <label class="pc50 left"><input id="tipopessoa" name="tipopessoa" type="radio" value="1" required />Pessoa Física</label>
                                <label class="pc50 left"><input id="tipopessoa" name="tipopessoa" type="radio" value="2" required />Pessoa Jurídica</label>
                            </p>
                            <p class="pessoa1">
                                <label for="nome">Nome Completo</label>
                                <input name="nome" id="nome" type="text" class="l70" maxlength="100" />
                            </p>
                            <div class="clear"></div>
                            <p class="pessoa1">
                                <label for="cpf">CPF</label>
                                <input name="cpf" id="cpf" type="text" class="left l40" placeholder="___.___.___-__" data-mask="000.000.000-00" maxlength="14" /><em class="left l60 padding-box-interno font10 margin-top15">Não fazemos consulta do CPF e nem fornecemos a outros usuários</em>
                            </p>
                            <div class="clear"></div>
                            <p class="pessoa1">
                                <label for="rg">RG</label>
                                <input name="rg" id="rg" type="text" class="left l40" maxlength="30" />
                            </p>
                            <div class="clear"></div>
                            <p class="pessoa1">
                                <label for="datadenascimento">Data de Nascimento</label>
                                <input name="datadenascimento" id="datadenascimento" type="text" class="l40" placeholder="__/__/____" data-mask="00/00/0000" maxlength="10" />
                            </p>
                            <div class="clear"></div>
                            <p class="pessoa2">
                                <label for="razaosocial">Razão Social</label>
                                <input name="razaosocial" id="razaosocial" type="text" class="l80" maxlength="100" />
                            </p>
                            <div class="clear"></div>
                            <p class="pessoa2">
                                <label for="nomefantasia">Nome Fantasia</label>
                                <input name="nomefantasia" id="nomefantasia" type="text" class="l60" maxlength="100" />
                            </p>
                            <div class="clear"></div>
                            <p class="pessoa2">
                                <label for="cnpj">CNPJ</label>
                                <input name="cnpj" id="cnpj" type="text" class="left l40" placeholder="__.___.___/____-__" data-mask="00.000.000/0000-00" maxlength="18" /><em class="left l50 padding-box-interno font10 margin-top15">Não fazemos consulta do CNPJ e nem fornecemos a outros usuários</em>
                            </p>
                            <div class="clear"></div>
                            <p class="pessoa2">
                                <label for="inscricaoestadual">Inscrição Estadual</label>
                                <input name="inscricaoestadual" id="inscricaoestadual" type="text" class="left l40" maxlength="20" /><em class="left l20 padding-box-interno font10 margin-top15">Opcional</em>
                            </p>
                            <div class="clear"></div>
                            <p class="pessoa2">
                                <label for="nomedoresponsavel">Nome do Responsável</label>
                                <input name="nomedoresponsavel" id="nomedoresponsavel" type="text" class="l70" maxlength="100" />
                            </p>
                            <div class="clear"></div>
                            <p>
                                <label for="email">E-mail</label>
                                <input name="email" id="email" type="text" class="left l70" maxlength="100" /><em class="left l30 margin-top10 padding-box-interno font10">Cadastre seu e-mail mais usado, em caso de contato é por ele que você será avisado.</em>
                            </p>
                            <div class="clear"></div>
                            <p>
                                <label for="re_email">Repetir E-mail</label>
                                <input name="re_email" id="re_email" type="text" class="l70" maxlength="100" />
                            </p>
                            <div class="clear"></div>
                            <h3>Endereço</h3>
                            <div class="clear"></div>
                            <p>
                                <label for="cep">CEP</label>
                                <input name="cep" id="cep" type="text" class="left l30" placeholder="_____-___" data-mask="00000-000" maxlength="9" /><em class="left l70 padding-box-interno font10 margin-top15">Não sabe seu CEP? <a href="http://www.buscacep.correios.com.br" target="_blank" title="Não sabe seu CEP? Clique aqui" class="font-10">Clique aqui</a></em>
                            </p>
                            <div class="clear"></div>
                            <p class="left l70">
                                <label for="endereco">Endereço</label>
                                <input name="endereco" id="endereco" type="text" placeholder="Avenida, Rua, Viela, etc..." maxlength="200" />
                            </p>
                            <p class="right l25">
                                <label for="numero">Número</label>
                                <input name="numero" id="numero" type="text" maxlength="15" />
                            </p>
                            <div class="clear"></div>
                            <p class="left l45">
                                <label for="bairro">Bairro</label>
                                <input name="bairro" id="bairro" type="text" maxlength="50" />
                            </p>
                            <p class="right l50">
                                <label for="complemento">Complemento</label>
                                <input name="complemento" id="complemento" type="text" placeholder="Quadra, Lote, Apartamento, etc..." maxlength="100" />
                            </p>
                            <div class="clear"></div>
                            <p class="left l30">
                                <label for="estado">Estado</label>
                                <select name="estado" id="estado">
                                    <option value="">Escolha...</option>
                                    <?php
                                        $estados = \CoffeeCore\Helper\ResourceManager::readFile("estados.json");
                                        foreach ($estados as $k => $estado) {
                                            echo "<option value='";
                                            echo $estado["name"] . "' > ";
                                            echo $estado["description"] . "</option>";
                                        }
                                    ?>
                                </select>
                            </p>
                            <p class="right l65">
                                <label for="cidade">Cidade</label>
                                <select name="cidade" id="cidade">
                                    <option value=''>Escolha o Estado primeiro...</option>
                                    <option value="1">São Luis de Montes Belos</option>
                                </select>
                            </p>
                            <div class="clear"></div>
                            <h3>Contato</h3>
                            <div class="clear"></div>
                            <p class="left l45">
                                <label for="celular">Celular</label>
                                <input name="celular" id="celular" type="text" placeholder="(__) ____-____" data-mask="(00) 0000-00009" maxlength="15" />
                            </p>
                            <p class="right l45">
                                <label for="telefone">Telefone</label>
                                <input name="telefone" id="telefone" type="text" placeholder="(__) ____-____" data-mask="(00) 0000-0000" maxlength="14" />
                            </p>
                            <div class="clear clearfix"></div>
                            <h3>Dados para Acesso</h3>
                            <div class="clear"></div>
                            <p>
                                <label for="login">Login/Apelido</label>
                                <input name="login" id="login" type="text" class="left l40" maxlength="20" /><em class="left l60 padding-box-interno font10 margin-top15">Não é permitido caracteres especiais ou letras maiúsculas.</em>
                            </p>
                            <div class="clear"></div>
                            <p>
                                <label for="senha">Senha</label>
                                <input name="senha" id="senha" type="password" class="left l40"  maxlength="12" /><em class="left l60 padding-box-interno font10 margin-top10">A senha deve ter entre 6 e 12 letras ou números. Não é permitido caracteres especiais. O sistema faz diferenciação entre maiúsculas e minúsculas.</em>
                            </p>
                            <div class="clear"></div>
                            <p>
                                <label for="re_senha">Repetir Senha</label>
                                <input name="re_senha" id="re_senha" type="password" class="left l40" maxlength="12" />
                            </p>
                            <div class="clear"></div>
                            <h3>Pesquisa</h3>
                            <div class="clear"></div>
                            <p>
                                <label for="comoconheceu">Como você conheceu o Guia Seu Bairro Tem?</label>
                                <select name="comoconheceu" id="comoconheceu">
                                    <option value="">Selecione</option>
                                    <?php
                                        $viasConhecimento = \CoffeeCore\Storage\DoctrineStorage::orm()->getRepository(ViaConhecimento\Entity::FULL_NAME)->findBy([], ["titulo" => "ASC"]);
                                        foreach ($viasConhecimento as $via) {
                                            echo "<option value='{$via->getId()}'>{$via->getTitulo()}</option>";
                                        }
                                        echo "<option value='outro'>Outro</option>";
                                    ?>
                                </select>
                            </p>
                            <div class="clear"></div>
                            <p class="box-comoconheceuoutro">
                                <label for="comoconheceuoutro">Outro</label>
                                <input name="comoconheceuoutro" id="comoconheceuoutro" type="text" placeholder="Se como você conheceu o site não estiver listado acima, informe-nos aqui" />
                            </p>
                            <div class="clear"></div>
                            <h3>Termos e condições de uso</h3>
                            <div class="clear"></div>
                            <div class="antes-box-termos">
                                <div class="box-termos">
                                    <?php
                                        echo \CoffeeCore\Storage\DoctrineStorage::orm()->getRepository(TextoSite\Entity::FULL_NAME)->findOneBy(["local" => "termos-de-uso"])->getTexto();
                                    ?>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <p>
                                <label for="termos"><input name="termos" id="termos" type="checkbox" value="1" />Li e aceito os termos e condições de uso.</label>
                            </p>
                            <div class="clear"></div>
                            <p class="l100 text-right">
                                <button name="bt-cadastrar" type="submit">Cadastrar</button>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php require_once('inc_footer.php'); ?>
<script>
    $(document).ready(function() {
        $('#cpf').mask('000.000.000-00');
        $('#datadenascimento').mask('00/00/0000');
        $('#cnpj').mask('00.000.000/0000-00');
        $('#cep').mask('00000-000');
        $('#celular').mask('(00) 0000-00009');
        $('#telefone').mask('(00) 0000-0000');

        //Lowercase
        $('#login').val($(this).val().toLowerCase());
        $("#form-cadastre-se").validate({
            rules : {
                tipopessoa: {
                    required: true,
                    range: [1,2]
                },
                email: {
                    required: true,
                    email: true,
                    minlength: 6
                },
                re_email : {
                    required: true,
                    email: true,
                    equalTo: '#email'
                },
                comoconheceuoutro: {
                    required: function(element){
                        if($("#comoconheceu").val() == 'outro'){
                            return true;
                        }else{
                            return false;
                        }
                    }
                },
                comoconheceu : {
                    required: true
                },
                cep: {
                    required: true,
                    cep: true
                },
                endereco: {
                    required: true
                },
                numero: {
                    required: true
                },
                bairro: {
                    required: true
                },
                estado: {
                    required: true
                },
                cidade: {
                    required: true
                },
                celular: {
                    required: true
                },
                login: {
                    //pattern: /[a-z0-9]+/,
                    required: true,
                    alpha: true
                },
                senha: {
                    required: true,
                    minlength: 6,
                    maxlength: 12
                },
                re_senha : {
                    required: true,
                    minlength: 6,
                    maxlength: 12,
                    equalTo: '#senha'
                },
                termos : {
                    required: true
                }
            }
        });

        $('#form-cadastre-se').find('p.pessoa1').hide();
        $('#form-cadastre-se').find('p.pessoa2').hide();
        $('#form-cadastre-se').find('p.box-comoconheceuoutro').hide();

        $("#estado").change(function(){
            var uf = $(this).val();
            $.ajax({
                url: "/cidades/" + uf.toLowerCase() ,
                dataType: "json",
                method: "get",
                beforeSend : function () {
                    if (uf.length > 0) {
                        $("#cidade").html("<option value=''>Selecione</option>");
                    } else {
                        $("#cidade").html("<option value=''>Escolha o Estado</option>");
                    }
                    $("#cidade").attr("disabled", true);

                }
            }).done(function(result){
                $.each(result, function (key, value) {
                    $("#cidade").append("<option value='"+ value.id +"'>" + value.title + "</option>")
                });
                $("#cidade").attr("disabled", false);
            });
        });

        $('select[name=comoconheceu]').change(function(){
            if($(this).val() != 'outro'){
                $('input[name=comoconheceuoutro]').val('');
                $('p.box-comoconheceuoutro').hide();
            }else{
                $('p.box-comoconheceuoutro').show();
            }
        });

        $('input[name=tipopessoa]').click(function(){
            if($(this).val() == 1){
                $('p.pessoa1').show();
                $('p.pessoa2').hide();

                $("#cpf").rules("add", {
                    required: true,
                    cpf:true
                });

                $("#rg").rules("add", {
                    required: true
                });

                $("#nome").rules("add", {
                    required: true
                });

                $("#datadenascimento").rules("add", {
                    required: true,
                    dateBR: true
                });

            }else{
                $('p.pessoa2').show();
                $('p.pessoa1').hide();

                $("#cnpj").rules("add", {
                    required: true,
                    cnpj: true
                });

                $("#razaosocial").rules("add", {
                    required: true,
                    minlength: 6
                });

                $("#nomefantasia").rules("add", {
                    required: true,
                    minlength: 6
                });

                $("#nomedoresponsavel").rules("add", {
                    required: true,
                    minlength: 6
                });
            }
        });
    });
</script>
</body>
</html>