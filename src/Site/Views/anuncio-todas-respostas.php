<?php
/** @var Anuncio\Entity $getAnuncio */
?>
<?php
$intra = Intranet\Authentication::getInstance();
$userAutenticated = $intra->getFromSession("entity");
?>
<a class="close-reveal-modal">&#215;</a>
<!-- .todas-perguntas-e-respostas -->
<div class="todas-perguntas-e-respostas">
	<h3 class="titulo-interno"><?=$getAnuncio->getTitulo();?><small class="right">(Cod. <?=$getAnuncio->getId();?>)</small></h3>
    <div class="wrapper">
        <ul class="lista-de-perguntas">
        <?php
        $conn = \CoffeeCore\Storage\DoctrineStorage::orm()->getConnection();
		$perguntas = $conn->fetchAll("SELECT * FROM anuncioperguntas a WHERE a.anuncio = {$getAnuncio->getId()} order by a.id DESC");
        if (empty($perguntas)) {
            echo "<li><br><br><br>Nenhuma pergunta feita neste anúncio.<br><br><br></li>";
        } else {
            $i = 0;

            /** @var array $pergunta */
            foreach ($perguntas as $pergunta) {

                ?>
                <li class="<?= ($i%2==0) ? "cinza" : "";?>">
                    <div>
                        <time class="col-um">Pergunta:<br/><?= (new DateTime($pergunta['datahorapergunta']))->format("d/m/Y H:i") ; ?></time>
                        <span class="col-dois pergunta">
                            <?php if($intra->isAuthenticated() and $userAutenticated->getId() == $getAnuncio->getUsuario()){ ?>
                                <?php
                                if($pergunta['usuario'] != "") {
                                    $usuario = \CoffeeCore\Storage\DoctrineStorage::orm()->getRepository(Usuario\Entity::FULL_NAME)->findOneBy(["id" => $pergunta['usuario']]);
                                    if ($usuario->getTipopessoa() == 1) {
                                        $nomeusuario = $usuario->getNome().' ('.$usuario->getEmail().')';
                                    } else {
                                        $nomeusuario = $usuario->getNomefantasia().' ('.$usuario->getEmail().')';
                                    }
                                }else{
                                    $nomeusuario = $pergunta['nome'].' ('.$pergunta['email'].')';
                                }
                                ?>
                                <strong><?=$nomeusuario;?></strong>
                            <?php } ?>
                            <?=$pergunta['pergunta'];?>
                        </span>
                    </div>
                    <?php
                    if (!empty($pergunta['resposta'])) {
                        ?>
                        <div>
                            <time class="col-um">Resposta:<br/><?= (new DateTime($pergunta['datahoraresposta']))->format("d/m/Y H:i") ; ?></time>
                            <span class="col-dois resposta"><?=$pergunta['resposta'];?></span>
                        </div>
                    <?php
                    } 
                    ?>
                </li>
                <?php
                $i++;
            }
        }
        ?>
        </ul>
    </div>
</div><!-- /.todas-perguntas-e-respostas -->