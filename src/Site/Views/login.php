<?php
$pagina = "login";
use CoffeeCore\Storage\DoctrineStorage;
require_once('inc_head.php');

foreach ($paginas as $k => $page) {
    if($page['slug'] == $pagina){
?>
<title><?=$page['titulo']; ?> | <?=$dadosDoSite['titulo'];?> - <?=$dadosDoSite['slogan'];?></title>
<meta name="title" content="<?=$page['titulo']; ?> | <?=$dadosDoSite['titulo'];?> - <?=$dadosDoSite['slogan'];?>"/>
<meta name="description" content="<?=$page['descricao']; ?>"/>
<meta name="keywords" content="<?=$page['palavraschave']; ?>"/>
<!-- Tags Facebook -->
<meta property="og:title" content="<?=$page['titulo']; ?> | <?=$dadosDoSite['titulo'];?> - <?=$dadosDoSite['slogan'];?>"/>
<meta property="og:description" content="<?=$page['descricao']; ?>"/>
<meta property="og:type" content="website"/>
<meta property="og:locale" content="pt_BR"/>
<meta property="og:url" content="<?= $prefixLink; ?>"/>
<meta property="og:image" content="<?= $prefixLink; ?>images/avatar.jpg"/>
<meta property="og:site_name" content="<?=$page['titulo']; ?> | <?=$dadosDoSite['titulo'];?> - <?=$dadosDoSite['slogan'];?>"/>
<?php }} ?>
</head>
<body>
<?php require_once('inc_header.php'); ?>
<div class="categorias-titulo-azul hide-for-medium-down"></div>
<section class="row">
    <div class="">
        <div class="off-canvas-wrap" data-offcanvas>
            <div class="inner-wrap">
                <?php require_once('inc_sidebar.php'); ?>
                <!-- CONTEUDO .large-9 .medium-12 .small-12 -->
                <div class="large-9 medium-12 small-12 columns">
                    <h2 class="titulo-azul">Faça seu login</h2>
                    <div class="box-branco">
                        <p>Já possui cadastro? Entre com seus dados abaixo.</p>
                        <?php if (isset($flash["error"]) and !empty($flash["error"])) { ?>
                        <div data-alert class="alert-box alert radius">
                            <?=$flash["error"];?>
                            <a href="#" class="close">&times;</a>
                        </div>
                        <?php } ?>
                        <form name="form-login" id="form-login" action="<?=$_SERVER["REQUEST_URI"];?>" method="post" enctype="multipart/form-data">
                            <input name="paginaAnterior" type="hidden" value="<?=$_GET['paginaAnterior'];?>" />
                            <p>
                                <label for="login">Login/Apelido</label>
                                <input name="login" id="login" type="text" class="l80" />
                            </p>
                            <p>
                                <label for="senha">Senha</label>
                                <input name="senha" id="senha" type="password" class="l80" maxlength="10" />
                            </p>
                            <p class="l80 text-right">
                                <button name="bt-entrar" type="submit">Entrar</button>
                                <br />
                                <a href="/cadastre-se" title="Cadastre-se" class="font10 right">Ainda não possui cadastro? Clique aqui!</a>
                            </p>
                        </form>
                        <div class="clearfix"><br /><br /><br /></div>
                        <div class="large-6 medium-6 small-12 text-center margin-bottom30 margin-top30 columns">
                            <a href="#"><img src="<?=$prefixLink;?>images/_temp/banner_voyage_300x250.jpg" alt="Voyage" /></a>
                        </div>
                        <div class="large-6 medium-6 small-12 text-center margin-bottom30 margin-top30 columns">
                            <a href="#"><img src="<?=$prefixLink;?>images/_temp/banner_longa_300x250.jpg" alt="Longa" /></a>
                        </div>
                    </div><!-- /.box-branco -->
                    <?php require_once('inc_bannerandfacebook.php'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php require_once('inc_footer.php'); ?>
<script>
$(document).ready(function() {
    // VALIDA FORM LOGIN
    $('#form-login').validate({
        // define regras para os campos
        rules: {
            login: {
                required: true
            },
            senha: {
                required: true
            }
        },
        // define messages para cada campo
        messages: {
            login		: "Informe o seu usuario",
            senha		: "informe a senha"
        }
    });
});
</script>
</body>
</html>