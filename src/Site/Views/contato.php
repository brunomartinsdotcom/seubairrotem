<?php
$pagina = "contato";
use CoffeeCore\Storage\DoctrineStorage;
require_once('inc_head.php');

foreach ($paginas as $k => $page) {
    if($page['slug'] == $pagina){
?>
<title><?=$page['titulo']; ?> | <?=$dadosDoSite['titulo'];?> - <?=$dadosDoSite['slogan'];?></title>
<meta name="title" content="<?=$page['titulo']; ?> | <?=$dadosDoSite['titulo'];?> - <?=$dadosDoSite['slogan'];?>"/>
<meta name="description" content="<?=$page['descricao']; ?>"/>
<meta name="keywords" content="<?=$page['palavraschave']; ?>"/>
<!-- Tags Facebook -->
<meta property="og:title" content="<?=$page['titulo']; ?> | <?=$dadosDoSite['titulo'];?> - <?=$dadosDoSite['slogan'];?>"/>
<meta property="og:description" content="<?=$page['descricao']; ?>"/>
<meta property="og:type" content="website"/>
<meta property="og:locale" content="pt_BR"/>
<meta property="og:url" content="<?= $prefixLink; ?>"/>
<meta property="og:image" content="<?= $prefixLink; ?>images/avatar.jpg"/>
<meta property="og:site_name" content="<?=$page['titulo']; ?> | <?=$dadosDoSite['titulo'];?> - <?=$dadosDoSite['slogan'];?>"/>
<?php }} ?>
</head>
<body>
<?php require_once('inc_header.php'); ?>
<div class="categorias-titulo-azul hide-for-medium-down"></div>
<section class="row">
    <div class="">
        <div class="off-canvas-wrap" data-offcanvas>
            <div class="inner-wrap">
                <?php require_once('inc_sidebar.php'); ?>
                <!-- CONTEUDO .large-9 .medium-12 .small-12 -->
                <div class="large-9 medium-12 small-12 columns">
                    <h2 class="titulo-azul">Contato</h2>
                    <div class="box-branco">
                        <?php if (isset($flash["error"]) and !empty($flash["error"])){ ?>
                        <div data-alert class="alert-box alert radius">
                            <?=$flash["error"];?>
                            <a href="#" class="close">&times;</a>
                        </div>
                        <?php }else if (isset($flash["success"]) and !empty($flash["success"])){ ?>
                        <div data-alert class="alert-box success radius">
                            <?=$flash["success"];?>
                            <a href="#" class="close">&times;</a>
                        </div>
                        <?php }else{ ?>
                        <div>
                            <?php
                                $texto = \CoffeeCore\Storage\DoctrineStorage::orm()->getRepository(\TextoSite\Entity::FULL_NAME)->findOneBy(['local' => 'contato']);
                                echo $texto->getTexto();
                            ?>
                        </div>
                        <?php } ?>
                        <form name="form-contato" id="form-contato" action="<?=$_SERVER["REQUEST_URI"];?>" method="post" enctype="multipart/form-data">
                            <p>
                                <label for="nome">Nome</label>
                                <input name="nome" id="nome" type="text" class="l80" />
                            </p>
                            <p>
                                <label for="email">E-mail</label>
                                <input name="email" id="email" type="text" class="l80" />
                            </p>
                            <p>
                                <label for="telefone">Telefone</label>
                                <input name="telefone" id="telefone" type="text" class="l40" placeholder="(__) ____-____" maxlength="15" />
                            </p>
                            <div class="clear"></div>
                            <p>
                                <label for="departamento">Departamento</label>
                                <select name="departamento" id="departamento" class="l40">
                                    <option value="">Selecione</option>
                                    <?php
                                        /** @var \Departamentos\Entity $departamento */
                                        $departamentos = DoctrineStorage::orm()->getRepository(\Departamentos\Entity::FULL_NAME)->findBy([], ['sortorder' => 'ASC']);
                                        foreach ($departamentos as $departamento):
                                    ?>
                                    <option value="<?=$departamento->getId();?>"><?=$departamento->getTitulo();?></option>
                                    <?php endforeach; ?>
                                </select>
                            </p>
                            <p>
                                <label for="mensagem">Mensagem</label>
                                <textarea name="mensagem" id="mensagem"></textarea>
                            </p>
                            <p class="l100 text-right">
                                <button name="bt-enviar" type="submit">Enviar</button>
                            </p>
                        </form>
                        <div class="large-6 medium-6 small-12 text-center margin-bottom30 margin-top30 columns">
                            <a href="#"><img src="<?=$prefixLink;?>images/_temp/banner_voyage_300x250.jpg" alt="Voyage" /></a>
                        </div>
                        <div class="large-6 medium-6 small-12 text-center margin-bottom30 margin-top30 columns">
                            <a href="#"><img src="<?=$prefixLink;?>images/_temp/banner_longa_300x250.jpg" alt="Longa" /></a>
                        </div>
                    </div><!-- /.box-branco -->
                    <?php require_once('inc_bannerandfacebook.php'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php require_once('inc_footer.php'); ?>
<script>
$(document).ready(function() {
    $('#telefone').mask('(00) 0000-00009');

    $("#form-contato").validate({
        // define regras para os campos
        rules: {
            nome: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            telefone: {
                required: true
            },
            departamento: {
                required: true
            },
            mensagem: {
                required: true
            }
        },
        // define messages para cada campo
        messages: {
            nome		: "Informe o seu nome",
            email		: { required:"Informe o seu e-mail", email:"E-mail Inválido!" },
            telefone	: "Informe o seu número de telefone",
            departamento: "Informe para qual departamento deseja enviar a mensagem",
            mensagem	: "Digite sua mensagem"
        }
    });
});
</script>
</body>
</html>