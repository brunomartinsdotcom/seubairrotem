<?php
require_once('inc_head.php');
$categoria = \CoffeeCore\Helper\ResourceManager::readFile("{$getTipo['titulo']}.json")[$getCategoria];
?>
<title><?=$categoria["titulo"];?> | Ofertas Agrícolas</title>
<?php
$conn = \CoffeeCore\Storage\DoctrineStorage::orm()->getConnection();
$carrosselSql = "SELECT * FROM anuncio WHERE status = 1 AND tipo={$getTipo['id']} AND categoria={$categoria['id']} AND id IN (SELECT anuncio FROM anunciodestaque WHERE destaque=2 AND datavencimento >= CURDATE()) ORDER BY rand()";
$anunciosCarrossel = $conn->fetchAll($carrosselSql);
?>
<?php if(!empty($anunciosCarrossel)){ ?><script src="<?=$prefixLink;?>js/jquery.bxslider.js"></script><?php } ?>
</head>
<body>
<?php require_once('inc_header.php'); ?>
<!-- .container -->
<div class="container row">
	<div class="off-canvas-wrap">
		<div class="inner-wrap">                
			<nav class="tab-bar show-for-medium-down">
				<section class="left-small"><a class="left-off-canvas-toggle menu-icon" ><span></span></a></section>
				<section class="middle tab-bar-section"></section>
			</nav>
			<aside class="left-off-canvas-menu">
				<?php include('inc_sidebar.php'); ?>
			</aside>
			<!-- .main-section -->
			<section class="main-section">
				<!-- CONTEUDO .large-9 .medium-12 .small-12 -->
				<div class="">
					<!-- .large-9 .medium-12 .small-12 -->
					<div class="large-9 medium-12 small-12 columns">
						<!-- .box-interno -->
						<article class="box-interno">
							<!-- .titulo-interno -->
							<h2 class="titulo-interno"><?=$categoria["titulo"];?></h2>
                            <div class="clear""></div>
                            <?php
                            if (!empty($anunciosCarrossel)) {
                                echo '<div class="carousel_destacado">';
                                foreach ($anunciosCarrossel as $anuncioCarrossel) {
                                    ?>
                                    <!-- .slide_i -->
                                    <div class="slide_i">
                                        <a href="/anuncio/<?= $anuncioCarrossel["id"] . '/' . date_format( new DateTime($anuncioCarrossel["datainclusao"]), "Y/m/d") . '/' . create_slug($anuncioCarrossel["titulo"]); ?>">
                                            <span class="border-<?php if($getTipo['id'] == 1){?>red<?php }else{ ?>green<?php } ?>"></span>
                                            <img src="<?= $prefixLink; ?>_uploads/anuncios/tumb-<?=$anuncioCarrossel["fotocapa"];?>" alt="Milho" style="height:109px;" />
                                            <?=$anuncioCarrossel["titulo"];?><br/>
                                            <em class="<?php if($getTipo['id'] == 1){?>red<?php }else{ ?>green<?php } ?>-price">Preço a vista: R$ <?=number_format($anuncioCarrossel["precoavista"], 2, ',', '.');?>/<?=$anuncioCarrossel["unidade"];?>.</em>
                                        </a>
                                    </div>
                                    <!-- /.slide_i -->
                                <?php
                                }
                                echo '</div><small class="titulo-galeria"><a href="/minha-conta/destacar-anuncio">Galeria</a></small>';
                            }
                            ?>
							<!-- .ordenar_por -->
							<div class="ordenar_por"><em>Ordenar por:</em>
                                <a href="<?="/{$getTipo['titulo']}/{$getCategoria}";?>/maior-preco" title="Maior Preço">Maior Preço</a>,
                                <a href="<?="/{$getTipo['titulo']}/{$getCategoria}";?>/menor-preco" title="Menor Preço">Menor Preço</a>,
                                <a href="<?="/{$getTipo['titulo']}/{$getCategoria}";?>/cidade" title="Cidade">Estado e Cidade</a>,
                                <a href="<?="/{$getTipo['titulo']}/{$getCategoria}";?>/estado" title="Estado">Estado</a>
                            </div><!-- .ordenar_por -->
                            <?php
                            if (empty($registros)) {
                                echo "<center>Nenhum registro encontrado.</center>";
                            } else {

                                $i = 0;
                                foreach ($registros as $registro) {

                                    $i++;
									$conn = \CoffeeCore\Storage\DoctrineStorage::orm()->getConnection();
									$sqlOferta = "SELECT * FROM anuncio WHERE id={$registro['id']} AND id IN
                                                 (SELECT anuncio FROM anunciodestaque WHERE destaque=4 AND datavencimento >= CURDATE() AND fatura IN
                                                 (SELECT id FROM fatura WHERE statuspagseguro=3 OR statuspagseguro=4) ) ";
									$totalOferta = count($conn->fetchAll($sqlOferta));
                                    ?>
                                    <!-- .lista-h .red -->
                                    <div class="lista-h <?php if($getTipo['id'] == 1){?>red<?php }else{ ?>green<?php } ?><?php if($totalOferta > 0){ ?> destacado<?php } ?>">
                                    	<a href="/anuncio/<?= $registro["id"] . '/' . date_format(new DateTime($registro["datainclusao"]), "Y/m/d") . '/' . create_slug($registro["titulo"]); ?>" title="<?= $registro["titulo"]; ?>">
                                        	<?php if($totalOferta > 0){ ?><span class="oferta"></span><?php } ?>
                                            <img src="<?= $prefixLink; ?>_uploads/anuncios/med-<?= $registro["fotocapa"]; ?>" alt="<?= $registro["titulo"];?>" />
                                            <span>
                                            	<strong><?= $registro["titulo"]; ?></strong>
                                                <br />
                                                <i class="hide-for-small-only">
													<?= $registro["descricaobreve"]; ?>
                                                    <br /><br />
                                                    <?=\CoffeeCore\Storage\DoctrineStorage::orm()->getRepository(Cidade\Entity::FULL_NAME)->findOneBy(["id" => $registro["cidade"]])->getNome(); ?>/<?=$registro["estado"];?>
                                                </i>
                                                <br /><br />
                                                <b>Preço a vista: R$ <?=number_format($registro["precoavista"], 2, ',', '.');?>/<?=$registro["unidade"];?>.</b>
                                                <em></em>
                                            </span>
                                        </a>
                                    </div><!-- /.lista-h /.red -->

                                    <?php
                                    if (in_array($i, [5, 10])) {
                                        echo '<!-- super-banner -->
                                            <div class="margin-top20 margin-bottom20 hide-for-small superbanner">';
                                        if (isset($publicidade['listagem1']['html']) and $i == 5) {
                                            echo '<small></small>'.$publicidade['listagem1']['html'];
                                        }

                                        if (isset($publicidade['listagem2']['html']) and $i == 10) {
                                            echo '<small></small>'.$publicidade['listagem2']['html'];
                                        }

                                        echo '</div>
                                            <!-- /super-banner -->';
                                    }
                                }
                            }
                            ?>


                            <!-- .links-help-rodape -->
                            <div class="links-help-rodape">
                            	<a href="/minha-conta/adicionar-anuncio" title="Publique agora seu anúncio Grátis!">Publique agora seu anúncio Grátis!</a>
                                <a href="/minha-conta/destaques-ativos" title="Anúncios Destacados">Anúncios Destacados</a>
                                <a href="/duvidas" title="Precisando de Ajuda?">Precisando de Ajuda?</a>
                            </div><!-- /.links-help-rodape -->
                            <?php
                            	$totalDeRegistros = $dados["totalRegistros"];
								$regitrosPorPagina = 25;
								if($totalDeRegistros > $regitrosPorPagina){
							?>
                            <!-- .paginacao -->
                            <div class="paginacao">
                                <?php
									$maximoPaginasNaPaginacao = 5;
	
									$paginaAtual = $dados["pagina"];
									$totalDePaginas = ceil($totalDeRegistros / $regitrosPorPagina);
									$paginator = (new \CoffeeCore\Helper\Paginator)
										->setUrl("/{$getTipo['titulo']}/{$getCategoria}/{$getSeed}", "/pagina/(:num)")
										->setItems($totalDeRegistros, $regitrosPorPagina, $maximoPaginasNaPaginacao)
										->setPrevNextTitle("«", "»")->setFirstLastTitle("««","»»")->setPage($paginaAtual);
									echo $paginator->toHtml();
                                ?>
                            </div><!-- /.paginacao -->
                            <?php } ?>
						</article><!-- /.box-interno -->
					</div><!-- /.large-9 /.medium-12 /.small-12 -->
				</div><!-- /CONTEUDO /.large-9 /.medium-12 /.small-12 -->
			</section><!-- /.main-section -->
			<a class="exit-off-canvas"></a>
		</div><!--/innerWrapp-->
	</div><!--/offCanvasWrap-->
</div><!-- /.container -->
<?php require_once('inc_footer.php'); ?>
<?php if(!empty($anunciosCarrossel)){ ?>
<script>
//ULTIMOS ANUNCIOS
$(document).ready(function(){
  $('.carousel_destacado').bxSlider({
    slideWidth:145,
    minSlides:1,
    maxSlides:4,
    slideMargin:25,
	controls:true,
	infiniteLoop:false
  });
});
</script>
<?php } ?>
</body>
</html>