<?php
$pagina = "";
require_once('inc_head.php');
?>
<title>Página não encontrada | <?=$dadosDoSite['titulo'];?> - <?=$dadosDoSite['slogan'];?></title>
</head>
<body>
<?php require_once('inc_header.php'); ?>
<div class="categorias-titulo-azul hide-for-medium-down"></div>
<section class="row">
    <div class="">
        <div class="off-canvas-wrap" data-offcanvas>
            <div class="inner-wrap">
                <?php require_once('inc_sidebar.php'); ?>
                <!-- CONTEUDO .large-9 .medium-12 .small-12 -->
                <div class="large-9 medium-12 small-12 columns">
                    <h2 class="titulo-azul">Ops! Erro 404</h2>
                    <div class="box-branco">
                        <div class="texto text-center">
                            <p class="font18"><br /><br /><br /><br />Desculpe, a página que você tentou acessar não está disponível ou não existe!<br /><br /><br /><br /></p>
                        </div>
                    </div><!-- /.box-branco -->
                    <?php require_once('inc_bannerandfacebook.php'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php require_once('inc_footer.php'); ?>
</body>
</html>