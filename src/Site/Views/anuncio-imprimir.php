<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<?php /** @var Anuncio\Entity $getAnuncio */ ?>
<title><?= $getAnuncio->getTitulo(); ?> | Ofertas Agrícolas - Classificados do Agronegócio</title>
<link rel="stylesheet" href="/css/foundation.css" />
<link rel="stylesheet" href="/css/imprimir.css" />
</head>
<body onload="window.print();">
<header id="header"><img src="/images/logo-ofertasagricolas.png" alt="Ofertas Agrícolas" /></header>
<article class="box-interno">
    <!-- .titulo-interno -->
    <h2 class="titulo-interno"><?= $getAnuncio->getTitulo(); ?><small class="right">(Cod. <?= $getAnuncio->getId(); ?>)</small></h2>
    <!-- .dados-anuncio -->
    <div class="dados-anuncio">
        <div class="left">
            <p class="price-a-vista">Preço a vista: R$ <?= number_format($getAnuncio->getPrecoavista(), 2, ",", "."); ?> / <?= $getAnuncio->getUnidade(); ?></p>
            <p class="price-a-prazo">Preço a prazo: <?= number_format($getAnuncio->getPrecoaprazo(), 2, ",", "."); ?>  / <?= $getAnuncio->getUnidade(); ?></p>
            <p class="empty"></p>
            <p><strong><?= $getAnuncio->getNomeCidade(); ?> - <?= $getAnuncio->getEstado(); ?></strong></p>
        </div>
        <div class="right">
            <p>
            	<em>
            		Atualizado em
					<?php
                    	if(empty($getAnuncio->getDataalteracao())){
							echo $getAnuncio->getDatainclusao()->format("d/m/Y");	
						}else{
							echo $getAnuncio->getDataalteracao()->format("d/m/Y");
						}
					?>
                </em>
            </p>
			
			
			<?php
            $pagamento = $getAnuncio->getFormaDePagamento();
            $pagamentos = [];
            foreach ($pagamento as $row) {
                $pagamentos[] = $row['titulo'];
            }

            $entrega = $getAnuncio->getFormaDeEntrega();
            $formaEngregas = [];
            foreach ($entrega as $row) {
                $formaEngregas[] = $row['titulo'];
            }

            ?>
            <p>Pagamento: <?= implode(', ', array_values($pagamentos)) ?></p>
            <p>Entrega: <?= implode(', ', $formaEngregas) ?></p>
            <p><em>Este anúncio já foi visualizado: <?= $getAnuncio->getQuantidadevizualizacao(); ?> vezes</em></p>
        </div>
    </div><!-- /.dados_anuncio -->
    <!-- .detalhes-anuncio -->
    <div class="detalhes-anuncio">
        <header><h3>+ Detalhes</h3></header>
        <p id="texto">
        	<?php $quebradelinha = new \CoffeeCore\Helper\Quebradelinha(); ?>
            <?=$quebradelinha->quebradelinha($getAnuncio->getDescricaocompleta());?>
        </p> 
    </div><!-- /.detalhes-anuncio -->
    <div class="clearfix"></div>
    <!-- GALERIA -->
    <div class="galeria show-for-print">
    	<h4>Galeria de Fotos</h4>
        <ul>
            <li><a href="#"><img src="/_uploads/anuncios/<?= $getAnuncio->getFotocapa(); ?>" alt="<?=$getAnuncio->getTitulo();?>" data-description="<?=$getAnuncio->getTitulo();?>" /></a></li>

            <?php
                foreach($getAnuncio->getGaleriaDeFotos() as $foto) {
                    ?>
                    <li><a href="#"><img src="/_uploads/anuncios/<?= $foto['foto']; ?>" alt="<?= $getAnuncio->getTitulo(); ?>" data-description="<?=$getAnuncio->getTitulo();?>" /></a></li>
                    <?php
                }
            ?>

        </ul>
    </div><!-- /.galeria -->
    <!-- .aviso-anuncio -->
    <p class="aviso-anuncio">
        <?php
        $texto = \CoffeeCore\Storage\DoctrineStorage::orm()->getRepository(\TextoSite\Entity::FULL_NAME)->findOneBy(['local' => 'rodape-do-anuncio']);
        echo str_replace('%titulo%', $getAnuncio->getTitulo(), $texto->getTexto());
        ?>
    </p><!-- /.aviso-anuncio -->
</article><!-- /.box-interno -->
<!-- #FOOTER -->
<footer id="footer"><p>OA Atividades de Internet Ltda - Copyright © 2014 Ofertas Agrícolas  - Todos os Direitos Reservados</p><p>Os anúncios aqui disponibilizados são de exclusiva responsabilidade dos Anunciantes.</p></footer>
</body>
</html>