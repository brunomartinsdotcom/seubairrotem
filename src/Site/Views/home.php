<?php
$pagina = "home";
require_once('inc_head.php');
/*$orm = \CoffeeCore\Storage\DoctrineStorage::orm()->getConnection();

//$anunciosClassificados = $orm->fetchAll("SELECT * FROM anuncio WHERE tipo = 1 AND status = 1 ORDER BY RAND() LIMIT 8");
//$anunciosServicos = $orm->fetchAll("SELECT * FROM anuncio WHERE tipo = 2 AND status = 1 ORDER BY RAND() LIMIT 4");

$sql = "SELECT a.* FROM anuncio a
        LEFT JOIN faturaplano fp ON a.id = fp.anuncio
        WHERE
        DATE_FORMAT(a.datavalidade,'%Y-%m-%d') = fp.datavencimento AND
        a.status = 1 AND
        a.datavalidade >= CURDATE() AND
        fp.fatura IN (SELECT id FROM fatura WHERE ((codigotransacao='GRATUITO' OR codigotransacao='') AND (statuspagseguro=0 OR statuspagseguro=''))
        OR (codigotransacao!='GRATUITO' AND codigotransacao!='' AND (fp.valor=0 OR (fp.valor > 0 AND (statuspagseguro=3 OR statuspagseguro=4) ) ) ) )
        ORDER BY a.niveldestaque1 DESC, rand() LIMIT 0,12";
$anuncios = $orm->fetchAll($sql);

$abreviador = new \CoffeeCore\Helper\Abrevia();*/
?>
<title><?=$dadosDoSite['titulo'];?> - <?=$dadosDoSite['slogan'];?></title>
<script src="<?=$prefixLink;?>js/jquery.bxslider.js"></script>
<meta property="og:url" content="<?=url_base();?>" />
<meta property="og:image" content="<?=url_base();?>/images/logo-ofertasagricolas.png" />
</head>
<body>
<?php require_once('inc_header.php'); ?>
<div class="categorias-home-azul hide-for-small"></div>
<section>
    <div class="row">
        <div class="large-8 medium-12 small-12 categorias-home columns">
            <ul>
                <li>
                    <a href="#">Academias</a>
                    <a href="#">Agências de Emprego</a>
                    <a href="#">Agências de Publicidade</a>
                    <a href="#">Agências de Turismo</a>
                    <a href="#">Alimentação</a>
                    <a href="#">Auto Elétricas</a>
                    <a href="#">Auto Peças</a>
                    <a href="#">Autônomos / Prof. Liberais</a>
                    <a href="#">Bancos</a>
                </li>
                <li>
                    <a href="#">Bares</a>
                    <a href="#">Bazares</a>
                    <a href="#">Bebidas</a>
                    <a href="#">Buffet / Decoração</a>
                    <a href="#">Calçados</a>
                    <a href="#">Construção</a>
                    <a href="#">Contabilidade</a>
                    <a href="#">Dentistas</a>
                    <a href="#">Escolas</a>
                </li>
                <li>
                    <a href="#">Faculdade / Universidades</a>
                    <a href="#">Farmácias</a>
                    <a href="#">Hospitais</a>
                    <a href="#">Hotéis</a>
                    <a href="#">Igrejas</a>
                    <a href="#">Imóveis</a>
                    <a href="#">Oficinas Mecânicas</a>
                    <a href="#">Restaurantes</a>
                    <a href="#">Supermercados</a>
                </li>
            </ul>
            <a href="#" class="bt-mais" title="Mais Categorias">Mais</a>
        </div>
        <div class="medium-6 hide-for-large-up hide-for-small small-12 marginbottom10 text-center columns"><a href="#"><img src="images/_temp/banner_longa_300x250.jpg" alt="Longa" /></a></div>
        <div class="large-4 medium-6 small-12 text-center columns"><a href="#"><img src="images/_temp/banner_voyage_300x250.jpg" alt="Novo Voyage" /></a></div>
    </div>
</section>
<section class="anuncios-home">
    <div class="row">
        <article class="large-6 medium-6 small-12 columns">
            <a href="#" title="Benigno Rosa Fotografo Digital">
                <img src="images/_temp/anuncio1.jpg" alt="Benigno Rosa Fotografo Digital" />
                <strong>Benigno Rosa Fotografo Digital</strong>
                <br />
                <span>Conjunto Alvorada - Uberlândia - MG</span>
                <i></i>
            </a>
        </article>
        <article class="large-6 medium-6 small-12 columns">
            <a href="#" title="Sacolão e Mercearia JK">
                <img src="images/_temp/anuncio2.jpg" alt="Sacolão e Mercearia JK" />
                <strong>Sacolão e Mercearia JK</strong>
                <br />
                <span>Setor Pedro Ludovico Teixeira - Uberlândia - MG</span>
                <i></i>
            </a>
        </article>
        <article class="large-6 medium-6 small-12 columns">
            <a href="#" title="Academia Fitness Brasil">
                <img src="images/_temp/anuncio3.jpg" alt="Academia Fitness Brasil" />
                <strong>Academia Fitness Brasil</strong>
                <br />
                <span>Bairro do Morumbi - Uberlândia - MG</span>
                <i></i>
            </a>
        </article>
        <article class="large-6 medium-6 small-12 columns">
            <a href="#" title="Heleno’s Pizzaria e Lanchonete">
                <img src="images/_temp/anuncio4.jpg" alt="Heleno’s Pizzaria e Lanchonete" />
                <strong>Heleno’s Pizzaria e Lanchonete</strong>
                <br />
                <span>Centro - Uberlândia - MG</span>
                <i></i>
            </a>
        </article>
    </div>
</section>
<div class="row">
    <div class="large-12 medium-12 small-12 marginbottom10 columns">
        <a href="#"><img src="images/_temp/banner_seubairrotem_970x120.jpg" /></a>
    </div>
</div>
<section class="anuncios-home">
    <div class="row">
        <article class="large-6 medium-6 small-12 columns">
            <a href="#" title="Cida Banho e Tosa e Taxi Dog">
                <img src="images/_temp/anuncio5.jpg" alt="Cida Banho e Tosa e Taxi Dog" />
                <strong>Cida Banho e Tosa e Taxi Dog</strong>
                <br />
                <span>Setor Leste - Tupaciguara/MG</span>
                <i></i>
            </a>
        </article>
        <article class="large-6 medium-6 small-12 columns">
            <a href="#" title="Líder Pedras">
                <img src="images/_temp/anuncio6.jpg" alt="Líder Pedras" />
                <strong>Líder Pedras</strong>
                <br />
                <span>Morumbi - Uberlândia/MG</span>
                <i></i>
            </a>
        </article>
        <article class="large-6 medium-6 small-12 columns">
            <a href="#" title="Malagueta Assados e Cia">
                <img src="images/_temp/anuncio7.jpg" alt="Malagueta Assados e Cia" />
                <strong>Malagueta Assados e Cia</strong>
                <br />
                <span>Vila Popular - Belo Horizonte/MG</span>
                <i></i>
            </a>
        </article>
        <article class="large-6 medium-6 small-12 columns">
            <a href="#" title="Drogaria Morumbi">
                <img src="images/_temp/anuncio8.jpg" alt="Drogaria Morumbi" />
                <strong>Drogaria Morumbi</strong>
                <br />
                <span>Morumbi - Uberlândia/MG</span>
                <i></i>
            </a>
        </article>
    </div>
</section>
<div class="row">
    <div class="large-4 small-12 hide-for-medium-only marginbottom30 columns"><a href="#"><img src="images/_temp/banner_longa_300x250.jpg" alt="Longa" /></a></div>
    <div class="large-8 medium-12 small-12 marginbottom30 ultimos-anuncios columns">
        <h3>Últimos Anúncios</h3>
        <!-- .slider1 -->
        <div class="slider1">
            <!-- .slide -->
            <div class="slide">
                <a href="#" title="Favo de Mel">
                    <img src="images/_temp/anuncio9.jpg" alt="Favo de Mel" />
                    <strong>Favo de Mel</strong>
                    <br />
                    <span>Morumbi - Uberlândia/MG</span>
                </a>
            </div><!-- /.slide -->
            <!-- .slide -->
            <div class="slide">
                <a href="#" title="Brasil Extintores">
                    <img src="images/_temp/anuncio10.jpg" alt="Brasil Extintores" />
                    <strong>Brasil Extintores</strong>
                    <br />
                    <span>Setor Leste - Tupaciguara/MG</span>
                </a>
            </div><!-- /.slide -->
            <!-- .slide -->
            <div class="slide">
                <a href="#" title="Supermercado Bom Preço">
                    <img src="images/_temp/anuncio11.jpg" alt="Supermercado Bom Preço" />
                    <strong>Supermercado Bom Preço</strong>
                    <br />
                    <span>Centro - Uberlândia - MG</span>
                </a>
            </div><!-- /.slide -->
            <!-- .slide -->
            <div class="slide">
                <a href="#" title="Pizzaria Pirandello">
                    <img src="images/_temp/anuncio12.jpg" alt="Pizzaria Pirandello" />
                    <strong>Pizzaria Pirandello</strong>
                    <br />
                    <span>Setor Bela Vista - São Luis de Montes Belos/GO</span>
                </a>
            </div><!-- /.slide -->
            <!-- .slide -->
            <div class="slide">
                <a href="#" title="Café com Ideias">
                    <img src="images/_temp/anuncio13.jpg" alt="Café com Ideias" />
                    <strong>Café com Ideias</strong>
                    <br />
                    <span>Centro - São Luis de Montes Belos/GO</span>
                </a>
            </div><!-- /.slide -->
            <!-- .slide -->
            <div class="slide">
                <a href="#" title="Colégio NEO">
                    <img src="images/_temp/anuncio14.jpg" alt="Colégio NEO" />
                    <strong>Colégio NEO</strong>
                    <br />
                    <span>Setor Central - Porangatu/GO</span>
                </a>
            </div><!-- /.slide -->
            <!-- .slide -->
            <div class="slide">
                <a href="#" title="Faculdade Montes Belos">
                    <img src="images/_temp/anuncio15.jpg" alt="Faculdade Montes Belos" />
                    <strong>Faculdade Montes Belos</strong>
                    <br />
                    <span>Setor Universitário - São Luis de Montes Belos/MG</span>
                </a>
            </div><!-- /.slide -->
        </div><!-- /.slider1 -->
    </div>
</div>
<!-- .classificados -->
<section class="classificados">
    <!-- .row -->
    <div class="row">
        <header class="large-12 medium-12 small-12 columns"><h3>Classificados</h3></header>
        <!-- article -->
        <article class="large-3 medium-3 small-6 columns">
            <a href="#" title="Celta 1.0 2003 Completíssimo">
                <img src="images/_temp/classificado1.jpg" alt="Celta 1.0 2003 Completíssimo" />
                <strong>Celta 1.0 2003 Completíssimo</strong>
                <br />
                <span>Morumbi - Uberlândia/MG</span>
            </a>
        </article><!-- /article -->
        <!-- article -->
        <article class="large-3 medium-3 small-6 columns">
            <a href="#" title="Papeis de Parede + instalação">
                <img src="images/_temp/classificado2.jpg" alt="Papeis de Parede + instalação" />
                <strong>Papeis de Parede + instalação</strong>
                <br />
                <span>Setor Aeroporto - Uberlândia/MG</span>
            </a>
        </article><!-- /article -->
        <!-- article -->
        <article class="large-3 medium-3 small-6 columns">
            <a href="#" title="Sobrado 3/4 - 2 suítes - Vaga para 3 carros">
                <img src="images/_temp/classificado3.jpg" alt="Sobrado 3/4 - 2 suítes - Vaga para 3 carros" />
                <strong>Sobrado 3/4 - 2 suítes - Vaga para 3 carros</strong>
                <br />
                <span>Condomínio Barcelona - Uberlândia/MG</span>
            </a>
        </article><!-- /article -->
        <!-- article -->
        <article class="large-3 medium-3 small-6 columns">
            <a href="#" title="Contrata vendedor com experiência">
                <img src="images/_temp/classificado4.jpg" alt="Contrata vendedor com experiência" />
                <strong>Contrata vendedor com experiência</strong>
                <br />
                <span>Uberlândia/MG</span>
            </a>
        </article><!-- /article -->
    </div><!-- /.row -->
</section><!-- /.classificados -->
<!-- .row -->
<div class="row">
    <!-- .previsao-do-tempo -->
    <div class="large-3 hide-for-medium-down small-6 previsao-do-tempo-home columns">
        <header><h3>Tempo</h3></header>
        <!-- Widget Previs&atilde;o de Tempo CPTEC/INPE -->
        <iframe allowtransparency="true" marginwidth="0" marginheight="0" hspace="0" vspace="0" frameborder="0" scrolling="no" src="http://www.cptec.inpe.br/widget/widget.php?p=5517&w=h&c=00bfff&f=ffffff" height="200px"></iframe>
        <noscript>Previs&atilde;o de <a href="http://www.cptec.inpe.br/cidades/tempo/5517">Uberlândia/MG</a> oferecido por <a href="http://www.cptec.inpe.br">CPTEC/INPE</a></noscript>
        <!-- Widget Previs&atilde;o de Tempo CPTEC/INPE -->
    </div><!-- /.previsao-do-tempo -->
    <!-- .newsletter -->
    <div class="large-3 medium-4 small-12 newsletter-home columns">
        <header><h3>Newsletter</h3></header>
        <p>Cadastre-se e receba nossos informativos em seu e-mail.</p>
        <form name="form-newsletter" id="form-newsletter" action="" method="post" enctype="multipart/form-data">
            <label><input name="nome" type="text" placeholder="Nome" /></label>
            <label><input name="celular" type="text" placeholder="Celular" /></label>
            <label><input name="email" type="text" placeholder="E-mail" /></label>
            <label class="right"><button name="bt-cadastrar" type="button">Ok</button></label>
        </form>
    </div><!-- /.newsletter -->
    <!-- .newsletter -->
    <div class="large-6 medium-8 small-12 facebook columns">
        <header><h3>Facebook</h3></header>
        <iframe src="http://www.facebook.com/plugins/likebox.php?href=<?=$dadosDoSite['facebook'];?>&amp;width&amp;height=230&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden;" allowTransparency="true"></iframe>
    </div><!-- /.newsletter -->
</div><!-- /.row -->
<?php require_once('inc_footer.php'); ?>
<script src="<?=$prefixLink;?>js/jquery.bxslider.js"></script>
<script>
    $(document).foundation();
    //ULTIMOS ANUNCIOS
    $(document).ready(function(){
        $('.slider1').bxSlider({
            slideWidth:140,
            minSlides:2,
            maxSlides:3,
            slideMargin:68,
            controls:true,
            infiniteLoop:false
        });
    });
</script>
</body>
</html>