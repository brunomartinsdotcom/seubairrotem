<?php

namespace Site;

use CoffeeCore\Core\AbstractApplication;
use CoffeeCore\Helper\ResourceManager;
use CoffeeCore\Storage\DoctrineStorage;
use Intranet\Authentication;
use Nette\Mail\Message;
use Slim\Slim;


/**
 * Class Application
 * @package Site
 */
class Application extends AbstractApplication
{
    /**
     * @param Slim $app
     * @return void
     */
    protected function setRoutes(Slim $app)
    {
        $controller = new Controller();

        $app->group('', function () use ($app, $controller) {

            $app->get('/', function () use ($app) {
                $app->render(__NAMESPACE__ . "/Views/home.php",
                    [
                        /*"noticias" => DoctrineStorage::orm()
                            ->getRepository(\Noticia\Entity::FULL_NAME)->findBy([], ["id" => "desc"], 3),
                        "videos" => DoctrineStorage::orm()
                            ->getRepository(\Video\Entity::FULL_NAME)->findBy([], ["id" => "desc"], 3),*/
                    ]
                );
            });

            $app->get('/o-guia/', function () use ($app) {
                $textos = DoctrineStorage::orm()->getRepository(\Oguia\Entity::FULL_NAME)->findBy([], ["sortorder" => "ASC"]);

                $app->render(__NAMESPACE__ . "/Views/o-guia.php", ["textos" => $textos]);
            });

            $app->get('/politicas-de-privacidade/', function () use ($app) {
                $texto = DoctrineStorage::orm()->getRepository(\TextoSite\Entity::FULL_NAME)
                            ->findOneBy(['local' => 'politicas-de-privacidade']);

                $app->render(__NAMESPACE__ . "/Views/texto.php",
                    [
                        "pagina" => "politicas-de-privacidade",
                        "texto" => [
                            "titulo" => $texto->getTitulo(),
                            "texto" => $texto->getTexto(),
                        ]
                    ]
                );
            });

            $app->get('/termos-de-uso/', function () use ($app) {
                $texto = DoctrineStorage::orm()->getRepository(\TextoSite\Entity::FULL_NAME)
                                    ->findOneBy(['local' => 'termos-de-uso']);

                $app->render(__NAMESPACE__ . "/Views/texto.php",
                    [
                        "pagina" => "termos-de-uso",
                        "texto" => [
                            "titulo" => $texto->getTitulo(),
                            "texto" => $texto->getTexto(),
                        ]
                    ]
                );
            });

            $app->get('/anuncie/', function () use ($app) {
                $texto = DoctrineStorage::orm()->getRepository(\TextoSite\Entity::FULL_NAME)
                                ->findOneBy(['local' => 'anuncie']);

                $app->render(__NAMESPACE__ . "/Views/texto.php",
                    [
                        "pagina" => "anuncie",
                        "texto" => [
                            "titulo" => $texto->getTitulo(),
                            "texto" => $texto->getTexto(),
                        ]
                    ]
                );
            });

            $app->post('/newsletter', function () use ($app, $controller) {
                try {
                    $controller->cadastrarNewsletter($_POST["n-nome"], $_POST["n-celular"], $_POST["n-email"]);
                    $app->flash("info",  "Cadastrado com Sucesso!");
                } catch (\Exception $e) {
                    $app->flash("info", $e->getMessage());
                }
                $app->redirect($_POST["current-page"]);
            });

            $app->map('/login/', function () use ($app, $controller) {


                if ($app->request()->isGet()) {
                    $app->render(__NAMESPACE__ . "/Views/login.php");
                }
                if ($app->request()->isPost()) {

                    try {
                        $controller->logar($_POST["login"], $_POST["senha"]);

                    } catch (\Exception $e) {
                        $app->flash("info", $e->getMessage());
                        $app->redirect(url_base() . "/login");
                    }
					if(!empty($_POST["paginaAnterior"])){
						$app->redirect($_POST["paginaAnterior"]);
					}else{
                    	//$app->redirect(url_base());
						$app->redirect('/minha-conta');
					}
                }
            })->via('GET', 'POST');

            $app->map('/cadastre-se/', function () use ($app, $controller) {
                $intra = \Intranet\Authentication::getInstance();if ($intra->isAuthenticated()) {
                    $app->redirect(url_base() . "/minha-conta/meus-dados/");
                }
                if ($app->request()->isGet()) {
                    $app->render(__NAMESPACE__ . "/Views/cadastre-se.php" );
                }
                if ($app->request()->isPost()) {
                    try {
                        $controller->cadastrar();

                        /** @var \TextoSite\Entity $texto */
                        $texto = DoctrineStorage::orm()->getRepository(\TextoSite\Entity::FULL_NAME)->findOneBy(['local' => 'cadastrado']);
                        $message = new Message();
                        $message->addTo($_POST['email'])
                            ->setSubject($texto->getTitulo())
                            ->setFrom('contato@seubairrotem.com.br', 'Guia Seu Bairro Tem')
                            ->setHtmlBody($texto->getTexto());
                        $app->mailer->send($message);

                        $app->flash("info", "Cadastrado com Sucesso.");
                    } catch (\Exception $e) {
                        $app->flash("info", "Ocorreu um erro ao cadastrar. ". $e->getMessage());
                        $app->redirect("/cadastre-se/");
                    }

                    $app->redirect(url_base());
                }
            })->via("GET", "POST");

            $app->get("/noticias(/:pagina)/", function ($pagina = 1) use ($app) {
                $noticiaRepo = DoctrineStorage::orm()->getRepository("Noticia\\Entity");
                $noticias = $noticiaRepo->findBy([], ["datanoticia" => "desc", "id" => "desc"], 15, ($pagina -1) * 15);

                $numRows = count($noticiaRepo->createQueryBuilder('a')->getQuery()->getArrayResult());
                if ($pagina > 1 and count($noticias) == 0) {
                    $app->redirect("/noticias/");
                }

                $app->render(
                    __NAMESPACE__ . "/Views/noticias.php",
                    [
                        "dados" => [
                            "pagina" => $pagina,
                            "totalRegistros" => $numRows,
                            "noticias" => $noticias,
                        ]
                    ]
                );
            });

            $app->get("/noticia(/:id(/:ano(/:mes(/:dia(/:slug)))))/",
                function ($id = null, $ano = null, $mes = null, $dia = null, $slug = null) use ($app) {
                    if (empty($id)) {
                        $app->redirect("/noticias/");
                    }

                    /** @var \Noticia\Entity $noticia */
                    $noticia = DoctrineStorage::orm()->getRepository("Noticia\\Entity")->findOneBy(["id" => $id]);
                    if (empty($noticia)) {
                        $app->redirect("/noticias");
                    }
                    $genSlug = create_slug($noticia->getTitulo());

                    $datanoticia = explode("/", $noticia->getDatanoticia()->format("Y/m/d"));

                    if (empty($ano) or empty($mes) or empty($dia)) {
                        $ano = $datanoticia[0];
                        $mes = $datanoticia[1];
                        $dia = $datanoticia[2];
                        $app->redirect("/noticia/{$id}/{$ano}/{$mes}/{$dia}/{$genSlug}/");
                    }

                    if ($ano != $datanoticia[0] or $mes != $datanoticia[1] or $dia != $datanoticia[2]) {
                        $ano = $datanoticia[0];
                        $mes = $datanoticia[1];
                        $dia = $datanoticia[2];
                        $app->redirect("/noticia/{$id}/{$ano}/{$mes}/{$dia}/{$genSlug}/");
                    }

                    if (empty($slug) or $genSlug != $slug) {
                        $app->redirect("/noticia/{$id}/{$ano}/{$mes}/{$dia}/{$genSlug}/");
                    }

                    $outrasNoticias = DoctrineStorage::orm()
                        ->getRepository("Noticia\\Entity")
                        ->findBy([], ["datanoticia" => "desc", "id" => "desc"], 14);

                    foreach ($outrasNoticias as $k => $noticiaItem) {
                        if ($noticiaItem == $noticia) {
                            unset($outrasNoticias[$k]);
                            break;
                        }
                    }
                    if (count($outrasNoticias) > 5) {
                        array_pop($outrasNoticias);
                    }

                    $app->render(
                        __NAMESPACE__ . "/Views/noticia.php",
                        [
                            "noticia" => $noticia,
                            'outrasNoticias' => $outrasNoticias
                        ]
                    );
                }
            );

            $app->get("/videos(/:pagina)/", function ($pagina = 1) use ($app) {
                $videoRepo = DoctrineStorage::orm()->getRepository("Video\\Entity");
                $video = $videoRepo->findBy([], ["data" => "desc", "id" => "desc"], 15, ($pagina -1) * 15);

                $numRows = count($videoRepo->createQueryBuilder('a')->getQuery()->getArrayResult());
                if ($pagina > 1 and count($video) == 0) {
                    $app->redirect("/videos/");
                }

                $app->render(
                    __NAMESPACE__ . "/Views/videos.php",
                    [
                        "dados" => [
                            "pagina" => $pagina,
                            "totalRegistros" => $numRows,
                            "videos" => $video,
                        ]
                    ]
                );
            });

            $app->get("/video(/:id(/:ano(/:mes(/:dia(/:slug)))))/",
                function ($id = null, $ano = null, $mes = null, $dia = null, $slug = null) use ($app) {
                    if (empty($id)) {
                        $app->redirect("/videos/");
                    }

                    /** @var \Video\Entity $video */
                    $video = DoctrineStorage::orm()->getRepository(\Video\Entity::FULL_NAME)->findOneBy(["id" => $id]);
                    if (empty($video)) {
                        $app->redirect("/noticias");
                    }

                    $genSlug = create_slug($video->getTitulo());

                    $data = explode("/", $video->getData()->format("Y/m/d"));

                    if (empty($ano) or empty($mes) or empty($dia)) {
                        $ano = $data[0];
                        $mes = $data[1];
                        $dia = $data[2];
                        $app->redirect("/video/{$id}/{$ano}/{$mes}/{$dia}/{$genSlug}/");
                    }

                    if ($ano != $data[0] or $mes != $data[1] or $dia != $data[2]) {
                        $ano = $data[0];
                        $mes = $data[1];
                        $dia = $data[2];
                        $app->redirect("/video/{$id}/{$ano}/{$mes}/{$dia}/{$genSlug}/");
                    }

                    if (empty($slug) or $genSlug != $slug) {
                        $app->redirect("/video/{$id}/{$ano}/{$mes}/{$dia}/{$genSlug}/");
                    }

                    $outrosVideos = DoctrineStorage::orm()
                        ->getRepository("Video\\Entity")
                        ->findBy([], ["data" => "desc", "id" => "desc"], 14);

                    foreach ($outrosVideos as $k => $noticiaItem) {
                        if ($noticiaItem == $video) {
                            unset($outrosVideos[$k]);
                            break;
                        }
                    }
                    if (count($outrosVideos) > 5) {
                        array_pop($outrosVideos);
                    }

                    $app->render(
                        __NAMESPACE__ . "/Views/video.php",
                        [
                            "video" => $video,
                            'outrosVideos' => $outrosVideos
                        ]
                    );
                }
            );

            $app->get("/anuncio(/:id(/:ano(/:mes(/:dia(/:slug)))))/",
                function ($id = null, $ano = null, $mes = null, $dia = null, $slug = null) use ($app) {
                    if (empty($id)) {
                        $app->redirect("/");
                    }


                    /** @var \Anuncio\Entity $anuncio */
                    $anuncio = DoctrineStorage::orm()->getRepository("Anuncio\\Entity")->findOneBy(["id" => $id, 'status' => 1]);

                    if (empty($anuncio)) {
                        $app->redirect("/");
                    }

                    $genSlug = create_slug($anuncio->getTitulo());

                    $data = explode("/", $anuncio->getDatainclusao()->format("Y/m/d"));

                    if (empty($ano) or empty($mes) or empty($dia)) {
                        $ano = $data[0];
                        $mes = $data[1];
                        $dia = $data[2];
                        $app->redirect("/anuncio/{$id}/{$ano}/{$mes}/{$dia}/{$genSlug}/");
                    }

                    if ($ano != $data[0] or $mes != $data[1] or $dia != $data[2]) {
                        $ano = $data[0];
                        $mes = $data[1];
                        $dia = $data[2];
                        $app->redirect("/anuncio/{$id}/{$ano}/{$mes}/{$dia}/{$genSlug}/");
                    }

                    if (empty($slug) or $genSlug != $slug) {
                        $app->redirect("/anuncio/{$id}/{$ano}/{$mes}/{$dia}/{$genSlug}/");
                    }

                    $app->render(
                        __NAMESPACE__ . "/Views/anuncio.php",
                        [
                            "getAnuncio" => $anuncio
                        ]
                    );
                }
            );

            $app->post("/anuncio-pergunta(/:id)/",
                function ($id = null) use ($app) {
                    if (empty($id)) {
                        $app->redirect("/");
                    }

                    try {
                        /** @var \Anuncio\Entity $anuncio */
                        $anuncio = DoctrineStorage::orm()
                            ->getRepository(\Anuncio\Entity::FULL_NAME)
                            ->findOneBy(["id" => $id]);

                        /** @var \Usuario\Entity $usuarioAnuncio */
                        $usuarioAnuncio = DoctrineStorage::orm()
                            ->getRepository(\Usuario\Entity::FULL_NAME)
                            ->findOneBy(["id" => $anuncio->getUsuario()]);

                        $pergunta = new \AnuncioPergunta\Entity();
                        $pergunta->setAnuncio($id);
						$pergunta->setPergunta($_POST['pergunta']);
                        $pergunta->setDatahorapergunta(new \DateTime());

                        if (isset($_POST['usuario']) and !empty($_POST['usuario'])) {
                            /** @var \Usuario\Entity $usuarioLogado */
                            $usuarioLogado = Authentication::getInstance()->getFromSession('entity');

                            $pergunta->setUsuario($usuarioLogado->getId());
                            $_POST['nome'] = ($usuarioLogado->getTipopessoa() == 1) ? $usuarioLogado->getNome() : $usuarioLogado->getRazaosocial();
                            $_POST['email'] = $usuarioLogado->getEmail();
                            $_POST['celular'] = $usuarioLogado->getCelular();
							$_POST['telefone'] = $usuarioLogado->getTelefone1();
                            $_POST['cidade'] = $usuarioLogado->getCidade();
                            $_POST['estado'] = $usuarioLogado->getEstado();
							$pergunta->setEmail('');
                        }else{
							$pergunta->setNome($_POST['nome']);
							$pergunta->setEmail($_POST['email']);
							$pergunta->setCelular($_POST['celular']);
							$pergunta->setTelefone($_POST['telefone']);
							$pergunta->setEstado($_POST['estado']);
							$pergunta->setCidade($_POST['cidade']);
						}

                        DoctrineStorage::orm()->persist($pergunta);
                        DoctrineStorage::orm()->flush();
						
						if($usuarioAnuncio->getTipopessoa() == 1){
							$nomeourazaosocial = $usuarioAnuncio->getNome();
							//echo $nomeourazaosocial; die;
						}else{
							$nomeourazaosocial = $usuarioAnuncio->getRazaosocial();
						}
						/** @var \TextoSite\Entity $texto */
						$texto = DoctrineStorage::orm()->getRepository(\TextoSite\Entity::FULL_NAME)->findOneBy(['local' => 'pergunta-anuncio-comprador']);
	
						$textoEmail = str_replace(
							[
								'%anuncio%',
								'%nomerazao%',
								'%email%',
								'%celular%',
								'%fone%'
							],
							[
								$anuncio->getTitulo(),
								$nomeourazaosocial,
								$usuarioAnuncio->getEmail(),
								$usuarioAnuncio->getCelular(),
								$usuarioAnuncio->getTelefone1()
							],
							$texto->getTexto()
						);
			   
						$app->mailer->send(
							(new Message())->setFrom('contato@seubairrotem.com.br', 'Guia Seu Bairro Tem')
								->addTo($_POST['email'])
								->setSubject($texto->getTitulo()." - ".$anuncio->getTitulo())
								->setHtmlBody($textoEmail)
						);
						
						/** @var \TextoSite\Entity $texto */
						$texto2 = DoctrineStorage::orm()->getRepository(\TextoSite\Entity::FULL_NAME)->findOneBy(['local' => 'pergunta-anuncio-vendedor']);
	
						$textoEmail2 = str_replace(
							[
								'%anuncio%',
								'%nomeusuario%',
								'%email%',
								'%celular%',
								'%telefone%',
								'%cidade%',
								'%estado%',
								'%site%',
								'%id%'
							],
							[
								$anuncio->getTitulo(),
								$_POST['nome'],
								$_POST['email'],
								$_POST['celular'],
								$_POST['telefone'],
								$_POST['cidade'],
								$_POST['estado'],
								url_base(),
								$id
							],
							$texto2->getTexto()
						);
			   
						$app->mailer->send(
							(new Message())->setFrom('contato@seubairrotem.com.br', 'Guia Seu Bairro Tem')
								->addTo($usuarioAnuncio->getEmail())
								->setSubject($texto2->getTitulo()." - ".$anuncio->getTitulo())
								->setHtmlBody($textoEmail2)
						);

                        $app->flash("info-pergunta", "Sua pergunta foi registrada com sucesso! Os dados de contato do vendedor foram enviados para seu e-mail. Caso o e-mail não chegue em sua caixa de entrada, verifique sua caixa de spam.");
						//$app->flash("info-pergunta", "Sua pergunta foi enviada para o vendedor <strong>".(($usuarioAnuncio->getTipopessoa() == 1) ? $usuarioAnuncio->getNome() : $usuarioAnuncio->getRazaosocial())."</strong><br /><br />Os dados para contato direto são:<br />E-mail: {$usuarioAnuncio->getEmail()}<br />Telefone(s): {$usuarioAnuncio->getCelular()} | {$usuarioAnuncio->getTelefone1()}");
                    } catch (\Exception $e) {
                        $app->flash("erro-pergunta", "Houve um erro e sua pergunta não foi enviada.");
                    }
					$app->flashKeep();
					
					//GERA SLUG E SETA A DATA
					$genSlug = create_slug($anuncio->getTitulo());
                    $data = $anuncio->getDatainclusao()->format("Y/m/d");
                    $app->redirect("/anuncio/{$id}/{$data}/{$genSlug}/");
                }
            );

            $app->post("/anuncio-resposta(/:id)/",
                function ($id = null) use ($app) {
                    if (empty($id)) {
                        $app->redirect("/");
                    }

                    try {
                        /** @var \AnuncioPergunta\Entity $pergunta */
                        $pergunta = DoctrineStorage::orm()->getRepository(\AnuncioPergunta\Entity::FULL_NAME)->findOneBy(['id' => $id]);
                        $pergunta->setResposta($_POST['resposta']);
                        $pergunta->setDatahoraresposta(new \DateTime());
						
						/** @var \Anuncio\Entity $anuncio */
                        $anuncio = DoctrineStorage::orm()->getRepository(\Anuncio\Entity::FULL_NAME)->findOneBy(["id" => $pergunta->getAnuncio()]);
							
                        if($pergunta->getUsuario() == ""){
							$emailusuario = $pergunta->getEmail();
							//echo $emailusuario; die;
							//print_r($pergunta); die;
						}else{
							/** @var \Usuario\Entity $usuarioAnuncio */
							$usuarioAnuncio = DoctrineStorage::orm()
								->getRepository(\Usuario\Entity::FULL_NAME)
								->findOneBy(["id" => $pergunta->getUsuario()]);
								
							$emailusuario = $usuarioAnuncio->getEmail();
						}
                        DoctrineStorage::orm()->merge($pergunta);
                        DoctrineStorage::orm()->flush();

						/** @var \TextoSite\Entity $texto */
						$texto = DoctrineStorage::orm()->getRepository(\TextoSite\Entity::FULL_NAME)->findOneBy(['local' => 'resposta-anuncio-comprador']);
	
						$textoEmail = str_replace(
							[
								'%anuncio%',
								'%site%',
								'%id%'
							],
							[
								$anuncio->getTitulo(),
								url_base(),
								$anuncio->getId()
							],
							$texto->getTexto()
						);
			   			
						$mail = new Message();
					    $mail->addTo($emailusuario)
							 ->setFrom('contato@seubairrotem.com.br', 'Guia Seu Bairro Tem')
							 ->setSubject($texto->getTitulo()." - ".$anuncio->getTitulo())
							 ->setHtmlBody($textoEmail);
	
					   	$app->mailer->send($mail);
				        
						$app->flash('info-pergunta', "Sua resposta foi enviada!");

                	} catch (\Exception $e) {
                   		$app->flash('erro-pergunta', "Houve um erro e sua resposta não foi enviada.");
                    }
					//GERA SLUG E SETA A DATA
					$genSlug = create_slug($anuncio->getTitulo());
                    $data = $anuncio->getDatainclusao()->format("Y/m/d");
                    $app->redirect("/anuncio/{$pergunta->getAnuncio()}/{$data}/{$genSlug}/");
                }
            );

            $app->post("/recuperar-senha/", function () use ($app) {
                try {
                   if (!isset($_POST['cpf']) or empty($_POST['cpf'])) {
                       throw new \Exception("CPF ou CNPJ não informado.");
                   }
                   $conn = DoctrineStorage::orm()->getConnection();
                   $usuario = $conn->fetchAll("SELECT id FROM usuario where
                                        usuario.cpf = '{$_POST['cpf']}'
                                        or usuario.cnpj = '{$_POST['cpf']}'");
                   if (count($usuario) < 1) {
                       throw new \Exception("CPF ou CNPJ não cadastrado.");
                   }
                    /** @var \Usuario\Entity $usuario */
                   $usuario = DoctrineStorage::orm()->getRepository(\Usuario\Entity::FULL_NAME)
                       ->findOneBy(['id' => $usuario[0]['id']]);
                   /** @var \TextoSite\Entity $texto */
                   $texto = DoctrineStorage::orm()->getRepository(\TextoSite\Entity::FULL_NAME)
                       ->findOneBy(['local' => 'recuperar-senha']);

                   $textoEmail = str_replace(
                        [
                            '%usuario%',
                            '%senha%'
                        ],
                        [
                            $usuario->getLogin(),
                            $usuario->getSenha()
                        ],
                        $texto->getTexto()
                   );

                   $mail = new Message();
                   $mail->addTo($usuario->getEmail())
                        ->setFrom('contato@seubairrotem.com.br', 'Guia Seu Bairro Tem')
                        ->setSubject($texto->getTitulo())
                        ->setHtmlBody($textoEmail);

                   $app->mailer->send($mail);

                   $app->flash('info', "Recuperação enviada com sucesso para o email {$usuario->getEmail()}");


                } catch (\Exception $e) {
                   $app->flash('info', "Houve uma falha na recuperação. {$e->getMessage()}");
                }
                $app->redirect("/");
            });

            $app->get("/anuncio-todas-respostas(/:id(/:ano(/:mes(/:dia(/:slug)))))/",
                function ($id = null, $ano = null, $mes = null, $dia = null, $slug = null) use ($app) {
                    
                    if (empty($id)) {
                        $app->redirect("/");
                    }

                    /** @var \Anuncio\Entity $anuncio */
                    $anuncio = DoctrineStorage::orm()->getRepository("Anuncio\\Entity")->findOneBy(["id" => $id]);
                    $genSlug = create_slug($anuncio->getTitulo());

                    $data = explode("/", $anuncio->getDatainclusao()->format("Y/m/d"));

                    if (empty($ano) or empty($mes) or empty($dia)) {
                        $ano = $data[0];
                        $mes = $data[1];
                        $dia = $data[2];
                        $app->redirect("/anuncio-todas-respostas/{$id}/{$ano}/{$mes}/{$dia}/{$genSlug}/");
                    }

                    if ($ano != $data[0] or $mes != $data[1] or $dia != $data[2]) {
                        $ano = $data[0];
                        $mes = $data[1];
                        $dia = $data[2];
                        $app->redirect("/anuncio-todas-respostas/{$id}/{$ano}/{$mes}/{$dia}/{$genSlug}/");
                    }

                    if (empty($slug) or $genSlug != $slug) {
                        $app->redirect("/anuncio-todas-respostas/{$id}/{$ano}/{$mes}/{$dia}/{$genSlug}/");
                    }

                    $app->render(
                        __NAMESPACE__ . "/Views/anuncio-todas-respostas.php",
                        [
                            "getAnuncio" => $anuncio
                        ]
                    );
                }
            );


            $app->get("/imprimir-anuncio(/:id(/:ano(/:mes(/:dia(/:slug)))))/",
                function ($id = null, $ano = null, $mes = null, $dia = null, $slug = null) use ($app) {
                    if (empty($id)) {
                        $app->redirect("/");
                    }


                    /** @var \Anuncio\Entity $anuncio */
                    $anuncio = DoctrineStorage::orm()->getRepository("Anuncio\\Entity")->findOneBy(["id" => $id, 'status' => 1]);
                    $genSlug = create_slug($anuncio->getTitulo());

                    $data = explode("/", $anuncio->getDatainclusao()->format("Y/m/d"));

                    if (empty($ano) or empty($mes) or empty($dia)) {
                        $ano = $data[0];
                        $mes = $data[1];
                        $dia = $data[2];
                        $app->redirect("/imprimir-anuncio/{$id}/{$ano}/{$mes}/{$dia}/{$genSlug}/");
                    }

                    if ($ano != $data[0] or $mes != $data[1] or $dia != $data[2]) {
                        $ano = $data[0];
                        $mes = $data[1];
                        $dia = $data[2];
                        $app->redirect("/imprimir-anuncio/{$id}/{$ano}/{$mes}/{$dia}/{$genSlug}/");
                    }

                    if (empty($slug) or $genSlug != $slug) {
                        $app->redirect("/imprimir-anuncio/{$id}/{$ano}/{$mes}/{$dia}/{$genSlug}/");
                    }

                    $app->render(
                        __NAMESPACE__ . "/Views/anuncio-imprimir.php",
                        [
                            "getAnuncio" => $anuncio
                        ]
                    );
                }
            );

            $app->get(
                '/guia-comercial/subcategorias/:categoria(/:subcategoria(/:seed(/pagina(/:pagina))))/',
                function ($categoria, $subcategoria = null, $seed = null, $pagina = 1) use ($app, $controller) {
                    if (empty($subcategoria)) {
                        $app->redirect("/guia-comercial/{$categoria}");
                    }
                    switch(true) {
                        case $seed == "maior-preco":
                            $order = " a.precoavista DESC, a.niveldestaque3 DESC, rand() ";
                            break;

                        case $seed == "menor-preco":
                            $order = " a.precoavista ASC, a.niveldestaque3 DESC, rand() ";
                            break;

                        case $seed == "cidade":
                            $order = " cid.nome ASC, a.niveldestaque3 DESC, rand() ";
                            break;

                        case $seed == "estado":
                            $order = " a.estado ASC, a.niveldestaque3 DESC, rand() ";
                            break;

                        case (empty($seed)):
                            srand(make_seed());
                            $seed = rand();
                            //$order = " RAND({$seed}) ";
                            $order = " a.niveldestaque3 DESC, rand() ";
                            break;

                        default:
                            //$order = " RAND({$seed}) ";
                            $order = " a.niveldestaque3 DESC, rand() ";
                            break;
                    }



                    $categoriaId = ResourceManager::readFile("guia-comercial.json")[$categoria]["id"];
                    if (empty($categoriaId)) {
                        $app->redirect("/");
                    }
                    $subcategoriaEntity = DoctrineStorage::orm()
                        ->getRepository(\Subcategoria\Entity::FULL_NAME)
                        ->findOneBy(["categoria" => $categoriaId, "slug" => $subcategoria]);

                    $conn = \CoffeeCore\Storage\DoctrineStorage::orm()->getConnection();
                    $sql = "SELECT a.* FROM anuncio a
                                LEFT JOIN faturaplano fp ON a.id = fp.anuncio
								LEFT JOIN cidade cid ON a.cidade = cid.id
                                WHERE
                                DATE_FORMAT(a.datavalidade,'%Y-%m-%d') = fp.datavencimento AND
                                a.status = 1 AND
                                a.categoria = {$categoriaId} AND
                                a.subcategoria = {$subcategoriaEntity->getId()} AND
                                a.datavalidade >= CURDATE() AND
                                fp.fatura IN (SELECT id FROM fatura WHERE ((codigotransacao='GRATUITO' OR codigotransacao='') AND (statuspagseguro=0 OR statuspagseguro=''))
                                OR (codigotransacao!='GRATUITO' AND codigotransacao!='' AND (fp.valor=0 OR (fp.valor > 0 AND (statuspagseguro=3 OR statuspagseguro=4) ) ) ) )
                                ORDER BY {$order} LIMIT " . ($pagina -1) * 25 .  ",25";
                    $registros = $conn->fetchAll($sql);

                    $app->render(
                        __NAMESPACE__ . "/Views/subcategorias.php",
                        [
                            "getTipo" => ['id' => 1, "titulo" => "classificados"],
                            "registros" => $registros,
                            "getCategoria" => $categoria,
                            "getSubcategoria" => $subcategoriaEntity,
                            "getSeed" => $seed,
                            "order" => $order,
                            "dados" => [
                                "totalRegistros" => count(
                                    $conn->fetchAll("SELECT a.* FROM anuncio a
                                                        LEFT JOIN faturaplano fp ON a.id = fp.anuncio
														LEFT JOIN cidade cid ON a.cidade = cid.id
                                                        WHERE
                                                        DATE_FORMAT(a.datavalidade,'%Y-%m-%d') = fp.datavencimento AND
                                                        a.status = 1 AND
                                                        a.categoria = {$categoriaId} AND
                                                        a.subcategoria = {$subcategoriaEntity->getId()} AND
                                                        a.datavalidade >= CURDATE() AND
                                                        fp.fatura IN (SELECT id FROM fatura WHERE ((codigotransacao='GRATUITO' OR codigotransacao='') AND (statuspagseguro=0 OR statuspagseguro=''))
                                                        OR (codigotransacao!='GRATUITO' AND codigotransacao!='' AND (fp.valor=0 OR (fp.valor > 0 AND (statuspagseguro=3 OR statuspagseguro=4) ) ) ) )")
                                ),
                                "pagina" => $pagina
                            ]
                        ]
                    );
                }
            );

            $app->get(
                '/guia-comercial/:categoria(/:seed(/pagina(/:pagina)))/',
                function ($categoria, $seed = null, $pagina = 1) use ($app, $controller) {

                    switch(true) {
                        case $seed == "maior-preco":
                            $order = " a.precoavista DESC, a.niveldestaque3 DESC, rand() ";
                            break;

                        case $seed == "menor-preco":
                            $order = " a.precoavista ASC, a.niveldestaque3 DESC, rand() ";
                            break;

                        case $seed == "cidade":
                            $order = " cid.nome ASC, a.niveldestaque3 DESC, rand() ";
                            break;

                        case $seed == "estado":
                            $order = " a.estado ASC, a.niveldestaque3 DESC, rand() ";
                            break;

                        case (empty($seed)):
                            srand(make_seed());
                            $seed = rand();
                            //$order = " RAND({$seed}) ";
                            $order = " a.niveldestaque3 DESC, rand() ";
                            break;

                        default:
                            //$order = " RAND({$seed}) ";
                            $order = " a.niveldestaque3 DESC, rand() ";
                            break;
                    }

                    $categoriaId = ResourceManager::readFile("guia-comercial.json")[$categoria]["id"];
                    if (empty($categoriaId)) {
                        $app->redirect("/");
                    }

                    $conn = \CoffeeCore\Storage\DoctrineStorage::orm()->getConnection();
                    $sql = "SELECT a.* FROM anuncio a
                                LEFT JOIN faturaplano fp ON a.id = fp.anuncio
								LEFT JOIN cidade cid ON a.cidade = cid.id
                                WHERE
                                DATE_FORMAT(a.datavalidade,'%Y-%m-%d') = fp.datavencimento AND
                                a.status = 1 AND
                                a.categoria = {$categoriaId} AND
                                a.datavalidade >= CURDATE() AND
                                fp.fatura IN (SELECT id FROM fatura WHERE ((codigotransacao='GRATUITO' OR codigotransacao='') AND (statuspagseguro=0 OR statuspagseguro=''))
                                OR (codigotransacao!='GRATUITO' AND codigotransacao!='' AND (fp.valor=0 OR (fp.valor > 0 AND (statuspagseguro=3 OR statuspagseguro=4) ) ) ) )
                                ORDER BY {$order} LIMIT " . ($pagina -1) * 25 .  ",25";

                    $registros = $conn->fetchAll($sql);

                    $app->render(
                        __NAMESPACE__ . "/Views/categorias.php",
                        [
                            "getTipo" => ['id' => 1, "titulo" => "classificados"],
                            "registros" => $registros,
                            "getCategoria" => $categoria,
                            "getSeed" => $seed,
                            "dados" => [
                                "totalRegistros" => count(
                                    $conn->fetchAll("SELECT a.* FROM anuncio a
                                                        LEFT JOIN faturaplano fp ON a.id = fp.anuncio
														LEFT JOIN cidade cid ON a.cidade = cid.id
                                                        WHERE
                                                        DATE_FORMAT(a.datavalidade,'%Y-%m-%d') = fp.datavencimento AND
                                                        a.status = 1 AND
                                                        a.categoria = {$categoriaId} AND
                                                        a.datavalidade >= CURDATE() AND
                                                        fp.fatura IN (SELECT id FROM fatura WHERE ((codigotransacao='GRATUITO' OR codigotransacao='') AND (statuspagseguro=0 OR statuspagseguro=''))
                                                        OR (codigotransacao!='GRATUITO' AND codigotransacao!='' AND (fp.valor=0 OR (fp.valor > 0 AND (statuspagseguro=3 OR statuspagseguro=4) ) ) ) )")
                                ),
                                "pagina" => $pagina
                            ]
                        ]
                    );
                }
            );

            $app->get(
                '/classificados/subcategorias/:categoria(/:subcategoria(/:seed(/pagina(/:pagina))))/',
                function ($categoria, $subcategoria = null, $seed = null, $pagina = 1) use ($app, $controller) {
                    if (empty($subcategoria)) {
                        $app->redirect("/classificados/{$categoria}");
                    }
                    switch(true) {
                        case $seed == "maior-preco":
                            $order = " a.precoavista DESC, a.niveldestaque3 DESC, rand() ";
                            break;

                        case $seed == "menor-preco":
                            $order = " a.precoavista ASC, a.niveldestaque3 DESC, rand() ";
                            break;

                        case $seed == "cidade":
                            $order = " cid.nome ASC, a.niveldestaque3 DESC, rand() ";
                            break;

                        case $seed == "estado":
                            $order = " a.estado ASC, a.niveldestaque3 DESC, rand() ";
                            break;

                        case (empty($seed)):
                            srand(make_seed());
                            $seed = rand();
                            //$order = " RAND({$seed}) ";
                            $order = " a.niveldestaque3 DESC, rand() ";
                            break;

                        default:
                            //$order = " RAND({$seed}) ";
                            $order = " a.niveldestaque3 DESC, rand() ";
                            break;
                    }



                    $categoriaId = ResourceManager::readFile("classificados.json")[$categoria]["id"];
                    if (empty($categoriaId)) {
                        $app->redirect("/");
                    }

                    $subcategoriaEntity = DoctrineStorage::orm()
                        ->getRepository(\Subcategoria\Entity::FULL_NAME)
                        ->findOneBy(["categoria" => $categoriaId, "slug" => $subcategoria]);

                    $conn = \CoffeeCore\Storage\DoctrineStorage::orm()->getConnection();
                    $sql = "SELECT a.* FROM anuncio a
                                LEFT JOIN faturaplano fp ON a.id = fp.anuncio
								LEFT JOIN cidade cid ON a.cidade = cid.id
                                WHERE
                                DATE_FORMAT(a.datavalidade,'%Y-%m-%d') = fp.datavencimento AND
                                a.status = 1 AND
                                a.categoria = {$categoriaId} AND
                                a.subcategoria = {$subcategoriaEntity->getId()} AND
                                a.datavalidade >= CURDATE() AND
                                fp.fatura IN (SELECT id FROM fatura WHERE ((codigotransacao='GRATUITO' OR codigotransacao='') AND (statuspagseguro=0 OR statuspagseguro=''))
                                OR (codigotransacao!='GRATUITO' AND codigotransacao!='' AND (fp.valor=0 OR (fp.valor > 0 AND (statuspagseguro=3 OR statuspagseguro=4) ) ) ) )
                                ORDER BY {$order} LIMIT " . ($pagina -1) * 25 .  ",25";
                    $registros = $conn->fetchAll($sql);

                    $app->render(
                        __NAMESPACE__ . "/Views/subcategorias.php",
                        [
                            "getTipo" => ['id' => 2, "titulo" => "servicos"],
                            "registros" => $registros,
                            "getCategoria" => $categoria,
                            "getSubcategoria" => $subcategoriaEntity,
                            "getSeed" => $seed,
                            "order" => $order,
                            "dados" => [
                                "totalRegistros" => count(
                                    $conn->fetchAll("SELECT a.* FROM anuncio a
                                                        LEFT JOIN faturaplano fp ON a.id = fp.anuncio
														LEFT JOIN cidade cid ON a.cidade = cid.id
                                                        WHERE
                                                        DATE_FORMAT(a.datavalidade,'%Y-%m-%d') = fp.datavencimento AND
                                                        a.status = 1 AND
                                                        a.categoria = {$categoriaId} AND
                                                        a.subcategoria = {$subcategoriaEntity->getId()} AND
                                                        a.datavalidade >= CURDATE() AND
                                                        fp.fatura IN (SELECT id FROM fatura WHERE ((codigotransacao='GRATUITO' OR codigotransacao='') AND (statuspagseguro=0 OR statuspagseguro=''))
                                                        OR (codigotransacao!='GRATUITO' AND codigotransacao!='' AND (fp.valor=0 OR (fp.valor > 0 AND (statuspagseguro=3 OR statuspagseguro=4) ) ) ) )")
                                ),
                                "pagina" => $pagina
                            ]
                        ]
                    );
                }
            );

            $app->get(
                '/classificados/:categoria(/:seed(/pagina(/:pagina)))/',
                function ($categoria, $seed = null, $pagina = 1) use ($app, $controller) {

                    switch(true) {
                        case $seed == "maior-preco":
                            $order = " a.precoavista DESC, a.niveldestaque3 DESC, rand() ";
                            break;

                        case $seed == "menor-preco":
                            $order = " a.precoavista ASC, a.niveldestaque3 DESC, rand() ";
                            break;

                        case $seed == "cidade":
                            $order = " cid.nome ASC, a.niveldestaque3 DESC, rand() ";
                            break;

                        case $seed == "estado":
                            $order = " a.estado ASC, a.niveldestaque3 DESC, rand() ";
                            break;

                        case (empty($seed)):
                            srand(make_seed());
                            $seed = rand();
                            //$order = " RAND({$seed}) ";
                            $order = " a.niveldestaque3 DESC, rand() ";
                            break;

                        default:
                            //$order = " RAND({$seed}) ";
                            $order = " a.niveldestaque3 DESC, rand() ";
                            break;
                    }

                    $categoriaId = ResourceManager::readFile("classificados.json")[$categoria]["id"];
                    if (empty($categoriaId)) {
                        $app->redirect("/");
                    }

                    $conn = \CoffeeCore\Storage\DoctrineStorage::orm()->getConnection();
                    $sql = "SELECT a.* FROM anuncio a
                                LEFT JOIN faturaplano fp ON a.id = fp.anuncio
								LEFT JOIN cidade cid ON a.cidade = cid.id
                                WHERE
                                DATE_FORMAT(a.datavalidade,'%Y-%m-%d') = fp.datavencimento AND
                                a.status = 1 AND
                                a.categoria = {$categoriaId} AND
                                a.datavalidade >= CURDATE() AND
                                fp.fatura IN (SELECT id FROM fatura WHERE ((codigotransacao='GRATUITO' OR codigotransacao='') AND (statuspagseguro=0 OR statuspagseguro=''))
                                OR (codigotransacao!='GRATUITO' AND codigotransacao!='' AND (fp.valor=0 OR (fp.valor > 0 AND (statuspagseguro=3 OR statuspagseguro=4) ) ) ) )
                                ORDER BY {$order} LIMIT " . ($pagina -1) * 25 .  ",25";

                    $registros = $conn->fetchAll($sql);

                    $app->render(
                        __NAMESPACE__ . "/Views/categorias.php",
                        [
                            "getTipo" => ['id' => 2, "titulo" => "servicos"],
                            "registros" => $registros,
                            "getCategoria" => $categoria,
                            "getSeed" => $seed,
                            "dados" => [
                                "totalRegistros" => count(
                                    $conn->fetchAll("SELECT a.* FROM anuncio a
                                                        LEFT JOIN faturaplano fp ON a.id = fp.anuncio
														LEFT JOIN cidade cid ON a.cidade = cid.id
                                                        WHERE
                                                        DATE_FORMAT(a.datavalidade,'%Y-%m-%d') = fp.datavencimento AND
                                                        a.status = 1 AND
                                                        a.categoria = {$categoriaId} AND
                                                        a.datavalidade >= CURDATE() AND
                                                        fp.fatura IN (SELECT id FROM fatura WHERE ((codigotransacao='GRATUITO' OR codigotransacao='') AND (statuspagseguro=0 OR statuspagseguro=''))
                                                        OR (codigotransacao!='GRATUITO' AND codigotransacao!='' AND (fp.valor=0 OR (fp.valor > 0 AND (statuspagseguro=3 OR statuspagseguro=4) ) ) ) )")
                                ),
                                "pagina" => $pagina
                            ]
                        ]
                    );
                }
            );

            $app->get('/usuario/:usuario(/:seed(/pagina(/:pagina)))/',
                function ($usuario, $seed = 'titulo',  $pagina = 1) use ($app, $controller) {
                    switch(true) {
                        case $seed == "maior-preco":
                            $order = " a.precoavista DESC, a.niveldestaque3 DESC, rand() ";
                            break;

                        case $seed == "menor-preco":
                            $order = " a.precoavista ASC, a.niveldestaque3 DESC, rand() ";
                            break;

                        case $seed == "cidade":
                            $order = " cid.nome ASC, a.niveldestaque3 DESC, rand() ";
                            break;

                        case $seed == "estado":
                            $order = " a.estado ASC, a.niveldestaque3 DESC, rand() ";
                            break;

                        default:
                            $seed = "titulo";
                            $order = " a.niveldestaque3 DESC, rand() ";
                            break;
                    }
                    /** @var \Usuario\Entity $usuarioEntity */
                    $usuarioEntity = DoctrineStorage::orm()
                                        ->getRepository(\Usuario\Entity::FULL_NAME)
                                        ->findOneBy(["login" => $usuario]);
					if($usuarioEntity->getTipopessoa() == 1){
						$usuarioNome = $usuarioEntity->getNome();
					}else{
						$usuarioNome = $usuarioEntity->getNomefantasia();
					}

                    $conn = \CoffeeCore\Storage\DoctrineStorage::orm()->getConnection();
                    $sql = "SELECT a.* FROM anuncio a
                                LEFT JOIN faturaplano fp ON a.id = fp.anuncio
								LEFT JOIN cidade cid ON a.cidade = cid.id
                                WHERE
                                DATE_FORMAT(a.datavalidade,'%Y-%m-%d') = fp.datavencimento AND
                                a.status = 1 AND
                                a.datavalidade >= CURDATE() AND
                                a.usuario = {$usuarioEntity->getId()} AND
                                fp.fatura IN (SELECT id FROM fatura WHERE ((codigotransacao='GRATUITO' OR codigotransacao='') AND (statuspagseguro=0 OR statuspagseguro=''))
                                OR (codigotransacao!='GRATUITO' AND codigotransacao!='' AND (fp.valor=0 OR (fp.valor > 0 AND (statuspagseguro=3 OR statuspagseguro=4) ) ) ) )
                                ORDER BY {$order} LIMIT " . ($pagina -1) * 25 .  ",25";
                    $registros = $conn->fetchAll($sql);

                    $app->render(
                        __NAMESPACE__ . "/Views/usuario.php",
                        [
                            "getBusca" => $usuario,
							"getNome" => $usuarioNome,
                            "registros" => $registros,
                            "getSeed" => $seed,
                            "dados" => [
                                "totalRegistros" => count(
                                    $conn->fetchAll("SELECT a.* FROM anuncio a
                                                        LEFT JOIN faturaplano fp ON a.id = fp.anuncio
														LEFT JOIN cidade cid ON a.cidade = cid.id
                                                        WHERE
                                                        DATE_FORMAT(a.datavalidade,'%Y-%m-%d') = fp.datavencimento AND
                                                        a.status = 1 AND
                                                        a.datavalidade >= CURDATE() AND
                                                        a.usuario = {$usuarioEntity->getId()} AND
                                                        fp.fatura IN (SELECT id FROM fatura WHERE ((codigotransacao='GRATUITO' OR codigotransacao='') AND (statuspagseguro=0 OR statuspagseguro=''))
                                                        OR (codigotransacao!='GRATUITO' AND codigotransacao!='' AND (fp.valor=0 OR (fp.valor > 0 AND (statuspagseguro=3 OR statuspagseguro=4) ) ) ) )")
                                ),
                                "pagina" => $pagina
                            ]
                        ]
                    );
                }
            );

            $app->get('/buscar/:busca(/:seed(/pagina(/:pagina)))/',
                function ($busca, $seed = 'titulo',  $pagina = 1) use ($app, $controller) {
                    switch(true) {
                        case $seed == "maior-preco":
                            $order = " a.precoavista DESC, a.niveldestaque3 DESC, rand() ";
                            break;

                        case $seed == "menor-preco":
                            $order = " a.precoavista ASC, a.niveldestaque3 DESC, rand() ";
                            break;

                        case $seed == "cidade":
                            $order = " cid.nome ASC, a.niveldestaque3 DESC, rand() ";
                            break;

                        case $seed == "estado":
                            $order = " a.estado ASC, a.niveldestaque3 DESC, rand() ";
                            break;

                        default:
                            $seed = "titulo";
                            $order = " a.niveldestaque3 DESC, rand() ";
                            break;
                    }
                    /*
                     * switch(true) {
                        case $seed == "maior-preco":
                            $order = " a.precoavista DESC, a.titulo ASC ";
                            break;

                        case $seed == "menor-preco":
                            $order = " a.precoavista ASC, a.titulo ASC ";
                            break;

                        case $seed == "cidade":
                            $order = " cid.nome ASC, a.titulo ASC ";
                            break;

                        case $seed == "estado":
                            $order = " a.estado ASC, a.titulo ASC ";
                            break;

                        default:
                            $seed = "titulo";
                            $order = " a.titulo ASC ";
                            break;
                    }*/
                    $buscaSersch = explode(' ', $busca);

                    $temp = $buscaSersch;
                    foreach ($temp as $k => $val) {
                        if (strlen($val) < 4) {
                            unset($buscaSersch[$k]);
                        }
                    }


                    $buscaSerschTitulo = implode("%' or a.titulo like '%", $buscaSersch);
                    $buscaSerschCidade = implode("%' or cid.nome like '%", $buscaSersch);

                    $conn = \CoffeeCore\Storage\DoctrineStorage::orm()->getConnection();
                    $sql = "SELECT a.* FROM anuncio a
                                LEFT JOIN faturaplano fp ON a.id = fp.anuncio
								LEFT JOIN cidade cid ON a.cidade = cid.id
                                WHERE
                                DATE_FORMAT(a.datavalidade,'%Y-%m-%d') = fp.datavencimento AND
                                a.status = 1 AND
                                (a.titulo LIKE '%{$buscaSerschTitulo}%' OR
                                cid.nome LIKE '%{$buscaSerschCidade}%') AND
                                a.datavalidade >= CURDATE() AND
                                fp.fatura IN (SELECT id FROM fatura WHERE ((codigotransacao='GRATUITO' OR codigotransacao='') AND (statuspagseguro=0 OR statuspagseguro=''))
                                OR (codigotransacao!='GRATUITO' AND codigotransacao!='' AND (fp.valor=0 OR (fp.valor > 0 AND (statuspagseguro=3 OR statuspagseguro=4) ) ) ) )
                                ORDER BY {$order} LIMIT " . ($pagina -1) * 25 .  ",25";
                    $registros = $conn->fetchAll($sql);

                    $app->render(
                        __NAMESPACE__ . "/Views/busca.php",
                        [
                            "getBusca" => $busca,
                            "registros" => $registros,
                            "getSeed" => $seed,
                            "dados" => [
                                "totalRegistros" => count(
                                    $conn->fetchAll("SELECT a.* FROM anuncio a
                                                        LEFT JOIN faturaplano fp ON a.id = fp.anuncio
														LEFT JOIN cidade cid ON a.cidade = cid.id
                                                        WHERE
                                                        DATE_FORMAT(a.datavalidade,'%Y-%m-%d') = fp.datavencimento AND
                                                        a.status = 1 AND
                                                        (a.titulo LIKE '%{$buscaSerschTitulo}%' OR
                                                        cid.nome LIKE '%{$buscaSerschCidade}%') AND
                                                        a.datavalidade >= CURDATE() AND
                                                        fp.fatura IN (SELECT id FROM fatura WHERE ((codigotransacao='GRATUITO' OR codigotransacao='') AND (statuspagseguro=0 OR statuspagseguro=''))
                                                        OR (codigotransacao!='GRATUITO' AND codigotransacao!='' AND (fp.valor=0 OR (fp.valor > 0 AND (statuspagseguro=3 OR statuspagseguro=4) ) ) ) )")
                                ),
                                "pagina" => $pagina
                            ]
                        ]
                    );
                }
            );

            $app->get('/sobre-nos/', function () use ($app) {
                $app->render(__NAMESPACE__ . "/Views/sobre.php");
            });

            $app->map('/duvidas/', function () use ($app) {

                $duvidas = [];

                if ($app->request()->isGet()) {
                    $duvidas = \CoffeeCore\Storage\DoctrineStorage::orm()
                        ->getRepository(\Duvidas\Entity::FULL_NAME)
                        ->findBy([], ["sortorder" => "ASC"]);

                }

                if ($app->request()->isPost()) {
                    $_POST['duvida'] = str_replace(' ', '%', $_POST["duvida"]);
                    $duvidas = \CoffeeCore\Storage\DoctrineStorage::orm()
                        ->getRepository(\Duvidas\Entity::FULL_NAME)
                        ->createQueryBuilder("a")->where("a.pergunta LIKE :pergunta")
                        ->setParameter("pergunta", '%'.$_POST["duvida"].'%')
                        ->orderBy("a.sortorder", "asc")
                        ->getQuery()
                        ->getResult();
                }

                $app->render(
                    __NAMESPACE__ . "/Views/duvidas.php",
                    [
                        "duvidas" => $duvidas
                    ]
                );
            })->via("GET", "POST");

            $app->map('/contato/', function () use ($app) {
                if ($app->request()->isGet()) {
                    $app->render(__NAMESPACE__ . "/Views/contato.php");
                }
                if ($app->request()->isPost()) {
                    try {
                        /** @var \Departamentos\Entity $departamento */
                        $departamento = DoctrineStorage::orm()->getRepository(\Departamentos\Entity::FULL_NAME)->findOneBy(['id' => $_POST["departamento"]]);

                        $message = new Message();
                        $message->addTo($departamento->getEmail())
                            ->setSubject("Contato pelo site [seubairrotem.com.br]")
                            ->setFrom('contato@seubairrotem.com.br', 'Guia Seu Bairro Tem')
							->addReplyTo($_POST["email"], $_POST["nome"])
                            ->setHtmlBody("<strong>Contato pelo site [seubairrotem.com.br]</strong>
									<br /><br>
                                    <strong>Nome:</strong> ".$_POST["nome"]."
									<br />
									<strong>Email:</strong> ".$_POST["email"]."
									<br />
                                    <strong>Telefone:</strong> ".$_POST["telefone"]."
									<br />
									<strong>Departamento:</strong> ".$departamento->getTitulo()."
									<br /><hr>
									<strong>Mensagem:</strong>
									<br />
									".str_replace("\r","<br />",$_POST["mensagem"])."");
                        $app->mailer->send($message);

                        $app->flash("success", "Mensagem Enviada com sucesso.");
                    } catch (\Exception $e) {
                        $app->flash("error", "Houve um erro ao enviar sua mensagem. ". $e->getMessage());
                        $app->redirect("/contato/");
                    }
                    $app->redirect("/contato/");
                }
            })->via("GET", "POST");

            $app->get("/cidades/:id/", function ($id) {
                $data = [];
                $cities = DoctrineStorage::orm()->getRepository(\TodasCidades\Entity::FULL_NAME)->findBy(["id_uf" => $id]);
                /** @var \TodasCidades\Entity $city */
                foreach ($cities as $city) {
                    $data[] = [
                        "id"    => $city->getId(),
                        "title" => $city->getNome()
                    ];
                }

                //header('Content-Type: application/json');
                echo json_encode($data);
            });

            $app->get("/api/categorias/:tipo/", function ($tipo) {
                $data = [];
                $categorias = DoctrineStorage::orm()
                    ->getRepository(\Categoria\Entity::FULL_NAME)
                    ->findBy(["tipo" => $tipo], ["titulo" => "ASC"]);
                /** @var \Categoria\Entity $categoria */
                foreach ($categorias as $categoria) {
                    $data[] = [
                        "id"    => $categoria->getId(),
                        "title" => $categoria->getTitulo()
                    ];
                }

                header('Content-Type: application/json');
                echo json_encode($data);
            });

            $app->get("/api/subcategorias/:categoria/", function ($categoria) {
                $data = [];
                $subcategorias = DoctrineStorage::orm()
                    ->getRepository(\Subcategoria\Entity::FULL_NAME)
                    ->findBy(["categoria" => $categoria], ["titulo" => "ASC"]);

                /** @var \Subcategoria\Entity $subcategoria */
                foreach ($subcategorias as $subcategoria) {
                    $data[] = [
                        "id"    => $subcategoria->getId(),
                        "title" => $subcategoria->getTitulo()
                    ];
                }

                header('Content-Type: application/json');
                echo json_encode($data);
            });

        });

        $app->map('/retorno-pagseguro/', function() use ($app) {

            if (isset($_POST)) {
                $type = $_POST['notificationType'];
                $code = $_POST['notificationCode'];

                $code = isset($_GET['transact']) ? $_GET['transact'] : $code;

                \PagSeguroLibrary::init();
                \PagSeguroConfig::init();
                \PagSeguroResources::init();

                /** @var \PagSeguroAccountCredentials $credenciais */
                $credenciais = \PagSeguroConfig::getAccountCredentials();

                $url = "https://ws.pagseguro.uol.com.br/v2/transactions/notifications/{$code}?email={$credenciais->getEmail()}&token={$credenciais->getToken()}";

                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,false);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $transactionCurl = curl_exec($curl);
                curl_close($curl);
                $transaction = simplexml_load_string($transactionCurl);

                /** @var \Fatura\Entity $fatura */
                $fatura = DoctrineStorage::orm()->getRepository(\Fatura\Entity::FULL_NAME)->findOneBy(['id' => $transaction->reference]);
                $fatura->setStatuspagseguro($transaction->status);
                $fatura->setDataautorizacao(new \DateTime($transaction->lastEventDate));
                DoctrineStorage::orm()->merge($fatura);
                DoctrineStorage::orm()->flush();

                if ($transaction->status == 3 or $transaction->status == 4) {
                    \Site\Controller::liberarPlano($transaction->reference);
                    \Site\Controller::liberarDestaque($transaction->reference);
                }

                $today = date('Y_m_d_H_i_s');
                //new \DateTime();
                //$today = $hoje->format("Y_m_d");
                $file = fopen("LogPagSeguro.$today.txt", "ab");
                $hour = date("H:i:s T");
                fwrite($file,"Log de Notificações e consulta\r\n");
                fwrite($file,"Hora da consulta: $hour \r\n");
                fwrite($file,"HTTP: ".$http['http_code']." \r\n");
                fwrite($file,"Código de Notificação:".$code." \r\n");
                fwrite($file, "Código da transação:".$transaction->code."\r\n");
                fwrite($file, "Status da transação:".$transaction->status."\r\n");
                fwrite($file,"______________________________________________________________________________ \r\n");
                fclose($file);
            }

        })->via('GET', 'POST');


        $app->get('/pagina-nao-encontrada/', function () use ($app) {
            $app->render(__NAMESPACE__ . "/Views/404.php");
        });

        $app->notFound(function() use ($app) {
            $app->redirect('/pagina-nao-encontrada/');
        });
    }
}
