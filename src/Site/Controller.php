<?php

namespace Site;

use CoffeeCore\Storage\DoctrineStorage;
use Exception;
use Intranet\Authentication;
use UsuariosDoSite\Entity;

/**
 * Class Controller
 * @package Site
 */
class Controller extends \CoffeeCore\Core\AbstractController
{

    /**
     * @param integer $fatura
     */
    public static function liberarPlano($fatura)
    {
        if (!empty($fatura)) {
            /** @var \FaturaPlano\Entity $faturaPlano */
            $faturaPlano = DoctrineStorage::orm()->getRepository(\FaturaPlano\Entity::FULL_NAME)->findOneBy(['fatura' =>$fatura]);
            if (!empty($faturaPlano)) {
                /** @var \Anuncio\Entity $anuncio */
                $anuncio = DoctrineStorage::orm()->getRepository(\Anuncio\Entity::FULL_NAME)->findOneBy(['id' => $faturaPlano->getAnuncio()]);
                $anuncio->setDataalteracao(new \DateTime());
                $anuncio->setDatavalidade((new \DateTime())->add(new \DateInterval("P{$faturaPlano->getDiascontratados()}D")));
                DoctrineStorage::orm()->merge($anuncio);

                $faturaPlano->setDatainicio($anuncio->getDataalteracao());
                $faturaPlano->setDatavencimento($anuncio->getDatavalidade());
                DoctrineStorage::orm()->merge($faturaPlano);

                DoctrineStorage::orm()->flush();
            }
        }
    }

    /**
     * @param integer $fatura
     */
    public static function liberarDestaque($fatura)
    {
        if (!empty($fatura)) {
            /** @var \FaturaDestaque\Entity $faturaDestaque */
            $faturaDestaques = DoctrineStorage::orm()->getRepository(\FaturaDestaque\Entity::FULL_NAME)->findBy(['fatura' =>$fatura]);
            if (!empty($faturaDestaques)) {
                $anunciosDestaques = [];
                foreach ($faturaDestaques as $faturaDestaque) {
                    /** @var \AnuncioDestaque\Entity $anuncioDestaqueconsulta */
                    $anuncioDestaqueconsulta = DoctrineStorage::orm()->getRepository(\AnuncioDestaque\Entity::FULL_NAME)->findOneBy(['anuncio' => $faturaDestaque->getAnuncio(), 'fatura' => $faturaDestaque->getFatura(), 'destaque' => $faturaDestaque->getDestaque()], ['id' => 'DESC']);

                    if(empty($anuncioDestaqueconsulta)) {
                        $conn = \CoffeeCore\Storage\DoctrineStorage::orm()->getConnection();
                        $insertSql = "INSERT INTO anunciodestaque VALUES ('', " . $faturaDestaque->getAnuncio() . ", " . $faturaDestaque->getDestaque() . ", NOW(), '" . (new \DateTime())->add(new \DateInterval("P{$faturaDestaque->getDiascontratados()}D"))->format('Y-m-d') . "', $fatura, " . $faturaDestaque->getDiascontratados() . ")";
                        $conn->executeQuery($insertSql);

                        /** @var \AnuncioDestaque\Entity $anuncioDestaque */

                        /*echo "Anuncio: ".$faturaDestaque->getAnuncio();
                        echo "Destaque: ".$faturaDestaque->getDestaque();
                        //echo "Data Início: ".new \DateTime()->format('Y-m-d');
                        echo "Data Vencimento: ".(new \DateTime())->add(new \DateInterval("P{$faturaDestaque->getDiascontratados()}D"))->format('Y-m-d');
                        echo "Fatura: ".$fatura;
                        echo "Dias Contratados: ".$faturaDestaque->getDiascontratados();

                        $anuncioDestaque->setAnuncio($faturaDestaque->getAnuncio());
                        $anuncioDestaque->setDestaque($faturaDestaque->getDestaque());
                        $anuncioDestaque->setDatainicio(new \DateTime());
                        $anuncioDestaque->setDatavencimento((new \DateTime())->add(new \DateInterval("P{$faturaDestaque->getDiascontratados()}D"))->format('Y-m-d'));
                        $anuncioDestaque->setFatura($fatura);
                        $anuncioDestaque->setDiascontratados($faturaDestaque->getDiascontratados());
                        $anuncioDestaques[] = $anuncioDestaque;

                        DoctrineStorage::orm()->persist($anuncioDestaque);*/

                        if($faturaDestaque->getDestaque() == 1){
                            $anuncio = DoctrineStorage::orm()->getRepository(\Anuncio\Entity::FULL_NAME)->findOneBy(['id' => $faturaDestaque->getAnuncio()]);
                            $anuncio->setNiveldestaque1(1);
                            DoctrineStorage::orm()->merge($anuncio);
                            DoctrineStorage::orm()->flush();
                        }else if($faturaDestaque->getDestaque() == 3){
                            $anuncio = DoctrineStorage::orm()->getRepository(\Anuncio\Entity::FULL_NAME)->findOneBy(['id' => $faturaDestaque->getAnuncio()]);
                            $anuncio->setNiveldestaque3(1);
                            DoctrineStorage::orm()->merge($anuncio);
                            DoctrineStorage::orm()->flush();
                        }
                    }else{
                        $hoje = new \DateTime();
                        $hoje = $hoje->format("Y-m-d");
                        $dataValidade = $anuncioDestaqueconsulta->getDatavencimento()->format("Y-m-d");
                        // 02/02/2015     03/03/2015
                        if ($dataValidade < $hoje) {

                            $conn = \CoffeeCore\Storage\DoctrineStorage::orm()->getConnection();
                            $insertSql = "INSERT INTO anunciodestaque VALUES ('', " . $faturaDestaque->getAnuncio() . ", " . $faturaDestaque->getDestaque() . ", NOW(), '" . (new \DateTime())->add(new \DateInterval("P{$faturaDestaque->getDiascontratados()}D"))->format('Y-m-d') . "', $fatura, " . $faturaDestaque->getDiascontratados() . ")";
                            $conn->executeQuery($insertSql);

                            if($anuncioDestaque->getDestaque() == 1){
                                $anuncio = DoctrineStorage::orm()->getRepository(\Anuncio\Entity::FULL_NAME)->findOneBy(['id' => $faturaDestaque->getAnuncio()]);
                                $anuncio->setNiveldestaque1(1);
                                DoctrineStorage::orm()->merge($anuncio);
                                DoctrineStorage::orm()->flush();
                            }else if($anuncioDestaque->getDestaque() == 3){
                                $anuncio = DoctrineStorage::orm()->getRepository(\Anuncio\Entity::FULL_NAME)->findOneBy(['id' => $faturaDestaque->getAnuncio()]);
                                $anuncio->setNiveldestaque3(1);
                                DoctrineStorage::orm()->merge($anuncio);
                                DoctrineStorage::orm()->flush();
                            }
                        }
                    }
                }
                if(!empty($anunciosDestaques)) {
                    foreach ($anunciosDestaques as $anuncioDestaque) {
                        DoctrineStorage::orm()->merge($anuncioDestaque);
                        if($anuncioDestaque->getDestaque() == 1){
                            $anuncio = DoctrineStorage::orm()->getRepository(\Anuncio\Entity::FULL_NAME)->findOneBy(['id' => $faturaDestaque->getAnuncio()]);
                            $anuncio->setNiveldestaque1(1);
                            DoctrineStorage::orm()->merge($anuncio);
                        }else if($anuncioDestaque->getDestaque() == 3){
                            $anuncio = DoctrineStorage::orm()->getRepository(\Anuncio\Entity::FULL_NAME)->findOneBy(['id' => $faturaDestaque->getAnuncio()]);
                            $anuncio->setNiveldestaque3(1);
                            DoctrineStorage::orm()->merge($anuncio);
                        }
                    }
                    DoctrineStorage::orm()->flush();
                }

            }
        }
    }

    /**
     * @param $login
     * @param $password
     * @throws Exception
     */
    public function logar($login, $password)
    {
        $userRepo = DoctrineStorage::orm()->getRepository(Entity::FULL_NAME);
        $user = $userRepo->findOneBy(["login" => $login]);

        if (!empty($user) and $user->getSenha() == $password) {
                Authentication::getInstance()->authenticate(
                    $login,
                    $user->getNome(),
                    $user->getEmail(),
                    $user
                );
            } else {
                throw new \Exception("Credenciais Inválidas.");
            }
    }

    /**
     * @throws Exception
     */
    public function cadastrar()
    {
        //print_r($_POST); die;
        try {

            $repo = DoctrineStorage::orm()->getRepository(Entity::FULL_NAME);
			
            if ($_POST["tipopessoa"] == 1) {
				$cpf = str_replace([' ', '.', ',', '-'], '', $_POST['cpf']);
                $cnpj = "";
                $dataNasc = explode('/', $_POST['datadenascimento']);
                $datadenascimento = new \DateTime("{$dataNasc[2]}-{$dataNasc[1]}-{$dataNasc[0]}");
                if(count($repo->findBy(["cpf" => $cpf])) > 0){
                    throw new Exception("O CPF informado já encontra-se cadastrado!");
                }
            }else if ($_POST["tipopessoa"] == 2) {
				$cnpj = str_replace([' ', '.', ',', '-', '/'], '', $_POST['cnpj']);
                $cpf = "";
                $datadenascimento = "";
                if(count($repo->findBy(["cnpj" => $cnpj])) > 0) {
                    throw new Exception("O CNPJ informado já encontra-se cadastrado!");
                }
            }

            if (count($repo->findBy(["email" => $_POST['email']])) > 0) {
                throw new Exception("O e-mail informado já encontra-se cadastrado!");
            }

            if (count($repo->findBy(["login" => strtolower($_POST['login'])])) > 0) {
                throw new Exception("O login/apelido informado já encontra-se cadastrado!");
            }
            if(preg_match("/^[a-z0-9]+$/", strtolower($_POST['login'])) == 0) {
                throw new Exception("Login/Apelido inválido!");
            }

            $usuario = New Entity();

            $usuario->setTipopessoa($_POST['tipopessoa']);
            $usuario->setNome($_POST['nome']);
            $usuario->setCpf($cpf);
            $usuario->setRg($_POST['rg']);
            if(!empty($_POST['datadenascimento'])) {
                $usuario->setDatadenascimento($datadenascimento);
            }
            $usuario->setRazaosocial($_POST['razaosocial']);
            $usuario->setNomefantasia($_POST['nomefantasia']);
            $usuario->setCnpj($cnpj);
            $usuario->setInscricaoestadual($_POST['inscricaoestadual']);
            $usuario->setNomedoresponsavel($_POST['nomedoresponsavel']);
            $usuario->setEmail($_POST['email']);
            $usuario->setCep($_POST['cep']);
            $usuario->setEndereco($_POST['endereco']);
            $usuario->setNumero($_POST['numero']);
            $usuario->setBairro($_POST['bairro']);
            $usuario->setComplemento($_POST['complemento']);
            $usuario->setEstado($_POST['estado']);
            $usuario->setCidade($_POST['cidade']);
            $usuario->setTelefone($_POST['telefone']);
            $usuario->setCelular($_POST['celular']);
            $usuario->setLogin(strtolower($_POST['login']));
            $usuario->setSenha($_POST['senha']);
            $usuario->setComoconheceu($_POST['comoconheceu']);
            $usuario->setComoconheceuoutro($_POST['comoconheceuoutro']);

            DoctrineStorage::orm()->persist($usuario);
            DoctrineStorage::orm()->flush();

        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $id
     * @param $slug
     * @return array
     */
    public function mostrarAnuncio($id, $slug)
    {
        return [$id, $slug];
    }

    /**
     * @param $nome
     * @param $email
     * @throws Exception
     */
    public function cadastrarNewsletter($nome, $celular, $email)
    {
        if (empty($nome)) {
            throw new \InvalidArgumentException("Nome não pode ser vazio.");
        }
        if(empty($email)) {
            throw new \InvalidArgumentException("E-mail não pode ser vazio.");
        }

        $newsModel = new \Newsletter\Model(new DoctrineStorage(\Newsletter\Entity::FULL_NAME));

        if (!$newsModel->findBy(["email" => $email])->valid()) {
            if(!empty($celular) and $newsModel->findBy(["celular" => $celular])->valid()){
                throw new Exception("Este celular já estava cadastrado");
            }
            try {
                $news = new \Newsletter\Entity();
                $news->setNome($nome);
                $news->setCelular($celular);
                $news->setEmail($email);

                $newsModel->insert($news);
                $newsModel->flush();

            } catch (Exception $e) {

                throw new Exception("Houve um erro: " . $e->getMessage());
            }
        } else {
			throw new Exception("Este e-mail já estava cadastrado");
		}
    }
}
