<?php
include 'inc_head.php';

$action = "Adicionar";
if (isset($registro)) {
    $action = "Editar";
    $dados = [
        'id' => $registro->getId(),
        'titulo' => $registro->getTitulo(),
        'email' => $registro->getEmail()
    ];
} else {
    $dados = [
        'id' => '',
        'titulo' => '',
        'email' => ''
    ];
}
?>
    <!-- styles -->
    <link href="<?=$prefix;?>css/bootstrap.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/jquery.gritter.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=$prefix;?>css/font-awesome.css">

    <!--[if IE 7]>
    <link rel="stylesheet" href="<?=$prefix;?>css/font-awesome-ie7.min.css">
    <![endif]-->
    <link href="<?=$prefix;?>css/tablecloth.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/styles.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie7.css" />
    <![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie8.css" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie9.css" />
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>

    <!--fav and touch icons -->
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="/favicon.png">

    <!--============ javascript ===========-->
    <script src="<?=$prefix;?>js/jquery.js"></script>
    <script src="<?=$prefix;?>js/bootstrap.js"></script>
    <script src="<?=$prefix;?>js/bootstrap-fileupload.js"></script>
    <script src="<?=$prefix;?>js/jquery.metadata.js"></script>
    <script src="<?=$prefix;?>js/jquery.tablesorter.min.js"></script>
    <script src="<?=$prefix;?>js/jquery.tablecloth.js"></script>
    <script src="<?=$prefix;?>js/excanvas.js"></script>
    <script src="<?=$prefix;?>js/jquery.collapsible.js"></script>
    <script src="<?=$prefix;?>js/accordion.nav.js"></script>
    <script src="<?=$prefix;?>js/custom.js"></script>
    <script src="<?=$prefix;?>js/respond.min.js"></script>
    <script src="<?=$prefix;?>js/ios-orientationchange-fix.js"></script>

    <script src="<?=$prefix;?>js/jquery.dataTables.js"></script>
    <script src="<?=$prefix;?>js/ZeroClipboard.js"></script>
    <script src="<?=$prefix;?>js/dataTables.bootstrap.js"></script>
    <script src="<?=$prefix;?>js/TableTools.js"></script>
    <script src="<?=$prefix;?>js/plugins/dataTables/date-uk.js"></script>

    <link href="<?=$prefix;?>css/tablecloth.css" rel="stylesheet">

    <script src="<?=$prefix;?>js/jquery.validate.js"></script>
</head>
<body>
    <div class="layout">
    <?php
        include_once 'inc_header.php';
        include_once 'inc_usuario.php';
        include_once 'inc_sidebar.php';
    ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">Assunto do Contato</h3>
                        <span class="pull-right top-right-toolbar">
                            <a href="<?=$prefix;?>assunto-do-contato/novo" class="btn btn-mini btn-success" title="Novo"><i class="icon-plus "></i> Adicionar Novo</a>
                        </span>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="<?=$prefix;?>" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li>
                            <a href="<?=$prefix;?>assunto-do-contato">Assunto do Contato</a><span class="divider">
                                <i class="icon-angle-right"></i>
                            </span>
                        </li>
                        <li class="active">Formulário</li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="content-widgets light-gray">
                    <div class="widget-head black">
                        <h3><?=$action;?> Assunto do Contato</h3>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function(){
                            // valida o formulário
                            $('#assunto-do-contato').validate({
                                // define regras para os campos
                                rules: {
                                    titulo: {
                                        required: true
                                    },
                                    email: {
                                        required: true,
                                        email: true
                                    }
                                },
                                // define messages para cada campo
                                messages: {
                                    titulo:"Informe o título",
                                    email: {
										required: "Informe o e-mail",
										email: "Informe um e-mail válido!"
									}
                                }
                            });
                        });
                    </script>
                    <div class="widget-container">
                        <form name="assunto-do-contato" id="assunto-do-contato" class="form-horizontal" action="<?=$_SERVER["REQUEST_URI"];?>" method="post">
                            <div class="control-group">
                                <label for="titulo" class="control-label">Título</label>
                                <div class="controls">
                                    <input name="id" id="id" type="hidden" value="<?=$dados['id'];?>">
                                    <input name="titulo" id="titulo" type="text" maxlength="150" class="span7" value="<?=$dados['titulo'];?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="email" class="control-label">Email</label>
                                <div class="controls">
                                    <input name="email" id="email" type="text" maxlength="255" class="span7" value="<?=$dados['email'];?>">
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" id="Bt" class="btn btn-success">Salvar</button>
                                <button type="button" onclick="window.open('<?=$prefix;?>assunto-do-contato', '_self');" class="btn right-float">Voltar</button>
                            </div>
                        </form>
                    </div>
                </div>
<?php
include 'inc_footer.php';
