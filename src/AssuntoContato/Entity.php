<?php

namespace AssuntoContato;

/**
 * AssuntoContato
 *
 * @Table(name="assuntocontato")
 * @Entity
 */
class Entity implements \CoffeeCore\Core\Entity
{
    /**
     *
     */
    const FULL_NAME = "AssuntoContato\\Entity";

    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="titulo", type="string", length=128, nullable=false)
     */
    private $titulo;

    /**
     * @var string
     *
     * @Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;
	
	/**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
	
    /**
     * @return boolean
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    /**
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
}
