<?php

namespace AssuntoContato;

use CoffeeCore\Core\AbstractController;
use CoffeeCore\Storage\DoctrineStorage;

/**
 * Class Controller
 * @package Categorias
 */
class Controller extends AbstractController
{
    /**
     * @var string
     */
    public $pagina = 'assunto-do-contato';
    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Entity
     */
    protected $entity;

    /**
     * @return void
     */
    public function getDataTable()
    {
        $this->startConnection();
        $columns = ['id', 'titulo', 'email'];

        $search = $_POST['search']['value'];

        $limit = $_POST['length'];
        $offset = $_POST['start'];
        $orderBy = [
            $_POST['order'][0]['column'],
            $_POST['order'][0]['dir']
        ];

        $dataTables = $this->model->getDataTableData($columns, $orderBy, $search, $limit, $offset);



        foreach ($dataTables['data'] as $k => &$row) {
            $row[] = "<span class='center'>
                        <button class='btn' onclick='javascript:window.location = \"/painel/{$this->pagina}/alterar/{$row[0]}\"'>
                            <span><i class='icon-pencil'></i></span>
                        </button>
                        <button data-url='/painel/{$this->pagina}/deletar/{$row[0]}' id='deletar' class='btn btn-primary'>
                            <span><i class='icon-trash'></i></span>
                        </button>
                     </span>";
        }

        header('Content-Type: application/json');
        echo json_encode($dataTables);
    }

    /**
     *
     */
    public function startConnection()
    {
        $this->entity = new Entity();
        $this->model = new Model(new DoctrineStorage($this->entity));
    }

    /**
     *
     */
    public function gravar()
    {
        $this->startConnection();

        $this->entity->setTitulo($_POST['titulo']);
		$this->entity->setEmail($_POST['email']);
		
        if (!isset($_POST['id']) or empty($_POST['id'])) {
            $this->model->insert($this->entity);
        } else {
			$this->entity->setId($_POST['id']);
            $this->model->update($this->entity);
        }
        $this->model->flush();
    }

    /**
     * @param int|null $id
     */
    public function deletar($id = null)
    {
        $this->startConnection();
        if (!empty($id)) {
            $this->entity = $this->model->findOneBy(["id"=>$id]);
            $this->model->delete($this->entity);
            $this->model->flush();
        }
    }
}
