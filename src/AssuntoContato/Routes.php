<?php

namespace AssuntoContato;


use Slim\Slim;

/**
 * Class Routes
 * @package AreaAtuacao
 */
class Routes
{
    /**
     * @param Slim $app
     */
    public static function getRoutes(Slim $app)
    {
        $controller = new Controller();

        $app->group( '/assunto-do-contato', function () use ($app, $controller) {


            $app->map(
                '/',
                function () use ($app, $controller) {

                    if ($app->request()->isGet()) {
                        $app->render( __NAMESPACE__ . "/Views/grid.php");
                    }

                    if ($app->request()->isPost()) {
                        $controller->getDataTable();
                    }
                }
            )->via("GET", "POST");

            $app->map(
                '/novo/',
                function () use ($app, $controller) {
                    if ($app->request()->isGet()) {
                        $app->render(__NAMESPACE__ . "/Views/form.php");
                    }
                    if ($app->request()->isPost()) {
                        try {
                            $controller->gravar();
                            $app->flash("info", "Inserido com sucesso!");
                        } catch (\Exception $e) {
                            $app->flash("info", "Erro ao inserir!");

                        }
                        $app->redirect("/painel/" . $controller->pagina);
                    }
                }
            )->via("GET", "POST");

            $app->map(
                '/alterar/:id',
                function ($id) use ($app, $controller) {
                    if ($app->request()->isGet()) {
                        $registro = \CoffeeCore\Storage\DoctrineStorage::orm()
                            ->getRepository(Entity::FULL_NAME)->findOneBy(["id" => $id]);
                        $app->render(__NAMESPACE__ . "/Views/form.php", ['registro' => $registro]);
                    }
                    if ($app->request()->isPost()) {
                        try {
                            $controller->gravar();
                            $app->flash("info", "Alterado com sucesso!");
                        } catch (\Exception $e) {
                            $app->flash("info", "Erro ao alterar!");

                        }
                        $app->redirect("/painel/" . $controller->pagina);

                    }
                }
            )->via("GET", "POST");


            $app->get(
                '/deletar/:id/',
                function ($id) use ($app, $controller) {
                    try {
                        $controller->deletar($id);
                        $app->flash("info", "Excluido com sucesso!");
                    } catch (\Exception $e) {
                        $app->flash("info", "Erro ao excluir!");

                    }
                    $app->redirect("/painel/" . $controller->pagina);
                }
            );
        });
    }
}
