<?php

namespace TodasCidades;

/**
 * Entity
 *
 * @Table(name="novo_cidades")
 * @Entity
 */
class Entity implements \CoffeeCore\Core\Entity
{
    const FULL_NAME = "TodasCidades\\Entity";

    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @Column(name="id_uf", type="integer", nullable=false)
     */
    private $id_uf;

    /**
     * @var string
     *
     * @Column(name="nome", type="string", length=50, nullable=false)
     */
    private $nome;

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $iduf
     */
    public function setIduf($id_uf)
    {
        $this->id_uf = $id_uf;
    }

    /**
     * @return int
     */
    public function getIduf()
    {
        return $this->id_uf;
    }

    /**
     * @param string $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }
}
