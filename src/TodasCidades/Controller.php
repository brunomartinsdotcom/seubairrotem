<?php

namespace TodasCidades;

use CoffeeCore\Core\AbstractController;
use CoffeeCore\Helper\ResourceManager;
use CoffeeCore\Storage\DoctrineStorage;

/**
 * Class Controller
 * @package TodasCidades
 */
class Controller extends AbstractController
{
    /**
     * @var string
     */
    public $pagina = 'todas-as-cidades';
    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Entity
     */
    protected $entity;

    /**
     * @return void
     */
    public function getDataTable()
    {
        $this->startConnection();
        $columns = ['id', 'nome', 'id_uf'];
        $noSearch = ['id_uf'];

        $search = isset($_POST['search']['value']) ? $_POST['search']['value'] : ""; //$_POST['search']['value'];

        $limit = isset($_POST['length']) ? $_POST['length'] : 10;
        $offset = isset($_POST['start']) ? $_POST['start'] : 0;
        if(isset($_POST['order'])){
            $orderBy = [
                $_POST['order'][0]['column'],
                $_POST['order'][0]['dir']
            ];
        }else{
            $orderBy = [0, 'ASC'];
        }

        $dataTables = $this->model->getDataTableData($columns, $orderBy, $search, $limit, $offset);

        foreach ($dataTables['data'] as $k => &$row) {
            $row[2] = $this->getNomeEstado($row[2]);
            $row[] = "<span class='center'>
                        <button class='btn btn-primary' onclick='javascript:window.location = \"/painel/{$this->pagina}/alterar/{$row[0]}\"'>
                            <span><i class='icon-pencil'></i></span>
                        </button>
                      </span>";
            $row[] = "<span class='center'>
                        <button data-url='/painel/{$this->pagina}/deletar/{$row[0]}' id='deletar' class='btn btn-danger'>
                            <span><i class='icon-trash'></i></span>
                        </button>
                     </span>";
            unset($k);
        }

        //header('Content-Type: application/json');
        echo json_encode($dataTables);
    }

    /**
     *
     */
    public function startConnection()
    {
        $this->entity = new Entity();
        $this->model = new Model(new DoctrineStorage($this->entity));
    }

    /**
     * @return string
     */
    public function getNomeEstado($iduf)
    {
        $nomeEstado = DoctrineStorage::orm()->getRepository(\Estados\Entity::FULL_NAME)->findOneBy(["id" => $iduf])->getNome();
        return $nomeEstado;
    }

    /**
     *
     */
    public function gravar()
    {
        $this->startConnection();

        $this->entity->setIduf($_POST['id_uf']);
        $this->entity->setNome($_POST['nome']);

        try {
            if (!isset($_POST['id']) or empty($_POST['id'])) {
                $this->model->insert($this->entity);
            } else {
                $this->entity->setId($_POST['id']);
                $this->model->update($this->entity);
            }
            $this->model->flush();
        } catch (\Exception $e) {
            return false;
        }

        //ESCREVE JSON
        $listaDeCidades = $this->model->findby([], ['nome' => 'ASC']);

        $cidades = [];

        while ($listaDeCidades->valid()) {
            $cidades[create_slug($listaDeCidades->current()->getNome())] = [
                "id" =>     $listaDeCidades->current()->getId(),
                "id_uf" => $listaDeCidades->current()->getIduf(),
                "nome" => $listaDeCidades->current()->getNome()
            ];
            $listaDeCidades->next();
        }

        ResourceManager::writeFile("cadastro_de_cidades.json", json_encode($cidades));

    }

    /**
     * @param int|null $id
     */
    public function deletar($id = null)
    {
        $this->startConnection();
        if (!empty($id)) {
            $this->entity = $this->model->findOneBy(["id"=>$id]);
            $this->model->delete($this->entity);
            $this->model->flush();

            //ESCREVE JSON
            $listaDeCidades = $this->model->findby([], ['nome' => 'ASC']);

            $cidades = [];

            while ($listaDeCidades->valid()) {
                $cidades[create_slug($listaDeCidades->current()->getNome())] = [
                    "id" =>     $listaDeCidades->current()->getId(),
                    "id_uf" => $listaDeCidades->current()->getIduf(),
                    "nome" => $listaDeCidades->current()->getNome()
                ];
                $listaDeCidades->next();
            }

            ResourceManager::writeFile("cadastro_de_cidades.json", json_encode($cidades));

        }
    }

    public function verificaexistente()
    {
        $id = isset($_POST['id']) ? $_POST['id'] : false;
        $iduf = $_POST['id_uf'];
        $nomecidade = $_POST['nome'];
        $this->startConnection();
        if (!empty($iduf) and !empty($nomecidade)) {
            if(!empty($id)){
                $conn = \CoffeeCore\Storage\DoctrineStorage::orm()->getConnection();
                $this->entity = $conn->fetchAll("SELECT * FROM novo_cadastro_de_cidades WHERE id_uf=".$iduf." AND nome='".$nomecidade."' AND id!=".$id);
            }else{
                $this->entity = $this->model->findOneBy(["id_uf" => $iduf, "nome" => $nomecidade]);
            }

            if(!empty($this->entity)){
                $array = ['error' => 1];
            }else{
                $array = ['error' => 0];
            }
            header('Content-Type: application/json');
            echo json_encode($array);
        }
    }
}
