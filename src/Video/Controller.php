<?php

namespace Video;

use CoffeeCore\Core\AbstractController;
use CoffeeCore\Storage\DoctrineStorage;
use CoffeeCore\Helper\Video;
//use PerguntaFrequente\Model;

/**
 * Class Controller
 * @package Categorias
 */
class Controller extends AbstractController
{
    /**
     * @var string
     */
    public $pagina = 'videos';
    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Entity
     */
    protected $entity;

    /**
     * @return void
     */
    public function getDataTable()
    {
        $this->startConnection();
        $columns = ['id', 'data', 'titulo', 'url'];

        $search = $_POST['search']['value'];

        $limit = $_POST['length'];
        $offset = $_POST['start'];
        $orderBy = [
            $_POST['order'][0]['column'],
            $_POST['order'][0]['dir']
        ];

        $dataTables = $this->model->getDataTableData($columns, $orderBy, $search, $limit, $offset);

		$videoManager = new \CoffeeCore\Helper\Video();

        foreach ($dataTables['data'] as $k => &$row) {
            $row[1] = $row[1]->format("d/m/Y H:i:s");
			$row[3] = $videoManager->identificaVideo($row[3], "menor");
			$row[] = "<span class='center'>
                        <button class='btn' onclick='javascript:window.location = \"/painel/{$this->pagina}/alterar/{$row[0]}\"'>
                            <i class='icon-pencil'></i>
                        </button></span>";
			$row[] = "<span class='center'>					
                        <button data-url='/painel/{$this->pagina}/deletar/{$row[0]}' id='deletar' class='btn btn-primary'>
                            <i class='icon-trash'></i>
                        </button>
                     </span>";
        }

        header('Content-Type: application/json');
        echo json_encode($dataTables);
    }

    /**
     *
     */
    public function startConnection()
    {
        $this->entity = new Entity();
        $this->model = new Model(new DoctrineStorage($this->entity));
    }

    /**
     *
     */
    public function gravar()
    {
        $this->startConnection();

        if (!empty($_POST["id"])) {
            $this->entity = $this->model->findOneBy(["id" => $_POST["id"]]);
        }

        $this->entity->setTitulo($_POST['titulo']);
        $this->entity->setUrl($_POST['url']);
        $this->entity->setTexto($_POST["texto"]);

        if (empty($this->entity->getId())) {
            $this->model->insert($this->entity);
        } else {
            $this->model->update($this->entity);
        }

        $this->model->flush();
    }

    /**
     * @param int|null $id
     */
    public function deletar($id = null)
    {
        $this->startConnection();
        if (!empty($id)) {
            $this->model->delete($this->model->findOneBy(["id"=>$id]));
            $this->model->flush();
        }
    }
}
