<?php

namespace AnuncioPagamento;

/**
 * Entity
 *
 * @Table(name="anunciopagamento")
 * @Entity
 */
class Entity implements \CoffeeCore\Core\Entity
{
    /**
     *
     */
    const FULL_NAME = "AnuncioPagamento\\Entity";

    /**
     * @var integer
     * @Id
     * @Column(name="anuncio", type="integer", nullable=false)
     */
    private $anuncio;

    /**
     * @var integer
     * @Id
     * @Column(name="formapagamento", type="integer", nullable=false)
     */
    private $formapagamento;

    /**
     * @return int
     */
    public function getAnuncio()
    {
        return $this->anuncio;
    }

    /**
     * @param int $anuncio
     */
    public function setAnuncio($anuncio)
    {
        $this->anuncio = $anuncio;
    }

    /**
     * @return int
     */
    public function getFormapagamento()
    {
        return $this->formapagamento;
    }

    /**
     * @param int $formapagamento
     */
    public function setFormapagamento($formapagamento)
    {
        $this->formapagamento = $formapagamento;
    }
}
