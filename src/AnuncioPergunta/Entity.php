<?php

namespace AnuncioPergunta;

/**
 * Entity
 *
 * @Table(name="anuncioperguntas")
 * @Entity
 */
class Entity implements \CoffeeCore\Core\Entity
{
    /**
     *
     */
    const FULL_NAME = "AnuncioPergunta\\Entity";

    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @Column(name="anuncio", type="integer", nullable=false)
     */
    private $anuncio;

    /**
     * @var integer
     *
     * @Column(name="usuario", type="integer", nullable=true)
     */
    private $usuario;
	
	/**
     * @var string
     *
     * @Column(name="nome", type="string", length=50, nullable=true)
     */
    private $nome;
	
	/**
     * @var string
     *
     * @Column(name="email", type="string", length=50, nullable=true)
     */
    private $email;
	
	/**
     * @var string
     *
     * @Column(name="celular", type="string", length=15, nullable=true)
     */
    private $celular;
	
	/**
     * @var string
     *
     * @Column(name="telefone", type="string", length=14, nullable=true)
     */
    private $telefone;
	
	/**
     * @var string
     *
     * @Column(name="estado", type="string", length=50, nullable=true)
     */
    private $estado;
	
	/**
     * @var string
     *
     * @Column(name="cidade", type="string", length=50, nullable=true)
     */
    private $cidade;

    /**
     * @var string
     *
     * @Column(name="pergunta", type="string", length=255, nullable=false)
     */
    private $pergunta;

    /**
     * @var string
     *
     * @Column(name="resposta", type="text")
     */
    private $resposta;

    /**
     * @var datetime
     *
     * @Column(name="datahorapergunta", type="datetime")
     */
    private $datahorapergunta;

    /**
     * @var datetime
     *
     * @Column(name="datahoraresposta", type="datetime")
     */
    private $datahoraresposta;
    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $pergunta
     */
    public function setPergunta($pergunta)
    {
        $this->pergunta = $pergunta;
    }

    /**
     * @return string
     */
    public function getPergunta()
    {
        return $this->pergunta;
    }

    /**
     * @param string $resposta
     */
    public function setResposta($resposta)
    {
        $this->resposta = $resposta;
    }

    /**
     * @return string
     */
    public function getResposta()
    {
        return $this->resposta;
    }

    /**
     * @return int
     */
    public function getAnuncio()
    {
        return $this->anuncio;
    }

    /**
     * @param int $anuncio
     */
    public function setAnuncio($anuncio)
    {
        $this->anuncio = $anuncio;
    }

    /**
     * @return datetime
     */
    public function getDatahorapergunta()
    {
        return $this->datahorapergunta;
    }

    /**
     * @param datetime $datahorapergunta
     */
    public function setDatahorapergunta($datahorapergunta)
    {
        $this->datahorapergunta = $datahorapergunta;
    }

    /**
     * @return datetime
     */
    public function getDatahoraresposta()
    {
        return $this->datahoraresposta;
    }

    /**
     * @param datetime $datahoraresposta
     */
    public function setDatahoraresposta($datahoraresposta)
    {
        $this->datahoraresposta = $datahoraresposta;
    }

    /**
     * @return int
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param int $usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }
	
	/**
     * @param string $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }
	
	/**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
	
	/**
     * @param string $celular
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;
    }

    /**
     * @return string
     */
    public function getCelular()
    {
        return $this->celular;
    }
	
	/**
     * @param string $telefone
     */
    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;
    }

    /**
     * @return string
     */
    public function getTelefone()
    {
        return $this->telefone;
    }
	
	/**
     * @param string $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }
	
	/**
     * @param string $cidade
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;
    }

    /**
     * @return string
     */
    public function getCidade()
    {
        return $this->cidade;
    }
}
