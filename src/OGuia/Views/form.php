<?php
include 'inc_head.php';

$action = "Adicionar";
/** @var \OGuia\Entity $oguia */
if (isset($oguia)) {
    $action = "Editar";
    $dados = [
        'id'        => $oguia->getId(),
        'titulo'    => $oguia->getTitulo(),
        'texto'     => $oguia->getTexto(),
        'sortorder' => $oguia->getSortorder()
    ];
} else {
    $dados = [
        'id'        => '',
        'titulo'    => '',
        'texto'     => '',
        'sortorder' => ''
    ];
}
?>
</head>
<body>
    <div class="layout">
    <?php
        include_once 'inc_header.php';
        include_once 'inc_usuario.php';
        include_once 'inc_sidebar.php';
    ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">O Guia</h3>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="<?=$prefix;?>" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li>
                            <a href="<?=$prefix;?>o-guia">O Guia</a><span class="divider">
                                <i class="icon-angle-right"></i>
                            </span>
                        </li>
                        <li class="active"><?=$action;?></li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="content-widgets light-gray">
                    <div class="widget-head black">
                        <h3><?=$action;?> Texto</h3>
                    </div>
                    <script type="text/javascript" src="<?=$prefix;?>editor/ckeditor/ckeditor.js"></script>
                    <script type="text/javascript">
                        $(document).ready(function(){
                            // INICIALIZA O EDITOR DE TEXTO
                            CKEDITOR.replace('texto');
                            // valida o formulário
                            $('#oguia').validate({
                                // define regras para os campos
                                ignore: [],
                                rules: {
                                    titulo: {
                                        required: true
                                    },
                                    texto: {
                                        required: function()
                                        {
                                            CKEDITOR.instances.texto.updateElement();
                                        }
                                    }
                                },
                                // define messages para cada campo
                                messages: {
                                    titulo:"Informe o título",
                                    texto:"Informe o texto"
                                }
                            });
                        });
                    </script>
                    <div class="widget-container">
                        <form name="oguia" id="oguia" enctype="multipart/form-data" class="form-horizontal" action="<?=$_SERVER["REQUEST_URI"];?>" method="post">
                            <?php if(isset($duvida)) { ?>
                                <input name="id" id="id" type="hidden" value="<?=$dados['id'];?>">
                            <?php } ?>
                            <input name="sortorder" id="sortorder" type="hidden" value="<?=$dados['sortorder'];?>">
                             <!-- .control-group -->
                            <div class="control-group">
                                <label for="titulo" class="control-label">Título</label>
                                <!-- .controls -->
                                <div class="controls">
                                    <input name="titulo" id="titulo" type="text" maxlength="100" class="span7" value="<?=$dados['titulo'];?>">
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                              <!-- .control-group -->
                            <div class="control-group">
                                <label for="texto" class="control-label">Texto</label>
                                <!-- .controls -->
                                <div class="controls" style="width: 80%;">
                                    <textarea name="texto" id="texto" rows="16" class="span12"><?=$dados['texto'];?></textarea>
                                </div><!-- /.controls -->
                            <div class="form-actions">
                                <button type="submit" id="Bt" class="btn btn-success">Salvar</button>
                                <button type="button" onClick="window.open('<?=$prefix;?>o-guia', '_self');" class="btn right-float">Voltar</button>
                            </div>
                        </form>
                    </div>
                </div>
<?php
include 'inc_footer.php';
