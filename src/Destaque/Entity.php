<?php

namespace Destaque;


/**
 * Entity
 *
 * @Table(name="destaque")
 * @Entity
 */
class Entity implements \CoffeeCore\Core\Entity
{
    /**
     *
     */
    const FULL_NAME = "Destaque\\Entity";

    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="SEQUENCE")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="titulo", type="string", length=45, nullable=false)
     */
    private $titulo = '';

    /**
     * @var string
     *
     * @Column(name="descricao", type="string", length=255)
     */
    private $descricao = '';

    /**
     * @var string
     *
     * @Column(name="tipo", type="string", length=25, nullable=false)
     */
    private $tipo = '';

    /**
     * @var integer
     *
     * @Column(name="diasplano1", type="integer", nullable=false)
     */
    private $diasplano1;

    /**
     * @var integer
     *
     * @Column(name="diasplano3", type="integer", nullable=false)
     */
    private $diasplano2;

    /**
     * @var integer
     *
     * @Column(name="diasplano2", type="integer", nullable=false)
     */
    private $diasplano3;

    /**
     * @var float
     *
     * @Column(name="valorplano1", type="float", precision=14, scale=2)
     */
    private $valorplano1;

    /**
     * @var float
     *
     * @Column(name="valorplano2", type="float", precision=14, scale=2)
     */
    private $valorplano2;

    /**
     * @var float
     *
     * @Column(name="valorplano3", type="float", precision=14, scale=2)
     */
    private $valorplano3;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param string $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @return int
     */
    public function getDiasplano1()
    {
        return $this->diasplano1;
    }

    /**
     * @param int $diasplano1
     */
    public function setDiasplano1($diasplano1)
    {
        $this->diasplano1 = $diasplano1;
    }

    /**
     * @return int
     */
    public function getDiasplano2()
    {
        return $this->diasplano2;
    }

    /**
     * @param int $diasplano2
     */
    public function setDiasplano2($diasplano2)
    {
        $this->diasplano2 = $diasplano2;
    }

    /**
     * @return int
     */
    public function getDiasplano3()
    {
        return $this->diasplano3;
    }

    /**
     * @param int $diasplano3
     */
    public function setDiasplano3($diasplano3)
    {
        $this->diasplano3 = $diasplano3;
    }

    /**
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param string $tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    /**
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param string $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    /**
     * @return float
     */
    public function getValorplano1()
    {
        return $this->valorplano1;
    }

    /**
     * @param float $valorplano1
     */
    public function setValorplano1($valorplano1)
    {
        $this->valorplano1 = $valorplano1;
    }

    /**
     * @return float
     */
    public function getValorplano2()
    {
        return $this->valorplano2;
    }

    /**
     * @param float $valorplano2
     */
    public function setValorplano2($valorplano2)
    {
        $this->valorplano2 = $valorplano2;
    }

    /**
     * @return float
     */
    public function getValorplano3()
    {
        return $this->valorplano3;
    }

    /**
     * @param float $valorplano3
     */
    public function setValorplano3($valorplano3)
    {
        $this->valorplano3 = $valorplano3;
    }
}
