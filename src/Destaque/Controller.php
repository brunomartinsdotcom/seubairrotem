<?php

namespace Destaque;

use CoffeeCore\Core\AbstractController;
use CoffeeCore\Storage\DoctrineStorage;

/**
 * Class Controller
 * @package Destaque
 */
class Controller extends AbstractController
{
    /**
     * @var string
     */
    public $pagina = 'destaques';
    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Entity
     */
    protected $entity;

    /**
     * @return void
     */
    public function getDataTable()
    {
        $this->startConnection();
        $columns = ['id', 'titulo', 'tipo', 'diasplano1', 'valorplano1'];

        $search = $_POST['search']['value'];

        $limit = $_POST['length'];
        $offset = $_POST['start'];
        $orderBy = [
            $_POST['order'][0]['column'],
            $_POST['order'][0]['dir']
        ];

        $dataTables = $this->model->getDataTableData($columns, $orderBy, $search, $limit, $offset);

        foreach ($dataTables['data'] as $k => &$row) {
            $row[] = "<span class='center'>
                        <button class='btn' onclick='window.location = \"/painel/{$this->pagina}/alterar/{$row[0]}\"'>
                            <span><i class='icon-pencil'></i></span>
                        </button>
                     </span>";
            unset($k);
        }

        header('Content-Type: application/json');
        echo json_encode($dataTables);
    }

    /**
     *
     */
    public function startConnection()
    {
        $this->entity = new Entity();
        $this->model = new Model(new DoctrineStorage($this->entity));
    }

    /**
     *
     */
    public function gravar()
    {
        $this->startConnection();

        $this->entity = $this->model->findOneBy(['id' => $_POST['id']]);

        $this->entity->setTitulo($_POST['titulo']);
        $this->entity->setDescricao($_POST['descricao']);
        $this->entity->setDiasplano1($_POST['diasplano1']);
        $this->entity->setDiasplano2($_POST['diasplano2']);
        $this->entity->setDiasplano3($_POST['diasplano3']);
        $this->entity->setValorplano1($_POST['valorplano1']);
        $this->entity->setValorplano2($_POST['valorplano2']);
        $this->entity->setValorplano3($_POST['valorplano3']);

        $this->model->update($this->entity);
        $this->model->flush();

        echo "
        <script>
            window.location = '/painel/{$this->pagina}';
        </script>
        ";
    }

}
