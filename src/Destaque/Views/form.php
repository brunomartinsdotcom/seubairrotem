<?php
include 'inc_head.php';

$action = "Adicionar";
/** @var $destaque \Destaque\Entity */
if (isset($destaque)) {
    $action = "Editar";
    $dados = [
        'id'            => $destaque->getId(),
        'titulo'        => $destaque->getTitulo(),
        'descricao'     => $destaque->getDescricao(),
        'diasplano1'    => $destaque->getDiasplano1(),
        'diasplano2'    => $destaque->getDiasplano2(),
        'diasplano3'    => $destaque->getDiasplano3(),
        'valorplano1'   => $destaque->getValorplano1(),
        'valorplano2'   => $destaque->getValorplano2(),
        'valorplano3'   => $destaque->getValorplano3()
    ];
} else {
    $dados = [
        'id'            => '',
        'titulo'        => '',
        'descricao'     => '',
        'diasplano1'    => '',
        'diasplano2'    => '',
        'diasplano3'    => '',
        'valorplano1'   => '',
        'valorplano2'   => '',
        'valorplano3'   => ''
    ];
}

?>
    <!-- styles -->
    <link href="<?=$prefix;?>css/bootstrap.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/jquery.gritter.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=$prefix;?>css/font-awesome.css">

    <!--[if IE 7]>
    <link rel="stylesheet" href="<?=$prefix;?>css/font-awesome-ie7.min.css">
    <![endif]-->
    <link href="<?=$prefix;?>css/tablecloth.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/styles.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie7.css" />
    <![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie8.css" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie9.css" />
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>

    <!--fav and touch icons -->
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="/favicon.png">

    <!--============ javascript ===========-->
    <script src="<?=$prefix;?>js/jquery.js"></script>
    <script src="<?=$prefix;?>js/bootstrap.js"></script>
    <script src="<?=$prefix;?>js/bootstrap-fileupload.js"></script>
    <script src="<?=$prefix;?>js/jquery.metadata.js"></script>
    <script src="<?=$prefix;?>js/jquery.tablesorter.min.js"></script>
    <script src="<?=$prefix;?>js/jquery.tablecloth.js"></script>
    <script src="<?=$prefix;?>js/excanvas.js"></script>
    <script src="<?=$prefix;?>js/jquery.collapsible.js"></script>
    <script src="<?=$prefix;?>js/accordion.nav.js"></script>
    <script src="<?=$prefix;?>js/custom.js"></script>
    <script src="<?=$prefix;?>js/respond.min.js"></script>
    <script src="<?=$prefix;?>js/ios-orientationchange-fix.js"></script>

    <script src="<?=$prefix;?>js/jquery.dataTables.js"></script>
    <script src="<?=$prefix;?>js/ZeroClipboard.js"></script>
    <script src="<?=$prefix;?>js/dataTables.bootstrap.js"></script>
    <script src="<?=$prefix;?>js/TableTools.js"></script>
    <script src="<?=$prefix;?>js/plugins/dataTables/date-uk.js"></script>

    <link href="<?=$prefix;?>css/tablecloth.css" rel="stylesheet">

    <script src="<?=$prefix;?>js/jquery.validate.js"></script>

</head>
<body>
    <div class="layout">
        <?php
        include_once 'inc_header.php';
        include_once 'inc_usuario.php';
        include_once 'inc_sidebar.php';
        ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">Destaques</h3>
                        <span class="pull-right top-right-toolbar"></span>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="<?=$prefix;?>" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li>
                            <a href="<?=$prefix;?>destaques">Destaques</a><span class="divider">
                                <i class="icon-angle-right"></i>
                            </span>
                        </li>
                        <li class="active">Formulário</li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="content-widgets light-gray">
                    <div class="widget-head black">
                        <h3><?=$action;?> Destaque</h3>
                    </div>

                    <script type="text/javascript">
                        $(document).ready(function(){
                            // valida o formulário
                            $('#destaques').validate({
                                // define regras para os campos
                                rules: {
                                    titulo: {
                                        required: true,
                                        minlength: 4
                                    },
                                    diasanuncio: {
                                        required: true
                                    },
                                    valor: {
                                        required: true
                                    }
                                },
                                // define messages para cada campo
                                messages: {
                                    titulo: "Informe o título para o destaque",
                                    diasanuncio: "Informe a duração para o destaque",
                                    valor: "Informe o valor para o destaque"
                                }
                            });
                        });
                    </script>

                    <div class="widget-container">
                        <form name="destaques" id="destaques" class="form-horizontal" action="<?=$_SERVER["REQUEST_URI"];?>" method="post">
                            <div class="control-group">
                                <label for="nome" class="control-label">Título</label>
                                <div class="controls">

                                    <input name="id" id="id" type="hidden" value="<?=$dados['id'];?>">
                                    <input name="titulo" id="titulo" type="text" maxlength="50" class="span5" value="<?=$dados['titulo'];?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="nome" class="control-label">Descrição</label>
                                <div class="controls">
                                    <input name="descricao" id="descricao" type="text" maxlength="255" class="span5" value="<?=$dados['descricao'];?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="nome" class="control-label">Dias Plano 1</label>
                                <div class="controls">
                                    <input name="diasplano1" id="diasplano1" type="number" class="span5" value="<?=$dados['diasplano1'];?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="nome" class="control-label">Valor Plano 1</label>
                                <div class="controls">
                                    <input name="valorplano1" id="valorplano1" type="number" step="0.01" min="0" class="span5" value="<?=$dados['valorplano1'];?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="nome" class="control-label">Dias Plano 2</label>
                                <div class="controls">
                                    <input name="diasplano2" id="diasplano2" type="number" class="span5" value="<?=$dados['diasplano2'];?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="nome" class="control-label">Valor Plano 2</label>
                                <div class="controls">
                                    <input name="valorplano2" id="valorplano2" type="number" step="0.01" min="0" class="span5" value="<?=$dados['valorplano2'];?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="nome" class="control-label">Dias Plano 3</label>
                                <div class="controls">
                                    <input name="diasplano3" id="diasplano3" type="number" class="span5" value="<?=$dados['diasplano3'];?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="nome" class="control-label">Valor Plano 3</label>
                                <div class="controls">
                                    <input name="valorplano3" id="valorplano3" type="number" step="0.01" min="0" class="span5" value="<?=$dados['valorplano3'];?>">
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" id="Bt" class="btn btn-success">Salvar</button>
                                <button type="button" onclick="window.open('<?=$prefix;?>destaques', '_self');" class="btn right-float">Voltar</button>
                            </div>
                        </form>
                    </div>
                </div>
<?php
include 'inc_footer.php';
