<?php

namespace Intranet;


/**
 * Class Authentication
 * @package Intranet
 */
class Authentication
{
    protected static $instace;

    /**
     * @return Authentication
     */
    protected function __construct()
    {
        if (!isset($_SESSION)) {
            session_start();
        }
        if (!isset($_SESSION[session_id()]['intra'])) {
            $_SESSION[session_id()] = ["intra" => []];
        }
    }

    /**
     * @return Authentication
     */
    public static function getInstance()
    {
        if (!(self::$instace instanceof self)) {
            self::newSelf();
        }
        return self::$instace;
    }

    /**
     * @return boolean
     */
    public function isAuthenticated()
    {
        if (isset($_SESSION[session_id()]['intra']) and !empty($_SESSION[session_id()]['intra']) and isset($_SESSION[session_id()]['intra']['entity'])) {
            return true;
        }
		//PEGA URL
		$dominio= $_SERVER['HTTP_HOST'];
		$url = "http://" . $dominio. $_SERVER['REQUEST_URI'];
		$_SESSION['paginaAnterior'] = $url;
        return false;
    }

    protected static function newSelf () {
        self::$instace = new self();
    }

    /**
     * @param string $user
     * @param string $name
     * @param string $email
     */
    public function authenticate($user, $name, $email, $entity)
    {
        if (!isset($_SESSION)) {
            self::newSelf();
        }
        $arrayBase = ["user" => $user, "name" => $name, "email" => $email, "entity" => $entity];
        foreach ($arrayBase as $key => $val)
        $this->putInSession($key, $val);
    }

    public function wipe()
    {
        session_destroy();
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getFromSession($key)
    {
        if (isset($_SESSION[session_id()]['intra'][$key])) {
            return unserialize($_SESSION[session_id()]['intra'][$key]);
        }

        return null;
    }

    /**
     * @param $key
     * @param $value
     */
    public function putInSession($key, $value)
    {
        $_SESSION[session_id()]['intra'][$key] = serialize($value);
    }
}
