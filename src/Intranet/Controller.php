<?php
/**
 * User: vieirasantosn
 * Date: 25/07/14
 * Time: 11:56
 */

namespace Intranet;

use CoffeeCore\Storage\DoctrineStorage;


/**
 * Class Controller
 * @package Intranet
 */
class Controller extends \CoffeeCore\Core\AbstractController
{
    /**
     * @param \Fatura\Entity|integer $fatura
     * @return void|string
     * @throws \Exception
     */
    public function criarPagamento($fatura)
    {
        try {

            if (empty($fatura)) {
                throw new \InvalidArgumentException("Fatura não informada ou incorreta.");
            }
            if (!($fatura instanceof \Fatura\Entity)) {
                /** @var \Fatura\Entity $fatura */
                $fatura = DoctrineStorage::orm()->getRepository(\Fatura\Entity::FULL_NAME)
                    ->findOneBy(['id' => $fatura]);
            }

            \PagSeguroLibrary::init();
            \PagSeguroConfig::init();
            \PagSeguroResources::init();

            $paymentRequest = new \PagSeguroPaymentRequest();

            // Sets the currency
            $paymentRequest->setCurrency("BRL");

            /** @var \FaturaPlano\Entity $plano */
            //$plano = DoctrineStorage::orm()->getRepository(\FaturaPlano\Entity::FULL_NAME)->findOneBy(['fatura' => $fatura->getId()]);
            $plano = DoctrineStorage::orm()->getRepository(\FaturaPlano\Entity::FULL_NAME)->findOneBy(['anuncio' => $fatura->getAnuncio()]);
            if (empty($plano)) {
                if ($plano->getValor() > 0) {
                    $paymentRequest->addItem($plano->getId(), "Plano de Anúncio: {$plano->getDescricao()} ({$plano->getDiascontratados()} dias) ", 1,
                        $plano->getValor());
                }
            }

            /** @var \FaturaDestaque\Entity $destaque */
            $destaquesFatura = DoctrineStorage::orm()->getRepository(\FaturaDestaque\Entity::FULL_NAME)->findBy(['fatura' => $fatura->getId()]);

            //print_r($destaquesFatura);
            foreach ($destaquesFatura as $destaque) {
                if ($destaque->getValor() > 0) {
                    $paymentRequest->addItem(9000 + (int)$destaque->getId(),
                        "Plano de Destaque: {$destaque->getDescricao()} ({$destaque->getDiascontratados()} dias) ", 1, $destaque->getValor());
                }
            }

            $paymentRequest->setShippingCost(0);

            // Sets a reference code for this payment request, it is useful to identify this payment in future notifications.
            $paymentRequest->setReference($fatura->getId());

            $paymentRequest->setShippingType(\PagSeguroShippingType::getCodeByType('NOT_SPECIFIED'));

            /** @var \Usuario\Entity $usuario */
            $usuario = DoctrineStorage::orm()->getRepository(\Usuario\Entity::FULL_NAME)
                ->findOneBy(['id' => $fatura->getUsuario()]);

            // Sets your customer information.
            $paymentRequest->setSender($usuario->getNome(), $usuario->getEmail(), null, null, 'CPF',
                $usuario->getCpf());
            $paymentRequest->addSenderDocument('id', $usuario->getId());


            // Another way to set checkout parameters
            $paymentRequest->addParameter('notificationURL', url_base() . "/retorno-pagseguro");


            //$crendentials = new \PagSeguroAccountCredentials('vieirasantosn@gmail.com', '913E6EAA9F9D4BEABF9CF1BC51A46A5D');


            $crendentials = \PagSeguroConfig::getAccountCredentials();

            $fatura->setCodigocheckout($paymentRequest->register($crendentials, true));

            DoctrineStorage::orm()->merge($fatura);
            DoctrineStorage::orm()->flush();

            return  $fatura->getCodigocheckout();


        } catch (\PagSeguroServiceException $e) {
            die($e->getMessage());
        }
    }
}
