<?php

namespace Intranet;

use Slim\Middleware;

/**
 * Class MidAuthentcate
 * @package Intranet
 */
class MidAuthentcate extends Middleware
{
    public function call ()
    {

        $auth = Authentication::getInstance();
        if (!$auth->isAuthenticated()) {
			//PEGA URL
			//session_start();
			$dominio= $_SERVER['HTTP_HOST'];
			$url = "http://" . $dominio. $_SERVER['REQUEST_URI'];
			$_SESSION['paginaAnterior'] = $url;
					
            $this->app->response()->redirect("/login/?paginaAnterior=".$url);
        }
        $this->next->call();
    }
}
