<?php require_once('../inc_head.php'); ?>
<title>Destaques Ativos | Ofertas Agrícolas - Classificados do Agronegócio</title>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
        _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
        $.src='//v2.zopim.com/?2THRePqyXmvi9IJOn1UUzHhxwhGHNEhH';z.t=+new Date;$.
            type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
</head>
<body>
<?php require_once('../inc_header.php'); ?>
<!-- .container -->
<div class="container row">
    <div class="off-canvas-wrap">
        <div class="inner-wrap">
            <nav class="tab-bar show-for-medium-down">
                <section class="left-small"><a class="left-off-canvas-toggle menu-icon" ><span></span></a></section>
                <section class="middle tab-bar-section"></section>
            </nav>
            <aside class="left-off-canvas-menu">
                <?php include('inc_sidebar_intranet.php'); ?>
            </aside>
            <!-- .main-section -->
            <section class="main-section">
                <!-- CONTEUDO .large-9 .medium-12 .small-12 -->
                <div class="">
                    <!-- .large-9 .medium-12 .small-12 -->
                    <div class="large-9 medium-12 small-12 columns">
                        <!-- .box-interno -->
                        <article class="box-interno">
                            <!-- .titulo-interno -->
                            <h2 class="titulo-interno">Destaques Ativos</h2>
                            <!-- .historico-de-destaques -->
                            <div class="historico-de-faturas">
                                <table>
                                    <thead>
                                    <tr>
                                        <th width="50" align="center">ID</th>
                                        <th>Destaque</th>
                                        <th>Anúncio</th>
                                        <th align="center">Início</th>
                                        <th align="center">Vencimento</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (count($getDestaques) > 0) {
                                        $i = 1;
                                        /** @var \AnuncioDestaque\Entity $destaque */
                                        foreach ($getDestaques as $destaque) {
                                            $anuncio = \CoffeeCore\Storage\DoctrineStorage::orm()->getRepository(Anuncio\Entity::FULL_NAME)->findOneBy(["id" => $destaque->getAnuncio()]);
                                            ?>
                                            <tr class="<?php if ($i%2==1) { ?>cinza<?php } ?>">
                                                <td align="center"><?=$destaque->getId();?></td>
                                                <td><?=$destaque->getDestaqueTitulo();?></td>
                                                <td><a href="/anuncio/<?=$anuncio->getId();?>/<?=$anuncio->getDatainclusao()->format('Y/m/d');?>/<?=create_slug($anuncio->getTitulo());?>"><?=$anuncio->getTitulo();?></a></td>
                                                <td align="center"><?=$destaque->getDatainicio()->format('d/m/Y');?></td>
                                                <td align="center"><?=$destaque->getDatavencimento()->format('d/m/Y');?></td>
                                            </tr>
                                        <?php
                                            $i++;
                                        }
                                    } else {
                                        echo "<tr><td align='center' colspan='6'>Nenhum Destaque Ativo</td></tr>";
                                    }

                                    ?>
                                    </tbody>
                                </table>
                            </div><!-- /.historico-de-destaques -->
                            <?php
                                $totalDeRegistros = $totalRegistros;
                                $regitrosPorPagina = 20;
                                $maximoPaginasNaPaginacao = 5;
								if($totalDeRegistros > $regitrosPorPagina){
							?>
                            <!-- .paginacao -->
                            <div class="paginacao">
                                <?php
                                $paginaAtual = $getPagina;
                                $totalDePaginas = ceil($totalDeRegistros / $regitrosPorPagina);
                                $paginator = (new \CoffeeCore\Helper\Paginator)
                                    ->setUrl("/minha-conta/destaques-ativos", "/pagina/(:num)")
                                    ->setItems($totalDeRegistros, $regitrosPorPagina, $maximoPaginasNaPaginacao)
                                    ->setPrevNextTitle("«", "»")->setFirstLastTitle("««","»»")->setPage($paginaAtual);
                                echo $paginator->toHtml();
                                ?>
                            </div><!-- /.paginacao -->
                            <?php }else{ ?>
                            <br /><br /><br /><br />
                            <?php } ?>
                        </article><!-- /.box-interno -->
                    </div><!-- /.large-9 /.medium-12 /.small-12 -->
                </div><!-- /CONTEUDO /.large-9 /.medium-12 /.small-12 -->
            </section><!-- /.main-section -->
            <a class="exit-off-canvas"></a>
        </div><!--/innerWrapp-->
    </div><!--/offCanvasWrap-->
</div><!-- /.container -->
<?php require_once('../inc_footer.php'); ?>
</body>
</html>