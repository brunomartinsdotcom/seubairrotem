<?php
$pagina = "minha-conta";
require_once('../inc_head.php');
?>
<title>Intranet | <?=$dadosDoSite['titulo'];?> - <?=$dadosDoSite['slogan'];?></title>
</head>
<body>
<?php require_once('../inc_header.php'); ?>
<div class="categorias-titulo-azul hide-for-medium-down"></div>
<section class="row">
    <div class="">
        <div class="off-canvas-wrap" data-offcanvas>
            <div class="inner-wrap">
                <?php require_once('inc_sidebar.php'); ?>
                <!-- CONTEUDO .large-9 .medium-12 .small-12 -->
                <div class="large-9 medium-12 small-12 columns">
                    <h2 class="titulo-azul">Intranet</h2>
                    <div class="box-branco">
                        <br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
                        <p class="large-12 text-center columns">Seja bem vindo à sua intranet administrativa</p>
                        <br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
                    </div><!-- /.box-branco -->
                    <?php require_once('../inc_bannerandfacebook.php'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php require_once('../inc_footer.php'); ?>
</body>
</html>