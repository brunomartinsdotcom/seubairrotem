<?php require_once('../inc_head.php'); ?>
<title>Finalizar Anúncio | Ofertas Agrícolas - Classificados do Agronegócio</title>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
        _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
        $.src='//v2.zopim.com/?2THRePqyXmvi9IJOn1UUzHhxwhGHNEhH';z.t=+new Date;$.
            type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
</head>
<body>
<?php require_once('../inc_header.php'); ?>
<!-- .container -->
<script type="text/javascript" src="https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js"></script>
<div class="container row">
	<div class="off-canvas-wrap">
		<div class="inner-wrap">                
			<nav class="tab-bar show-for-medium-down">
				<section class="left-small"><a class="left-off-canvas-toggle menu-icon" ><span></span></a></section>
				<section class="middle tab-bar-section"></section>
			</nav>
			<aside class="left-off-canvas-menu">
				<?php include('inc_sidebar_intranet.php'); ?>
			</aside>
			<!-- .main-section -->
			<section class="main-section">
				<!-- CONTEUDO .large-9 .medium-12 .small-12 -->
				<div class="">
					<!-- .large-9 .medium-12 .small-12 -->
					<div class="large-9 medium-12 small-12 columns">
						<!-- .box-interno -->
						<article class="box-interno">
							<!-- .titulo-interno -->
							<h2 class="titulo-interno">Finalizar Anúncio</h2>
                            <?php
                            /** @var \Destaque\Entity $paginaInicial */
                            /** @var \Plano\Entity $plano */

                            $paginaInicial = \CoffeeCore\Storage\DoctrineStorage::orm()
                                ->getRepository(\Destaque\Entity::FULL_NAME)
                                ->findOneBy(['tipo' => 'pagina-inicial']);

                            $carrossel = \CoffeeCore\Storage\DoctrineStorage::orm()
                                ->getRepository(\Destaque\Entity::FULL_NAME)
                                ->findOneBy(['tipo' => 'carossel-listagem']);

                            $primeirasPosicoes = \CoffeeCore\Storage\DoctrineStorage::orm()
                                ->getRepository(\Destaque\Entity::FULL_NAME)
                                ->findOneBy(['tipo' => 'primeira-posicao']);

                            $oferta = \CoffeeCore\Storage\DoctrineStorage::orm()
                                ->getRepository(\Destaque\Entity::FULL_NAME)
                                ->findOneBy(['tipo' => 'oferta']);
                            ?>
                            <!-- .finalizar-anuncio -->
                            <div class="finalizar-anuncio">
                                <!--<form action="/minha-conta/adicionar-anuncio/finalizar" method="post" enctype="multipart/form-data">-->

                                <table>
                                	<thead>
                                    	<tr>
                                        	<th width="50">Item</th>
                                            <th>Descrição</th>
                                            <th align="center">Valor</th>
                                      	</tr>
                                    </thead>
                                    <tbody>
                                      	<tr class="cinza">
                                    		<td>1</td>
                                            <td>Criação de Anúncio</td>
                                            <td align="center">
                                                <select required name="plano" id="plano">
                                                    <option data-type="<?= $paginaInicial->getTipo();?>" data-value="0" value="">Selecione</option>
                                                    <?php
                                                    $planos = \CoffeeCore\Storage\DoctrineStorage::orm()
                                                        ->getRepository(Plano\Entity::FULL_NAME)
                                                        ->findBy([]);
                                                    $planoSelecionado = \Intranet\Authentication::getInstance()->getFromSession('novoAnuncioPlano');

                                                    foreach ($planos as $plano) {
                                                        echo '<option data-type="<?= $paginaInicial->getTipo();?>" data-value="'.$plano->getValor().'" value="'.$plano->getId().'"
                                                        '.(($planoSelecionado == $plano->getId()) ? " selected" : " ").'>'.$plano->getTitulo().'</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                    		<td>2</td>
                                            <td><strong><?=$paginaInicial->getTitulo();?></strong> - <?=$paginaInicial->getDescricao();?></td>
                                            <td align="center">
                                            	<select id="<?=$paginaInicial->getTipo();?>" class="destaques" name="planos_pagina_inicial">
                                                    <option data-type="<?= $paginaInicial->getTipo();?>" data-value="0" value="">Nenhum</option>
                                                    <option data-type="<?= $paginaInicial->getTipo();?>" data-value="<?= $paginaInicial->getValorplano1();?>"
                                                            value="1"><?= $paginaInicial->getDiasplano1() . ' dias - ' .
                                                        number_format($paginaInicial->getValorplano1(), 2, ',', '.') ;?></option>
                                                    <option data-type="<?= $paginaInicial->getTipo();?>" data-value="<?= $paginaInicial->getValorplano2();?>"
                                                            value="2"><?= $paginaInicial->getDiasplano2() . ' dias - ' .
                                                        number_format($paginaInicial->getValorplano2(), 2, ',', '.') ;?></option>
                                                    <option data-type="<?= $paginaInicial->getTipo();?>" data-value="<?= $paginaInicial->getValorplano3();?>"
                                                            value="3"><?= $paginaInicial->getDiasplano3() . ' dias - ' .
                                                        number_format($paginaInicial->getValorplano3(), 2, ',', '.') ;?></option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr class="cinza">
                                    		<td>3</td>
                                            <td><strong><?=$carrossel->getTitulo();?></strong> - <?=$carrossel->getDescricao() ;?></td>
                                            <td align="center">
                                            	<select id="<?=$carrossel->getTipo();?>" class="destaques" name="planos_carousel">
                                                    <option data-type="<?= $carrossel->getTipo();?>" data-value="0" value="">Nenhum</option>
                                                    <option data-type="<?= $carrossel->getTipo();?>" data-value="<?= $carrossel->getValorplano1();?>"
                                                            value="1"><?= $carrossel->getDiasplano1() . ' dias - ' .
                                                        number_format($carrossel->getValorplano1(), 2, ',', '.') ;?></option>
                                                    <option data-type="<?= $carrossel->getTipo();?>" data-value="<?= $carrossel->getValorplano2();?>"
                                                            value="2"><?= $carrossel->getDiasplano2() . ' dias - ' .
                                                        number_format($carrossel->getValorplano2(), 2, ',', '.') ;?></option>
                                                    <option data-type="<?= $carrossel->getTipo();?>" data-value="<?= $carrossel->getValorplano3();?>"
                                                            value="3"><?= $carrossel->getDiasplano3() . ' dias - ' .
                                                        number_format($carrossel->getValorplano3(), 2, ',', '.') ;?></option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                    		<td>4</td>
                                            <td><strong><?=$primeirasPosicoes->getTitulo()?></strong> - <?=$primeirasPosicoes->getDescricao();?></td>
                                            <td align="center">
                                            	<select id="<?=$primeirasPosicoes->getTipo();?>" class="destaques" name="planos_primeiras_posicoes">
                                                    <option data-type="<?= $primeirasPosicoes->getTipo();?>" data-value="0" value="">Nenhum</option>
                                                    <option data-type="<?= $primeirasPosicoes->getTipo();?>" data-value="<?= $primeirasPosicoes->getValorplano1();?>"
                                                            value="1"><?= $primeirasPosicoes->getDiasplano1() . ' dias - ' .
                                                        number_format($primeirasPosicoes->getValorplano1(), 2, ',', '.') ;?></option>
                                                    <option data-type="<?= $primeirasPosicoes->getTipo();?>" data-value="<?= $primeirasPosicoes->getValorplano2();?>"
                                                            value="2"><?= $primeirasPosicoes->getDiasplano2() . ' dias - ' .
                                                        number_format($primeirasPosicoes->getValorplano2(), 2, ',', '.') ;?></option>
                                                    <option data-type="<?= $primeirasPosicoes->getTipo();?>" data-value="<?= $primeirasPosicoes->getValorplano3();?>"
                                                            value="3"><?= $primeirasPosicoes->getDiasplano3() . ' dias - ' .
                                                        number_format($primeirasPosicoes->getValorplano3(), 2, ',', '.') ;?></option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr class="cinza">
                                    		<td>5</td>
                                            <td><strong><?=$oferta->getTitulo();?></strong> - <?=$oferta->getDescricao();?></td>
                                            <td align="center">
                                            	<select id="<?= $oferta->getTipo();?>" class="destaques" name="planos_oferta">
                                                    <option data-type="<?= $oferta->getTipo();?>" data-value="0" value="">Nenhum</option>
                                                    <option data-type="<?= $oferta->getTipo();?>" data-value="<?= $oferta->getValorplano1();?>"
                                                            value="1"><?= $oferta->getDiasplano1() . ' dias - ' .
                                                        number_format($oferta->getValorplano1(), 2, ',', '.') ;?></option>
                                                    <option data-type="<?= $oferta->getTipo();?>" data-value="<?= $oferta->getValorplano2();?>"
                                                            value="2"><?= $oferta->getDiasplano2() . ' dias - ' .
                                                        number_format($oferta->getValorplano2(), 2, ',', '.') ;?></option>
                                                    <option data-type="<?= $oferta->getTipo();?>" data-value="<?= $oferta->getValorplano3();?>"
                                                            value="3"><?= $oferta->getDiasplano3() . ' dias - ' .
                                                        number_format($oferta->getValorplano3(), 2, ',', '.') ;?></option>
                                                </select>
                                            </td>
                                        </tr>
                                    <tbody>
                                    <thead class="total">
                                    	<tr>
                                        	<th colspan="2" style="text-align:right;">Total</th>
                                            <th>R$ <span id="totalFatura">0,00</span></th>
                                        </tr>
                                    </thead>
                                </table>
                                <div class="clear"></div>
                                <p class="l100 text-right"><br />
                                    <button id="bt-finalizar" name="bt-finalizar" type="submit">FINALIZAR ANÚNCIO</button>
                                </p>
                                <!--</form>-->
                                <script>
                                    function fmtMoney(n, c, d, t){
                                        var m = (c = Math.abs(c) + 1 ? c : 2, d = d || ",", t = t || ".",
                                            /(\d+)(?:(\.\d+)|)/.exec(n + "")), x = m[1].length > 3 ? m[1].length % 3 : 0;
                                        return (x ? m[1].substr(0, x) + t : "") + m[1].substr(x).replace(/(\d{3})(?=\d)/g,
                                            "$1" + t) + (c ? d + (+m[2] || 0).toFixed(c).substr(2) : "");
                                    };


                                    function trocaValor() {
                                        var total = 0;
                                        $(".finalizar-anuncio select").each(function() {
                                            var selected = $(this).find("option:selected");

                                            total = parseFloat(total) + parseFloat(selected.attr('data-value'));
                                        });

                                        $('#totalFatura').html(fmtMoney(total));
                                    };

                                $(document).ready(function () {

                                    var idfatura;

                                    <?php
                                    $destaques = \Intranet\Authentication::getInstance()->getFromSession('destaques');
                                    foreach ($destaques as $k => $destaque) {
                                    ?>
                                        $("select#<?=$k;?>").val(<?=$destaque;?>);
                                    <?php
                                    }
                                    ?>

                                    trocaValor();

                                    $(".finalizar-anuncio select").change(function(){
                                        trocaValor();
                                    });



                                    $("#bt-finalizar").click(function () {
                                        var destaques = [];

                                        $("select.destaques").each(function () {
                                            var option = $(this).find("option:selected");
                                            var type = $(option).attr('data-type');
                                            if ($(this).val().length > 0)
                                                destaques.push(type + '|' + $(this).val());
                                        });

                                        var destaquesString = JSON.stringify(destaques);

                                        $("#bt-finalizar").attr("disabled", "disabled");

                                        $.ajax({
                                            type: "POST",
                                            url: "/minha-conta/adicionar-anuncio/finalizar",
                                            data: {
                                                'plano': $("#plano").val(),
                                                'destaques': destaquesString
                                            },
                                            dataType: 'json'
                                        }).done(function (retorno) {
                                            if (retorno.totalFatura > 0) {
                                                PagSeguroLightbox({
                                                    code: retorno.checkoutCode
                                                }, {
                                                    success : function(transactionCode) {
                                                        $.ajax({
                                                            type: "POST",
                                                            url: "/minha-conta/gravar-codigo-pagseguro",
                                                            data: {
                                                                'fatura': retorno.idFatura,
                                                                'transactionCode': transactionCode
                                                            }

                                                        }).done(function(){
                                                            $("#bt-finalizar").removeAttr("disabled");
                                                            window.location ='/minha-conta/adicionar-anuncio/obrigado';
                                                        })
                                                    },
                                                    abort : function() {
                                                        alert("Você não completou o processo de criação da fatura.");
                                                        $("#bt-finalizar").removeAttr("disabled");
                                                    }
                                                });
                                            } else {
                                                $.ajax({
                                                    type: "POST",
                                                    url: "/minha-conta/gravar-codigo-pagseguro",
                                                    data: {
                                                        'fatura': retorno.idFatura,
                                                        'transactionCode': 'GRATUITO'
                                                    }

                                                }).done(function(){
                                                    $("#bt-finalizar").removeAttr("disabled");
                                                    window.location ='/minha-conta/adicionar-anuncio/obrigado';
                                                })
                                            }

                                        }).fail(function () {
                                            $("#bt-finalizar").removeAttr("disabled");
                                            alert("Houve um erro ao realizar o pagamento.");
                                        })
                                    })
                                })
                                </script>
                            </div><!-- /.historico-de-faturas -->
						</article><!-- /.box-interno -->
					</div><!-- /.large-9 /.medium-12 /.small-12 -->
				</div><!-- /CONTEUDO /.large-9 /.medium-12 /.small-12 -->
			</section><!-- /.main-section -->
			<a class="exit-off-canvas"></a>
		</div><!--/innerWrapp-->
	</div><!--/offCanvasWrap-->
</div><!-- /.container -->
<?php require_once('../inc_footer.php'); ?>
</body>
</html>