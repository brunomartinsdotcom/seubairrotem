<?php
$pagina = "guia-comercial";
require_once('../inc_head.php');
?>
<title>Intranet Guia Comercial | <?=$dadosDoSite['titulo'];?> - <?=$dadosDoSite['slogan'];?></title>
</head>
<body>
<?php require_once('../inc_header.php'); ?>
<div class="categorias-titulo-azul hide-for-medium-down"></div>
<section class="row">
    <div class="">
        <div class="off-canvas-wrap" data-offcanvas>
            <div class="inner-wrap">
                <?php require_once('inc_sidebar.php'); ?>
                <!-- CONTEUDO .large-9 .medium-12 .small-12 -->
                <div class="large-9 medium-12 small-12 columns">
                    <h2 class="titulo-azul">Guia Comercial</h2>
                    <div class="box-branco">
                        <!-- .historico-de-faturas -->
                        <div class="historico-de-faturas">
                            <table>
                                <thead>
                                    <tr>
                                        <th width="50" align="center">ID</th>
                                        <th>Empresa</th>
                                        <th>Cidade/UF</th>
                                        <th>Validade do Anúncio</th>
                                        <th width="16" align="center">Ed</th>
                                        <th width="16" align="center">Ca</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        if (count($getAnuncios) > 0) {
                                            $i = 1;
                                            /** @var \Anuncio\Entity $anuncio */
                                            foreach ($getAnuncios as $anuncio) {
                                    ?>
                                    <tr class="<?php if ($i%2==1) { ?>cinza<?php } ?>">
                                        <td align="center"><?=$anuncio->getId();?></td>
                                        <td><a href="/guia-comercial/<?=$anuncio->getId();?>/<?=$anuncio->getDatainclusao()->format('Y/m/d');?>/<?=create_slug($anuncio->getEmpresa());?>"><?=$anuncio->getEmpresa();?></a></td>
                                        <td><?=$anuncio->getNomeCidade() .'/'. $anuncio->getNomeEstado() ;?></td>
                                        <td><?=(!empty($anuncio->getDatavalidade())) ? $anuncio->getDatavalidade()->format('d/m/Y') : "";?></td>
                                        <td align="center">
                                            <?=(isset($getAcaoEditar))?"<a href='{$getAcaoEditar['url']}{$anuncio->getId()}' title='{$getAcaoEditar['texto']}'>{$getAcaoEditar['img']}</a>" : '';?>
                                        </td>
                                        <td align="center">
                                            <?=(isset($getAcao))?"<a href='{$getAcao['url']}{$anuncio->getId()}' title='{$getAcao['texto']}'>{$getAcao['img']}</a>" : '';?>
                                        </td>
                                    </tr>
                                    <?php
                                                $i++;
                                            }
                                        } else {
                                            echo "<tr><td align='center' colspan='6'>Nenhum anúncio cadastrado!</td></tr>";
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div><!-- /.historico-de-faturas -->
                        <?php
                            $totalDeRegistros = $totalRegistros;
                            $regitrosPorPagina = 20;
                            $maximoPaginasNaPaginacao = 5;
                            if($totalDeRegistros > $regitrosPorPagina){
                        ?>
                        <!-- .paginacao -->
                        <div class="paginacao">
                            <?php
                                $paginaAtual = $getPagina;
                                $totalDePaginas = ceil($totalDeRegistros / $regitrosPorPagina);
                                $paginator = (new \CoffeeCore\Helper\Paginator)
                                    ->setUrl("/minha-conta/guia-comercial", "/pagina/(:num)")
                                    ->setItems($totalDeRegistros, $regitrosPorPagina, $maximoPaginasNaPaginacao)
                                    ->setPrevNextTitle("«", "»")->setFirstLastTitle("««","»»")->setPage($paginaAtual);
                                echo $paginator->toHtml();
                            ?>
                        </div><!-- /.paginacao -->
                        <?php }else{ ?>
                            <br /><br /><br /><br />
                        <?php } ?>
                    </div><!-- /.box-branco -->
                    <?php require_once('../inc_bannerandfacebook.php'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php require_once('../inc_footer.php'); ?>
</body>
</html>