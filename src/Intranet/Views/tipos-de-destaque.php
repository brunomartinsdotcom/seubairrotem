<?php require_once('../inc_head.php'); ?>
<title>Tipos de Destaque | Ofertas Agrícolas - Classificados do Agronegócio</title>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
        _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
        $.src='//v2.zopim.com/?2THRePqyXmvi9IJOn1UUzHhxwhGHNEhH';z.t=+new Date;$.
            type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
</head>
<body>
<?php require_once('../inc_header.php'); ?>
<!-- .container -->
<div class="container row">
	<div class="off-canvas-wrap">
		<div class="inner-wrap">                
			<nav class="tab-bar show-for-medium-down">
				<section class="left-small"><a class="left-off-canvas-toggle menu-icon" ><span></span></a></section>
				<section class="middle tab-bar-section"></section>
			</nav>
			<aside class="left-off-canvas-menu">
				<?php include('inc_sidebar_intranet.php'); ?>
			</aside>
			<!-- .main-section -->
			<section class="main-section">
				<!-- CONTEUDO .large-9 .medium-12 .small-12 -->
				<div class="">
					<!-- .large-9 .medium-12 .small-12 -->
					<div class="large-9 medium-12 small-12 columns">
						<!-- .box-interno -->
						<article class="box-interno">
							<!-- .titulo-interno -->
							<h2 class="titulo-interno">Tipos de Destaque</h2>
							<p class="large-12 medium-12 small-12 padding-box-interno font12">Confiram as formas de destaque que oferecemos para seu anúncio sair na frente!</p>
                            <div class="tipos-de-anuncio">
                                <?php
                                $destaquesRepo = \CoffeeCore\Storage\DoctrineStorage::orm()
                                    ->getRepository(Destaque\Entity::FULL_NAME);
                                /** @var Destaque\Entity $pInicial */
                                $pInicial = $destaquesRepo->findOneBy(["tipo" => "pagina-inicial"]);
                                $carrossel = $destaquesRepo->findOneBy(["tipo" => "carossel-listagem"]);
                                $pPosicao = $destaquesRepo->findOneBy(["tipo" => "primeira-posicao"]);
                                $oferta = $destaquesRepo->findOneBy(["tipo" => "oferta"]);
                                ?>

                            	<p class="large-6 medium-6 small-12 columns">
                                    <strong><?=$pInicial->getTitulo()?></strong><br /> <em><?=$pInicial->getDescricao()?></em>
                                    <br />
                                    R$ <?=number_format($pInicial->getValorplano1());?> - <?=$pInicial->getDiasplano1();?> dias
                                    <br />
                                    R$ <?=number_format($pInicial->getValorplano2());?> - <?=$pInicial->getDiasplano2();?> dias
                                    <br />
                                    R$ <?=number_format($pInicial->getValorplano3());?> - <?=$pInicial->getDiasplano3();?> dias
                                    <br />
                                    <img src="<?=url_base();?>/images/destaques/pagina-inicial.jpg" />
                                </p>
                                <p class="large-6 medium-6 small-12 columns">
                                    <strong><?=$carrossel->getTitulo();?></strong><br /> <em><?=$carrossel->getDescricao();?></em>
                                    <br />
                                    R$ <?=number_format($carrossel->getValorplano1());?> - <?=$carrossel->getDiasplano1();?> dias
                                    <br />
                                    R$ <?=number_format($carrossel->getValorplano2());?> - <?=$carrossel->getDiasplano2();?> dias
                                    <br />
                                    R$ <?=number_format($carrossel->getValorplano3());?> - <?=$carrossel->getDiasplano3();?> dias
                                    <br />
                                    <img src="<?=url_base();?>/images/destaques/carousel.jpg" />
                                </p>
                                <p class="large-6 medium-6 small-12 columns">
                                    <strong><?=$pPosicao->getTitulo();?></strong><br /> <em><?=$pPosicao->getDescricao();?></em>
                                    <br />
                                    R$ <?=number_format($pPosicao->getValorplano1());?> - <?=$pPosicao->getDiasplano1();?> dias
                                    <br />
                                    R$ <?=number_format($pPosicao->getValorplano2());?> - <?=$pPosicao->getDiasplano2();?> dias
                                    <br />
                                    R$ <?=number_format($pPosicao->getValorplano3());?> - <?=$pPosicao->getDiasplano3();?> dias
                                    <img src="<?=url_base();?>/images/destaques/primeiras-posicoes.jpg" />
                                </p>
                                <p class="large-6 medium-6 small-12 columns">
                                    <strong><?=$oferta->getTitulo()?></strong><br /> <em><?=$oferta->getDescricao();?></em>
                                    <br />
                                    R$ <?=number_format($oferta->getValorplano1());?> - <?=$oferta->getDiasplano1();?> dias
                                    <br />
                                    R$ <?=number_format($oferta->getValorplano2());?> - <?=$oferta->getDiasplano2();?> dias
                                    <br />
                                    R$ <?=number_format($oferta->getValorplano3());?> - <?=$oferta->getDiasplano3();?> dias
                                    <img src="<?=url_base();?>/images/destaques/oferta.jpg" />
                                </p>
                            </div><!-- /.tipos-de-anuncio -->
						</article><!-- /.box-interno -->
					</div><!-- /.large-9 /.medium-12 /.small-12 -->
				</div><!-- /CONTEUDO /.large-9 /.medium-12 /.small-12 -->
			</section><!-- /.main-section -->
			<a class="exit-off-canvas"></a>
		</div><!--/innerWrapp-->
	</div><!--/offCanvasWrap-->
</div><!-- /.container -->
<?php require_once('../inc_footer.php'); ?>
</body>
</html>