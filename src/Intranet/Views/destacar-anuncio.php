<?php require_once('../inc_head.php'); ?>
<title>Destacar Anúncio | Ofertas Agrícolas - Classificados do Agronegócio</title>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
        _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
        $.src='//v2.zopim.com/?2THRePqyXmvi9IJOn1UUzHhxwhGHNEhH';z.t=+new Date;$.
            type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
</head>
<body>
<?php require_once('../inc_header.php'); ?>
<!-- .container -->
<div class="container row">
	<div class="off-canvas-wrap">
		<div class="inner-wrap">                
			<nav class="tab-bar show-for-medium-down">
				<section class="left-small"><a class="left-off-canvas-toggle menu-icon" ><span></span></a></section>
				<section class="middle tab-bar-section"></section>
			</nav>
			<aside class="left-off-canvas-menu">
				<?php include('inc_sidebar_intranet.php'); ?>
			</aside>
			<!-- .main-section -->
			<section class="main-section">
				<!-- CONTEUDO .large-9 .medium-12 .small-12 -->
				<div class="">
					<!-- .large-9 .medium-12 .small-12 -->
					<div class="large-9 medium-12 small-12 columns">
						<!-- .box-interno -->
						<article class="box-interno">
							<!-- .titulo-interno -->
							<h2 class="titulo-interno">Destacar Anúncio</h2>
							<p class="large-12 medium-12 small-12 padding-box-interno font12">Quer ter mais visibilidade? Escolha formas de destaque para seu anúncio sair na frente!</p>
                            <form name="form-destacar-anuncio" id="form-destacar-anuncio" action="/minha-conta/adicionar-anuncio/destacar" method="post" enctype="multipart/form-data">
                            	<h3>Destaque seu anúncio</h3>

                                <?php
                                /** @var \Destaque\Entity $paginaInicial*/

                                $paginaInicial = \CoffeeCore\Storage\DoctrineStorage::orm()
                                    ->getRepository(\Destaque\Entity::FULL_NAME)
                                    ->findOneBy(['tipo' => 'pagina-inicial']);

                                $carrossel = \CoffeeCore\Storage\DoctrineStorage::orm()
                                    ->getRepository(\Destaque\Entity::FULL_NAME)
                                    ->findOneBy(['tipo' => 'carossel-listagem']);

                                $primeirasPosicoes = \CoffeeCore\Storage\DoctrineStorage::orm()
                                    ->getRepository(\Destaque\Entity::FULL_NAME)
                                    ->findOneBy(['tipo' => 'primeira-posicao']);

                                $oferta = \CoffeeCore\Storage\DoctrineStorage::orm()
                                    ->getRepository(\Destaque\Entity::FULL_NAME)
                                    ->findOneBy(['tipo' => 'oferta']);

                                ?>

                                <p class="large-6 medium-6 small-12 columns">
                                	<label>
                                        <strong><?=$paginaInicial->getTitulo();?></strong> <br /> <?=$paginaInicial->getDescricao();?>
                                    </label>
                                    <select name="planos_pagina_inicial">
                                        <option value="">Nenhum</option>
                                        <option value="1"><?= $paginaInicial->getDiasplano1() . ' dias - ' .
                                            number_format($paginaInicial->getValorplano1(), 2, ',', '.') ;?></option>
                                        <option value="2"><?= $paginaInicial->getDiasplano2() . ' dias - ' .
                                            number_format($paginaInicial->getValorplano2(), 2, ',', '.') ;?></option>
                                        <option value="3"><?= $paginaInicial->getDiasplano3() . ' dias - ' .
                                            number_format($paginaInicial->getValorplano3(), 2, ',', '.') ;?></option>
                                    </select>
                                    <img src="/images/destaques/pagina-inicial.jpg" />
                                </p>
                                <p class="large-6 medium-6 small-12 columns">
                                	<label>
                                        <strong><?=$carrossel->getTitulo();?></strong> <br /> <?=$carrossel->getDescricao() ;?>
                                    </label>
                                    <select name="planos_carousel">
                                        <option value="">Nenhum</option>
                                        <option value="1"><?= $carrossel->getDiasplano1() . ' dias - ' .
                                            number_format($carrossel->getValorplano1(), 2, ',', '.') ;?></option>
                                        <option value="2"><?= $carrossel->getDiasplano2() . ' dias - ' .
                                            number_format($carrossel->getValorplano2(), 2, ',', '.') ;?></option>
                                        <option value="3"><?= $carrossel->getDiasplano3() . ' dias - ' .
                                            number_format($carrossel->getValorplano3(), 2, ',', '.') ;?></option>
                                    </select>
                                    <img src="/images/destaques/carousel.jpg" />
                                </p>
                                <p class="large-6 medium-6 small-12 columns">
                                	<label>
                                        <strong><?=$primeirasPosicoes->getTitulo()?></strong> <br /> <?=$primeirasPosicoes->getDescricao();?>
                                    </label>
                                    <select name="planos_primeiras-posicoes">
                                        <option value="">Nenhum</option>
                                        <option value="1"><?= $primeirasPosicoes->getDiasplano1() . ' dias - ' .
                                            number_format($primeirasPosicoes->getValorplano1(), 2, ',', '.') ;?></option>
                                        <option value="2"><?= $primeirasPosicoes->getDiasplano2() . ' dias - ' .
                                            number_format($primeirasPosicoes->getValorplano2(), 2, ',', '.') ;?></option>
                                        <option value="3"><?= $primeirasPosicoes->getDiasplano3() . ' dias - ' .
                                            number_format($primeirasPosicoes->getValorplano3(), 2, ',', '.') ;?></option>
                                    </select>
                                    <img src="/images/destaques/primeiras-posicoes.jpg" />
                                    
                                </p>
                                <p class="large-6 medium-6 small-12 columns">
                                	<label>
                                        <strong><?=$oferta->getTitulo();?></strong> <br /> <?=$oferta->getDescricao();?>
                                    </label>
                                    <select name="planos_oferta">
                                        <option value="">Nenhum</option>
                                        <option value="1"><?= $oferta->getDiasplano1() . ' dias - ' .
                                            number_format($oferta->getValorplano1(), 2, ',', '.') ;?></option>
                                        <option value="2"><?= $oferta->getDiasplano2() . ' dias - ' .
                                            number_format($oferta->getValorplano2(), 2, ',', '.') ;?></option>
                                        <option value="3"><?= $oferta->getDiasplano3() . ' dias - ' .
                                            number_format($oferta->getValorplano3(), 2, ',', '.') ;?></option>
                                    </select>
                                    <img src="/images/destaques/oferta.jpg" />
                                </p>
                                <div class="clear"></div>
                                <p class="l100 text-right"><br />
                                    <!--<button name="bt-finalizar" type="submit">Finalizar</button>-->
                                    <button name="bt-finalizar" type="submit">Finalizar</button>
                                </p>
                            </form>
						</article><!-- /.box-interno -->
					</div><!-- /.large-9 /.medium-12 /.small-12 -->
				</div><!-- /CONTEUDO /.large-9 /.medium-12 /.small-12 -->
			</section><!-- /.main-section -->
			<a class="exit-off-canvas"></a>
		</div><!--/innerWrapp-->
	</div><!--/offCanvasWrap-->
</div><!-- /.container -->
<?php require_once('../inc_footer.php'); ?>
</body>
</html>