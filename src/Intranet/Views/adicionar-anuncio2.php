<?php require_once('../inc_head.php'); ?>
<title>Adicionar Anúncio | Ofertas Agrícolas - Classificados do Agronegócio</title>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
        _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
        $.src='//v2.zopim.com/?2THRePqyXmvi9IJOn1UUzHhxwhGHNEhH';z.t=+new Date;$.
            type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
</head>
<body>
<?php require_once('../inc_header.php'); ?>
<!-- .container -->
<div class="container row">
	<div class="off-canvas-wrap">
		<div class="inner-wrap">                
			<nav class="tab-bar show-for-medium-down">
				<section class="left-small"><a class="left-off-canvas-toggle menu-icon" ><span></span></a></section>
				<section class="middle tab-bar-section"></section>
			</nav>
			<aside class="left-off-canvas-menu">
				<?php include('inc_sidebar_intranet.php'); ?>
			</aside>
			<!-- .main-section -->
			<section class="main-section">
				<!-- CONTEUDO .large-9 .medium-12 .small-12 -->
				<div class="">
					<!-- .large-9 .medium-12 .small-12 -->
					<div class="large-9 medium-12 small-12 columns">
						<!-- .box-interno -->
						<article class="box-interno">
							<!-- .titulo-interno -->
							<h2 class="titulo-interno">Adicionar Anúncio</h2>
							<p class="large-12 medium-12 small-12 padding-box-interno font12">Envie mais fotos! Além de enriquecer seu anúncio, os interessados verão com mais detalhes o produto que você está anunciando.<br /><br /><strong>Envie no máximo 19 fotos.</strong></p>
                            <form name="form-add-anuncio" id="form-add-anuncio" action="/minha-conta/adicionar-anuncio/galeria" method="post" enctype="multipart/form-data">
                            	<h3>Envie mais fotos</h3>
                                <div class="clear"></div>
                                <div id="advancedUpload">Selecione...</div>
                                <div id="status"></div>
                                <script>
                                    $(document).ready(function()
                                    {
                                        uploadObj = $("#advancedUpload").uploadFile({
                                            url: "/minha-conta/adicionar-anuncio/galeria/upload/",
                                            maxFileSize:1024*1024*8,
                                            multiple:true,
                                            fileName: "myfile",
                                            allowedTypes:"jpg,jpeg,png,gif",
                                            returnType:"json",
                                            /*onSuccess:*/
											afterUploadAll: function(files,data,xhr){
                                                $("#form-add-anuncio").submit();
												// alert((data));
                                            },
                                            showDelete:false,
											autoSubmit:false
                                        });
										$("#startUpload").click(function(){
											uploadObj.startUpload();
										});
                                    });
                                </script>
                                <div class="clear"></div>
                                <p class="l100 text-right"><br />
                                	<button name="bt-cadastrar" id="startUpload" class="left" type="button">Enviar Fotos</button>
                                    <button name="bt-passo-tres" id="bt-passo-tres" class="right" type="submit">Pular Etapa</button>
                                    <br><br>
                                </p>
                            </form>
						</article><!-- /.box-interno -->
					</div><!-- /.large-9 /.medium-12 /.small-12 -->
				</div><!-- /CONTEUDO /.large-9 /.medium-12 /.small-12 -->
			</section><!-- /.main-section -->
			<a class="exit-off-canvas"></a>
		</div><!--/innerWrapp-->
	</div><!--/offCanvasWrap-->
</div><!-- /.container -->
<link href="/css/uploadfile.css" rel="stylesheet">
<script src="/js/jquery.uploadfile.min.js"></script>
<?php require_once('../inc_footer.php'); ?>
</body>
</html>