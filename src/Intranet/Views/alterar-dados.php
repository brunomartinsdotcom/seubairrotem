<?php
$pagina = "meus-dados";
require_once('../inc_head.php');
/** @var \UsuariosDoSite\Entity $usuario */
$dadosdosite = Intranet\Authentication::getInstance()->getFromSession('entity');
?>
<title>Meus Dados | <?=$dadosDoSite['titulo'];?> - <?=$dadosDoSite['slogan'];?></title>
</head>
<body>
<?php require_once('../inc_header.php'); ?>
<div class="categorias-titulo-azul hide-for-medium-down"></div>
<section class="row">
    <div class="">
        <div class="off-canvas-wrap" data-offcanvas>
            <div class="inner-wrap">
                <?php require_once('inc_sidebar.php'); ?>
                <!-- CONTEUDO .large-9 .medium-12 .small-12 -->
                <div class="large-9 medium-12 small-12 columns">
                    <h2 class="titulo-azul">Meus Dados</h2>
                    <div class="box-branco">
                        <?php if (isset($flash["error"]) and !empty($flash["error"])){ ?>
                        <div data-alert class="alert-box alert radius">
                            <?=$flash["error"];?>
                            <a href="#" class="close">&times;</a>
                        </div>
                        <?php }else{ ?>
                        <p>Você poderá alterar aqui seus dados de cadastro.</p>
                        <?php } ?>
                        <form name="form-cadastre-se" id="form-cadastre-se" action="<?=$_SERVER["REQUEST_URI"];?>" method="post" enctype="multipart/form-data">
                            <h3>Dados Pessoais</h3>
                            <div class="clear"></div>
                            <p>
                                <input name="tipopessoa" type="hidden" value="<?=$dadosdosite->getTipopessoa();?>" />
                                <label class="pc50 left"><input id="tipo_pessoa" name="tipo_pessoa" type="radio" value="1" disabled<?php if ($dadosdosite->getTipopessoa() == 1) { ?> checked<?php } ?> />Pessoa Física</label>
                                <label class="pc50 left"><input id="tipo_pessoa" name="tipo_pessoa" type="radio" value="2" disabled<?php if ($dadosdosite->getTipopessoa() == 2) { ?> checked<?php } ?> />Pessoa Jurídica</label>
                            </p>
                            <?php if ($dadosdosite->getTipopessoa() == 1) { ?>
                            <p class="pessoa1">
                                <label for="nome">Nome Completo</label>
                                <input name="nome" id="nome" type="text" class="l70" maxlength="100" value="<?= $dadosdosite->getNome(); ?>" />
                            </p>
                            <div class="clear"></div>
                            <p class="pessoa1">
                                <label for="cpf">CPF</label>
                                <input name="cpf" id="cpf" type="text" class="left l40" placeholder="___.___.___-__" data-mask="000.000.000-00" maxlength="14" value="<?= $dadosdosite->getCpfmask(); ?>" disabled />
                            </p>
                            <div class="clear"></div>
                            <p class="pessoa1">
                                <label for="rg">RG</label>
                                <input name="rg" id="rg" type="text" class="left l40" maxlength="30" value="<?=$dadosdosite->getRg();?>" disabled />
                            </p>
                            <div class="clear"></div>
                            <p class="pessoa1">
                                <label for="datadenascimento">Data de Nascimento</label>
                                <input name="datadenascimento" id="datadenascimento" type="text" class="l40" placeholder="__/__/____" data-mask="00/00/0000" maxlength="10" value="<?=$dadosdosite->getDatadenascimento()->format("d/m/Y");?>" />
                            </p>
                            <?php }else if ($dadosdosite->getTipopessoa() == 2) { ?>
                            <div class="clear"></div>
                            <p class="pessoa2">
                                <label for="razaosocial">Razão Social</label>
                                <input name="razaosocial" id="razaosocial" type="text" class="l80" maxlength="100" value="<?=$dadosdosite->getRazaosocial();?>" />
                            </p>
                            <div class="clear"></div>
                            <p class="pessoa2">
                                <label for="nomefantasia">Nome Fantasia</label>
                                <input name="nomefantasia" id="nomefantasia" type="text" class="l60" maxlength="100" value="<?=$dadosdosite->getNomefantasia();?>" />
                            </p>
                            <div class="clear"></div>
                            <p class="pessoa2">
                                <label for="cnpj">CNPJ</label>
                                <input name="cnpj" id="cnpj" type="text" class="left l40" placeholder="__.___.___/____-__" data-mask="00.000.000/0000-00" maxlength="18" value="<?=$dadosdosite->getCnpjmask();?>" disabled /><em class="left l50 padding-box-interno font10 margin-top15">Não fazemos consulta do CNPJ e nem fornecemos a outros usuários</em>
                            </p>
                            <div class="clear"></div>
                            <p class="pessoa2">
                                <label for="inscricaoestadual">Inscrição Estadual</label>
                                <input name="inscricaoestadual" id="inscricaoestadual" type="text" class="left l40" maxlength="20" value="<?=$dadosdosite->getInscricaoestadual();?>" disabled /><em class="left l20 padding-box-interno font10 margin-top15">Opcional</em>
                            </p>
                            <div class="clear"></div>
                            <p class="pessoa2">
                                <label for="nomedoresponsavel">Nome do Responsável</label>
                                <input name="nomedoresponsavel" id="nomedoresponsavel" type="text" class="l70" maxlength="100" value="<?=$dadosdosite->getNomedoresponsavel();?>" />
                            </p>
                            <?php } ?>
                            <div class="clear"></div>
                            <p>
                                <label for="email">E-mail</label>
                                <input name="email" id="email" type="text" class="left l70" maxlength="100" value="<?=$dadosdosite->getEmail();?>" /><em class="left l30 margin-top10 padding-box-interno font10">Cadastre seu e-mail mais usado, em caso de contato é por ele que você será avisado.</em>
                            </p>
                            <div class="clear"></div>
                            <h3>Endereço</h3>
                            <div class="clear"></div>
                            <p>
                                <label for="cep">CEP</label>
                                <input name="cep" id="cep" type="text" class="left l30" placeholder="_____-___" data-mask="00000-000" maxlength="9" value="<?=$dadosdosite->getCep();?>" /><em class="left l70 padding-box-interno font10 margin-top15">Não sabe seu CEP? <a href="http://www.buscacep.correios.com.br" target="_blank" title="Não sabe seu CEP? Clique aqui" class="font-10">Clique aqui</a></em>
                            </p>
                            <div class="clear"></div>
                            <p class="left l70">
                                <label for="endereco">Endereço</label>
                                <input name="endereco" id="endereco" type="text" placeholder="Avenida, Rua, Viela, etc..." maxlength="200" value="<?=$dadosdosite->getEndereco();?>" />
                            </p>
                            <p class="right l25">
                                <label for="numero">Número</label>
                                <input name="numero" id="numero" type="text" maxlength="15" value="<?=$dadosdosite->getNumero();?>" />
                            </p>
                            <div class="clear"></div>
                            <p class="left l45">
                                <label for="bairro">Bairro</label>
                                <input name="bairro" id="bairro" type="text" maxlength="50" value="<?=$dadosdosite->getBairro();?>" />
                            </p>
                            <p class="right l50">
                                <label for="complemento">Complemento</label>
                                <input name="complemento" id="complemento" type="text" placeholder="Quadra, Lote, Apartamento, etc..." maxlength="100" value="<?=$dadosdosite->getComplemento();?>" />
                            </p>
                            <div class="clear"></div>
                            <p class="left l30">
                                <label for="estado">Estado</label>
                                <select name="estado" id="estado">
                                    <option value="">Selecione</option>
                                    <?php
                                    $estados = \CoffeeCore\Helper\ResourceManager::readFile("estados.json");
                                    foreach ($estados as $k => $estado) {
                                        echo "<option " . (($estado["name"] == $dadosdosite->getEstado()) ? ' selected ' : '') . " value='{$estado["name"]}'>{$estado["description"]}</option>";
                                    }
                                    ?>
                                </select>
                            </p>
                            <p class="right l65">
                                <label for="cidade">Cidade</label>
                                <select name="cidade" id="cidade">
                                    <option value=''>Escolha o Estado primeiro...</option>
                                </select>
                            </p>
                            <div class="clear"></div>
                            <h3>Contato</h3>
                            <div class="clear"></div>
                            <p class="left l45">
                                <label for="celular">Celular</label>
                                <input name="celular" id="celular" type="text" placeholder="(__) ____-____" data-mask="(00) 0000-00009" maxlength="15" value="<?=$dadosdosite->getCelular();?>" />
                            </p>
                            <p class="right l45">
                                <label for="telefone">Telefone</label>
                                <input name="telefone" id="telefone" type="text" placeholder="(__) ____-____" data-mask="(00) 0000-0000" maxlength="14" value="<?=$dadosdosite->getTelefone();?>" />
                            </p>
                            <div class="clear clearfix"></div>
                            <h3>Dados para Acesso</h3>
                            <div class="clear"></div>
                            <p>
                                <label for="login">Login/Apelido</label>
                                <input name="login" id="login" type="text" class="left l40" maxlength="20" value="<?=$dadosdosite->getLogin();?>" /><em class="left l60 padding-box-interno font10 margin-top15">Não é permitido caracteres especiais ou letras maiúsculas.</em>
                            </p>
                            <div class="clear"></div>
                            <p>
                                <label for="senha">Nova Senha</label>
                                <input name="senha" id="senha" type="password" class="left l40"  maxlength="12" placeholder="Somente se quiser mudar a senha" /><em class="left l60 padding-box-interno font10 margin-top10">A senha deve ter entre 6 e 12 letras ou números. Não é permitido caracteres especiais. O sistema faz diferenciação entre maiúsculas e minúsculas.</em>
                            </p>
                            <div class="clear"></div>
                            <p>
                                <label for="re_senha">Repetir Senha</label>
                                <input name="re_senha" id="re_senha" type="password" class="left l40" maxlength="12" placeholder="Repita sua senha" />
                            </p>
                            <div class="clear"></div>
                            <p class="l100 text-right">
                                <button name="bt-cadastrar" type="submit">Alterar</button>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php require_once('../inc_footer.php'); ?>
<script>
    $(document).ready(function() {
        $('#datadenascimento').mask('00/00/0000');
        $('#cep').mask('00000-000');
        $('#celular').mask('(00) 0000-00009');
        $('#telefone').mask('(00) 0000-0000');

        $("#form-cadastre-se").validate({
            rules : {
                email: {
                    required: true,
                    email: true,
                    minlength: 6
                },
                cep: {
                    required: true,
                    cep: true
                },
                endereco: {
                    required: true
                },
                numero: {
                    required: true
                },
                bairro: {
                    required: true
                },
                estado: {
                    required: true
                },
                cidade: {
                    required: true
                },
                celular: {
                    required: true
                },
                login: {
                    //pattern: /[a-z0-9]+/,
                    required: true,
                    alpha: true
                },
                senha: {
                    minlength: 6,
                    maxlength: 12
                },
                re_senha : {
                    minlength: 6,
                    maxlength: 12,
                    equalTo: '#senha'
                }
            }
        });

        if ($("#estado").val().length > 0) {
            var uf = $("#estado").val();
            $.ajax({
                url: "/cidades/" + uf.toLowerCase() ,
                dataType: "json",
                beforeSend : function () {
                    if (uf.length > 0) {
                        $("#cidade").html("<option value=''>Selecione</option>");
                    } else {
                        $("#cidade").html("<option value=''>Escolha o estado</option>");
                    }
                    $("#cidade").attr("disabled", true);

                }

            }).done(function(result){
                $.each(result, function (key, value) {
                    $("#cidade").append("<option value='"+ value.id +"'>" + value.title + "</option>")
                });
                $("#cidade").attr("disabled", false);
                <?php
                if (!empty($dadosdosite->getCidade())) {
                    echo '$("#cidade").val("'. $dadosdosite->getCidade() .'");';
                }
                ?>
            });
        }

        $("#estado").change(function(){
            var uf = $(this).val();
            $.ajax({
                url: "/cidades/" + uf.toLowerCase() ,
                dataType: "json",
                method: "get",
                beforeSend : function () {
                    if (uf.length > 0) {
                        $("#cidade").html("<option value=''>Selecione</option>");
                    } else {
                        $("#cidade").html("<option value=''>Escolha o Estado</option>");
                    }
                    $("#cidade").attr("disabled", true);

                }
            }).done(function(result){
                $.each(result, function (key, value) {
                    $("#cidade").append("<option value='"+ value.id +"'>" + value.title + "</option>")
                });
                $("#cidade").attr("disabled", false);
            });
        });
    });
</script>
</body>
</html>