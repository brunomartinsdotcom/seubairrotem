<?php require_once('../inc_head.php'); ?>
<title>Destacar Anúncio | Ofertas Agrícolas - Classificados do Agronegócio</title>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
        _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
        $.src='//v2.zopim.com/?2THRePqyXmvi9IJOn1UUzHhxwhGHNEhH';z.t=+new Date;$.
            type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
</head>
<body>
<?php require_once('../inc_header.php'); ?>
<script type="text/javascript" src="https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js"></script>
<!-- .container -->
<div class="container row">
	<div class="off-canvas-wrap">
		<div class="inner-wrap">                
			<nav class="tab-bar show-for-medium-down">
				<section class="left-small"><a class="left-off-canvas-toggle menu-icon" ><span></span></a></section>
				<section class="middle tab-bar-section"></section>
			</nav>
			<aside class="left-off-canvas-menu">
				<?php include('inc_sidebar_intranet.php'); ?>
			</aside>
			<!-- .main-section -->
			<section class="main-section">
				<!-- CONTEUDO .large-9 .medium-12 .small-12 -->
				<div class="">
					<!-- .large-9 .medium-12 .small-12 -->
					<div class="large-9 medium-12 small-12 columns">
						<!-- .box-interno -->
						<article class="box-interno">
							<!-- .titulo-interno -->
							<h2 class="titulo-interno">Destacar Anúncio</h2>
							<p class="large-12 medium-12 small-12 padding-box-interno font12">Quer ter mais visibilidade? Escolha formas de destaque para seu anúncio sair na frente!</p>
                            <form name="form-destacar-anuncio" id="form-destacar-anuncio" action="/minha-conta/destacar-anuncio" method="post" enctype="multipart/form-data">
                            	<h3>Destaque seu anúncio</h3>

                                <?php
                                /** @var \Destaque\Entity $paginaInicial*/

                                $paginaInicial = \CoffeeCore\Storage\DoctrineStorage::orm()
                                    ->getRepository(\Destaque\Entity::FULL_NAME)
                                    ->findOneBy(['tipo' => 'pagina-inicial']);

                                $carrossel = \CoffeeCore\Storage\DoctrineStorage::orm()
                                    ->getRepository(\Destaque\Entity::FULL_NAME)
                                    ->findOneBy(['tipo' => 'carossel-listagem']);

                                $primeirasPosicoes = \CoffeeCore\Storage\DoctrineStorage::orm()
                                    ->getRepository(\Destaque\Entity::FULL_NAME)
                                    ->findOneBy(['tipo' => 'primeira-posicao']);

                                $oferta = \CoffeeCore\Storage\DoctrineStorage::orm()
                                    ->getRepository(\Destaque\Entity::FULL_NAME)
                                    ->findOneBy(['tipo' => 'oferta']);

                                ?>

                                <p class="large-12 medium-12 small-12 columns">
                                    <label>
                                        <strong>Anúncios Ativos</strong>
                                    </label>
                                    <select required name="anuncio" id="anuncio">
                                        <option value="">Selecione</option>
                                        <?php
                                        foreach ($getAnunciosAtivos as $anuncio) {
                                            echo "<option value='{$anuncio->getId()}'>{$anuncio->getTitulo()}</option>";
                                        }
                                        ?>

                                    </select>
                                </p>


                                <p class="large-6 medium-6 small-12 columns">
                                	<label>
                                        <strong><?=$paginaInicial->getTitulo();?></strong> <br /> <?=$paginaInicial->getDescricao();?>
                                    </label>
                                    <select id="<?=$paginaInicial->getTipo();?>" class="destaques" name="planos_pagina_inicial">
                                        <option data-type="<?= $paginaInicial->getTipo();?>" data-value="0" value="">Nenhum</option>
                                        <option data-type="<?= $paginaInicial->getTipo();?>" data-value="<?= $paginaInicial->getValorplano1();?>"
                                                value="1"><?= $paginaInicial->getDiasplano1() . ' dias - ' .
                                            number_format($paginaInicial->getValorplano1(), 2, ',', '.') ;?></option>
                                        <option data-type="<?= $paginaInicial->getTipo();?>" data-value="<?= $paginaInicial->getValorplano2();?>"
                                                value="2"><?= $paginaInicial->getDiasplano2() . ' dias - ' .
                                            number_format($paginaInicial->getValorplano2(), 2, ',', '.') ;?></option>
                                        <option data-type="<?= $paginaInicial->getTipo();?>" data-value="<?= $paginaInicial->getValorplano3();?>"
                                                value="3"><?= $paginaInicial->getDiasplano3() . ' dias - ' .
                                            number_format($paginaInicial->getValorplano3(), 2, ',', '.') ;?></option>
                                    </select>
                                    <img src="/images/destaques/pagina-inicial.jpg" />
                                </p>
                                <p class="large-6 medium-6 small-12 columns">
                                	<label>
                                        <strong><?=$carrossel->getTitulo();?></strong> <br /> <?=$carrossel->getDescricao() ;?>
                                    </label>
                                    <select id="<?=$carrossel->getTipo();?>" class="destaques" name="planos_carousel">
                                        <option data-type="<?= $carrossel->getTipo();?>" data-value="0" value="">Nenhum</option>
                                        <option data-type="<?= $carrossel->getTipo();?>" data-value="<?= $carrossel->getValorplano1();?>"
                                                value="1"><?= $carrossel->getDiasplano1() . ' dias - ' .
                                            number_format($carrossel->getValorplano1(), 2, ',', '.') ;?></option>
                                        <option data-type="<?= $carrossel->getTipo();?>" data-value="<?= $carrossel->getValorplano2();?>"
                                                value="2"><?= $carrossel->getDiasplano2() . ' dias - ' .
                                            number_format($carrossel->getValorplano2(), 2, ',', '.') ;?></option>
                                        <option data-type="<?= $carrossel->getTipo();?>" data-value="<?= $carrossel->getValorplano3();?>"
                                                value="3"><?= $carrossel->getDiasplano3() . ' dias - ' .
                                            number_format($carrossel->getValorplano3(), 2, ',', '.') ;?></option>
                                    </select>
                                    <img src="/images/destaques/carousel.jpg" />
                                </p>
                                <p class="large-6 medium-6 small-12 columns">
                                	<label>
                                        <strong><?=$primeirasPosicoes->getTitulo()?></strong> <br /> <?=$primeirasPosicoes->getDescricao();?>
                                    </label>
                                    <select id="<?=$primeirasPosicoes->getTipo();?>" class="destaques" name="planos_primeiras_posicoes">
                                        <option data-type="<?= $primeirasPosicoes->getTipo();?>" data-value="0" value="">Nenhum</option>
                                        <option data-type="<?= $primeirasPosicoes->getTipo();?>" data-value="<?= $primeirasPosicoes->getValorplano1();?>"
                                                value="1"><?= $primeirasPosicoes->getDiasplano1() . ' dias - ' .
                                            number_format($primeirasPosicoes->getValorplano1(), 2, ',', '.') ;?></option>
                                        <option data-type="<?= $primeirasPosicoes->getTipo();?>" data-value="<?= $primeirasPosicoes->getValorplano2();?>"
                                                value="2"><?= $primeirasPosicoes->getDiasplano2() . ' dias - ' .
                                            number_format($primeirasPosicoes->getValorplano2(), 2, ',', '.') ;?></option>
                                        <option data-type="<?= $primeirasPosicoes->getTipo();?>" data-value="<?= $primeirasPosicoes->getValorplano3();?>"
                                                value="3"><?= $primeirasPosicoes->getDiasplano3() . ' dias - ' .
                                            number_format($primeirasPosicoes->getValorplano3(), 2, ',', '.') ;?></option>
                                    </select>
                                    <img src="/images/destaques/primeiras-posicoes.jpg" />
                                    
                                </p>
                                <p class="large-6 medium-6 small-12 columns">
                                	<label>
                                        <strong><?=$oferta->getTitulo();?></strong> <br /> <?=$oferta->getDescricao();?>
                                    </label>
                                    <select id="<?= $oferta->getTipo();?>" class="destaques" name="planos_oferta">
                                        <option data-type="<?= $oferta->getTipo();?>" data-value="0" value="">Nenhum</option>
                                        <option data-type="<?= $oferta->getTipo();?>" data-value="<?= $oferta->getValorplano1();?>"
                                                value="1"><?= $oferta->getDiasplano1() . ' dias - ' .
                                            number_format($oferta->getValorplano1(), 2, ',', '.') ;?></option>
                                        <option data-type="<?= $oferta->getTipo();?>" data-value="<?= $oferta->getValorplano2();?>"
                                                value="2"><?= $oferta->getDiasplano2() . ' dias - ' .
                                            number_format($oferta->getValorplano2(), 2, ',', '.') ;?></option>
                                        <option data-type="<?= $oferta->getTipo();?>" data-value="<?= $oferta->getValorplano3();?>"
                                                value="3"><?= $oferta->getDiasplano3() . ' dias - ' .
                                            number_format($oferta->getValorplano3(), 2, ',', '.') ;?></option>
                                    </select>
                                    <img src="/images/destaques/oferta.jpg" />
                                </p>
                                <div class="clear clearfix"></div>
                                <p class="large-6 medium-6 small-6 columns">
                                    Total: R$ <span id="totalFatura">460,00</span>
                                </p>
                                <p class="l100 text-right"><br />
                                    <!--<button name="bt-finalizar" type="submit">Finalizar</button>-->
                                    <button name="bt-finalizar" type="submit">Finalizar</button>
                                </p>
                            </form>

                            <script>
                                function fmtMoney(n, c, d, t){
                                    var m = (c = Math.abs(c) + 1 ? c : 2, d = d || ",", t = t || ".",
                                        /(\d+)(?:(\.\d+)|)/.exec(n + "")), x = m[1].length > 3 ? m[1].length % 3 : 0;
                                    return (x ? m[1].substr(0, x) + t : "") + m[1].substr(x).replace(/(\d{3})(?=\d)/g,
                                            "$1" + t) + (c ? d + (+m[2] || 0).toFixed(c).substr(2) : "");
                                };

                                function existeDestaqueSelecionado () {
                                    var selecionado = false;
                                    $("form select.destaques").each(function() {
                                        if (!($(this).val()  == ''))
                                            selecionado = true;
                                    });
                                    return selecionado;
                                }

                                function trocaValor() {
                                    var total = 0;
                                    $("form select.destaques").each(function() {
                                        var selected = $(this).find("option:selected");

                                        total = parseFloat(total) + parseFloat(selected.attr('data-value'));
                                    });

                                    $('#totalFatura').html(fmtMoney(total));
                                };

                                $(document).ready(function () {

                                    trocaValor();

                                    $("form select.destaques").change(function(){
                                        trocaValor();
                                    });

                                    $("#form-destacar-anuncio").validate({
                                        // define regras para os campos
                                        rules: {
                                            planos_carousel: {
                                                required: function(element){
                                                    return !existeDestaqueSelecionado();
                                                }
                                            },
                                            planos_pagina_inicial: {
                                                required: function(element){
                                                    return !existeDestaqueSelecionado();
                                                }
                                            },
                                            planos_oferta: {
                                                required: function(element){
                                                    return !existeDestaqueSelecionado();
                                                }
                                            },
                                            planos_primeiras_posicoes: {
                                                required: function(element){
                                                    return !existeDestaqueSelecionado();
                                                }
                                            },
                                            anuncio: {
                                                required: true
                                            }
                                        },
                                        submitHandler: function(){
                                            var destaques = [];

                                            $("form select.destaques").each(function () {
                                                var option = $(this).find("option:selected");
                                                var type = $(option).attr('data-type');
                                                if ($(this).val().length > 0)
                                                    destaques.push(type + '|' + $(this).val());
                                            });

                                            var destaquesString = JSON.stringify(destaques);

                                            $.ajax({
                                                type: "POST",
                                                url: "/minha-conta/adicionar-anuncio/finalizar",
                                                data: {
                                                    'anuncio': $("#anuncio").val(),
                                                    'destaques': destaquesString
                                                },
                                                dataType: 'json'
                                            }).done(function (retorno) {
                                                PagSeguroLightbox({
                                                    code: retorno.checkoutCode
                                                }, {
                                                    success : function(transactionCode) {
                                                        $.ajax({
                                                            type: "POST",
                                                            url: "/minha-conta/gravar-codigo-pagseguro",
                                                            data: {
                                                                'fatura': retorno.idFatura,
                                                                'transactionCode': transactionCode
                                                            }

                                                        }).done(function(){
                                                            alert("Destaque realizado com sucesso.")
                                                            window.location ='/minha-conta/';
                                                        })
                                                    },
                                                    abort : function() {
                                                        alert("Você não completou o processo de criação da fatura.");
                                                    }
                                                });

                                            }).fail(function () {
                                                alert("Houve um erro ao realizar o pagamento.");
                                            })
                                        }
                                    });

                                })
                            </script>
						</article><!-- /.box-interno -->
					</div><!-- /.large-9 /.medium-12 /.small-12 -->
				</div><!-- /CONTEUDO /.large-9 /.medium-12 /.small-12 -->
			</section><!-- /.main-section -->
			<a class="exit-off-canvas"></a>


		</div><!--/innerWrapp-->
	</div><!--/offCanvasWrap-->
</div><!-- /.container -->
<?php require_once('../inc_footer.php'); ?>
</body>
</html>