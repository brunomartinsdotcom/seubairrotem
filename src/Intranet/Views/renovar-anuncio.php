<?php require_once('../inc_head.php'); ?>
<title>Renovar Anúncio | Ofertas Agrícolas - Classificados do Agronegócio</title>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
        _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
        $.src='//v2.zopim.com/?2THRePqyXmvi9IJOn1UUzHhxwhGHNEhH';z.t=+new Date;$.
            type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
</head>
<body>
<?php require_once('../inc_header.php'); ?>
<script type="text/javascript" src="https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js">
</script>
<!-- .container -->
<div class="container row">
	<div class="off-canvas-wrap">
		<div class="inner-wrap">                
			<nav class="tab-bar show-for-medium-down">
				<section class="left-small"><a class="left-off-canvas-toggle menu-icon" ><span></span></a></section>
				<section class="middle tab-bar-section"></section>
			</nav>
			<aside class="left-off-canvas-menu">
				<?php include('inc_sidebar_intranet.php'); ?>
			</aside>
			<!-- .main-section -->
			<section class="main-section">
				<!-- CONTEUDO .large-9 .medium-12 .small-12 -->
				<div class="">
					<!-- .large-9 .medium-12 .small-12 -->
					<div class="large-9 medium-12 small-12 columns">
						<!-- .box-interno -->
						<article class="box-interno">
                            <!-- .titulo-interno -->
                            <h2 class="titulo-interno">Renovar Anúncio</h2>
                            <form name="form-add-anuncio" id="form-add-anuncio" method="post" enctype="multipart/form-data">
                                <p>
                                    <label for="anuncio">Anúncios Expirados</label>
                                    <select required name="anuncio" id="anuncio">
                                        <option value="">Selecione</option>
                                        <?php
                                        foreach ($getAnunciosExpirados as $anuncio) {
                                            echo "<option value='{$anuncio->getId()}'>{$anuncio->getTitulo()}</option>";
                                        }
                                        ?>
                                    </select>
                                </p>
                                <br />
                                <p>
                                    <label for="plano">Planos</label>
                                    <select required name="plano" id="plano">
                                        <option value="">Selecione</option>
                                        <?php
                                        $planos = \CoffeeCore\Storage\DoctrineStorage::orm()->getRepository(\Plano\Entity::FULL_NAME)->findBy([]);
                                        /** @var \Plano\Entity $plano */
                                        foreach ($planos as $plano) {
                                            echo "<option value='{$plano->getId()}'>{$plano->getTitulo()} - {$plano->getDiasanuncio()}  dias - R$ {$plano->getValor(true)}</option>";
                                        }
                                        ?>
                                    </select>
                                </p>
                                <div class="clear clearfix"></div>
                                <p class="l100 text-right"><br />
                                    <!--<button name="bt-finalizar" type="submit">Finalizar</button>-->
                                    <button name="bt-finalizar" type="submit">Finalizar</button>
                                </p>
                            </form>

                            <script>
                                function fmtMoney(n, c, d, t){
                                    var m = (c = Math.abs(c) + 1 ? c : 2, d = d || ",", t = t || ".",
                                        /(\d+)(?:(\.\d+)|)/.exec(n + "")), x = m[1].length > 3 ? m[1].length % 3 : 0;
                                    return (x ? m[1].substr(0, x) + t : "") + m[1].substr(x).replace(/(\d{3})(?=\d)/g,
                                            "$1" + t) + (c ? d + (+m[2] || 0).toFixed(c).substr(2) : "");
                                }

                                $(document).ready(function () {

                                    $("#anuncio").val('<?=$getAnuncio;?>');

                                    $("#form-add-anuncio").validate({
                                        // define regras para os campos
                                        rules: {
                                            plano: {
                                                required: true
                                            },
                                            anuncio: {
                                                required: true
                                            }
                                        },
                                        submitHandler: function(){

                                            $.ajax({
                                                type: "POST",
                                                url: "/minha-conta/adicionar-anuncio/finalizar",
                                                data: {
                                                    'anuncio': $("#anuncio").val(),
                                                    'plano': $("#plano").val()
                                                },
                                                dataType: 'json'
                                            }).done(function (retorno) {
                                                if (retorno.totalFatura > 0) {
                                                    PagSeguroLightbox({
                                                        code: retorno.checkoutCode
                                                    }, {
                                                        success : function(transactionCode) {
                                                            $.ajax({
                                                                type: "POST",
                                                                url: "/minha-conta/gravar-codigo-pagseguro",
                                                                data: {
                                                                    'fatura': retorno.idFatura,
                                                                    'transactionCode': transactionCode
                                                                }

                                                            }).done(function(){
                                                                alert("Obrigado por renovar seu anuncio.");
                                                                window.location ='/minha-conta/';
                                                            })
                                                        },
                                                        abort : function() {
                                                            alert("Você não completou o processo de criação da fatura.");
                                                        }
                                                    });
                                                } else {
                                                    $.ajax({
                                                        type: "POST",
                                                        url: "/minha-conta/gravar-codigo-pagseguro",
                                                        data: {
                                                            'fatura': retorno.idFatura,
                                                            'transactionCode': 'GRATUITO'
                                                        }

                                                    })/*.done(function(){
                                                        alert("Obrigado por renovar seu anuncio.");
                                                        window.location ='/minha-conta/';
                                                    })*/
                                                }

                                            }).fail(function () {
                                                alert("Houve um erro ao realizar o pagamento.");
                                            })
                                        }
                                    });

                                })
                            </script>
						</article><!-- /.box-interno -->
					</div><!-- /.large-9 /.medium-12 /.small-12 -->
				</div><!-- /CONTEUDO /.large-9 /.medium-12 /.small-12 -->
			</section><!-- /.main-section -->
			<a class="exit-off-canvas"></a>


		</div><!--/innerWrapp-->
	</div><!--/offCanvasWrap-->
</div><!-- /.container -->
<?php require_once('../inc_footer.php'); ?>
</body>
</html>