<?php require_once('../inc_head.php'); ?>
<title>Minha Conta | Ofertas Agrícolas - Classificados do Agronegócio</title>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
        _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
        $.src='//v2.zopim.com/?2THRePqyXmvi9IJOn1UUzHhxwhGHNEhH';z.t=+new Date;$.
            type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
</head>
<body>
<?php require_once('../inc_header.php'); ?>
<!-- .container -->
<div class="container row">
	<div class="off-canvas-wrap">
		<div class="inner-wrap">                
			<nav class="tab-bar show-for-medium-down">
				<section class="left-small"><a class="left-off-canvas-toggle menu-icon" ><span></span></a></section>
				<section class="middle tab-bar-section"></section>
			</nav>
			<aside class="left-off-canvas-menu">
				<?php include('inc_sidebar_intranet.php'); ?>
			</aside>
			<!-- .main-section -->
			<section class="main-section">
				<!-- CONTEUDO .large-9 .medium-12 .small-12 -->
				<div class="">
					<!-- .large-9 .medium-12 .small-12 -->
					<div class="large-9 medium-12 small-12 columns">
						<!-- .box-interno -->
						<article class="box-interno">
							<!-- .titulo-interno -->
							<h2 class="titulo-interno">Alterar Meus Dados</h2>
							<p class="large-12 medium-12 small-12 padding-box-interno font12">Você poderá alterar aqui seus dados de cadastro.</p>
                            <form name="form-cadastre-se" id="form-cadastre-se" action="" method="post" enctype="multipart/form-data">
                            	<h3>Dados Pessoais</h3>
                                <p>
                                	<label class="pc50 left"><input name="tipo_pessoa" type="radio" value="1" />Pessoa Física</label>
                                    <label class="pc50 left"><input name="tipo_pessoa" type="radio" value="2" />Pessoa Jurídica</label>
                                </p>
                                <p class="pessoa1">
                                	<label for="nome_completo">Nome Completo</label>
                                    <input name="nome_completo" id="nome_completo" type="text" class="l80" />
                                </p>
                                <p class="pessoa1">
                                	<label for="cpf">CPF</label>
                                    <input disabled name="cpf" id="cpf" type="text" class="left l40" placeholder="___.___.___-__" data-mask="000.000.000-00" maxlength="14" /><em class="left l60 padding-box-interno font10 margin-top10">Não fazemos consulta do CPF e nem fornecemos a outros usuários</em>
                                </p>
                                <div class="clear"></div>
                                <p class="pessoa1">
                                	<label for="rg">RG</label>
                                    <input name="rg" id="rg" type="text" class="l40" placeholder="Número + Órgão Expedidor" />
                                </p>
                                <p class="pessoa2">
                                	<label for="razao_social">Razão Social</label>
                                    <input name="razao_social" id="razao_social" type="text" class="l80" />
                                </p>
                                <p class="pessoa2">
                                	<label for="nome_fantasia">Nome Fantasia</label>
                                    <input name="nome_fantasia" id="nome_fantasia" type="text" class="l60" />
                                </p>
                                <p class="pessoa2">
                                	<label for="cnpj">CNPJ</label>
                                    <input disabled name="cnpj" id="cnpj" type="text" class="left l50" placeholder="__.___.___/____-__" data-mask="00.000.000/0000-00" maxlength="18" /><em class="left l50 padding-box-interno font10 margin-top10">Não fazemos consulta do CNPJ e nem fornecemos a outros usuários</em>
                                </p>
                                <div class="clear"></div>
                                <p class="pessoa2">
                                	<label for="inscricao_estadual">Inscrição Estadual</label>
                                    <input name="inscricao_estadual" id="inscricao_estadual" type="text" class="l40" />
                                </p>
                                <p>
                                	<label for="email">E-mail</label>
                                    <input name="email" id="email" type="text" class="left l80" /><em class="left l20 padding-box-interno font10">Informe o e-mail que mais usa, você receberá notificações através dele.</em>
                                </p>
                                <div class="clear"></div>
                                <p>
                                	<label for="re-email">Repetir E-mail</label>
                                    <input name="re-email" id="re-email" type="text" class="l80" />
                                </p>
                                <p class="pessoa2">
                                	<label for="nome_responsavel">Nome do Responsável</label>
                                    <input name="nome_responsavel" id="nome_responsavel" type="text" class="l80" />
                                </p>
                                <p>
                                	<label for="data_nascimento">Data de Nascimento</label>
                                    <input name="data_nascimento" id="data_nascimento" type="text" class="l40" placeholder="__/__/____" data-mask="00/00/0000" maxlength="10" />
                                </p>
                                <p>
                                	<label for="email_1">E-mail Adicional 01</label>
                                    <input name="email_1" id="email_1" type="text" class="left l80" /><em class="left l20 padding-box-interno font10 margin-top10">Opcional</em>
                                </p>
                                <div class="clear"></div>
                                <p>
                                	<label for="email_2">E-mail Adicional 02</label>
                                    <input name="email_2" id="email_2" type="text" class="left l80" /><em class="left l20 padding-box-interno font10 margin-top10">Opcional</em>
                                </p>
                                <div class="clear"></div>
                                <h3>Endereço</h3>
                                <p>
                                	<label for="cep">CEP</label>
                                    <input name="cep" id="cep" type="text" class="left l40" placeholder="_____-___" data-mask="00000-000" maxlength="9" /><em class="left l60 padding-box-interno font10 margin-top10">Não sabe seu CEP? <a href="http://www.buscacep.correios.com.br" target="_blank" title="Não sabe seu CEP? Clique aqui" class="font-10">Clique aqui</a></em>
                                </p>
                                <div class="clear"></div>
                                <p>
                                	<p class="left l70">
                                		<label for="endereco">Endereço</label>
                                    	<input name="endereco" id="endereco" type="text" placeholder="Avenida, Rua, Viela, etc..." />
                                    </p>
                                    <p class="left l30">
                                		<label for="numero">Número</label>
                                    	<input name="numero" id="numero" type="text" />
                                    </p>
                                </p>
                                <p>
                                	<p class="left l60">
                                		<label for="bairro">Bairro</label>
                                    	<input name="bairro" id="bairro" type="text" />
                                    </p>
                                    <p class="left l40">
                                		<label for="complemento">Complemento</label>
                                    	<input name="complemento" id="complemento" type="text" placeholder="Quadra, Lote, Apartamento, etc..." />
                                    </p>
                                </p>
                                <p>
                                	<p class="left l30">
                                		<label for="estado">Estado</label>
                                        <select name="estado" id="estado">
                                        	<option>Selecione</option>
                                        </select>
                                    </p>
                                    <p class="left l70">
                                		<label for="cidade">Cidade</label>
                                    	<select name="cidade" id="cidade">
                                        	<option>Escolha primeiramente o Estado</option>
                                        </select>
                                    </p>
                                </p>
                                <h3>Contato</h3>
                                <p>
                                	<label for="telefone_1">Telefone</label>
                                    <input name="telefone_1" id="telefone_1" type="text" class="left l30" placeholder="(__) ____-____" data-mask="(00) 0000-0000" maxlength="14" /><em class="left l70 padding-box-interno font10 margin-top10">O telefone deve ser de linha fixa, não podendo ser celular ou móvel. Avisamos que este número será verificado</em>
                                </p>
                                <div class="clear"></div>
                                <p>
                                	<label for="telefone_2">Telefone 02</label>
                                    <input name="telefone_2" id="telefone_2" type="text" class="left l30" placeholder="(__) ____-____" data-mask="(00) 0000-00009" maxlength="15" /><em class="left l70 padding-box-interno font10 margin-top10">Opcional</em>
                                </p>
                                <div class="clear"></div>
                                <p>
                                	<label for="fax">Fax</label>
                                    <input name="fax" id="fax" type="text" class="left l30" placeholder="(__) ____-____" data-mask="(00) 0000-0000" maxlength="14" /><em class="left l70 padding-box-interno font10 margin-top10">Opcional</em>
                                </p>
                                <div class="clear"></div>
                                <p>
                                	<label for="celular">Celular</label>
                                    <input name="celular" id="celular" type="text" class="left l30" placeholder="(__) ____-____" data-mask="(00) 0000-00009" maxlength="15" />
                                </p>
                                <h3>Área de Atuação</h3>
                                <p>
                                	<label class="pc50 left"><input name="area_de_atuacao" type="radio" value="1" />Pecuarista</label>
                                    <label class="pc50 left"><input name="area_de_atuacao" type="radio" value="2" />Agricultor</label>
                                    <label class="pc50 left"><input name="area_de_atuacao" type="radio" value="3" />Corretor</label>
                                    <label class="pc50 left"><input name="area_de_atuacao" type="radio" value="4" />Empresário</label>
                                    <label class="pc50 left"><input name="area_de_atuacao" type="radio" value="5" />Veterinário</label>
                                    <label class="pc50 left"><input name="area_de_atuacao" type="radio" value="6" />Agrônomo</label>
                                    <label class="pc50 left"><input name="area_de_atuacao" type="radio" value="7" />Zootecnista</label>
                                    <label class="pc50 left"><input name="area_de_atuacao" type="radio" value="8" class="outro" />Outro</label>
                                    <div class="clear"></div>
                                    <p class="area_de_atuacao_outro hide">
                                    	<label class="l100" for="area_de_atuacao_outro">Qual?</label>
                                        <input name="area_de_atuacao_outro" id="area_de_atuacao_outro" type="text" placeholder="Informe qual sua área de atuação" />
                                    </p>
                                </p>
                                <h3>Dados para Acesso</h3>
                                <p>
                                	<label for="login">Login/Apelido</label>
                                    <input name="login" id="login" type="text" class="left l40" /><em class="left l60 padding-box-interno font10 margin-top10">Não aceitamos caracteres especiais. Proibido o uso de letras maiúsculas.</em>
                                </p>
                                <div class="clear"></div>
                                <p>
                                	<label for="senha">Senha</label>
                                    <input name="senha" id="senha" type="password" class="left l40" maxlength="10" /><em class="left l60 padding-box-interno font10 margin-top5">A senha deve ter entre 4 e 10 letras ou números. Não aceitamos caracteres especiais. O sistema faz diferenciação entre maiúsculas e minúsculas.</em>
                                </p>
                                <div class="clear"></div>
                                <p>
                                	<label for="re_senha">Repetir Senha</label>
                                    <input name="re_senha" id="re_senha" type="password" class="left l40" maxlength="10" />
                                </p>
                                <div class="clear"></div>
                                <p class="l100 text-right"><br />
                                	<button name="bt-cadastrar" type="submit">Cadastrar</button>
                                </p>
                            </form>
						</article><!-- /.box-interno -->
					</div><!-- /.large-9 /.medium-12 /.small-12 -->
				</div><!-- /CONTEUDO /.large-9 /.medium-12 /.small-12 -->
			</section><!-- /.main-section -->
			<a class="exit-off-canvas"></a>
		</div><!--/innerWrapp-->
	</div><!--/offCanvasWrap-->
</div><!-- /.container -->
<?php require_once('../inc_footer.php'); ?>
<script>
$(document).ready(function() {
    $('#form-cadastre-se').find('p.pessoa1').hide();
	$('#form-cadastre-se').find('p.pessoa2').hide();
	
	$('input[name=tipo_pessoa]').click(function(){
		if($(this).val() == 1){
			$('p.pessoa1').show();
			$('p.pessoa2').hide();
		}else{
			$('p.pessoa2').show();
			$('p.pessoa1').hide();
		}
	});
	$('input[name=area_de_atuacao]').click(function(){
		if($(this).attr('class') == 'outro'){
			$('p.area_de_atuacao_outro').show();
		}else{
			$('p.area_de_atuacao_outro').hide();
		}
	});
});
</script>
</body>
</html>