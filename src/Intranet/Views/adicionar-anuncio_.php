<?php require_once('../inc_head.php'); ?>
<title>Adicionar Anúncio | Ofertas Agrícolas - Classificados do Agronegócio</title>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
        _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
        $.src='//v2.zopim.com/?2THRePqyXmvi9IJOn1UUzHhxwhGHNEhH';z.t=+new Date;$.
            type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
</head>
<body>
<?php require_once('../inc_header.php'); ?>
<!-- .container -->
<div class="container row">
	<div class="off-canvas-wrap">
		<div class="inner-wrap">                
			<nav class="tab-bar show-for-medium-down">
				<section class="left-small"><a class="left-off-canvas-toggle menu-icon" ><span></span></a></section>
				<section class="middle tab-bar-section"></section>
			</nav>
			<aside class="left-off-canvas-menu">
				<?php include('inc_sidebar_intranet.php'); ?>
			</aside>
			<!-- .main-section -->
			<section class="main-section">
				<!-- CONTEUDO .large-9 .medium-12 .small-12 -->
				<div class="">
					<!-- .large-9 .medium-12 .small-12 -->
					<div class="large-9 medium-12 small-12 columns">
						<!-- .box-interno -->
						<article class="box-interno">
							<!-- .titulo-interno -->
							<h2 class="titulo-interno">Adicionar Anúncio</h2>
							<p class="large-12 medium-12 small-12 padding-box-interno font12">Anuncie seus produtos em alguns minutos!!! Comece escolhendo uma categoria abaixo.</p>
                            <?php
                            $planos = \CoffeeCore\Storage\DoctrineStorage::orm()
                                        ->getRepository(Plano\Entity::FULL_NAME)
                                        ->findBy([]);
                            ?>
                            <form name="form-add-anuncio" id="form-add-anuncio" action="/minha-conta/adicionar-anuncio/" method="post" enctype="multipart/form-data">
                            	<h3>Plano</h3>
                                <p>
                                	<label for="plano">Plano</label>
                                    <select name="plano" id="plano">
                                        <option value="">Escolha</option>
                                        <?php
                                        foreach ($planos as $plano) {
                                            echo '<option value="' . $plano->getId() . '">' . $plano->getTitulo() . '</option>';
                                        }
                                        ?>
                                    </select>
                                </p>
                                <h3>Segmento do Anúncio</h3>
                                <p>
                                	<label for="tipo">Tipo</label>
                                    <select name="tipo" id="tipo">
                                        <option value="">Escolha</option>
                                        <option value="1">Classificados</option>
                                        <option value="2">Serviços</option>
                                    </select>
                                </p>
                                <p class="margem-rodape">
                                	<label for="categoria">Categoria</label >
                                    <select name="categoria" id="categoria">
                                        <option value="">Escolha primeiramente o tipo</option>
                                    </select>
                                </p>
                                <p class="margem-rodape">
                                	<label for="sub-categoria">Sub-Categoria</label>
                                    <select name="sub-categoria" id="sub-categoria">
                                        <option value="">Escolha primeiramente o tipo e a categoria</option>
                                    </select>
                                </p>
                                <br />
                                <br />
                                <h3>Dados do Anúncio</h3>
                                <p class="aviso"><strong>ATENÇÃO!</strong><br /><small>Informamos que todo o anúncio deve possuir quantidade e valores corretos. Pedimos que não cadastre mais de um produto por anúncio e que não informe seus dados para contato, pois os interessados receberão o contato quando clicarem no botão "Fale com o Vendedor". Caso alguma dessas regras seja descumprida o anúncio será bloqueado, se houver reincidência o anúncio será excluído.</small></p>
                                <p>
                                	<label for="titulo">Título</label>
                                    <input name="titulo" id="titulo" type="text" class="left l80" maxlength="120" /><em class="left l20 padding-box-interno font10">Informe o título que aparecerá em seu anúncio.</em>
                                </p>
                                <div class="clear"></div>
                                <p>
                                	<label for="sub_titulo">Sub-Título</label>
                                    <input name="sub_titulo" id="sub_titulo" type="text" class="left l80" maxlength="255" /><em class="left l20 padding-box-interno font10">Coloque uma frase de efeito para atrair compradores.</em>
                                </p>
                                <div class="clear"></div>
                                <p>
                                	<label for="quantidade">Quantidade</label>
                                    <input name="quantidade" id="quantidade" type="text" maxlength="3" class="left l10" />
                                    <em class="left l20 padding-box-interno font10">Coloque a quantidade de produtos para a venda. (Máximo 999)</em>
                                    <select name="unidade" id="unidade" class="left l60" style="margin-left:15px;">
                                        <option value="">Selecione uma Unidade</option>
                                        <option value="Unidade">Unidade</option>
                                        <option value="Hectare">Hectare</option>
                                        <option value="Kg">Kg</option>
                                        <option value="M²">M²</option>
                                        <option value="M³">M³</option>
                                        <option value="Peça">Peça</option>
                                        <option value="Pacote">Pacote</option>
                                        <option value="Saco">Saco</option>
                                        <option value="Frasco">Frasco</option>
                                        <option value="Metro">Metro</option>
                                        <option value="Litro">Litro</option>
                                        <option value="Alqueire">Alqueire</option>
                                        <option value="Alqueirão">Alqueirão</option>
                                        <option value="Centímetro">Centímetro</option>
                                        <option value="CM³">CM³</option>
                                        <option value="Dose">Dose</option>
                                        <option value="Dúzia">Dúzia</option>
                                        <option value="Tonelada">Tonelada</option>
                                        <option value="Lote">Lote</option>
                                        <option value="Milheiro">Milheiro</option>
                                        <option value="Valor Total">Valor Total</option>
                                    </select>
                                </p>
                                <div class="clear"></div>
                                <p>
                                    <label for="outra_unidade">Outra Unidade: </label>
                                    <input name="outra_unidade" id="outra_unidade" type="text" class="left l60" maxlength="15" />
                                    <em class="left l20 padding-box-interno font10">Caso não encontre a unidade no campo acima, informe-o aqui.</em>
                                </p>
                                <div class="clear"></div>
                                <p>
                                	<p class="left l40">
                                    	<label for="valor_a_vista">Valor a vista</label>
                                    	<input name="valor_a_vista" id="valor_a_vista" type="text" placeholder="R$ 000000,00" />
                                    </p>
                                	<p class="left l40">
                                		<label for="valor_a_prazo">Valor a prazo</label>
                                    	<input name="valor_a_prazo" id="valor_a_prazo" type="text" placeholder="R$ 000000,00" />
                                	</p>
                                </p>
                                <div class="clear"></div>
                                <p>
                                	<p class="left l30">
                                		<label for="estado">Estado</label>
                                        <select name="estado" id="estado">
                                        	<option value="">Selecione</option>
                                            <?php
                                            $estados = \CoffeeCore\Helper\ResourceManager::readFile("estados.json");
                                            foreach ($estados as $k => $estado) {
                                                echo "<option value='";
                                                echo $estado["name"] . "' > ";
                                                echo $estado["description"] . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </p>
                                    <p class="left l70">
                                		<label for="cidade">Cidade</label>
                                    	<select name="cidade" id="cidade" class="">
                                        	<option value="">Escolha primeiramente o Estado</option>
                                        </select>
                                    </p>
                                </p>
                                <br />
                                <br />
                                <h3>Condições de Pagamento e Entrega</h3>
                                <div class="clear"></div>
                                <p>
                                	<label for="pagamento">Pagamento</label>
                                    <?php
                                    $formasPagamento = \CoffeeCore\Storage\DoctrineStorage::orm()
                                                            ->getRepository(FormaPagamento\Entity::FULL_NAME)
                                                            ->findAll();
                                    /* @var $forma FormaPagamento\Entity */
                                    foreach ($formasPagamento as $forma) {
                                        ?>
                                        <label class="pc50 left"><input name="condicoes_de_pagamento[]" type="checkbox"
                                                value="<?=$forma->getId();?>"/><?=$forma->getTitulo();?></label>
                                    <?php
                                    }
                                    ?>
                                </p>
                                <div class="clear"></div>
                                <p>
                                	<label for="entrega">Entrega</label>
                                    <?php
                                    $formasEntrega = \CoffeeCore\Storage\DoctrineStorage::orm()
                                                        ->getRepository(FormaEntrega\Entity::FULL_NAME)
                                                        ->findAll();
                                    /* @var $entrega  FormaEntrega\Entity */
                                    foreach ($formasEntrega as $entrega) {
                                    ?>
                                    <label class="pc50 left">
                                        <input name="condicoes_de_entrega[]" type="checkbox" value="<?=$entrega->getId();?>"/>
                                        <?=$entrega->getTitulo();?>
                                    </label>
                                    <?php
                                    }
                                    ?>
                                    <div class="clear"></div>
                                </p>
                                <div class="clear"></div>
                                <h3>Descrição do Produto</h3>
                                <p>
                                	<p class="aviso">
                                    	<strong>ATENÇÃO! É proibido inserir dados de contato.</strong><br />
                                    	<small>A administração do Ofertas Agrícolas se coloca no direito de excluir o anúncio que violar as regras de cadastro.</small>
                                    </p>
                                    <p><textarea name="descricao" id="descricao" style="height:200px;" placeholder="Seja criativo. Coloque o máximo de informações de seu produto e aumente suas chances de venda."></textarea></p>
                                </p>
                                <div class="clear"></div>
                                <h3>Foto e Vídeo</h3>
                                <p>
                                    <label for="img_capa">Imagem de Capa: </label>
                                    <input name="img_capa" id="img_capa" type="file" class="left l80" />
                                    <em class="left l20 padding-box-interno font10">Informe a imagem que será a capa do seu anúncio</em>
                                </p>
                                <div class="clear"><br /><br /></div>
                                <p>
                                    <label for="video">Vídeo: <span style="margin-left:40px; font-size:12px;">(Opcional)</span></label>
                                    <input name="video" id="video" type="text" class="left l80" placeholder="http://" />
                                    <em class="left l20 padding-box-interno font10">Informe o endereço do seu vídeo no youtube</em>
                                </p>
                                <div class="clearfix"></div>
                                <p class="l100 text-right"><br />
                                	<button name="bt-cadastrar" type="submit">Próximo Passo</button>
                                </p>
                            </form>
						</article><!-- /.box-interno -->
					</div><!-- /.large-9 /.medium-12 /.small-12 -->
				</div><!-- /CONTEUDO /.large-9 /.medium-12 /.small-12 -->
			</section><!-- /.main-section -->
			<a class="exit-off-canvas"></a>
		</div><!--/innerWrapp-->
	</div><!--/offCanvasWrap-->
</div><!-- /.container -->
<?php require_once('../inc_footer.php'); ?>
<script src="<?=$prefixLink;?>js/jquery.validate.js"></script>
<script src="<?=$prefixLink;?>js/jquery.maskMoney.js"></script>
<script>
$(document).ready(function(){
	// Configuração para campos de Real.
    $("#valor_a_vista").maskMoney({showSymbol:true, symbol:"R$ ", decimal:",", thousands:"."});
	$("#valor_a_prazo").maskMoney({showSymbol:true, symbol:"R$ ", decimal:",", thousands:"."});

	$("#unidade").change(function(){
		$("#outra_unidade").val('');
	});
	$("#outra_unidade").keypress(function(){
		$("#unidade").val('');
	});
    $("#form-add-anuncio").validate({
        rules: {
            "plano": {
                required: true
            },
            "tipo": {
                required: true
            },
            "categoria": {
                required: true
            },
            /*"sub-categoria": {
                required: true
            },*/
            "titulo": {
                required: true
            },
            "sub_titulo": {
                required: true
            },
            "quantidade": {
                required: true
            },
			"unidade": {
				required: function(element){
					if($("#outra_unidade").val() == ''){
						return true;
					}else{
						return false;
					}
				}
			},
			"outra_unidade": {
				required: function(element){
					if($("#unidade").val() == ''){
						return true;
					}else{
						return false;
					}
				},
                maxlength:15
			},
            "valor_a_vista": {
                required: true
            },
            "valor_a_prazo": {
                required: true
            },
            "cidade": {
                required: true
            },
            "estado": {
                required: true
            },
            "condicoes_de_pagamento[]": {
                required: true
            },
            "condicoes_de_entrega[]": {
                required: true
            },
            "descricao": {
                required: true
            },
            "img_capa": {
                required: true
            }

        }
    });

    $("#estado").change(function(){
        var uf = $(this).val();
        $.ajax({
            url: "/cidades/" + uf.toLowerCase() ,
            dataType: "json",
            beforeSend : function () {
                if (uf.length > 0) {
                    $("#cidade").html("<option value=''>Selecione</option>").attr("disabled", true);
                } else {
                    $("#cidade").html("<option value=''>Escolha o estado</option>").attr("disabled", true);
                }
            }

        }).done(function(result){
            $.each(result, function (key, value) {
                $("#cidade").append("<option value='"+ value.id +"'>" + value.title + "</option>")
            });
            $("#cidade").attr("disabled", false);
        });
    });


    $("#tipo").change(function(){
        var tipo = $(this).val();
        $.ajax({
            url: "/api/categorias/" + tipo.toLowerCase() ,
            dataType: "json",
            beforeSend : function () {
                if (tipo.length > 0) {
                    $("#categoria").html("<option value=''>Selecione</option>").attr("disabled", true);
                } else {
                    $("#categoria").html("<option value=''>Escolha o tipo</option>").attr("disabled", true);
                }
            }
        }).done(function(result){
            $.each(result, function (key, value) {
                $("#categoria").append("<option value='"+ value.id +"'>" + value.title + "</option>");
            });
            $("#categoria").attr("disabled", false);
        });
    });


    $("#categoria").change(function(){
        var categoria = $(this).val();
        $.ajax({
            url: "/api/subcategorias/" + categoria.toLowerCase() ,
            dataType: "json",
            beforeSend : function () {
                if (categoria.length > 0) {
                    $("#sub-categoria").html("<option value=''>Selecione</option>").attr("disabled", true);
                } else {
                    $("#sub-categoria").html("<option value=''>Escolha a categoria</option>").attr("disabled", true);
                }
            }
        }).done(function(result){
            $.each(result, function (key, value) {
                $("#sub-categoria").append("<option value='"+ value.id +"'>" + value.title + "</option>");
            });
            $("#sub-categoria").attr("disabled", false);
        });
    });

});
</script>
</body>
</html>