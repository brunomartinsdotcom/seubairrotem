<?php

namespace Intranet;

use CoffeeCore\Core\AbstractApplication;
use CoffeeCore\Helper\Canvas;
use CoffeeCore\Storage\DoctrineStorage;
use Doctrine\Common\Proxy\Exception\InvalidArgumentException;
use Nette\Mail\Message;
use Exception;
use Slim\Slim;

/**
 * Class Application
 * @package Intranet
 */
class Application extends AbstractApplication
{
    /**
     * @param Slim $app
     * @return void
     */
    protected function setRoutes(Slim $app)
    {
        $controller = new Controller();

        $app->group('', function () use ($app, $controller) {

            /*$app->map("/", function () use ($app, $controller) {
                $app->redirect('/minha-conta');
            })->via("GET", "POST");*/

            $app->get("/", function () use ($app, $controller) {
                $usuario = Authentication::getInstance()->getFromSession('entity');
                if(empty($usuario)){
                    //PEGA URL
                    //session_start();
                    $dominio= $_SERVER['HTTP_HOST'];
                    $url = "http://" . $dominio. $_SERVER['REQUEST_URI'];
                    $_SESSION['paginaAnterior'] = $url;

                    $app->redirect("/login/?paginaAnterior=".$url);
                }

                $app->render(__NAMESPACE__ . "/Views/home.php", []);
            });

            $app->map("/cadastrar-sua-empresa/", function () use ($app, $controller) {
                if ($app->request()->isGet()) {

                    /** @var \UsuariosDoSite\Entity $usuario */
                    $usuario = Authentication::getInstance()->getFromSession('entity');
                    if(empty($usuario)){
                        //PEGA URL
                        session_start();
                        $dominio= $_SERVER['HTTP_HOST'];
                        $url = "http://" . $dominio. $_SERVER['REQUEST_URI'];
                        $_SESSION['paginaAnterior'] = $url;

                        $app->redirect("/login/?paginaAnterior=".$url);
                    }

                    $app->render(__NAMESPACE__ . "/Views/cadastrar-sua-empresa.php");
                }

                if ($app->request()->isPost()) {
                    try {

                        $usuario = Authentication::getInstance()->getFromSession('entity');

                        /* BRUNO MARTINS
                        $planos = DoctrineStorage::orm()->getRepository(\Plano\Entity::FULL_NAME)->findOneBy(['id' => $_POST['tipo']]);
                        $planosDias = $planos->getDiasAnuncio();
                        if($planos->getDiasAnuncio() == 1){
                            $planosDias = $planos->getDiasAnuncio()." day";
                        }else if($planos->getDiasAnuncio() > 1){
                            $planosDias = $planos->getDiasAnuncio()." days";
                        }else if($planos->getDiasAnuncio() == 0){
                            $planosDias = $planos->getDiasAnuncio()." days";
                        }*/
                        $novoAnuncio = new \Anuncio\Entity();
                        //$dataValidade = $novoAnuncio->getDatainclusao()->modify('+'.$planosDias);
                        $novoAnuncio->setUsuario($usuario->getId());
                        $novoAnuncio->setPlano($_POST['plano']);
                        $novoAnuncio->setTipo($_POST['tipo']);
                        //$novoAnuncio->setDatavalidade($dataValidade);
                        $novoAnuncio->setDatainclusao(new \DateTime());
                        $novoAnuncio->setCategoria($_POST['categoria']);
                        $novoAnuncio->setSubcategoria($_POST['sub-categoria']);
                        $novoAnuncio->setTitulo($_POST['titulo']);
                        $novoAnuncio->setDescricaobreve($_POST['sub_titulo']);
                        $novoAnuncio->setDescricaocompleta($_POST['descricao']);

                        $novoAnuncio->setQuantidade($_POST['quantidade']);
                        $novoAnuncio->setUnidade($_POST['unidade']);
                        $novoAnuncio->setUnidadeoutra($_POST['outra_unidade']);

                        $novoAnuncio->setPrecoavista(str_replace(".","",$_POST['valor_a_vista']));
                        $novoAnuncio->setPrecoaprazo(str_replace(".","",$_POST['valor_a_prazo']));
                        //$novoAnuncio->setPrecoaprazo($_POST['valor_a_prazo']);

                        $novoAnuncio->setEstado($_POST['estado']);
                        $novoAnuncio->setCidade($_POST['cidade']);

                        if (!is_array($_FILES["img_capa"]["name"])) {
                            $output_dir = $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "_uploads/anuncios/";

                            $fileName = nome_arquivo($_FILES["img_capa"]["name"]);

                            move_uploaded_file($_FILES["img_capa"]["tmp_name"], $output_dir . $fileName);

                            $canvas2 = new Canvas($output_dir.$fileName);
                            $canvas2->resize(367, 276, 'fill')->save($output_dir . 'med-' . $fileName);

                            $canvas3 = new Canvas($output_dir . $fileName);
                            $canvas3->resize(200, 150, "fill")->save($output_dir . 'tumb-' . $fileName);

                            $canvas = new Canvas($output_dir . $fileName);
                            $canvas->resize(700)->merge($_SERVER['DOCUMENT_ROOT'] .'/images/logoToMerge.png', ["right", "bottom"])
                                ->save($output_dir . $fileName);

                            $novoAnuncio->setFotocapa($fileName);
                        }
                        $novoAnuncio->setVideo($_POST['video']);
                        $novoAnuncio->setStatus(0);

                        DoctrineStorage::orm()->persist($novoAnuncio);
                        DoctrineStorage::orm()->flush();

                        foreach ($_POST['condicoes_de_pagamento'] as $pagamento) {
                            $anuncioPagamento = new \AnuncioPagamento\Entity();
                            $anuncioPagamento->setAnuncio($novoAnuncio->getId());
                            $anuncioPagamento->setFormapagamento($pagamento);
                            DoctrineStorage::orm()->persist($anuncioPagamento);
                        }

                        $anuncioEntrega = new \AnuncioEntrega\Entity();
                        $anuncioEntrega->setAnuncio($novoAnuncio->getId());
                        foreach ($_POST['condicoes_de_entrega'] as $entrega) {
                            $anuncioEntrega->setFormaentrega($entrega);
                            DoctrineStorage::orm()->persist($anuncioEntrega);
                        }
                        DoctrineStorage::orm()->flush();

                        Authentication::getInstance()->putInSession('novoAnuncio', $novoAnuncio);
                        Authentication::getInstance()->putInSession('novoAnuncioPlano', $_POST['plano']);

                    } catch (\Exception $e) {
                        echo $e; die;
                        $app->flash('error', 'Não foi possível gravar seu anúncio.');
                        $app->redirect("/minha-conta/adicionar-anuncio");
                    }
                    $app->redirect("/minha-conta/adicionar-anuncio/galeria/");
                }

            })->via("GET", "POST");

            $app->get("/historico-de-faturas(/pagina(/:pagina))/", function ($pagina = 1) use ($app, $controller) {
                $limite = 20;
                $offset = ($pagina-1) * $limite;

                $usuario = Authentication::getInstance()->getFromSession('entity');
				if(empty($usuario)){
					//PEGA URL
					//session_start();
					$dominio= $_SERVER['HTTP_HOST'];
					$url = "http://" . $dominio. $_SERVER['REQUEST_URI'];
					$_SESSION['paginaAnterior'] = $url;
							
					$app->redirect("/login/?paginaAnterior=".$url);
				}

                $faturas = DoctrineStorage::orm()->getRepository(\Fatura\Entity::FULL_NAME)
                    ->findBy(["usuario" => $usuario->getId()], ["id" => "DESC"], $limite, $offset);
                $totalRegistros = count(DoctrineStorage::orm()->getRepository(\Fatura\Entity::FULL_NAME)
                    ->findBy(["usuario" => $usuario->getId()]));

                $app->render(
                    __NAMESPACE__ . "/Views/historico-de-faturas.php",
                    [
                        'getPagina' => $pagina,
                        'getFaturas' => $faturas,
                        'totalRegistros' => $totalRegistros,
                    ]
                );
            });

            $app->get("/guia-comercial(/pagina(/:pagina))/", function ($pagina = 1) use ($app, $controller) {
                $limite = 20;
                $offset = ($pagina-1) * $limite;

                $usuario = Authentication::getInstance()->getFromSession('entity');
                if(empty($usuario)){
                    //PEGA URL
                    session_start();
                    $dominio= $_SERVER['HTTP_HOST'];
                    $url = "http://" . $dominio. $_SERVER['REQUEST_URI'];
                    $_SESSION['paginaAnterior'] = $url;

                    $app->redirect("/login/?paginaAnterior=".$url);
                }

                $data = new \DateTime();

                $query = DoctrineStorage::orm()->createQuery('SELECT a FROM \\Anuncio\\Entity a WHERE a.usuario = ?1 ORDER BY a.empresa ASC');
                $query->setParameter(1, $usuario->getId());

                $totalRegistros = count($query->getResult());

                $query->setMaxResults($limite);
                $query->setFirstResult($offset);
                $anuncios = $query->getResult();

                $acaoEditar = ['url' => "/minha-conta/guia-comercial/editar/", 'texto' => 'Editar Anúncio', 'img' => '<img src="/images/btn-edit.png" alt="Editar Anúncio" />'];
                $acao = ['url' => "/minha-conta/cancelar-guia-comercial/", 'texto' => 'Cancelar Anúncio', 'img' => '<img src="/images/btn-cancel.png" alt="Cancelar Anúncio" />'];

                $app->render(
                    __NAMESPACE__ . "/Views/guia-comercial.php",
                    [
                        'getAcaoEditar' => $acaoEditar,
                        'getAcao' => $acao,
                        'getPagina' => $pagina,
                        'getAnuncios' => $anuncios,
                        'totalRegistros' => $totalRegistros,
                        'getRoute' => 'guia-comercial'
                    ]
                );
            });

            $app->get("/anuncios-expirados(/pagina(/:pagina))/", function ($pagina = 1) use ($app, $controller) {
                $limite = 20;
                $offset = ($pagina-1) * $limite;

                $usuario = Authentication::getInstance()->getFromSession('entity');
                if(empty($usuario)){
                    //PEGA URL
                    session_start();
                    $dominio= $_SERVER['HTTP_HOST'];
                    $url = "http://" . $dominio. $_SERVER['REQUEST_URI'];
                    $_SESSION['paginaAnterior'] = $url;

                    $app->redirect("/login/?paginaAnterior=".$url);
                }

                $data = new \DateTime();

                $query = DoctrineStorage::orm()->createQuery('SELECT a FROM \\Anuncio\\Entity a where a.usuario = ?1 and a.datavalidade < ?2 and a.status = 1 ORDER BY a.titulo ASC');
                $query->setParameter(1, $usuario->getId());
                $query->setParameter(2, $data->format('Y-m-d'));

                $totalRegistros = count($query->getResult());

                $query->setMaxResults($limite);
                $query->setFirstResult($offset);
                $anuncios = $query->getResult();

                $app->render(
                    __NAMESPACE__ . "/Views/anuncios-expirados.php",
                    [
                        'getPagina' => $pagina,
                        'getAnuncios' => $anuncios,
                        'totalRegistros' => $totalRegistros,
                        'getRoute' => 'anuncios-expirados'
                    ]
                );
            });

            /*
             BRUNO MARTINS
             $app->get("/renovar-anuncio/:anuncio/", function ($anuncio) use ($app, $controller) {

                /** @var \Usuario\Entity $usuario
                $usuario = Authentication::getInstance()->getFromSession('entity');
                if(empty($usuario)){
                    //PEGA URL
                    session_start();
                    $dominio= $_SERVER['HTTP_HOST'];
                    $url = "http://" . $dominio. $_SERVER['REQUEST_URI'];
                    $_SESSION['paginaAnterior'] = $url;

                    $app->redirect("/login/?paginaAnterior=".$url);
                }

                $data = new \DateTime();

                $query = DoctrineStorage::orm()->createQuery('SELECT a FROM \\Anuncio\\Entity a where a.usuario = ?1 and a.datavalidade < ?2 and a.status = 1 ORDER BY a.titulo ASC');
                $query->setParameter(1, $usuario->getId());
                $query->setParameter(2, $data->format('Y-m-d'));

                $anunciosExpirados = $query->getResult();

                $app->render(
                    __NAMESPACE__ . "/Views/renovar-anuncio.php",
                    [
                        'getAnunciosExpirados' => $anunciosExpirados,
                        'getAnuncio' => $anuncio
                    ]
                );
            });*/

            $app->get("/anuncios-bloqueados(/pagina(/:pagina))/", function ($pagina = 1) use ($app, $controller) {
                $limite = 20;
                $offset = ($pagina-1) * $limite;

                $usuario = Authentication::getInstance()->getFromSession('entity');
                if(empty($usuario)){
                    //PEGA URL
                    session_start();
                    $dominio= $_SERVER['HTTP_HOST'];
                    $url = "http://" . $dominio. $_SERVER['REQUEST_URI'];
                    $_SESSION['paginaAnterior'] = $url;

                    $app->redirect("/login/?paginaAnterior=".$url);
                }

                $query = DoctrineStorage::orm()->createQuery('SELECT a FROM \\Anuncio\\Entity a where a.usuario = ?1 and a.status = 2 ORDER BY a.titulo ASC');
                $query->setParameter(1, $usuario->getId());

                $totalRegistros = count($query->getResult());

                $query->setMaxResults($limite);
                $query->setFirstResult($offset);
                $anuncios = $query->getResult();

                $app->render(
                    __NAMESPACE__ . "/Views/anuncios-bloqueados.php",
                    [
                        'getPagina' => $pagina,
                        'getAnuncios' => $anuncios,
                        'totalRegistros' => $totalRegistros,
                        'getRoute' => 'anuncios-bloqueados'
                    ]
                );
            });

            $app->get("/cancelar-anuncio/:anuncio/", function ($anuncio = 1) use ($app, $controller) {

                /** @var \Anuncio\Entity $anuncio */
                $anuncio =  DoctrineStorage::orm()->getRepository(\Anuncio\Entity::FULL_NAME)->findOneBy(['id' => $anuncio]);
                if (!empty($anuncio)) {
                    /** @var \Usuario\Entity $usuario */
                    $usuario = Authentication::getInstance()->getFromSession('entity');
                    if ($anuncio->getStatus() == 1 and $usuario->getId() == $anuncio->getUsuario()) {
                        $anuncio->setStatus(3);
                        DoctrineStorage::orm()->merge($anuncio);
                        DoctrineStorage::orm()->flush();
                    }
                }

                $app->redirect("/minha-conta/anuncios-ativos");
            });

            $app->get("/anuncios-cancelados(/pagina(/:pagina))/", function ($pagina = 1) use ($app, $controller) {
                $limite = 20;
                $offset = ($pagina-1) * $limite;

                $usuario = Authentication::getInstance()->getFromSession('entity');
                if(empty($usuario)){
                    //PEGA URL
                    session_start();
                    $dominio= $_SERVER['HTTP_HOST'];
                    $url = "http://" . $dominio. $_SERVER['REQUEST_URI'];
                    $_SESSION['paginaAnterior'] = $url;

                    $app->redirect("/login/?paginaAnterior=".$url);
                }

                $query = DoctrineStorage::orm()->createQuery('SELECT a FROM \\Anuncio\\Entity a where a.usuario = ?1 and a.status = 3 ORDER BY a.titulo ASC');
                $query->setParameter(1, $usuario->getId());

                $totalRegistros = count($query->getResult());

                $query->setMaxResults($limite);
                $query->setFirstResult($offset);
                $anuncios = $query->getResult();

                $acao = ['url' => "/minha-conta/reativar-anuncio/", 'texto' => 'Reativar Anúncio', 'img' => '<img src="/images/btn-ativar.png" alt="Reativar Anúncio" />'];

                $app->render(
                    __NAMESPACE__ . "/Views/anuncios-cancelados.php",
                    [
                        'getAcao' => $acao,
                        'getPagina' => $pagina,
                        'getAnuncios' => $anuncios,
                        'totalRegistros' => $totalRegistros,
                        'getRoute' => 'anuncios-cancelados'
                    ]
                );
            });

            $app->get("/anuncios-pendentes(/pagina(/:pagina))/", function ($pagina = 1) use ($app, $controller) {
                $limite = 20;
                $offset = ($pagina-1) * $limite;

                $usuario = Authentication::getInstance()->getFromSession('entity');
                if(empty($usuario)){
                    //PEGA URL
                    session_start();
                    $dominio= $_SERVER['HTTP_HOST'];
                    $url = "http://" . $dominio. $_SERVER['REQUEST_URI'];
                    $_SESSION['paginaAnterior'] = $url;

                    $app->redirect("/login/?paginaAnterior=".$url);
                }

                $query = DoctrineStorage::orm()->createQuery('SELECT a FROM \\Anuncio\\Entity a where a.usuario = ?1 and a.status = 0 ORDER BY a.titulo ASC');
                $query->setParameter(1, $usuario->getId());

                $totalRegistros = count($query->getResult());

                $query->setMaxResults($limite);
                $query->setFirstResult($offset);
                $anuncios = $query->getResult();

                $app->render(
                    __NAMESPACE__ . "/Views/anuncios-pendentes.php",
                    [
                        'getPagina' => $pagina,
                        'getAnuncios' => $anuncios,
                        'totalRegistros' => $totalRegistros,
                        'getRoute' => 'anuncios-pendentes'
                    ]
                );
            });

            /*
             BRUNO MARTINS
             $app->get("/reativar-anuncio/:anuncio/", function ($anuncio = 1) use ($app, $controller) {

                /** @var \Anuncio\Entity $anuncio
                $anuncio =  DoctrineStorage::orm()->getRepository(\Anuncio\Entity::FULL_NAME)->findOneBy(['id' => $anuncio]);
                if (!empty($anuncio)) {
                    /** @var \Usuario\Entity $usuario
                    $usuario = Authentication::getInstance()->getFromSession('entity');
                    if ($anuncio->getStatus() == 3 and $usuario->getId() == $anuncio->getUsuario()) {
                        $anuncio->setStatus(1);
                        DoctrineStorage::orm()->merge($anuncio);
                        DoctrineStorage::orm()->flush();
                    }
                }

                $app->redirect("/minha-conta/anuncios-cancelados");
            });*/


            $app->get("/destaques-ativos(/pagina(/:pagina))/", function ($pagina = 1) use ($app, $controller) {
                $limite = 20;
                $offset = ($pagina-1) * $limite;

                $usuario = Authentication::getInstance()->getFromSession('entity');
				if(empty($usuario)){
					//PEGA URL
					session_start();
					$dominio= $_SERVER['HTTP_HOST'];
					$url = "http://" . $dominio. $_SERVER['REQUEST_URI'];
					$_SESSION['paginaAnterior'] = $url;
							
					$app->redirect("/login/?paginaAnterior=".$url);
				}

                $query = DoctrineStorage::orm()->createQuery('SELECT ad FROM \\AnuncioDestaque\\Entity ad WHERE ad.anuncio IN
                                                            ( SELECT a.id FROM \\Anuncio\\Entity a WHERE a.usuario = ?1) AND ad.datavencimento >= ?2 AND ad.fatura IN
                                                            ( SELECT f.id FROM \\Fatura\\Entity f WHERE f.statuspagseguro=3 OR f.statuspagseguro=4)
                                                              ORDER BY ad.datainicio DESC');
                $query->setMaxResults($limite);
                $query->setFirstResult($offset);
                $query->setParameter(1,$usuario->getId());
                $query->setParameter(2,date('Y-m-d'));
                $destaques = $query->getResult();

                $query = DoctrineStorage::orm()->createQuery('SELECT ad FROM \\AnuncioDestaque\\Entity ad WHERE ad.anuncio IN
                                                            ( SELECT a.id FROM \\Anuncio\\Entity a WHERE a.usuario = ?1) AND ad.datavencimento >= ?2 AND ad.fatura IN
                                                            ( SELECT f.id FROM \\Fatura\\Entity f WHERE f.statuspagseguro=3 OR f.statuspagseguro=4)
                                                              ORDER BY ad.datainicio DESC');
                $query->setParameter(1,$usuario->getId());
                $query->setParameter(2,date('Y-m-d'));
                $totalRegistros = count($query->getResult());

                $app->render(
                    __NAMESPACE__ . "/Views/destaques-ativos.php",
                    [
                        'getPagina' => $pagina,
                        'getDestaques' => $destaques,
                        'totalRegistros' => $totalRegistros,
                    ]
                );
            });

            $app->get("/historico-destaques(/pagina(/:pagina))/", function ($pagina = 1) use ($app, $controller) {
                $limite = 20;
                $offset = ($pagina-1) * $limite;

                $usuario = Authentication::getInstance()->getFromSession('entity');
                if(empty($usuario)){
                    //PEGA URL
                    session_start();
                    $dominio= $_SERVER['HTTP_HOST'];
                    $url = "http://" . $dominio. $_SERVER['REQUEST_URI'];
                    $_SESSION['paginaAnterior'] = $url;

                    $app->redirect("/login/?paginaAnterior=".$url);
                }

                $query = DoctrineStorage::orm()->createQuery('SELECT ad FROM \\AnuncioDestaque\\Entity ad WHERE ad.anuncio IN (SELECT a.id FROM \\Anuncio\\Entity a WHERE a.usuario = ?1) ORDER BY ad.datainicio DESC');
                $query->setMaxResults($limite);
                $query->setFirstResult($offset);
                $query->setParameter(1,$usuario->getId());
                $destaques = $query->getResult();

                $query = DoctrineStorage::orm()->createQuery('SELECT ad FROM \\AnuncioDestaque\\Entity ad WHERE ad.anuncio IN (SELECT a.id FROM \\Anuncio\\Entity a WHERE a.usuario = ?1)');
                $query->setParameter(1,$usuario->getId());
                $totalRegistros = count($query->getResult());

                $app->render(
                    __NAMESPACE__ . "/Views/historico-de-destaques.php",
                    [
                        'getPagina' => $pagina,
                        'getDestaques' => $destaques,
                        'totalRegistros' => $totalRegistros,
                    ]
                );
            });

            $app->get("/perguntas-anuncio(/pagina(/:pagina))/", function ($pagina = 1) use ($app, $controller) {
                $limite = 20;
                $offset = ($pagina-1) * $limite;

                $usuario = Authentication::getInstance()->getFromSession('entity');
                if(empty($usuario)){
                    //PEGA URL
                    session_start();
                    $dominio= $_SERVER['HTTP_HOST'];
                    $url = "http://" . $dominio. $_SERVER['REQUEST_URI'];
                    $_SESSION['paginaAnterior'] = $url;

                    $app->redirect("/login/?paginaAnterior=".$url);
                }

                $query = DoctrineStorage::orm()->createQuery('SELECT ad FROM \\AnuncioPergunta\\Entity ad WHERE ad.anuncio IN
                                                            ( SELECT a.id FROM \\Anuncio\\Entity a WHERE a.usuario = ?1) ORDER BY ad.datahorapergunta DESC');
                $query->setMaxResults($limite);
                $query->setFirstResult($offset);
                $query->setParameter(1,$usuario->getId());
                $perguntas = $query->getResult();

                $query = DoctrineStorage::orm()->createQuery('SELECT ad FROM \\AnuncioPergunta\\Entity ad WHERE ad.anuncio IN
                                                            ( SELECT a.id FROM \\Anuncio\\Entity a WHERE a.usuario = ?1)');
                $query->setParameter(1,$usuario->getId());
                $totalRegistros = count($query->getResult());

                $app->render(
                    __NAMESPACE__ . "/Views/perguntas-anuncios.php",
                    [
                        'getPagina' => $pagina,
                        'getPerguntas' => $perguntas,
                        'totalRegistros' => $totalRegistros,
                    ]
                );
            });

            $app->get("/destacar-anuncio/", function () use ($app, $controller) {

                /** @var \Usuario\Entity $usuario */
                $usuario = Authentication::getInstance()->getFromSession('entity');
                if(empty($usuario)){
					//PEGA URL
					session_start();
					$dominio= $_SERVER['HTTP_HOST'];
					$url = "http://" . $dominio. $_SERVER['REQUEST_URI'];
					$_SESSION['paginaAnterior'] = $url;
							
					$app->redirect("/login/?paginaAnterior=".$url);
				}
				$data = new \DateTime();

                $query = DoctrineStorage::orm()->createQuery('SELECT a FROM \\Anuncio\\Entity a where a.usuario = ?1 and a.datavalidade >= ?2 and a.status = 1 ORDER BY a.titulo ASC');
                $query->setParameter(1, $usuario->getId());
                $query->setParameter(2, $data->format('Y-m-d'));

                $anunciosAtivos = $query->getResult();

                $app->render(
                    __NAMESPACE__ . "/Views/destacar-anuncio-ativo.php",
                    [
                        'getAnunciosAtivos' => $anunciosAtivos,
                    ]
                );
            });

            $app->map("/meus-dados/", function () use ($app, $controller) {
                if ($app->request()->isGet()) {

                    /** @var \UsuariosDoSite\Entity $usuario */
                    $usuario = Authentication::getInstance()->getFromSession('entity');
                    if(empty($usuario)){
                        //PEGA URL
                        session_start();
                        $dominio= $_SERVER['HTTP_HOST'];
                        $url = "http://" . $dominio. $_SERVER['REQUEST_URI'];
                        $_SESSION['paginaAnterior'] = $url;

                        $app->redirect("/login/?paginaAnterior=".$url);
                    }

                    $app->render(__NAMESPACE__ . "/Views/alterar-dados.php");
                }
                if ($app->request()->isPost()) {
                    try {

                        /** @var \UsuariosDoSite\Entity $usuario */
                        $usuario = Authentication::getInstance()->getFromSession('entity');
                        $repo = DoctrineStorage::orm()->getRepository(\UsuariosDoSite\Entity::FULL_NAME);

                        if (count($repo->findBy(["email" => $_POST['email']])) > 0 and $usuario->getEmail() != $_POST['email']) {
                            throw new Exception("O e-mail informado já encontra-se cadastrado!");
                        }

                        if (count($repo->findBy(["login" => strtolower($_POST['login'])])) > 0 and $usuario->getLogin() != strtolower($_POST['login'])) {
                            throw new Exception("O login/apelido informado já encontra-se cadastrado!");
                        }
                        if(preg_match("/^[a-z0-9]+$/", strtolower($_POST['login'])) == 0) {
                            throw new Exception("Login/Apelido inválido!");
                        }

                        if ($_POST["tipopessoa"] == 1) {
                            $usuario->setNome($_POST['nome']);
                            $dataNasc = explode('/', $_POST['datadenascimento']);
                            $datadenascimento = new \DateTime("{$dataNasc[2]}-{$dataNasc[1]}-{$dataNasc[0]}");
                            $usuario->setDatadenascimento($datadenascimento);
                        }
                        if ($_POST["tipopessoa"] == 2) {
                            $usuario->setRazaosocial($_POST['razaosocial']);
                            $usuario->setNomefantasia($_POST['nomefantasia']);
                            $usuario->setNomedoresponsavel($_POST['nomedoresponsavel']);
                        }
                        $usuario->setEmail($_POST['email']);
                        $usuario->setCep($_POST['cep']);
                        $usuario->setEndereco($_POST['endereco']);
                        $usuario->setNumero($_POST['numero']);
                        $usuario->setBairro($_POST['bairro']);
                        $usuario->setComplemento($_POST['complemento']);
                        $usuario->setEstado($_POST['estado']);
                        $usuario->setCidade($_POST['cidade']);
                        $usuario->setTelefone($_POST['telefone']);
                        $usuario->setCelular($_POST['celular']);
                        $usuario->setLogin(strtolower($_POST['login']));
                        if(!empty($_POST['senha'])) {
                            $usuario->setSenha($_POST['senha']);
                        }

                        DoctrineStorage::orm()->merge($usuario);
                        DoctrineStorage::orm()->flush();

                        Authentication::getInstance()->putInSession('entity', $usuario);

                        $app->flash('info', 'Dados alterados com sucesso.');

                    } catch (Exception $e) {
                        $app->flash('info', 'Erro ao alterar dados. '. $e->getMessage());
                    }
                    $app->redirect("/minha-conta/meus-dados/");
                }
            })->via("GET", "POST");

            $app->get("/tipos-de-destaque/", function () use ($app) {

                /** @var \Usuario\Entity $usuario */
                $usuario = Authentication::getInstance()->getFromSession('entity');
                if(empty($usuario)){
                    //PEGA URL
                    session_start();
                    $dominio= $_SERVER['HTTP_HOST'];
                    $url = "http://" . $dominio. $_SERVER['REQUEST_URI'];
                    $_SESSION['paginaAnterior'] = $url;

                    $app->redirect("/login/?paginaAnterior=".$url);
                }

                $app->render(__NAMESPACE__ . "/Views/tipos-de-destaque.php");
            });

            $app->get(
                "/sair/",
                function () use ($app) {
                    session_destroy();
                    $app->redirect("/");
                }
            );

            $app->map("/adicionar-anuncio/", function () use ($app, $controller) {
				if ($app->request()->isGet()) {

                    /** @var \Usuario\Entity $usuario */
                    $usuario = Authentication::getInstance()->getFromSession('entity');
                    if(empty($usuario)){
                        //PEGA URL
                        session_start();
                        $dominio= $_SERVER['HTTP_HOST'];
                        $url = "http://" . $dominio. $_SERVER['REQUEST_URI'];
                        $_SESSION['paginaAnterior'] = $url;

                        $app->redirect("/login/?paginaAnterior=".$url);
                    }

                    $app->render(__NAMESPACE__ . "/Views/adicionar-anuncio.php");
                }

                if ($app->request()->isPost()) {
                    try {

                        $usuario = Authentication::getInstance()->getFromSession('entity');
						
						/* BRUNO MARTINS
						$planos = DoctrineStorage::orm()->getRepository(\Plano\Entity::FULL_NAME)->findOneBy(['id' => $_POST['tipo']]);
						$planosDias = $planos->getDiasAnuncio();
						if($planos->getDiasAnuncio() == 1){
							$planosDias = $planos->getDiasAnuncio()." day";
						}else if($planos->getDiasAnuncio() > 1){
							$planosDias = $planos->getDiasAnuncio()." days";
						}else if($planos->getDiasAnuncio() == 0){
							$planosDias = $planos->getDiasAnuncio()." days";
						}*/
                        $novoAnuncio = new \Anuncio\Entity();
						//$dataValidade = $novoAnuncio->getDatainclusao()->modify('+'.$planosDias);
                        $novoAnuncio->setUsuario($usuario->getId());
                        $novoAnuncio->setPlano($_POST['plano']);
                        $novoAnuncio->setTipo($_POST['tipo']);
						//$novoAnuncio->setDatavalidade($dataValidade);
                        $novoAnuncio->setDatainclusao(new \DateTime());
                        $novoAnuncio->setCategoria($_POST['categoria']);
                        $novoAnuncio->setSubcategoria($_POST['sub-categoria']);
                        $novoAnuncio->setTitulo($_POST['titulo']);
                        $novoAnuncio->setDescricaobreve($_POST['sub_titulo']);
                        $novoAnuncio->setDescricaocompleta($_POST['descricao']);

                        $novoAnuncio->setQuantidade($_POST['quantidade']);
                        $novoAnuncio->setUnidade($_POST['unidade']);
                        $novoAnuncio->setUnidadeoutra($_POST['outra_unidade']);

                        $novoAnuncio->setPrecoavista(str_replace(".","",$_POST['valor_a_vista']));
                        $novoAnuncio->setPrecoaprazo(str_replace(".","",$_POST['valor_a_prazo']));
                        //$novoAnuncio->setPrecoaprazo($_POST['valor_a_prazo']);

                        $novoAnuncio->setEstado($_POST['estado']);
                        $novoAnuncio->setCidade($_POST['cidade']);

                        if (!is_array($_FILES["img_capa"]["name"])) {
                            $output_dir = $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "_uploads/anuncios/";

                            $fileName = nome_arquivo($_FILES["img_capa"]["name"]);

                            move_uploaded_file($_FILES["img_capa"]["tmp_name"], $output_dir . $fileName);

                            $canvas2 = new Canvas($output_dir.$fileName);
                            $canvas2->resize(367, 276, 'fill')->save($output_dir . 'med-' . $fileName);

                            $canvas3 = new Canvas($output_dir . $fileName);
                            $canvas3->resize(200, 150, "fill")->save($output_dir . 'tumb-' . $fileName);

                            $canvas = new Canvas($output_dir . $fileName);
                            $canvas->resize(700)->merge($_SERVER['DOCUMENT_ROOT'] .'/images/logoToMerge.png', ["right", "bottom"])
                                ->save($output_dir . $fileName);

                            $novoAnuncio->setFotocapa($fileName);
                        }
                        $novoAnuncio->setVideo($_POST['video']);
						$novoAnuncio->setStatus(0);

                        DoctrineStorage::orm()->persist($novoAnuncio);
                        DoctrineStorage::orm()->flush();

                        foreach ($_POST['condicoes_de_pagamento'] as $pagamento) {
                            $anuncioPagamento = new \AnuncioPagamento\Entity();
                            $anuncioPagamento->setAnuncio($novoAnuncio->getId());
                            $anuncioPagamento->setFormapagamento($pagamento);
                            DoctrineStorage::orm()->persist($anuncioPagamento);
                        }

                        $anuncioEntrega = new \AnuncioEntrega\Entity();
                        $anuncioEntrega->setAnuncio($novoAnuncio->getId());
                        foreach ($_POST['condicoes_de_entrega'] as $entrega) {
                            $anuncioEntrega->setFormaentrega($entrega);
                            DoctrineStorage::orm()->persist($anuncioEntrega);
                        }
                        DoctrineStorage::orm()->flush();

                        Authentication::getInstance()->putInSession('novoAnuncio', $novoAnuncio);
                        Authentication::getInstance()->putInSession('novoAnuncioPlano', $_POST['plano']);

                    } catch (\Exception $e) {
						echo $e; die;
                        $app->flash('error', 'Não foi possível gravar seu anúncio.');
                        $app->redirect("/minha-conta/adicionar-anuncio");
                    }
                    $app->redirect("/minha-conta/adicionar-anuncio/galeria/");
                }

            })->via("GET", "POST");

            $app->map("/adicionar-anuncio/galeria/", function () use ($app, $controller) {
                if ($app->request()->isGet()) {

                    /** @var \Usuario\Entity $usuario */
                    $usuario = Authentication::getInstance()->getFromSession('entity');
                    if(empty($usuario)){
                        //PEGA URL
                        session_start();
                        $dominio= $_SERVER['HTTP_HOST'];
                        $url = "http://" . $dominio. $_SERVER['REQUEST_URI'];
                        $_SESSION['paginaAnterior'] = $url;

                        $app->redirect("/login/?paginaAnterior=".$url);
                    }

                    $novoAnuncio = Authentication::getInstance()->getFromSession('novoAnuncio');
                    if (empty($novoAnuncio)) {
                        $app->redirect("/minha-conta/adicionar-anuncio/");
                    }

                    $app->render(__NAMESPACE__ . "/Views/adicionar-anuncio2.php");
                }

                if ($app->request()->isPost()) {
                    $app->redirect("/minha-conta/adicionar-anuncio/destacar");
                }

            })->via("GET", "POST");

            $app->map("/adicionar-anuncio/galeria/upload/", function () use ($app, $controller) {

                /** @var \Usuario\Entity $usuario */
                $usuario = Authentication::getInstance()->getFromSession('entity');
                if(empty($usuario)){
                    //PEGA URL
                    session_start();
                    $dominio= $_SERVER['HTTP_HOST'];
                    $url = "http://" . $dominio. $_SERVER['REQUEST_URI'];
                    $_SESSION['paginaAnterior'] = $url;

                    $app->redirect("/login/?paginaAnterior=".$url);
                }

                $novoAnuncio = Authentication::getInstance()->getFromSession('novoAnuncio');
                if (empty($novoAnuncio)) {
                    $app->redirect("/minha-conta/adicionar-anuncio/");
                }

                $output_dir = $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "_uploads/anuncios/";

                if (isset($_FILES["myfile"])) {
                    $ret   = [];

                    if (!is_array($_FILES["myfile"]["name"])) {
                        $fileName = nome_arquivo($_FILES["myfile"]["name"]);
                        //$fileName = create_slug(date("Y-m-d-H:i:s")."_".$_FILES["myfile"]["name"]); //date("Y-m-d-H:i:s") . $_FILES["myfile"]["name"];
                        move_uploaded_file($_FILES["myfile"]["tmp_name"], $output_dir.$fileName);

                        $canvas2 = new Canvas($output_dir . $fileName);
                        $canvas2->resize(400)->save($output_dir . 'med-' . $fileName);

                        $canvas3 = new Canvas($output_dir . $fileName);
                        $canvas3->resize(300)->save($output_dir . 'tumb-' . $fileName);

                        $canvas = new Canvas($output_dir . $fileName);
                        $canvas->resize(700)->merge($_SERVER['DOCUMENT_ROOT'] .'/images/logoToMerge.png', ["right", "bottom"])
                            ->save($output_dir . $fileName);

                        $novoAnuncio->setFotocapa($fileName);

                        $ret[] = $fileName;
                    } else {

                        $fileCount = count($_FILES["myfile"]["name"]);
                        for($i=0; $i < $fileCount; $i++)
                        {
                            $fileName = nome_arquivo($_FILES["myfile"]["name"][$i]);
                            //$fileName = create_slug(date("Y-m-d-H:i:s")."_".$_FILES["myfile"]["name"][$i]);
                            $canvas2 = new Canvas($output_dir . $fileName);
                            $canvas2->resize(400)->save($output_dir . 'med-' . $fileName);

                            $canvas3 = new Canvas($output_dir . $fileName);
                            $canvas3->resize(300)->save($output_dir . 'tumb-' . $fileName);

                            $canvas = new Canvas($output_dir . $fileName);
                            $canvas->resize(700)->merge($_SERVER['DOCUMENT_ROOT'] .'/images/logoToMerge.png', ["right", "bottom"])
                                ->save($output_dir . $fileName);

                            $ret[] = $fileName;
                        }

                    }
                    foreach ($ret as $k => $r) {
                        $anuncioFoto = new \AnuncioFoto\Entity();
                        $anuncioFoto->setAnuncio($novoAnuncio->getId());
                        $anuncioFoto->setFoto($r);
                        DoctrineStorage::orm()->persist($anuncioFoto);
                    }
                    DoctrineStorage::orm()->flush();

                    echo json_encode($ret);
                }

            })->via("GET", "POST");

            $app->map("/adicionar-anuncio/destacar/", function () use ($app, $controller) {
                if ($app->request()->isGet()) {

                    /** @var \Usuario\Entity $usuario */
                    $usuario = Authentication::getInstance()->getFromSession('entity');
                    if(empty($usuario)){
                        //PEGA URL
                        session_start();
                        $dominio= $_SERVER['HTTP_HOST'];
                        $url = "http://" . $dominio. $_SERVER['REQUEST_URI'];
                        $_SESSION['paginaAnterior'] = $url;

                        $app->redirect("/login/?paginaAnterior=".$url);
                    }

                    $app->render(__NAMESPACE__ . "/Views/destacar-anuncio.php");
                }

                if ($app->request()->isPost()) {

                    $intra = Authentication::getInstance();
                    $destaques = [
                        'pagina-inicial'    => $_POST['planos_pagina_inicial'],
                        'carossel-listagem' => $_POST['planos_carousel'],
                        'primeira-posicao'  => $_POST['planos_primeiras-posicoes'],
                        'oferta'            => $_POST['planos_oferta']
                    ];
                    $intra->putInSession('destaques', $destaques);

                    $app->redirect("/minha-conta/adicionar-anuncio/finalizar");
                }

            })->via("GET", "POST");

            $app->map("/adicionar-anuncio/finalizar/", function () use ($app, $controller) {
                if ($app->request()->isGet()) {
                    /** @var \Usuario\Entity $usuario */
                    $usuario = Authentication::getInstance()->getFromSession('entity');
                    if(empty($usuario)){
                        //PEGA URL
                        session_start();
                        $dominio= $_SERVER['HTTP_HOST'];
                        $url = "http://" . $dominio. $_SERVER['REQUEST_URI'];
                        $_SESSION['paginaAnterior'] = $url;

                        $app->redirect("/login/?paginaAnterior=".$url);
                    }

                    $app->render(__NAMESPACE__ . "/Views/finalizar-anuncio.php");
                }

                if ($app->request()->isPost()) {

                    $descricaoFatura = "";
                    $somaFatura = 0.0;

                    if(isset($_POST['plano']) and !empty($_POST['plano'])) {
                        $planoId = $_POST['plano'];

                    }else{
                        //$pesquisaPlano = DoctrineStorage::orm()->getRepository(\Anuncio\Entity::FULL_NAME)->findOneBy(['id' => $_POST['anuncio']]);
                        $pesquisaPlano = DoctrineStorage::orm()->getRepository(\Anuncio\Entity::FULL_NAME)->findOneBy(['id' => $_POST['anuncio']]);

                        $planoId = $pesquisaPlano->getPlano();
                    }

                    /** @var \Plano\Entity $plano */
                    $plano = DoctrineStorage::orm()->getRepository(\Plano\Entity::FULL_NAME)->findOneBy(['id' => $planoId]);

                    /** @var \FaturaPlano\Entity $faturaPlano */
                    $faturaPlano = DoctrineStorage::orm()->getRepository(\FaturaPlano\Entity::FULL_NAME)->findOneBy(['anuncio' => $_POST['anuncio']]);
                    //$faturaPlano = new \FaturaPlano\Entity();

                    if(empty($faturaPlano)) {
                        /** @var \FaturaPlano\Entity $faturaPlano */
                        $faturaPlano = new \FaturaPlano\Entity();
                        //$faturaPlano->setAnuncio($_POST['anuncio']);
                        $faturaPlano->setDiascontratados($plano->getDiasanuncio());
                        $faturaPlano->setValor($plano->getValor());
                        $faturaPlano->setDescricao($plano->getTitulo());

                        $descricaoFatura .= "Plano: {$plano->getTitulo()} ({$plano->getDiasanuncio()} dias). ";
                        $somaFatura += $faturaPlano->getValor();
                    }

                    if (isset($_POST['destaques']) and !empty($_POST['destaques'])) {

                        $getDetaques = json_decode(stripslashes($_POST['destaques']));

                        $destaques = [];
                        $anunciodestaques = [];

                        /** \Destaque\Entity $plano */
                        $destaqueRepo = DoctrineStorage::orm()->getRepository(\Destaque\Entity::FULL_NAME);
                        $destaquesDesc =[];

                        foreach ($getDetaques as $eachDest) {
                            $data = explode('|', $eachDest);
                            $tipo = $data[0]; $id = $data[1];

                            if (!in_array($tipo, ['pagina-inicial', 'carossel-listagem', 'primeira-posicao', 'oferta'])) {
                                continue;
                            }

                            $destaque = new \FaturaDestaque\Entity();

                            //$anunciodestaque = new \AnuncioDestaque\Entity();

                            /** @var \Destaque\Entity $eDestaque */
                            $eDestaque = $destaqueRepo->findOneBy(['tipo' => $tipo]);

                            if (empty($eDestaque)) {
                                continue;
                            }

                            switch ($id) {
                                case "3":
                                    $destaque->setValor($eDestaque->getValorplano3());
                                    $destaque->setDiascontratados($eDestaque->getDiasplano3());
                                    break;

                                case "2":
                                    $destaque->setValor($eDestaque->getValorplano2());
                                    $destaque->setDiascontratados($eDestaque->getDiasplano2());
                                    break;

                                case "1":
                                    $destaque->setValor($eDestaque->getValorplano1());
                                    $destaque->setDiascontratados($eDestaque->getDiasplano1());
                                    break;

                                default: break;
                            }

                            $somaFatura += $destaque->getValor();

                            $destaquesDesc[] = $eDestaque->getTitulo()." (".$destaque->getDiascontratados()." dias)";

                            $destaque->setDestaque($eDestaque->getId());
                            $destaque->setDescricao($eDestaque->getDescricao());
                            $destaques[] = $destaque;


                            /* BRUNO MARTINS
                            $anunciodestaque->setDestaque($eDestaque->getId());
                            $anunciodestaque->setDiascontratados($destaque->getDiascontratados());
                            $anunciodestaque->setAnuncio($_POST['anuncio']);
                            $anunciodestaques[] = $anunciodestaque;*/
                        }

                        if (!empty($destaques)) {
                            if(count($destaquesDesc) == 1){
								$descricaoFatura .= "Destaque: ";
							}else if (count($destaquesDesc) > 1) {
                                $descricaoFatura .= "Destaques: ";
                            }

                            $descricaoFatura .= implode(', ', $destaquesDesc) . '. ';
                        }
                    }

                    /** @var \Anuncio\Entity $novoAnuncio */
                    if (isset($_POST['anuncio'])and !empty($_POST['anuncio'])) {

                        $novoAnuncio = DoctrineStorage::orm()->getRepository(\Anuncio\Entity::FULL_NAME)->findOneBy(['id' => $_POST['anuncio']]);

                        /* BRUNO MARTINS
                        $modelAnuncio = new \Anuncio\Model(new DoctrineStorage($novoAnuncio));

                        if($plano->getDiasAnuncio() == 1){
                            $planosDias = $plano->getDiasAnuncio()." day";
                        }else if($plano->getDiasAnuncio() > 1){
                            $planosDias = $plano->getDiasAnuncio()." days";
                        }else if($plano->getDiasAnuncio() == 0){
                            $planosDias = $plano->getDiasAnuncio()." days";
                        }
                        //$novoAnuncio = new \Anuncio\Entity();
                        $novoAnuncio->setDataalteracao(new \DateTime());
                        $dataValidade = $novoAnuncio->getDataalteracao()->modify('+'.$planosDias);
                        $novoAnuncio->setDatavalidade($dataValidade);
                        $novoAnuncio->setDataalteracao(new \DateTime());

                        $modelAnuncio->update($novoAnuncio);*/


                    } else {
                        $novoAnuncio = Authentication::getInstance()->getFromSession('novoAnuncio');
                    }
                    $descricaoFatura .= "Anúncio: '" . $novoAnuncio->getTitulo() . "'";

                    /** @var \Usuario\Entity $usuario */
                    $usuario = Authentication::getInstance()->getFromSession('entity');

                    $fatura = new \Fatura\Entity();
                    $fatura->setDescricao($descricaoFatura);
                    $fatura->setValortotal($somaFatura);
                    $fatura->setUsuario($usuario->getId());
                    $fatura->setAnuncio($novoAnuncio->getId());

                    $orm = DoctrineStorage::orm();

                    $orm->persist($fatura);

                    $orm->flush();

                    if (isset($faturaPlano) and !empty($faturaPlano)) {
                        $faturaPlano->setAnuncio($fatura->getAnuncio());
                        $faturaPlano->setFatura($fatura->getId());
                        $orm->persist($faturaPlano);
                    }

                    if (isset($destaques) and !empty($destaques)) {
                        /** @var \FaturaDestaque\Entity $destaque */
                        foreach ($destaques as $destaque) {
                            $destaque->setAnuncio($fatura->getAnuncio());
                            $destaque->setFatura($fatura->getId());
                            $orm->persist($destaque);
                        }
                    }
                    $orm->flush();
                    /* BRUNO MARTINS
                    if (isset($anunciodestaques) and !empty($anunciodestaques)) {
                        /** @var \AnuncioDestaque\Entity $anunciodestaques
                        foreach ($anunciodestaques as $anunciodestaque) {
                            $anunciodestaque->setFatura($fatura->getId());
                            $dataVencimento = $anunciodestaque->getDatainicio()->modify('+'.$anunciodestaque->getDiascontratados().' days');
                            $anunciodestaque->setDatavencimento($dataVencimento);
                            $anunciodestaque->setDatainicio(new \DateTime());
                            $orm->persist($anunciodestaque);
                        }
                    }
                    $orm->flush();*/

                    $retorno = [
                        'idFatura' => $fatura->getId(),
                        'totalFatura' => $fatura->getValortotal(),
                    ];

                    if ($fatura->getValortotal() > 0) {
                        $retorno['checkoutCode'] = $controller->criarPagamento($fatura);
                    }

                    echo json_encode($retorno);
                }

            })->via("GET", "POST");

            $app->post('/criar-pagamento/', function () use ($app, $controller) {
                try {
                    if (!isset($_POST['fatura'])) {
                        throw new InvalidArgumentException("Fatura não informada.");
                    }

                    echo json_encode(['checkoutCode' => $controller->criarPagamento($_POST['fatura'])]);

                } catch(\Exception $e) {
                    die($e->getMessage());
                }
            });

            $app->post('/gravar-codigo-pagseguro/', function () use ($app, $controller) {
                try {
                    /** @var \Fatura\Entity $fatura */
                    $fatura = DoctrineStorage::orm()->getRepository(\Fatura\Entity::FULL_NAME)->findOneBy(['id' => $_POST['fatura']]);
                    $fatura->setCodigotransacao($_POST['transactionCode']);

                    /** @var \FaturaPlano\Entity $faturaplano */
                    $faturaplano = DoctrineStorage::orm()->getRepository(\FaturaPlano\Entity::FULL_NAME)->findOneBy(['anuncio' => $fatura->getAnuncio()]);

                    //BRUNO MARTINS
                    if(empty($faturaplano)){
                        if ($fatura->getCodigotransacao() == 'GRATUITO') {
                            //$fatura->statuspagseguro = 3;
                            $fatura->statuspagseguro = 0;
                            print_r($fatura);
                            DoctrineStorage::orm()->merge($fatura);
                            \Site\Controller::liberarPlano($fatura->getId());
                        } else {
                            $fatura->statuspagseguro = 1;
                            if ($faturaplano->getValor() == 0) {
                                \Site\Controller::liberarPlano($fatura->getId());
                            }
                        }
                    }else{
                        if ($fatura->getCodigotransacao() == 'GRATUITO') {
                            //$fatura->statuspagseguro = 3;
                            $fatura->statuspagseguro = 0;
                            print_r($fatura);
                            DoctrineStorage::orm()->merge($fatura);
                            \Site\Controller::liberarPlano($fatura->getId());
                        } else {
                            $fatura->statuspagseguro = 1;
                            if ($faturaplano->getValor() == 0) {
                                \Site\Controller::liberarPlano($fatura->getId());
                            }
                        }
                    }
                    DoctrineStorage::orm()->flush();

                } catch(\Exception $e) {
                    die($e->getMessage());
                }
            });

            $app->get('/adicionar-anuncio/obrigado/', function () use ($app) {

                /** @var \Usuario\Entity $usuario */
                $usuario = Authentication::getInstance()->getFromSession('entity');
                if(empty($usuario)){
                    //PEGA URL
                    session_start();
                    $dominio= $_SERVER['HTTP_HOST'];
                    $url = "http://" . $dominio. $_SERVER['REQUEST_URI'];
                    $_SESSION['paginaAnterior'] = $url;

                    $app->redirect("/login/?paginaAnterior=".$url);
                }

                /** @var \TextoSite\Entity $texto */
				$textoEmailAdm = DoctrineStorage::orm()->getRepository(\TextoSite\Entity::FULL_NAME)->findOneBy(['local' => 'anuncio-cadastrado-adm']);
				
				/** @var \TextoSite\Entity $texto */
				$textoEmailUser = DoctrineStorage::orm()->getRepository(\TextoSite\Entity::FULL_NAME)->findOneBy(['local' => 'anuncio-cadastrado-user']);

				/** @var \Usuario\Entity $usuarioLogado */
				$usuarioLogado = Authentication::getInstance()->getFromSession('entity');
				
				/** @var \Plano\Entity $plano */
				$anuncio = DoctrineStorage::orm()->getRepository(\Anuncio\Entity::FULL_NAME)->findOneBy(['usuario' => $usuarioLogado->getId()], ['id' => 'DESC']);

				if($usuarioLogado->getTipopessoa() == 1){
					$nomeourazaosocial = $usuarioLogado->getNome();
					//echo $nomeourazaosocial; die;
				}else{
					$nomeourazaosocial = $usuarioLogado->getRazaosocial();
				}
						
				$textoEmailAdmSub = str_replace(
					[
						'%usuario%',
						'%anuncio%'
					],
					[
						$nomeourazaosocial,
						$anuncio->getTitulo()
					],
					$textoEmailAdm->getTexto()
				);
				
				$app->mailer->send(
					(new Message())->setFrom('contato@ofertasagricolas.com.br', 'Ofertas Agrícolas')
						->addTo('contato@ofertasagricolas.com.br')
						->setSubject($textoEmailAdm->getTitulo()." - ".$anuncio->getTitulo())
						->setHtmlBody($textoEmailAdmSub)
				);
				
				$textoEmailUserSub = str_replace(
					[
						'%usuario%',
						'%anuncio%'
					],
					[
						$nomeourazaosocial,
						$anuncio->getTitulo()
					],
					$textoEmailUser->getTexto()
				);
				
				$app->mailer->send(
					(new Message())->setFrom('contato@ofertasagricolas.com.br', 'Ofertas Agrícolas')
						->addTo($usuarioLogado->getEmail())
						->setSubject($textoEmailUser->getTitulo()." - ".$anuncio->getTitulo())
						->setHtmlBody($textoEmailUserSub)
				);
				
                $texto = DoctrineStorage::orm()->getRepository(\TextoSite\Entity::FULL_NAME)->findOneBy(['local' => 'obrigado-por-anunciar']);
                $app->render(__NAMESPACE__ . "/Views/texto.php",
                    [
                        "texto" => [
							"obrigado" => "sim",
                            "titulo" => $texto->getTitulo(),
                            "texto" => $texto->getTexto(),
                        ]
                    ]
                );

            });

        });


        $app->notFound(function() use ($app) {
            $app->redirect("/minha-conta/");
        });

        $app->add(new MidAuthentcate($app));
    }
}
