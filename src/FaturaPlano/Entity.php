<?php

namespace FaturaPlano;

/**
 * Entity
 *
 * @Table(name="faturaplano")
 * @Entity
 */
class Entity implements \CoffeeCore\Core\Entity
{
    /**
     *
     */
    const FULL_NAME = "FaturaPlano\\Entity";

    /**
     * @var integer
     *
     * @Column(name="fatura", type="integer", nullable=false)
     * @Id
     */
    private $fatura;

    /**
     * @var integer
     * @id
     * @Column(name="anuncio", type="integer")
     */
    private $anuncio;

    /**
     * @var integer
     *
     * @Column(name="diascontratados", type="integer")
     */
    private $diascontratados;

    /**
     * @var string
     *
     * @Column(name="descricao", type="string")
     */
    private $descricao;

    /**
     * @var float
     *
     * @Column(name="valor", type="float")
     */
    private $valor;

    /**
     * @var datetime
     * @Column(name="datavencimento", type="datetime")
     */
    private $datavencimento;

    /**
     * @var datetime
     * @Column(name="datainicio", type="datetime")
     */
    private $datainicio;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getAnuncio()
    {
        return $this->anuncio;
    }

    /**
     * @param int $anuncio
     */
    public function setAnuncio($anuncio)
    {
        $this->anuncio = $anuncio;
    }

    /**
     * @return int
     */
    public function getDiascontratados()
    {
        return $this->diascontratados;
    }

    /**
     * @param int $diascontratados
     */
    public function setDiascontratados($diascontratados)
    {
        $this->diascontratados = $diascontratados;
    }

    /**
     * @return int
     */
    public function getFatura()
    {
        return $this->fatura;
    }

    /**
     * @param int $fatura
     */
    public function setFatura($fatura)
    {
        $this->fatura = $fatura;
    }

    /**
     * @return mixed
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * @param mixed $valor
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    }

    /**
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param string $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @return datetime
     */
    public function getDatainicio()
    {
        return $this->datainicio;
    }

    /**
     * @param datetime $datainicio
     */
    public function setDatainicio($datainicio)
    {
        $this->datainicio = $datainicio;
    }

    /**
     * @return datetime
     */
    public function getDatavencimento()
    {
        return $this->datavencimento;
    }

    /**
     * @param datetime $datavencimento
     */
    public function setDatavencimento($datavencimento)
    {
        $this->datavencimento = $datavencimento;
    }
}
