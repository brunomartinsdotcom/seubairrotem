<?php

namespace UsuariosDoSite;

use CoffeeCore\Storage\DoctrineStorage;
use Doctrine\ORM\Mapping as ORM;

/**
 * Usuario
 *
 * @Table(name="novo_usuarios_do_site")
 * @Entity
 */
class Entity implements \CoffeeCore\Core\Entity
{
    const FULL_NAME = "UsuariosDoSite\\Entity";

    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @Column(name="tipopessoa", type="integer", length=1, nullable=false)
     */
    private $tipopessoa;

    /**
     * @var string
     *
     * @Column(name="nome", type="string", length=100, nullable=true)
     */
    private $nome;

    /**
     * @var string
     *
     * @Column(name="cpf", type="string", length=11, nullable=true)
     */
    private $cpf;

    /**
     * @var string
     *
     * @Column(name="rg", type="string", length=30, nullable=true)
     */
    private $rg;

    /**
     * @var \DateTime
     *
     * @Column(name="datadenascimento", type="date", nullable=true)
     */
    private $datadenascimento;

    /**
     * @var string
     *
     * @Column(name="razaosocial", type="string", length=100, nullable=true)
     */
    private $razaosocial;

    /**
     * @var string
     *
     * @Column(name="nomefantasia", type="string", length=100, nullable=true)
     */
    private $nomefantasia;

    /**
     * @var string
     *
     * @Column(name="cnpj", type="string", length=14, nullable=true)
     */
    private $cnpj;

    /**
     * @var string
     *
     * @Column(name="inscricaoestadual", type="string", length=20, nullable=true)
     */
    private $inscricaoestadual;

    /**
     * @var string
     *
     * @Column(name="nomedoresponsavel", type="string", length=100, nullable=true)
     */
    private $nomedoresponsavel;

    /**
     * @var string
     *
     * @Column(name="email", type="string", length=100, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @Column(name="cep", type="string", length=9, nullable=false)
     */
    private $cep;

    /**
     * @var string
     *
     * @Column(name="endereco", type="string", length=200, nullable=false)
     */
    private $endereco;

    /**
     * @var string
     *
     * @Column(name="numero", type="string", length=15, nullable=false)
     */
    private $numero;

    /**
     * @var string
     *
     * @Column(name="bairro", type="string", length=50, nullable=false)
     */
    private $bairro;

    /**
     * @var string
     *
     * @Column(name="complemento", type="string", length=100, nullable=true)
     */
    private $complemento;

    /**
     * @var integer
     *
     * @Column(name="estado", type="integer", length=11, nullable=false)
     */
    private $estado;

    /**
     * @var integer
     *
     * @Column(name="cidade", type="integer", length=11, nullable=false)
     */
    private $cidade;

    /**
     * @var string
     *
     * @Column(name="telefone", type="string", length=14, nullable=true)
     */
    private $telefone;

    /**
     * @var string
     *
     * @Column(name="celular", type="string", length=15, nullable=true)
     */
    private $celular;

    /**
     * @var string
     *
     * @Column(name="login", type="string", length=20, nullable=false)
     */
    private $login;

    /**
     * @var string
     *
     * @Column(name="senha", type="string", length=12, nullable=false)
     */
    private $senha;

    /**
     * @var integer
     *
     * @Column(name="comoconheceu", type="integer", nullable=true)
     */
    private $comoconheceu;

    /**
     * @var string
     *
     * @Column(name="comoconheceuoutro", type="string", length=100, nullable=true)
     */
    private $comoconheceuoutro;

    /**
     * @var boolean
     *
     * @Column(name="ativo", type="boolean", nullable=false)
     */
    private $ativo = '1';



    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $tipopessoa
     */
    public function setTipopessoa($tipopessoa)
    {
        $this->tipopessoa = $tipopessoa;
    }

    /**
     * @return int
     */
    public function getTipopessoa()
    {
        return $this->tipopessoa;
    }

    /**
     * @param string $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $cpf
     */
    public function setCpf($cpf)
    {
        $cpf = str_replace(['.', ',', '/', '\\', '-'], '', $cpf);
        $this->cpf = $cpf;
    }

    /**
     * @return string
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * @return string
     */
    public function getCpfmask()
    {
        return substr($this->cpf,0,3).".".substr($this->cpf,3,3).".".substr($this->cpf,6,3)."-".substr($this->cpf,9,2);
    }

    /**
     * @param string $rg
     */
    public function setRg($rg)
    {
        $this->rg = $rg;
    }

    /**
     * @return string
     */
    public function getRg()
    {
        return $this->rg;
    }

    /**
     * @param \DateTime $datadenascimento
     */
    public function setDatadenascimento($datadenascimento)
    {
        $this->datadenascimento = $datadenascimento;
    }

    /**
     * @return \DateTime
     */
    public function getDatadenascimento()
    {
        return $this->datadenascimento;
    }


    /**
     * @param string $razaosocial
     */
    public function setRazaosocial($razaosocial)
    {
        $this->razaosocial = $razaosocial;
    }

    /**
     * @return string
     */
    public function getRazaosocial()
    {
        return $this->razaosocial;
    }

    /**
     * @param string $nomefantasia
     */
    public function setNomefantasia($nomefantasia)
    {
        $this->nomefantasia = $nomefantasia;
    }

    /**
     * @return string
     */
    public function getNomefantasia()
    {
        return $this->nomefantasia;
    }

    /**
     * @param string $cnpj
     */
    public function setCnpj($cnpj)
    {
        $cnpj = str_replace(['.', ',', '/', '\\', '-'], '', $cnpj);
        $this->cnpj = $cnpj;
    }

    /**
     * @return string
     */
    public function getCnpj()
    {
        return $this->cnpj;
    }

    /**
     * @return string
     */
    public function getCnpjmask()
    {
        return substr($this->cnpj,0,2).".".substr($this->cnpj,2,3).".".substr($this->cnpj,5,3)."/".substr($this->cnpj,8,4)."-".substr($this->cnpj,12,2);
    }

    /**
     * @param string $inscricaoestadual
     */
    public function setInscricaoestadual($inscricaoestadual)
    {
        $this->inscricaoestadual = $inscricaoestadual;
    }

    /**
     * @return string
     */
    public function getInscricaoestadual()
    {
        return $this->inscricaoestadual;
    }

    /**
     * @param string $nomedoresponsavel
     */
    public function setNomedoresponsavel($nomedoresponsavel)
    {
        $this->nomedoresponsavel = $nomedoresponsavel;
    }

    /**
     * @return string
     */
    public function getNomedoresponsavel()
    {
        return $this->nomedoresponsavel;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $cep
     */
    public function setCep($cep)
    {
        $this->cep = $cep;
    }

    /**
     * @return string
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * @param string $endereco
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    }

    /**
     * @return string
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param string $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    /**
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param string $bairro
     */
    public function setBairro($bairro)
    {
        $this->bairro = $bairro;
    }

    /**
     * @return string
     */
    public function getBairro()
    {
        return $this->bairro;
    }

    /**
     * @param string $complemento
     */
    public function setComplemento($complemento)
    {
        $this->complemento = $complemento;
    }

    /**
     * @return string
     */
    public function getComplemento()
    {
        return $this->complemento;
    }

    /**
     * @param int $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return int
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @return string
     */
    public function getNomeEstado()
    {
        $estados = \CoffeeCore\Helper\ResourceManager::readFile("estados.json");
        foreach ($estados as $k => $estado) {
            if($estado["name"] = $this->estado){
                return $estado['description'];
            }
        }
    }

    /**
     * @param int $cidade
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;
    }

    /**
     * @return int
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * @return string
     */
    public function getNomeCidade()
    {
        $nomeCidade = DoctrineStorage::orm()
            ->getRepository(\TodasCidades\Entity::FULL_NAME)
            ->findOneBy(["id" => $this->cidade])
            ->getNome();
        return $nomeCidade;
    }

    /**
     * @param string $telefone
     */
    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;
    }

    /**
     * @return string
     */
    public function getTelefone()
    {
        return $this->telefone;
    }

    /**
     * @param string $celular
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;
    }

    /**
     * @return string
     */
    public function getCelular()
    {
        return $this->celular;
    }


    /**
     * @param string $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param string $senha
     */
    public function setSenha($senha)
    {
        $this->senha = $senha;
    }

    /**
     * @return string
     */
    public function getSenha()
    {
        return $this->senha;
    }

    /**
     * @param int $comoconheceu
     */
    public function setComoconheceu($comoconheceu)
    {
        $this->comoconheceu = $comoconheceu;
    }

    /**
     * @return int
     */
    public function getComoconheceu()
    {
        return $this->comoconheceu;
    }

    /**
     * @param string $comoconheceuoutro
     */
    public function setComoconheceuoutro($comoconheceuoutro)
    {
        $this->comoconheceuoutro = $comoconheceuoutro;
    }

    /**
     * @return string
     */
    public function getComoconheceuoutro()
    {
        return $this->comoconheceuoutro;
    }

    /**
     * @param boolean $ativo
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
    }

    /**
     * @return boolean
     */
    public function getAtivo()
    {
        return $this->ativo;
    }
}
