<?php

namespace UsuariosDoSite;

use Anuncio\Entity;
use CoffeeCore\Core\AbstractController;
use CoffeeCore\Storage\DoctrineStorage;

/**
 * Class Controller
 * @package UsuariosDoSite
 */
class Controller extends AbstractController
{
    /**
     * @var string
     */
    public $pagina = 'usuarios-do-site';
    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Entity
     */
    protected $entity;

    /**
     * @return void
     */
    public function getDataTable()
    {
        $this->startConnection();
        $columns = ['id', 'nome', 'nomefantasia', 'email', 'login'];

        $search = isset($_POST['search']['value']) ? $_POST['search']['value'] : ""; //$_POST['search']['value'];

        $limit = isset($_POST['length']) ? $_POST['length'] : 10;
        $offset = isset($_POST['start']) ? $_POST['start'] : 0;
        if(isset($_POST['order'])){
            $orderBy = [
                $_POST['order'][0]['column'],
                $_POST['order'][0]['dir']
            ];
        }else{
            $orderBy = [0, 'ASC'];
        }

        $dataTables = $this->model->getDataTableData($columns, $orderBy, $search, $limit, $offset);

        foreach ($dataTables['data'] as $k => &$row) {
            if (empty($row[1])) {
                $row[1] = $row[2];
            }

            unset($dataTables['data'][$k][2]);
            $dataTables['data'][$k] = array_values($dataTables['data'][$k]);

            $row[] = "<span class='center'>
                        <button class='btn btn-info' onclick='javascript:window.location = \"/painel/{$this->pagina}/vizualizar/{$row[0]}\"'>
                            <span><i class='icon-eye-open'></i></span>
                        </button>
                        </span>";
            $row[] = "<span class='center'>
                        <button data-url='/painel/{$this->pagina}/deletar/{$row[0]}' data-msg='ATENÇÃO!!! Tudo relacionado a este usuário será perdido.<br>Tem certeza que deseja excluir?<br>Isso será um ato irreversível!' id='deletar' class='btn btn-danger'>
                            <span><i class='icon-trash'></i></span>
                        </button>
                     </span>";
        }

        //header('Content-Type: application/json');
        echo json_encode($dataTables);
    }

    /**
     *
     */
    public function startConnection()
    {
        $this->entity = new \UsuariosDoSite\Entity();
        $this->model = new Model(new DoctrineStorage($this->entity));
    }

    /**
     * @param int|null $id
     */
    public function deletar($id = null)
    {
        $this->startConnection();
        if (!empty($id)) {

            //$anuncios = DoctrineStorage::orm()->getRepository(Entity::FULL_NAME)->findBy(['usuario' => $id]);

            //foreach ($anuncios as $anuncio) {

            //}

            $this->entity = $this->model->findOneBy(["id"=>$id]);
            $this->model->delete($this->entity);
            $this->model->flush();
        }
    }
}
