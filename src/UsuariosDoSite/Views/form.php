<?php
include 'inc_head.php';
?>
</head>
<body>
    <div class="layout">
    <?php
        include_once 'inc_header.php';
        include_once 'inc_usuario.php';
        include_once 'inc_sidebar.php';
    ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">Usuários do Site</h3>
                        <span class="pull-right top-right-toolbar">
                        </span>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="<?=$prefix;?>" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li>
                            <a href="<?=$prefix;?>usuarios-do-site" title="Usuários do Site">Usuários do Site</a><span class="divider">
                                <i class="icon-angle-right"></i>
                            </span>
                        </li>
                        <li class="active">Vizualização</li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="content-widgets light-gray">
                    <div class="widget-head black">
                        <h3>Dados do Usuário</h3>
                    </div>
                    <div class="widget-container">
                        <div class="tab-content">
                            <div class="tab-pane active" id="user">
                                <div class=" information-container">
                                    <form name="form1" id="form1" class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                                    <!-- .control-group -->
                                    <div class="control-group">
                                        <label class="control-label"><b>Tipo de Pessoa:</b></label>
                                        <!-- .controls -->
                                        <div class="controls">
                                            <input type="text" maxlength="70" style="border:none; background-color: #eee" readonly value=" <?=($usuario->getTipopessoa() == 1 ? "Física" : "Jurídica");?>" class="span7" />
                                        </div><!-- /.controls -->
                                    </div><!-- /.control-group -->
                                        <?php if($usuario->getTipopessoa() == 1) { ?>
                                        <!-- .control-group -->
                                        <div class="control-group" id="nome">
                                            <label class="control-label"><b>Nome:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <input type="text" maxlength="70" style="border:none; background-color: #eee" readonly value="<?= $usuario->getNome(); ?>" class="span7" />
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <!-- .control-group -->
                                        <div class="control-group" id="cpf">
                                            <label class="control-label"><b>CPF:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <input type="text" maxlength="70" style="border:none; background-color: #eee" readonly value="<?= $usuario->getCpf(); ?>" class="span7" />
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Data de Nascimento:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <input type="text" maxlength="70" style="border:none; background-color: #eee" readonly value="<?= $usuario->getDatadenascimento()->format("d/m/Y"); ?>" class="span7" />
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <?php }else{ ?>
                                        <!-- .control-group -->
                                        <div class="control-group" id="razaosocial">
                                            <label class="control-label"><b>Razão Social:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <input type="text" maxlength="70" style="border:none; background-color: #eee" readonly value="<?= $usuario->getRazaosocial(); ?>" class="span7" />
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <!-- .control-group -->
                                        <div class="control-group" id="nomedefantasia">
                                            <label class="control-label"><b>Nome de Fantasia:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <input type="text" maxlength="70" style="border:none; background-color: #eee" readonly value="<?= $usuario->getNomefantasia();?>" class="span7" />
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <!-- .control-group -->
                                        <div class="control-group" id="cnpj">
                                            <label class="control-label"><b>CNPJ:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <input type="text" maxlength="70" style="border:none; background-color: #eee" readonly value="<?= $usuario->getCnpj(); ?>" class="span7" />
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <!-- .control-group -->
                                        <div class="control-group" id="ie">
                                            <label class="control-label"><b>Inscrição Estadual:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <input type="text" maxlength="70" style="border:none; background-color: #eee" readonly value="<?= $usuario->getInscricaoestadual(); ?>" class="span7" />
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <div class="control-group" id="nomedoresponsavel">
                                            <label class="control-label"><b>Nome do Responsável:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <input type="text" maxlength="70" style="border:none; background-color: #eee" readonly value="<?= $usuario->getNomeresponsavel(); ?>" class="span7" />
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <?php } ?>
                                        <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>CEP:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <input type="text" maxlength="70" style="border:none; background-color: #eee" readonly value="<?= $usuario->getCep(); ?> " class="span7" />
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Endereço:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <input type="text" maxlength="70" style="border:none; background-color: #eee" readonly value="<?= $usuario->getEndereco();?>" class="span7" />
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Número:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <input type="text" maxlength="70" style="border:none; background-color: #eee" readonly value="<?= $usuario->getNumero();?>" class="span7" />
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Bairro:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <input type="text" maxlength="70" style="border:none; background-color: #eee" readonly value="<?= $usuario->getBairro();?>" class="span7" />
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Complemento:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <input type="text" maxlength="70" style="border:none; background-color: #eee" readonly value="<?= $usuario->getComplemento();?>" class="span7" />
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Cidade:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <input type="text" maxlength="70" style="border:none; background-color: #eee" readonly value="<?= $usuario->getNomeCidade() . ' - ' . $usuario->getNomeEstado();?>" class="span7" />
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Telefone:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <input type="text" maxlength="70" style="border:none; background-color: #eee" readonly value="<?= $usuario->getTelefone();?>" class="span7" />
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Celular:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <input type="text" maxlength="70" style="border:none; background-color: #eee" readonly value="<?= $usuario->getCelular();?>" class="span7" />
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <!-- .control-group -->
                                        <?php
                                            if(!empty($usuario->getComoconheceu())) {
                                                $comoconheceu = \CoffeeCore\Storage\DoctrineStorage::orm()
                                                    ->getRepository(ViaConhecimento\Entity::FULL_NAME)
                                                    ->findOneBy(['id' => $usuario->getComoconheceu()])
                                                    ->getTitulo();
                                            }else{
                                                $comoconheceu = $usuario->getComoconheceuoutro();
                                            }
                                        ?>
                                        <div class="control-group">
                                            <label class="control-label"><b>Como Conheceu:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <input type="text" maxlength="70" style="border:none; background-color: #eee" readonly value="<?= $comoconheceu ?>" class="span7" />
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Email:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <input type="text" maxlength="70" style="border:none; background-color: #eee" readonly value="<?= $usuario->getEmail();?>" class="span7" />
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Usuário/Login:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <input type="text" maxlength="70" style="border:none; background-color: #eee" readonly value="<?= $usuario->getLogin();?>" class="span7" />
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <div class="form-actions">
                                            <a href="/painel/usuarios-do-site" class="btn btn-default right-float">Voltar</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

<?php
include 'inc_footer.php';
