<?php

namespace FaturaDestaque;

/**
 * Entity
 *
 * @Table(name="faturadestaque")
 * @Entity
 */
class Entity implements \CoffeeCore\Core\Entity
{
    /**
     *
     */
    const FULL_NAME = "FaturaDestaque\\Entity";

    /**
     * @var integer
     *
     * @Column(name="fatura", type="integer", nullable=false)
     * @Id
     */
    private $fatura;

    /**
     * @var integer
     * @id
     * @Column(name="anuncio", type="integer")
     */
    private $anuncio;

    /**
     * @var integer
     * @id
     * @Column(name="destaque", type="integer")
     */
    private $destaque;

    /**
     * @var integer
     *
     * @Column(name="diascontratados", type="integer")
     */
    private $diascontratados;

    /**
     * @var float
     *
     * @Column(name="valor", type="float")
     */
    private $valor;

    /**
     * @var string
     *
     * @Column(name="descricao", type="string")
     */
    private $descricao;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getAnuncio()
    {
        return $this->anuncio;
    }

    /**
     * @param int $anuncio
     */
    public function setAnuncio($anuncio)
    {
        $this->anuncio = $anuncio;
    }

    /**
     * @return int
     */
    public function getDestaque()
    {
        return $this->destaque;
    }

    /**
     * @param int $destaque
     */
    public function setDestaque($destaque)
    {
        $this->destaque = $destaque;
    }

    /**
     * @return int
     */
    public function getDiascontratados()
    {
        return $this->diascontratados;
    }

    /**
     * @param int $diascontratados
     */
    public function setDiascontratados($diascontratados)
    {
        $this->diascontratados = $diascontratados;
    }

    /**
     * @return int
     */
    public function getFatura()
    {
        return $this->fatura;
    }

    /**
     * @param int $fatura
     */
    public function setFatura($fatura)
    {
        $this->fatura = $fatura;
    }

    /**
     * @return mixed
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * @param mixed $valor
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    }

    /**
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param string $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }
}
