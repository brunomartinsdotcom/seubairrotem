<?php

namespace AnuncioEntrega;

/**
 * Entity
 *
 * @Table(name="anuncioformaentrega")
 * @Entity
 */
class Entity implements \CoffeeCore\Core\Entity
{
    /**
     *
     */
    const FULL_NAME = "AnuncioEntrega\\Entity";

    /**
     * @var integer
     * @Id
     * @Column(name="anuncio", type="integer", nullable=false)
     */
    private $anuncio;

    /**
     * @var integer
     * @Id
     * @Column(name="formaentrega", type="integer", nullable=false)
     */
    private $formaentrega;

    /**
     * @return int
     */
    public function getAnuncio()
    {
        return $this->anuncio;
    }

    /**
     * @param int $anuncio
     */
    public function setAnuncio($anuncio)
    {
        $this->anuncio = $anuncio;
    }

    /**
     * @return int
     */
    public function getFormaentrega()
    {
        return $this->formaentrega;
    }

    /**
     * @param int $formaentrega
     */
    public function setFormaentrega($formaentrega)
    {
        $this->formaentrega = $formaentrega;
    }
}