<?php
include 'inc_head.php';

$action = "Adicionar";
if (isset($viaConhecimento)) {
    $action = "Editar";
    $dados = [
        'id' => $viaConhecimento->getId(),
        'titulo' => $viaConhecimento->getTitulo()
    ];
} else {
    $dados = [
        'id' => '',
        'titulo' => ''
    ];
}
?>
</head>
<body>
    <div class="layout">
    <?php
        include_once 'inc_header.php';
        include_once 'inc_usuario.php';
        include_once 'inc_sidebar.php';
    ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">Vias de Conhecimento</h3>

                    </div>
                    <ul class="breadcrumb">
                        <li><a href="<?=$prefix;?>" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li>
                            <a href="<?=$prefix;?>vias-de-conhecimento">Vias de Conhecimento</a><span class="divider">
                                <i class="icon-angle-right"></i>
                            </span>
                        </li>
                        <li class="active"><?=$action;?></li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="content-widgets light-gray">
                    <div class="widget-head black">
                        <h3><?=$action;?> Via de Conhecimento</h3>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function(){
                            // valida o formulário
                            $('#vias-de-conhecimento').validate({
                                // define regras para os campos
                                rules: {
                                    titulo: {
                                        required: true
                                    }
                                },
                                // define messages para cada campo
                                messages: {
                                    titulo:"Informe o título para a via de conhecimento"
                                }
                            });
                        });
                    </script>
                    <div class="widget-container">
                        <form name="vias-de-conhecimento" id="vias-de-conhecimento" class="form-horizontal" action="<?=$_SERVER["REQUEST_URI"];?>" method="post">
                            <input name="id" id="id" type="hidden" value="<?=$dados['id'];?>">
                            <div class="control-group">
                                <label for="nome" class="control-label">Título</label>
                                <div class="controls">
                                    <input name="titulo" id="titulo" type="text" maxlength="50" class="span6" value="<?=$dados['titulo'];?>">
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" id="Bt" class="btn btn-primary">Salvar</button>
                                <button type="button" onclick="window.open('<?=$prefix;?>vias-de-conhecimento', '_self');" class="btn right-float">Voltar</button>
                            </div>
                        </form>
                    </div>
                </div>

<?php
include 'inc_footer.php';
