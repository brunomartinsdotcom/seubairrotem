<?php

namespace DadosDoSite;

/**
 * Entity
 *
 * @Table(name="novo_dados_do_site")
 * @Entity
 */
class Entity implements \CoffeeCore\Core\Entity
{
    /**
     *
     */
    const FULL_NAME = "DadosDoSite\\Entity";

    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="SEQUENCE")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="titulo", type="string", length=100, nullable=false)
     */
    private $titulo;
	
	/**
     * @var string
     *
     * @Column(name="slogan", type="string", length=100, nullable=false)
     */
    private $slogan;
	
	/**
     * @var string
     *
     * @Column(name="telefone1", type="string", length=15, nullable=true)
     */
    private $telefone1;
	
	/**
     * @var string
     *
     * @Column(name="telefone2", type="string", length=15, nullable=true)
     */
    private $telefone2;

    /**
     * @var string
     *
     * @Column(name="endereco", type="string", length=255, nullable=true)
     */
    public $endereco;

    /**
     * @var string
     *
     * @Column(name="cep", type="string", length=9, nullable=true)
     */
    public $cep;
	
	/**
     * @var string
     *
     * @Column(name="email", type="string", length=255, nullable=false)
     */
    public $email;
	
    /**
     * @var string
     *
     * @Column(name="url", type="string", length=255, nullable=false)
     */
    private $url;
	
	/**
     * @var string
     *
     * @Column(name="facebook", type="string", length=255, nullable=true)
     */
    private $facebook;

    /**
     * @var string
     *
     * @Column(name="twitter", type="string", length=255, nullable=true)
     */
    private $twitter;

    /**
     * @var string
     *
     * @Column(name="instagram", type="string", length=255, nullable=true)
     */
    private $instagram;

    /**
     * @var string
     *
     * @Column(name="youtube", type="string", length=255, nullable=true)
     */
    private $youtube;

    /**
     * @var string
     *
     * @Column(name="googlemaps", type="text", nullable=true)
     */
    private $googlemaps;

    /**
     * @var string
     *
     * @Column(name="analytics", type="text", nullable=true)
     */
    private $analytics;

    /**
     * @param boolean $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return boolean
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

	/**
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }
	
	/**
     * @param string $slogan
     */
    public function setSlogan($slogan)
    {
        $this->slogan = $slogan;
    }

    /**
     * @return string
     */
    public function getSlogan()
    {
        return $this->slogan;
    }

    /**
     * @param string $telefone1
     */
    public function setTelefone1($telefone1)
    {
        $this->telefone1 = $telefone1;
    }

    /**
     * @return string
     */
    public function getTelefone1()
    {
        return $this->telefone1;
    }

    /**
     * @return string
     */
    public function getTelefone2()
    {
        return $this->telefone2;
    }

    /**
     * @param string $telefone2
     */
    public function setTelefone2($telefone2)
    {
        $this->telefone2 = $telefone2;
    }

    /**
     * @return string
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param string $endereco
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    }

    /**
     * @return string
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * @param string $cep
     */
    public function setCep($cep)
    {
        $this->cep = $cep;
    }
	
	/**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
	
	/**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
	
	/**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }
	
	/**
     * @return string
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * @param string $facebook
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;
    }

    /**
     * @return string
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * @param string $twitter
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;
    }
	
	/**
     * @return string
     */
    public function getInstagram()
    {
        return $this->instagram;
    }

    /**
     * @param string $instagram
     */
    public function setInstagram($instagram)
    {
        $this->instagram = $instagram;
    }

    /**
     * @return string
     */
    public function getYoutube()
    {
        return $this->youtube;
    }

    /**
     * @param string $youtube
     */
    public function setYoutube($youtube)
    {
        $this->youtube = $youtube;
    }

    /**
     * @return string
     */
    public function getGooglemaps()
    {
        return $this->googlemaps;
    }

    /**
     * @param string $googlemaps
     */
    public function setGooglemaps($googlemaps)
    {
        $this->googlemaps = $googlemaps;
    }

    /**
     * @return string
     */
    public function getAnalytics()
    {
        return $this->analytics;
    }

    /**
     * @param string $analytics
     */
    public function setAnalytics($analytics)
    {
        $this->analytics = $analytics;
    }
}