<?php

namespace DadosDoSite;

use CoffeeCore\Core\AbstractController;
use CoffeeCore\Helper\ResourceManager;
use CoffeeCore\Storage\DoctrineStorage;

/**
 * Class Controller
 * @package Subcategoria
 */
class Controller extends AbstractController
{
    /**
     * @var string
     */
    public $pagina = 'dados-do-site';
    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Entity
     */
    protected $entity;

    /**
     * @return void
     */

    public function startConnection()
    {
        $this->entity = new \DadosDoSite\Entity();
        $this->model = new Model(new DoctrineStorage($this->entity));
    }

    /**
     *
     */
    public function gravar()
    {

        if (empty($this->entity) or empty($this->model)) {
            $this->startConnection();
        }

        $this->entity = $this->model->findOneBy(['id' => $_POST['id']]);

        $this->entity->setTitulo($_POST['titulo']);
        $this->entity->setSlogan($_POST['slogan']);
        if (!empty($_POST['telefone1'])) {
            $this->entity->setTelefone1($_POST['telefone1']);
        }
        if (!empty($_POST['telefone2'])) {
            $this->entity->setTelefone2($_POST['telefone2']);
        }
        if (!empty($_POST['endereco'])) {
            $this->entity->setEndereco($_POST['endereco']);
        }
        if (!empty($_POST['cep'])) {
            $this->entity->setCep($_POST['cep']);
        }
        $this->entity->setEmail($_POST['email']);
        $this->entity->setUrl($_POST['url']);
        if (!empty($_POST['facebook'])) {
            $this->entity->setFacebook($_POST['facebook']);
        }
        if (!empty($_POST['twitter'])) {
            $this->entity->setTwitter($_POST['twitter']);
        }
        if (!empty($_POST['instagram'])) {
            $this->entity->setInstagram($_POST['instagram']);
        }
        if (!empty($_POST['youtube'])) {
            $this->entity->setYoutube($_POST['youtube']);
        }
        if (!empty($_POST['googlemaps'])) {
            $this->entity->setGooglemaps($_POST['googlemaps']);
        }
        if (!empty($_POST['analytics'])) {
            $this->entity->setAnalytics($_POST['analytics']);
        }

        $this->model->update($this->entity);
        $this->model->flush();

        $lista = $this->model->findBy([]);

        $pubs = [];
        while ($lista->valid()) {
            $pubs["titulo"] = $lista->current()->getTitulo();
            $pubs["slogan"] = $lista->current()->getSlogan();
            $pubs["telefone1"] = $lista->current()->getTelefone1();
            $pubs["telefone2"] = $lista->current()->getTelefone2();
            $pubs["endereco"] = $lista->current()->getEndereco();
            $pubs["cep"] = $lista->current()->getCep();
            $pubs["email"] = $lista->current()->getEmail();
            $pubs["url"] = $lista->current()->getUrl();
            $pubs["facebook"] = $lista->current()->getFacebook();
            $pubs["twitter"] = $lista->current()->getTwitter();
            $pubs["instagram"] = $lista->current()->getInstagram();
            $pubs["youtube"] = $lista->current()->getYoutube();

            $pubs["analytics"] = $lista->current()->getAnalytics();
            $pubs["googlemaps"] = $lista->current()->getGooglemaps();
            $lista->next();
        }

        ResourceManager::writeFile("dadosDoSite.json", json_encode($pubs));
    }
}
