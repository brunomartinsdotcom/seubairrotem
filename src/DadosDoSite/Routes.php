<?php

namespace DadosDoSite;

use CoffeeCore\Storage\DoctrineStorage;
use Slim\Slim;

/**
 * Class Routes
 * @package DadosDoSite
 */
class Routes
{
    /**
     * @param Slim $app
     */
    public static function getRoutes(Slim $app)
    {

        $controller = new Controller();

        $app->group("/{$controller->pagina}", function () use ($app, $controller) {

            $app->map(
                '/',
                function () use ($app, $controller) {
                    // Variável que recebe o registro cadastrado no banco.
                    $id = 1;
                    if ($app->request()->isGet()) {
                        $registro = DoctrineStorage::orm()
                            ->getRepository(__NAMESPACE__."\\Entity")
                            ->findOneBy(["id" => $id]);
                        $app->render(__NAMESPACE__."/Views/form.php", ["dadosdosite" => $registro]);
                    }
                    if ($app->request()->isPost()) {
                        try {
                            $controller->gravar();
                            $app->flash("info", "Alterado com sucesso!");
                        } catch (\Exception $e) {
                            $app->flash("info", "Erro ao alterar. " . addslashes($e->getMessage()));
                        }
                        $app->redirect("/painel/{$controller->pagina}/");
                    }
                }
            )->via("GET", "POST");
        });
    }
}
