<?php
include 'inc_head.php';

/** @var \DadosDoSite\Entity $dadosdosite */
if (isset($dadosdosite)) {
    $action = "Editar";
    $dados = [
        'id'          => $dadosdosite->getId(),
        'titulo'      => $dadosdosite->getTitulo(),
        'slogan'      => $dadosdosite->getSlogan(),
        'telefone1'   => $dadosdosite->getTelefone1(),
        'telefone2'   => $dadosdosite->getTelefone2(),
        'endereco'    => $dadosdosite->getEndereco(),
        'cep'         => $dadosdosite->getCep(),
        'email'       => $dadosdosite->getEmail(),
        'url'         => $dadosdosite->getUrl(),
        'facebook'    => $dadosdosite->getFacebook(),
        'twitter'     => $dadosdosite->getTwitter(),
        'instagram'   => $dadosdosite->getInstagram(),
        'youtube'     => $dadosdosite->getYoutube(),
        'googlemaps'  => $dadosdosite->getGooglemaps(),
        'analytics'   => $dadosdosite->getAnalytics()
    ];
}
?>
<title>Dados do Site - Painel</title>
</head>
<body>
    <div class="layout">
    <?php
        include_once 'inc_header.php';
        include_once 'inc_usuario.php';
        include_once 'inc_sidebar.php';
        $NameMsg = 'Site';
    ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">Dados do Site</h3>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="<?=$prefix;?>" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li>Dados do Site<span class="divider"><i class="icon-angle-right"></i></span></li>
                        <li class="active"><?=$action;?></li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="content-widgets light-gray">
                    <div class="widget-head black">
                        <h3><?=$action;?> Dados do Site</h3>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function(){
                            // valida o formulário
                            $('#dadosdosite').validate({
                                // define regras para os campos
                                rules: {
                                    titulo: {
                                        required: true,
                                        minlength: 4
                                    },
                                    slogan: {
                                        required: true,
                                        minlength: 4
                                    },
                                    email: {
                                        required: true,
                                        email:true
                                    },
                                    url: {
                                        required: true,
                                        url:true
                                    }
                                },
                                // define messages para cada campo
                                messages: {
                                    titulo:       "Informe o título do <?= $NameMsg; ?>",
                                    slogan:       "Informe o slogan do <?= $NameMsg; ?>",
                                    email:        "Informe o email do <?= $NameMsg; ?>",
                                    url:          "Informe a url do <?= $NameMsg; ?>"
                                }
                            });
                        });
                    </script>
                    <div class="widget-container">
                        <form name="dadosdosite" id="dadosdosite" enctype="multipart/form-data" class="form-horizontal" action="<?=$_SERVER["REQUEST_URI"];?>" method="post">
                            <input name="id" id="id" type="hidden" value="<?=$dados['id'];?>">
                             <!-- .control-group -->
                            <div class="control-group">
                                <label for="titulo" class="control-label">Título</label>
                                <!-- .controls -->
                                <div class="controls">
                                    <input name="titulo" id="titulo" type="text" maxlength="100" class="span8" value="<?=$dados['titulo'];?>">
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                              <!-- .control-group -->
                            <div class="control-group">
                                <label for="slogan" class="control-label">Slogan</label>
                                <!-- .controls -->
                                <div class="controls">
                                    <input name="slogan" id="slogan" type="text" maxlength="100" class="span8" value="<?=$dados['slogan'];?>">
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                             <!-- .control-group -->
                            <div class="control-group">
                                <label for="telefone1" class="control-label">Telefone 01</label>
                                <!-- .controls -->
                                <div class="controls">
                                    <input name="telefone1" id="telefone1" type="text" maxlength="14" class="span4" value="<?=$dados['telefone1'];?>">
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                              <!-- .control-group -->
                            <div class="control-group">
                                <label for="telefone2" class="control-label">Telefone 02</label>
                                <!-- .controls -->
                                <div class="controls">
                                    <input name="telefone2" id="telefone2" type="text" maxlength="15" class="span4" value="<?=$dados['telefone2'];?>">
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                            <!-- .control-group -->
                            <div class="control-group">
                                <label for="endereco" class="control-label">Endereço</label>
                                <!-- .controls -->
                                <div class="controls">
                                    <input name="endereco" id="endereco" type="text" maxlength="255" class="span10" value="<?=$dados['endereco'];?>">
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                             <!-- .control-group -->
                            <div class="control-group">
                                <label for="cep" class="control-label">CEP</label>
                                <!-- .controls -->
                                <div class="controls">
                                    <input name="cep" id="cep" type="text" maxlength="9" class="span2" value="<?=$dados['cep'];?>" />
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                             <!-- .control-group -->
                            <div class="control-group">
                                <label for="email" class="control-label">E-mail</label>
                                <!-- .controls -->
                                <div class="controls">
                                    <input name="email" id="email" type="text" maxlength="255" class="span5" value="<?=$dados['email'];?>" />
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                             <!-- .control-group -->
                            <div class="control-group">
                                <label for="url" class="control-label">URL</label>
                                <!-- .controls -->
                                <div class="controls">
                                    <input name="url" id="url" type="text" maxlength="255" class="span5" value="<?=$dados['url'];?>" />
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                             <!-- .control-group -->
                            <div class="control-group">
                                <label for="facebook" class="control-label">Facebook</label>
                                <!-- .controls -->
                                <div class="controls">
                                    <input name="facebook" id="facebook" type="text" maxlength="255" class="span5" value="<?=$dados['facebook'];?>" placeholder="http://" />
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                            <!-- .control-group -->
                            <div class="control-group">
                                <label for="twitter" class="control-label">Twitter</label>
                                <!-- .controls -->
                                <div class="controls">
                                    <input name="twitter" id="twitter" type="text" maxlength="255" class="span5" value="<?=$dados['twitter'];?>" placeholder="http://" /> <em>(Opcional)</em>
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                             <!-- .control-group -->
                            <div class="control-group">
                                <label for="instagram" class="control-label">Instagram</label>
                                <!-- .controls -->
                                <div class="controls">
                                    <input name="instagram" id="instagram" type="text" maxlength="255" class="span5" value="<?=$dados['instagram'];?>" placeholder="http://" /> <em>(Opcional)</em>
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                             <!-- .control-group -->
                            <div class="control-group">
                                <label for="youtube" class="control-label">Youtube</label>
                                <!-- .controls -->
                                <div class="controls">
                                    <input name="youtube" id="youtube" type="text" maxlength="255" class="span5" value="<?=$dados['youtube'];?>" placeholder="http://" /> <em>(Opcional)</em>
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                             <!-- .control-group -->
                                <div class="control-group">
									<label for="googlemaps" class="control-label">Google Maps</label>
			    					<!-- .controls -->
                                    <div class="controls">
                						<textarea name="googlemaps" id="googlemaps" class="span8" style="height:180px;"><?=htmlentities(stripslashes($dados["googlemaps"]));?></textarea>
									</div><!-- /.controls -->
			  					</div><!-- /.control-group -->
			  					<!-- .control-group -->
                                <div class="control-group">
									<label for="analytics" class="control-label">Analytics</label>
			    					<!-- .controls -->
                                    <div class="controls">
                						<textarea name="analytics" id="analytics" class="span6" style="height:120px;"><?=htmlentities(stripslashes($dados["analytics"]));?></textarea>
									</div><!-- /.controls -->
			  					</div><!-- /.control-group -->
                            <div class="form-actions">
                                <button type="submit" id="Bt" class="btn btn-primary">Salvar</button>
                            </div>
                        </form>
                    </div>
                </div>

<?php
include 'inc_footer.php';
