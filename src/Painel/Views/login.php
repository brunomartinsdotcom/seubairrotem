<?php
$prefix = "/painel/";
?>
<!DOCTYPE HTML>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="Hospital VitalPainel | Hospital Vital">
    <meta name="author" content="Agência Café com Ideias">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
    <!-- styles -->
    <link href="<?=$prefix;?>css/bootstrap.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/jquery.gritter.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=$prefix;?>css/font-awesome.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="<?=$prefix;?>css/font-awesome-ie7.min.css">
    <![endif]-->
    <link href="<?=$prefix;?>css/tablecloth.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/styles.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie7.css" />
    <![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie8.css" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie9.css" />
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>
    <!--============ javascript ===========-->
    <script src="<?=$prefix;?>js/jquery.js"></script>
    <script src="<?=$prefix;?>js/bootstrap.js"></script>
    <script src="<?=$prefix;?>js/accordion.nav.js"></script>
    <script src="<?=$prefix;?>js/custom.js"></script>
    <script src="<?=$prefix;?>js/respond.min.js"></script>
    <script src="<?=$prefix;?>js/ios-orientationchange-fix.js"></script>
    <script src="<?=$prefix;?>js/jquery.validate.js"></script>
    <script src="<?=$prefix;?>js/bootbox.js"></script>

    <script type="text/javascript">
        /**=========================
         NOME DA PAGINA
         ==============================**/
    var pagina = 'index';

    </script><title>Painel | Seu Bairro Tem</title>
    <script type="text/javascript">
        // Inicia o validador ao carregar a página
        $(function () {
            $("#formlogin").validate({
                // define regras para os campos
                rules: {
                    login: {
                        required: true,
                        minlength: 2
                    },
                    senha: {
                        required: true,
                        minlength: 6
                    }
                },
                // define messages para cada campo
                messages: {
                    login: {
                        required: "Informe seu login",
                        minlength: "Seu login é muito curto"
                    },
                    senha: {
                        required: "Informe sua senha",
                        minlength: "Sua senha é muito curta"
                    }
                }
            });
        });
    </script>
</head>
<body style="background-image: url('<?=url_base();?><?=$prefix;?>images/bg.png');" class="index" onLoad="document.formlogin.login.focus();">
<div class="layout">
    <div class="container">
        <form id="formlogin" name="formlogin" class="form-signin" method="post" enctype="multipart/form-data" action="/painel/logar">
            <div><img src="<?=$prefix;?>images/logo-large.jpg" width="300" height="105" alt="" style="margin-bottom:20px;" class="logo" /></div>
            <h3 class="form-signin-heading">Entrar no Painel </h3>
            <div class="controls input-icon">
                <i class=" icon-user-md"></i>
                <input id="login" name="login" type="text" class="input-block-level" placeholder="Login">
            </div>
            <div class="controls input-icon">
                <i class=" icon-key"></i><input name="senha" type="password" class="input-block-level" placeholder="Senha">
            </div>
            <button class="btn btn-primary btn-block" type="submit">Entrar</button>

        </form>
    </div>
</div>
<script type="application/javascript">
    $(document).ready(function(){
        <?php
        if (isset($flash['error']) and !empty($flash['error'])) {
            echo "bootbox.alert('{$flash['error']}');";
        }
        ?>
    });
</script>
</body>
</html>
