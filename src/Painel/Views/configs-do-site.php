<?php
include 'inc_head.php';

$preview = (empty($dados['foto'])) ? "http://www_antigo.placehold.it/200x150/EFEFEF/AAAAAA":'/_uploads/usuarios/'.$dados['foto'];
?>
    <!-- styles -->
    <link href="<?=$prefix;?>css/bootstrap.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/jquery.gritter.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=$prefix;?>css/font-awesome.css">
    <link rel="stylesheet" href="<?=$prefix;?>css/chosen.css">

    <!--[if IE 7]>
    <link rel="stylesheet" href="<?=$prefix;?>css/font-awesome-ie7.min.css">
    <![endif]-->
    <link href="<?=$prefix;?>css/tablecloth.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/styles.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie7.css" />
    <![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie8.css" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie9.css" />
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>

    <!--fav and touch icons -->
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="/favicon.png">

    <!--============ javascript ===========-->
    <script src="<?=$prefix;?>js/jquery.js"></script>
    <script src="<?=$prefix;?>js/bootstrap.js"></script>
    <script src="<?=$prefix;?>js/jquery.metadata.js"></script>
    <script src="<?=$prefix;?>js/excanvas.js"></script>
    <script src="<?=$prefix;?>js/jquery.collapsible.js"></script>
    <script src="<?=$prefix;?>js/accordion.nav.js"></script>
    <script src="<?=$prefix;?>js/custom.js"></script>
    <script src="<?=$prefix;?>js/respond.min.js"></script>
    <script src="<?=$prefix;?>js/ios-orientationchange-fix.js"></script>

    <script src="<?=$prefix;?>js/jquery.validate.js"></script>
    <script src="<?=$prefix;?>js/chosen.jquery.js"></script>
    <script src="<?=$prefix;?>js/bootstrap-fileupload.js"></script>

</head>
<body>
    <div class="layout">
    <?php
        include_once 'inc_header.php';
        include_once 'inc_usuario.php';
        include_once 'inc_sidebar.php';
    ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">Configurações do Site</h3>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="/painel/" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li class="active">Configurações do Site</li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="content-widgets light-gray">
                    <div class="widget-head black">
                        <h3>Configurações do Site</h3>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function(){
                        });
                    </script>
                    <div class="widget-container">
                        <form name="meus-dados" id="meus-dados" enctype="multipart/form-data"
                              class="form-horizontal" action="<?=$_SERVER["REQUEST_URI"];?>" method="post">

                            <div class="control-group">
                                <label for="blog" class="control-label">Blog</label>
                                <div class="controls">
                                    <input name="blog" id="blog" type="text" maxlength="255" class="span7" value="<?=$dados['nome'];?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="facebook" class="control-label">Facebook</label>
                                <div class="controls">
                                    <input name="facebok" id="facebook" type="text" maxlength="255" class="span7" value="<?=$dados['usuario'];?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="twitter" class="control-label">Twitter</label>
                                <div class="controls">
                                    <input name="twitter" id="twitter" type="text" maxlength="255" class="span7" value="<?=$dados['email'];?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="instagram" class="control-label">Instagram</label>
                                <div class="controls">
                                    <input name="instagram" id="instagram" type="text" maxlength="255" class="span7">
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="analitcs" class="control-label">Analitcs</label>
                                <div class="controls">
                                    <textarea name="analitcs" id="analitcs"></textarea>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="wg-cotacao" class="control-label">Home Cotação</label>
                                <div class="controls">
                                    <textarea name="wg-cotacao" id="wg-cotacao"></textarea>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="wg-clima" class="control-label">Home Clima</label>
                                <div class="controls">
                                    <textarea name="wg-clima" id="wg-clima"></textarea>
                                </div>
                            </div>

                            <div class="form-actions">
                                <button type="submit" id="Bt" class="btn btn-success">Salvar</button>
                            </div>
                        </form>
                    </div>
                </div>

<?php
include 'inc_footer.php';
