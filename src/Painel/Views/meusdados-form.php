<?php
include 'inc_head.php';

$tiW = 200;
$tiH = 200;

$usuario = \CoffeeCore\ACL\Authentication::getFromSession('usuario');

/** @var \Usuarios\Entity $usuario */
$dados = [
    'id'        => $usuario->getId(),
    'nome'      => $usuario->getNome(),
    'usuario'   => $usuario->getUsuario(),
    'email'     => $usuario->getEmail(),
    'foto'      => $usuario->getFoto()
];
$preview = (empty($dados['foto'])) ? "http://www.placehold.it/".$tiH."x".$tiH : $prefix."_uploads/usuarios/".$dados['foto'];
?>
</head>
<body>
    <div class="layout">
    <?php
        include_once 'inc_header.php';
        include_once 'inc_usuario.php';
        include_once 'inc_sidebar.php';
    ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">Meus Dados</h3>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="/painel/" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li class="active">Meus Dados</li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="content-widgets light-gray">
                    <div class="widget-head black">
                        <h3>Meus Dados</h3>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function(){

                            $(function () {
                                $(".chzn-select").chosen();
                                $(".chzn-select-deselect").chosen({
                                    allow_single_deselect: true
                                });
                            });

                            // valida o formulário
                            $('#meus-dados').validate({
                                // define regras para os campos
                                rules: {
                                    nome: {
                                        required: true,
                                        minlength: 4
                                    },
                                    usuario: {
                                        required: true,
                                        minlength: 4
                                    },
									email: {
                                        required:true,
                                        email:true
                                    },
                                    senha: {
                                        minlength:6
                                    }
                                },
                                // define messages para cada campo
                                messages: {
                                    nome:"Informe o seu nome",
                                    usuario:"Informe o seu login",
                                    email: {
                                        required: "Informe o seu e-mail",
                                        email: "Informe um e-mail válido"
                                    }
                                }
                            });
                        });
                    </script>
                    <div class="widget-container">
                        <form name="meus-dados" id="meus-dados" enctype="multipart/form-data" class="form-horizontal" action="<?=$_SERVER["REQUEST_URI"];?>" method="post">
                            <!-- CAMPOS OCULTOS PARA CORTE DA IMAGEM -->
                            <input type="hidden" id="tiW" name="tiW" value="<?=$tiW;?>" />
                            <input type="hidden" id="tiH" name="tiH" value="<?=$tiH;?>" />
                            <input type="hidden" id="x1" name="x1" />
                            <input type="hidden" id="y1" name="y1" />
                            <input type="hidden" id="x2" name="x2" />
                            <input type="hidden" id="y2" name="y2" />
                            <input type="hidden" id="val_resize" name="val_resize" />
                            <input type="hidden" id="filesize" name="filesize" />
                            <input type="hidden" id="filetype" name="filetype" />
                            <input type="hidden" id="filedim" name="filedim" />
                            <input type="hidden" id="w" name="w" />
                            <input type="hidden" id="h" name="h" />
                            <input type="hidden" id="ratio" name="ratio" value="<?=($tiW)/760;?>" />
                            <!-- TERMINA OS CAMPOS OCULTOS -->
                            <input name="id" id="id" type="hidden" value="<?=$dados['id'];?>">
                             <!-- .control-group -->
                            <div class="control-group">
                                <label for="nome" class="control-label">Nome</label>
                                <!-- .controls -->
                                <div class="controls">
                                    <input name="nome" id="nome" type="text" maxlength="60" class="span7" value="<?=$dados['nome'];?>">
                                </div> <!-- /.controls -->
                            </div><!-- /.control-group -->
                            <!-- .control-group -->
                            <div class="control-group">
                                <label for="email" class="control-label">Email</label>
                                <!-- .controls -->
                                <div class="controls">
                                    <input name="email" id="email" type="text" maxlength="120" class="span7" value="<?=$dados['email'];?>">
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                            <!-- .control-group -->
                            <div class="control-group">
                                <label for="foto" class="control-label">Foto<br><em style="font-size: 11px; color:red;">(Tamanho: <?=$tiW;?>x<?=$tiH;?> pixels)</em></label>
                                <!-- .controls -->
                                <div class="controls">
                                    <!-- .fileupload -->
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="error" style="color:#F00;"></div>
                                        <!-- .fileupload-new -->
                                        <div class="fileupload-new thumbnail">
                                            <img id="preview" src="<?=$preview;?>" alt="img"/>
                                            <div class="info"></div>
                                        </div><!-- /.fileupload-new -->
                                        <!-- .fileupload-preview .ileupload-exists .thumbnail -->
                                        <div class="" style="max-width:<?=$tiW;?>px; max-height:<?=$tiH;?>px; line-height:20px;"></div>
                                        <div>
                                            <!-- .btn .btn-file -->
                                            <span class="btn btn-file">
                                                <span class="fileupload-new">Selecione a Foto</span>
                                                <span class="fileupload-exists">Trocar</span>
                                                <input name="fotoantiga" id="fotoantiga" value="<?=$dados['foto'];?>" type="hidden"/>
                                                <input name="foto" id="foto" type="file" onChange="fileSelectHandler('foto', <?=$tiW;?>, <?=$tiH;?>, '<?=($tiW)/($tiH);?>')" />
                                            </span><!-- /.btn /.btn-file -->
                                        </div>
                                    </div><!-- /.fileupload -->
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                            <?php if($dados['foto'] != ""){ ?>
                            <!-- .control-group -->
                            <div class="control-group">
                                <label class="control-label"></label>
                                <!-- .controls -->
                                <div class="controls">
                                    <label for="excluir_foto"><input name="excluir_foto" id="excluir_foto" type="checkbox" value="sim" /> Excluir Foto</label>
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                            <?php } ?>
                            <!-- .control-group -->
                            <div class="control-group">
                                <label for="usuario" class="control-label">Login</label>
                                <!-- .controls -->
                                <div class="controls">
                                    <input name="usuario" id="usuario" type="text" maxlength="60" class="span7" value="<?=$dados['usuario'];?>">
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                            <!-- .control-group -->
                            <div class="control-group">
                                <label for="senha" class="control-label">Senha</label>
                                <!-- .controls -->
                                <div class="controls">
                                    <input name="senha" id="senha" type="password" maxlength="60" class="span7">
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                            <div class="form-actions">
                                <button type="submit" id="Bt" class="btn btn-primary">Salvar</button>
                                <div class="clear clearfix"><br></div>
                                <em>Você será desconectado ao clicar em salvar para acessar com os novos dados</em>
                            </div>
                        </form>
                    </div>
                </div>

<?php
include 'inc_footer.php';
