<?php
$pagina = 'home';
include 'inc_head.php';
?>
</head>
<body>
<div class="layout">
    <?php
        require_once 'inc_header.php';
        require_once 'inc_usuario.php';
        require_once 'inc_sidebar.php';
    ?>
</div>
<div class="main-wrapper">
<div class="container-fluid">
<div class="row-fluid ">
    <div class="span12">
        <div class="primary-head">
            <h3 class="page-header">Painel Administrativo</h3>
        </div>
    </div>
</div>
<div class="content-widgets light-gray">
<?php
include 'inc_footer.php';
