<?php

namespace Painel;

use CoffeeCore\ACL\Authentication;
use \CoffeeCore\Core\AbstractController;
use \CoffeeCore\Storage\DoctrineStorage;
use Slim\Slim;

/**
 * Class Controller
 * @package Site
 */
class Controller extends AbstractController
{
    public function alterarDados()
    {
        /* @var $usuario \Usuarios\Entity */
        $usuario = \CoffeeCore\ACL\Authentication::getFromSession('usuario');
        if (isset($_POST['usuario']) and !empty($_POST)) {
        }

        $userModel = new \Usuarios\Model(new DoctrineStorage());
        $userModel->update($usuario);
    }

    /**
     * @param Slim $app
     */
    public function logar(Slim $app)
    {
        $login = $_POST['login'];
        $senha = $_POST['senha'];

        /** @var $user \Usuarios\Entity */
        $user = DoctrineStorage::orm()->getRepository(\Usuarios\Entity::FULL_NAME)->findOneBy(['usuario' => $login]);

        if (!empty($user)) {
            if ($user->getSenha() == $senha) {
                $user->setNumeroacesso($user->getNumeroacesso() + 1);
                $userModel = new \Usuarios\Model(new DoctrineStorage());
                $userModel->update($user);
                $userModel->flush();

                Authentication::putInSession('usuario', $user);
            } else {
                $app->flash('error', 'Credenciais Inválidas.');
                $app->redirect("/painel/login");
            }
        }

        $app->redirect("/painel/");
    }
}
