<?php

namespace Painel;

use CoffeeCore\Core\AbstractView;

/**
 * Class MeusDados
 * @package Painel\View
 */
final class MeusDados extends AbstractView
{
    protected $fileName = 'meusdados-form.phtml';
}
