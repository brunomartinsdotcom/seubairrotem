<?php

namespace Painel;


use CoffeeCore\ACL\Authentication;
use CoffeeCore\Core\AbstractApplication;
use Slim\Slim;

/**
 * Class Application
 * @package Site
 */
class Application extends AbstractApplication
{
    /**
     * @var \stdClass
     */
    protected $session;

    /**
     *
     */
    public function __construct(Slim $app)
    {
        parent::__construct($app);
    }

    /**
     * @param \Slim\Slim $app
     * @return void
     */
    protected function setRoutes(Slim $app)
    {
        $app->get(
            '/',
            function () use ($app) {
                $app->render( __NAMESPACE__ . "/Views/home.php");
            }
        );

        $app->map(
            '/meus-dados/',
            function () use ($app) {
                if ($app->request()->isGet()) {
                    $app->render(__NAMESPACE__ . "/Views/meusdados-form.php");
                }

                if ($app->request()->isPost()) {
                    $userControler = new \Usuarios\Controller();
                    $userControler->gravar();

                    $app->flash("info", "Dados alterados com sucesso!");

                    $app->redirect("/painel/sair/");
                }
            }
        )->via("GET", "POST");

        $app->get(
            '/login/',
            function () use ($app) {
                $app->render(__NAMESPACE__."/Views/login.php");
            }
        );

        $app->map(
            '/logar/',
            function () use ($app) {
                if ($app->request()->isGet()) {
                    $app->redirect("/painel/login");
                }
                if ($app->request()->isPost()){
                    $controller = new Controller();
                    $controller->logar($app);
                }
            }
        )->via("GET", "POST");

        $app->get('/sair/', function () use ($app) {
            Authentication::endSession();
            $app->redirect("/painel/login");
        });

        $app->notFound(function () use ($app) {
            $app->redirect("/painel/");
        });

        \Categorias\Routes::getRoutes($app);
        \Estados\Routes::getRoutes($app);
        \Cidades\Routes::getRoutes($app);
        \Bairros\Routes::getRoutes($app);
        \UsuariosDoSite\Routes::getRoutes($app);
        \AnuncioPlano\Routes::getRoutes($app);
        //\GuiaComercial\Routes::getRoutes($app);
        //\FaturasGuiaComercial\Routes::getRoutes($app);
        //\Classificados\Routes::getRoutes($app);
        //\FaturasClassificados\Routes::getRoutes($app);
        //\Banners\Routes::getRoutes($app);
        //\FaturasBanners\Routes::getRoutes($app);
        \Duvidas\Routes::getRoutes($app);
        \Oguia\Routes::getRoutes($app);
        \GuiaImpresso\Routes::getRoutes($app);
        \Newsletter\Routes::getRoutes($app);
        \ViaConhecimento\Routes::getRoutes($app);
        \TextoSite\Routes::getRoutes($app);
        \Departamentos\Routes::getRoutes($app);
        \Paginas\Routes::getRoutes($app);
        \DadosDoSite\Routes::getRoutes($app);
        \Usuarios\Routes::getRoutes($app);
        \FormasDePagamento\Routes::getRoutes($app);
        /*

        \Promocoes\Routes::getRoutes($app);
        */
    }
}
