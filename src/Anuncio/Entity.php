<?php

namespace Anuncio;
use CoffeeCore\Storage\DoctrineStorage;

/**
 * Class Entity
 * @package Anuncio
 *
 * @Table(name="novo_anuncio")
 * @Entity
 */
class Entity implements \CoffeeCore\Core\Entity
{
    /**
     *
     */
    const FULL_NAME = "Anuncio\\Entity";

    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @Column(name="usuario", type="integer", nullable=false)
     */
    private $usuario;

    /**
     * @var integer
     *
     * @Column(name="plano", type="integer")
     */
    private $plano;

    /**
     * @var integer
     *
     * @Column(name="categoria", type="integer")
     */
    private $categoria;

    /**
     * @var string
     *
     * @Column(name="empresa", type="string", length=100, nullable=false)
     */
    private $empresa;

    /**
     * @var string
     *
     * @Column(name="slogan", type="string", length=100, nullable=false)
     */
    private $slogan;

    /**
     * @var string
     *
     * @Column(name="servico", type="text")
     */
    private $servico;

    /**
     * @var string
     *
     * @Column(name="img", type="string", length=255, nullable=false)
     */
    private $img;

    /**
     * @var string
     *
     * @Column(name="capa", type="string", length=255, nullable=true)
     */
    private $capa;

    /**
     * @var string
     *
     * @Column(name="logotipo", type="string", length=255, nullable=true)
     */
    private $logotipo;

    /**
     * @var string
     *
     * @Column(name="telefone", type="string", length=15, nullable=false)
     */
    private $telefone;

    /**
     * @var string
     *
     * @Column(name="telefone1", type="string", length=15, nullable=true)
     */
    private $telefone1;

    /**
     * @var string
     *
     * @Column(name="telefone2", type="string", length=15, nullable=true)
     */
    private $telefone2;

    /**
     * @var string
     *
     * @Column(name="celular", type="string", length=15, nullable=true)
     */
    private $celular;

    /**
     * @var string
     *
     * @Column(name="whatsapp", type="string", length=15, nullable=true)
     */
    private $whatsapp;

    /**
     * @var string
     *
     * @Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @Column(name="site", type="string", length=100, nullable=true)
     */
    private $site;

    /**
     * @var string
     *
     * @Column(name="endereco", type="string", length=200, nullable=false)
     */
    private $endereco;

    /**
     * @var string
     *
     * @Column(name="numero", type="string", length=10, nullable=true)
     */
    private $numero;

    /**
     * @var string
     *
     * @Column(name="complemento", type="string", length=100, nullable=true)
     */
    private $complemento;

    /**
     * @var string
     *
     * @Column(name="bairro", type="integer", nullable=false)
     */
    private $bairro;

    /**
     * @var string
     *
     * @Column(name="cidade", type="integer", nullable=false)
     */
    private $cidade;

    /**
     * @var string
     *
     * @Column(name="estado", type="integer", nullable=false)
     */
    private $estado;

    /**
     * @var string
     *
     * @Column(name="facebook", type="string", length=255, nullable=true)
     */
    private $facebook;

    /**
     * @var string
     *
     * @Column(name="googleplus", type="string", length=255, nullable=true)
     */
    private $googleplus;

    /**
     * @var string
     *
     * @Column(name="instagram", type="string", length=255, nullable=true)
     */
    private $instagram;

    /**
     * @var string
     *
     * @Column(name="twitter", type="string", length=255, nullable=true)
     */
    private $twitter;

    /**
     * @var string
     *
     * @Column(name="youtube", type="string", length=255, nullable=true)
     */
    private $youtube;

    /**
     * @var string
     *
     * @Column(name="quemsomos", type="text", nullable=true)
     */
    private $quemsomos;

    /**
     * @var string
     *
     * @Column(name="formasdepagamento", type="integer", nullable=true)
     */
    private $formasdepagamento;

    /**
     * @var integer
     *
     * @Column(name="cliques", type="integer")
     */
    private $cliques = 0;

    /**
     * @var datetime
     *
     * @Column(name="datainclusao", type="datetime", nullable=false)
     */
    private $datainclusao;

    /**
     * @var datetime
     *
     * @Column(name="dataalteracao", type="datetime")
     */
    private $dataalteracao;

    /**
     * @var integer
     *
     * @Column(name="status", type="integer", nullable=false)
     */
    private $status = 0;

    /**
     *
     */
    public function __construct()
    {
        $this->datainclusao = new \DateTime();
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param int $usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * @return array
     */
    public function getNomeUsuario($id)
    {
        if ($id) {
            $f = [];
            $formas =  DoctrineStorage::orm()->getConnection()->fetchAll("SELECT * FROM novo_usuarios_do_site WHERE id={$id}");
            foreach ($formas as $forma) {
                if($forma['tipopessoa'] == 1){
                    $nome = $forma['nome'];
                }else if($forma['tipopessoa'] == 2){
                    $nome = $forma['nomefantasia'];
                }
            }
            return $nome;
        }
    }

    /**
     * @return int
     */
    public function getPlano()
    {
        return $this->plano;
    }

    /**
     * @param int $plano
     */
    public function setPlano($plano)
    {
        $this->plano = $plano;
    }

    /**
     * @return int
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @return string
     */
    public function getNomecategoria()
    {
        $nomeCategoria = DoctrineStorage::orm()
            ->getRepository(\Categorias\Entity::FULL_NAME)
            ->findOneBy(["id" => $this->getCategoria()])
            ->getTitulo();
        return $nomeCategoria;
    }

    /**
     * @param int $categoria
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }

    /**
     * @return string
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param string $empresa
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
    }

    /**
     * @return string
     */
    public function getSlogan()
    {
        return $this->slogan;
    }

    /**
     * @param string $slogan
     */
    public function setSlogan($slogan)
    {
        $this->slogan = $slogan;
    }

    /**
     * @return string
     */
    public function getServico()
    {
        return $this->servico;
    }

    /**
     * @param string $servico
     */
    public function setServico($servico)
    {
        $this->servico = $servico;
    }

    /**
     * @return string
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param string $img
     */
    public function setImg($img)
    {
        $this->img = $img;
    }

    /**
     * @return string
     */
    public function getCapa()
    {
        return $this->capa;
    }

    /**
     * @param string $capa
     */
    public function setCapa($capa)
    {
        $this->capa = $capa;
    }

    /**
     * @return string
     */
    public function getLogotipo()
    {
        return $this->logotipo;
    }

    /**
     * @param string $logotipo
     */
    public function setLogotipo($logotipo)
    {
        $this->logotipo = $logotipo;
    }

    /**
     * @return string
     */
    public function getTelefone()
    {
        return $this->telefone;
    }

    /**
     * @param string $telefone
     */
    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;
    }

    /**
     * @return string
     */
    public function getTelefone1()
    {
        return $this->telefone1;
    }

    /**
     * @param string $telefone1
     */
    public function setTelefone1($telefone1)
    {
        $this->telefone1 = $telefone1;
    }

    /**
     * @return string
     */
    public function getTelefone2()
    {
        return $this->telefone2;
    }

    /**
     * @param string $telefone2
     */
    public function setTelefone2($telefone2)
    {
        $this->telefone2 = $telefone2;
    }

    /**
     * @return string
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * @param string $celular
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;
    }

    /**
     * @return string
     */
    public function getWhatsapp()
    {
        return $this->whatsapp;
    }

    /**
     * @param string $whatsapp
     */
    public function setWhatsapp($whatsapp)
    {
        $this->whatsapp = $whatsapp;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @param string $site
     */
    public function setSite($site)
    {
        $this->site = $site;
    }

    /**
     * @return string
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param string $endereco
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    }

    /**
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param string $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    /**
     * @return string
     */
    public function getComplemento()
    {
        return $this->complemento;
    }

    /**
     * @param string $complemento
     */
    public function setComplemento($complemento)
    {
        $this->complemento = $complemento;
    }

    /**
     * @return string
     */
    public function getBairro()
    {
        return $this->bairro;
    }

    /**
     * @return string
     */
    public function getNomeBairro()
    {
        $nomeBairro = DoctrineStorage::orm()
            ->getRepository(\Bairros\Entity::FULL_NAME)
            ->findOneBy(["id" => $this->getBairro()])
            ->getNome();
        return $nomeBairro;
    }

    /**
     * @param string $bairro
     */
    public function setBairro($bairro)
    {
        $this->bairro = $bairro;
    }

    /**
     * @return int
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * @return string
     */
    public function getNomeCidade()
    {
        $nomeCidade = DoctrineStorage::orm()
            ->getRepository(\Cidades\Entity::FULL_NAME)
            ->findOneBy(["id" => $this->getCidade()])
            ->getNome();
        return $nomeCidade;
    }

    /**
     * @param int $cidade
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;
    }

    /**
     * @return int
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @return string
     */
    public function getNomeEstado()
    {
        $nomeEstado = DoctrineStorage::orm()
            ->getRepository(\Estados\Entity::FULL_NAME)
            ->findOneBy(["id" => $this->getEstado()])
            ->getNome();
        return $nomeEstado;
    }

    /**
     * @param int $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return string
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * @param string $facebook
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;
    }

    /**
     * @return string
     */
    public function getGoogleplus()
    {
        return $this->googleplus;
    }

    /**
     * @param string $googleplus
     */
    public function setGoogleplus($googleplus)
    {
        $this->googleplus = $googleplus;
    }

    /**
     * @return string
     */
    public function getInstagram()
    {
        return $this->instagram;
    }

    /**
     * @param string $instagram
     */
    public function setInstagram($instagram)
    {
        $this->instagram = $instagram;
    }

    /**
     * @return string
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * @param string $twitter
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;
    }

    /**
     * @return string
     */
    public function getYoutube()
    {
        return $this->youtube;
    }

    /**
     * @param string $youtube
     */
    public function setYoutube($youtube)
    {
        $this->youtube = $youtube;
    }

    /**
     * @return string
     */
    public function getQuemsomos()
    {
        return $this->quemsomos;
    }

    /**
     * @param string $quemsomos
     */
    public function setQuemsomos($quemsomos)
    {
        $this->quemsomos = $quemsomos;
    }

    /**
     * @return array
     */
    public function getFormaDePagamento($string = false)
    {
        if ($string) {
            $f= [];
            $formas =  DoctrineStorage::orm()
                ->getConnection()
                ->fetchAll("SELECT * FROM novo_forma_de_pagamento fp WHERE fp.id IN
                          (SELECT formadepagamento FROM novo_anuncio_forma_de_pagamento WHERE anuncio = {$this->id}) ORDER BY fp.titulo ASC");
            foreach ($formas as $forma) {
                $f[] = $forma['titulo'];
            }
            return implode(', ', $f);
        }

        return DoctrineStorage::orm()
            ->getConnection()
            ->fetchAll("SELECT * FROM novo_forma_de_pagamento fp WHERE fp.id IN
                          (SELECT formadepagamento FROM novo_anuncio_forma_de_pagamento WHERE anuncio = {$this->id}) ORDER BY fp.titulo ASC");
    }

    /**
     * @return array
     */
    public function getGaleriaDeFotos($string = flase)
    {
        return DoctrineStorage::orm()
            ->getConnection()
            ->fetchAll("SELECT * FROM anuncio_galeria WHERE anuncio = {$this->id} ORDER BY id ASC");
    }

    /**
     * @return int
     */
    public function getCliques()
    {
        return $this->cliques;
    }

    /**
     * @return void
     */
    public function incrementClick()
    {
        $this->cliques += 1;
    }

    /**
     * @param int $cliques
     */
    public function setCliques($cliques)
    {
        $this->cliques = $cliques;
    }

    /**
     * @return \DateTime
     */
    public function getDatainclusao()
    {
        return $this->datainclusao;
    }

    /**
     * @param datetime $datainclusao
     */
    public function setDatainclusao($datainclusao)
    {
        $this->datainclusao = $datainclusao;
    }

    /**
     * @return datetime
     */
    public function getDataalteracao()
    {
        if (empty($this->dataalteracao)) {
            return null;
        }
        return $this->dataalteracao;
    }

    /**
     * @param datetime $dataalteracao
     */
    public function setDataalteracao($dataalteracao)
    {
        $this->dataalteracao = $dataalteracao;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
}
