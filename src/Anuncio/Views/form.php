<?php
include 'inc_head.php';
$jsonReader = new \CoffeeCore\Helper\JsonReader('paginas');

/** @var Anuncio\Entity $anuncio */

?>
    <!-- styles -->
    <link href="<?= url_base() . $prefix;?>css/bootstrap.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/jquery.gritter.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=$prefix;?>css/font-awesome.css">

    <!--[if IE 7]>
    <link rel="stylesheet" href="<?=$prefix;?>css/font-awesome-ie7.min.css">
    <![endif]-->
    <link href="<?=$prefix;?>css/tablecloth.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/styles.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie7.css" />
    <![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie8.css" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie9.css" />
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>

    <!--fav and touch icons -->
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="/favicon.png">

    <!--============ javascript ===========-->
    <script src="<?=$prefix;?>js/jquery.js"></script>
    <script src="<?=$prefix;?>js/bootstrap.js"></script>
    <script src="<?=$prefix;?>js/jquery.metadata.js"></script>
    <script src="<?=$prefix;?>js/excanvas.js"></script>
    <script src="<?=$prefix;?>js/jquery.collapsible.js"></script>
    <script src="<?=$prefix;?>js/accordion.nav.js"></script>
    <script src="<?=$prefix;?>js/custom.js"></script>
    <script src="<?=$prefix;?>js/respond.min.js"></script>
    <script src="<?=$prefix;?>js/ios-orientationchange-fix.js"></script>

    <script src="<?=$prefix;?>js/jquery.validate.js"></script>
    <link href="<?=$prefix;?>css/chosen.css" rel="stylesheet">
    <script src="<?=$prefix;?>js/chosen.jquery.js"></script>
    <script src="<?=$prefix;?>js/bootstrap-fileupload.js"></script>

</head>
<body>
    <div class="layout">
    <?php
        include_once 'inc_header.php';
        include_once 'inc_usuario.php';
        include_once 'inc_sidebar.php';
    ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">Anúncios</h3>
                        <span class="pull-right top-right-toolbar">
                        </span>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="<?=$prefix;?>" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li>
                            <a href="<?=$prefix;?>anuncios">Anúncios</a><span class="divider">
                                <i class="icon-angle-right"></i>
                            </span>
                        </li>
                        <li class="active">Vizualização</li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="content-widgets light-gray">
                    <div class="widget-head black">
                        <h3>Dados do Anúncio</h3>
                    </div>
                    <div class="widget-container">
                        <div class="tab-content">
                            <div class="tab-pane active" id="user">
                                <div class=" information-container">

                                    <form name="form1" id="form1" class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                                        <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Titulo:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <p><h3 style="margin-top:-6px;"><?= $anuncio->getTitulo(); ?></h3></p>
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Usuário:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <p><?= $anuncio->getNomeUsuario($anuncio->getUsuario()); ?></p>
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Preço a vista:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <p><?= $anuncio->getPrecoavista(); ?></p>
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Preço a prazo:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <p><?= $anuncio->getPrecoaprazo(); ?></p>
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Cidade:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <p><?= $anuncio->getNomeCidade() . '/' . $anuncio->getEstado();?></p>
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Forma de Pagamento:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <p><?= $anuncio->getFormaDePagamento(true); ?></p>
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Forma de Entrega:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <p><?= $anuncio->getFormaDeEntrega(true); ?></p>
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Descrição Breve:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <p><?= $anuncio->getDescricaobreve(); ?></p>
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Descrição Completa:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <?php $quebradelinha = new \CoffeeCore\Helper\Quebradelinha(); ?>
                                    			<p><?=$quebradelinha->quebradelinha($anuncio->getDescricaocompleta()); ?></p>
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Anúncio Visualizado:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <p><?= $anuncio->getQuantidadevizualizacao(); ?> vez<?php if($anuncio->getQuantidadevizualizacao() != 1){ ?>es<?php } ?></p>
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Inserido em:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <p><?php echo $anuncio->getDatainclusao()->format("d/m/Y"); ?></p>
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
										<?php if($anuncio->getDataalteracao() != ''){ ?>
                                        <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Atualizado em:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <p><?php echo $anuncio->getDataalteracao()->format("d/m/Y"); ?></p>
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <?php } ?>
                                        <?php if($anuncio->getDatavalidade() != ''){ ?>
                                        <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Válido até:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <p><?php echo $anuncio->getDatavalidade()->format("d/m/Y"); ?></p>
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <?php } ?>
                                        <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Imagens:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <div id="container" class="masonry">
                                                    <div class="item masonry-brick" style="float:left; margin-right:10px;">
                                                        <div class="thumbnail"><img src="/_uploads/anuncios/<?=$anuncio->getFotocapa();?>" width="300" /></div>
                                                    </div>
													<?php
                                                        $conn = \CoffeeCore\Storage\DoctrineStorage::orm()->getConnection();
                                                        $sqlFotos = "SELECT * FROM anunciofoto WHERE anuncio = {$anuncio->getId()}";
                                                        $fotos = $conn->fetchAll($sqlFotos);
                                            
                                                        foreach($fotos as $foto):
                                                    ?>
                                                    <div class="item masonry-brick" style="float:left;">
                                                        <div class="thumbnail">
                                                            <img src="/_uploads/anuncios/<?='tumb-'.$foto['foto'];?>" />
                                                        </div>
                                                    </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <!-- .control-group -->
                                </form>

                                    <?php if($anuncio->getStatus() == 0){ ?>
                                    <a href="<?=$prefix;?>anuncios/liberar/<?=$anuncio->getId();?>" class="btn btn-success">Liberar Anúncio</a>
                                    <?php }else{ ?>
                                    <a href="<?=$prefix;?>anuncios/bloquear/<?=$anuncio->getId();?>" class="btn btn-warning">Bloquear Anúncio</a>
                                    <?php } ?>
                                    <a href="<?=$prefix;?>anuncios" class="btn btn-default right-float">Voltar</a>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>

<?php
include 'inc_footer.php';
