<?php

namespace Anuncio;

use Anuncio\Entity;
use CoffeeCore\Core\AbstractController;
use CoffeeCore\Storage\DoctrineStorage;

/**
 * Class Controller
 * @package Anuncio
 */
class Controller extends AbstractController
{
    /**
     * @var string
     */
    public $pagina = 'anuncios';
    
	/**
     * @var Model
     */
    protected $model;

    /**
     * @var Entity
     */
    protected $entity;

    /**
     * @return void
     */
    public function getDataTable()
    {
        $this->startConnection();
        $columns = ['id', 'datainclusao', 'titulo', 'usuario', 'datavalidade', 'status'];

        $search = $_POST['search']['value'];

        $limit = $_POST['length'];
        $offset = $_POST['start'];
        $orderBy = [
            //1, 'DESC'
			$_POST['order'][0]['column'],
            $_POST['order'][0]['dir']
        ];

        $dataTables = $this->model->getDataTableData($columns, $orderBy, $search, $limit, $offset);
		
        foreach ($dataTables['data'] as $k => &$row) {
            $row[1] = $row[1]->format("d/m/Y");
			$row[3] = $this->getNomeUsuario($row[3]);
			if(empty($row[4])) {
				$row[4] = "Aguardando PagSeguro";
			}else{
				$row[4] = $row[4]->format("d/m/Y");
			}

			 //   ->format("d/m/Y");
			if($row[5] == 0){ $row[5] = "Pendente"; }else if($row[5] == 1){ $row[5] = "Liberado"; }else if($row[5] == 2){ $row[5] = "Bloqueado"; }else if($row[5] == 3){ $row[5] = "Cancelado pelo Usuário"; }
			if($row[5] == "Pendente" or $row[5] == "Bloqueado" or $row[5] == "Cancelado pelo Usuário"){
				$row[] = "<span class='center'>
                        <button class='btn btn-success' onclick='javascript:window.location = \"/painel/{$this->pagina}/liberar/{$row[0]}\"' title='Liberar Anúncio'>
                            <i class='icon-ok'></i>
                        </button></span>";
			}else if($row[5] == "Liberado"){
				$row[] = "<span class='center'>
                        <button class='btn btn-warning' onclick='javascript:window.location = \"/painel/{$this->pagina}/bloquear/{$row[0]}\"' title='Bloquear Anúncio'>
                            <i class='icon-ban-circle'></i>
                        </button></span>";
			}
			$row[] = "<span class='center'>
                        <button class='btn btn-info' onclick='javascript:window.location = \"/painel/{$this->pagina}/vizualizar/{$row[0]}\"'>
                            <i class='icon-search'></i>
                        </button></span>";
			$row[] = "<span class='center'>
                        <button data-url='/painel/{$this->pagina}/deletar/{$row[0]}' id='deletar' class='btn btn-primary'>
                            <i class='icon-trash'></i>
                        </button>
                     </span>";
        }
		
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Length: " . $dataTables);

		header('Content-Type: application/json');
		header("Content-Disposition: attachment; filename=usuario.json");

        //header('Content-Type: application/json');
        echo json_encode($dataTables);
    }
	
	/**
	 * @return string
	 */
	public function getNomeUsuario($id)
	{
		$tipoUsuario = DoctrineStorage::orm()
			->getRepository(\UsuariosDoSite\Entity::FULL_NAME)
			->findOneBy(["id" => $id])
			->getTipopessoa();
		    $nomeUsuario = '';
		if($tipoUsuario == 1) {
			$nomeUsuario = DoctrineStorage::orm()
				->getRepository(\UsuariosDoSite\Entity::FULL_NAME)
				->findOneBy(["id" => $id])
				->getNome();
		}else if($tipoUsuario == 2){
			$nomeUsuario = DoctrineStorage::orm()
				->getRepository(\UsuariosDoSite\Entity::FULL_NAME)
				->findOneBy(["id" => $id])
				->getNomefantasia();
		}
		return $nomeUsuario;
	}


    /**
     *
     */
    public function startConnection()
    {
        $this->entity = new Entity();
        $this->model = new Model(new DoctrineStorage($this->entity));
    }

	/**
     *
     */
    public function gravar($id, $status)
    {
        $this->startConnection();
        if(!empty($id)) {
            $this->entity = $this->model->findOneBy(['id' => $id]);
			
			$this->entity->setStatus($status);
		
			//UPDATE
            $this->model->update($this->entity);
			//die;
        }
        $this->model->flush();
    }
	
    /**
     * @param int|Entity|null $entity
     */
    public function deletar($id = null)
    {
     	$this->startConnection();
        if (!empty($id)) {
			//SETA A ENTIDADE
			$this->entity = $this->model->findOneBy(["id" => $id]);
			
			$output_dir = $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "_uploads/anuncios/";
			//EXCLUI A FOTO DA CAPA
			if(is_file($output_dir.$this->entity->getFotocapa())) {
				unlink($output_dir.$this->entity->getFotocapa());
			}
			
			$conn = \CoffeeCore\Storage\DoctrineStorage::orm()->getConnection();
			$sqlFotos = "SELECT * FROM anunciofoto WHERE anuncio = {$id}";
			$fotos = $conn->fetchAll($sqlFotos);

			foreach($fotos as $foto){
				if(is_file($output_dir.$foto['foto'])) {
					unlink($output_dir.$foto['foto']);
				}
				if(is_file($output_dir.'med-'.$foto['foto'])) {
					unlink($output_dir.'med-'.$foto['foto']);
				}
				if(is_file($output_dir.'tumb-'.$foto['foto'])) {
					unlink($output_dir.'tumb-'.$foto['foto']);
				}
			}
			$sqlFotosDel = "DELETE FROM anunciofoto WHERE anuncio = {$id}";
			$fotos = $conn->executeQuery($sqlFotosDel);
			
			//EXCLUI OS DESTAQUES DO ANUNCIO
			$sqlDestaquesDel = "DELETE FROM anunciodestaque WHERE anuncio = {$id}";
			$destaque = $conn->executeQuery($sqlDestaquesDel);
			
			//EXCLUI AS FORMAS DE ENTREGA DO ANUNCIO
			$sqlEntregaDel = "DELETE FROM anuncioformaentrega WHERE anuncio = {$id}";
			$entrega = $conn->executeQuery($sqlEntregaDel);
			
			//EXCLUI AS FORMAS DE PAGAMENTO DO ANUNCIO
			$sqlPagamentoDel = "DELETE FROM anunciopagamento WHERE anuncio = {$id}";
			$pagamento = $conn->executeQuery($sqlPagamentoDel);
			
			//EXCLUI AS PERGUNTAS DO ANUNCIO
			$sqlPerguntasDel = "DELETE FROM anuncioperguntas WHERE anuncio = {$id}";
			$perguntas = $conn->executeQuery($sqlPerguntasDel);

			//EXCLUI AS FATURAS
			$sqlFaturasDel = "DELETE FROM fatura WHERE anuncio = {$id}";
			$faturas = $conn->executeQuery($sqlFaturasDel);

			//EXCLUI AS FATURAS PLANO
			$sqlFaturaPlanoDel = "DELETE FROM faturaplano WHERE anuncio = {$id}";
			$faturaplanoDel = $conn->executeQuery($sqlFaturaPlanoDel);

			//EXCLUI AS FATURAS DESTAQUE
			$sqlFaturaDestaqueDel = "DELETE FROM faturadestaque WHERE anuncio = {$id}";
			$faturaDestaqueDel = $conn->executeQuery($sqlFaturaDestaqueDel);
			
            $this->model->delete($this->entity);
            $this->model->flush();
        }
    }
}
