<?php

namespace Anuncio;

use CoffeeCore\Storage\DoctrineStorage;
use Slim\Slim;

/**
 * Class Routes
 * @package Anuncio
 */
class Routes
{
    /**
     * @param Slim $app
     */
    public static function getRoutes(Slim $app)
    {
        $controller = new Controller();

        $app->group("/{$controller->pagina}", function () use ($app, $controller) {

            $app->map(
                '/',
                function () use ($app, $controller) {
                    if ($app->request()->isGet()) {
                        $app->render(__NAMESPACE__ . "/Views/grid.php");
                    }
                    if ($app->request()->isPost()) {
                        $controller->getDataTable();
                    }
                }
            )->via("GET", "POST");

            $app->get(
                '/vizualizar/:id',
                function ($id) use ($app, $controller) {
                    if ($app->request()->isGet()) {
                        $registro = DoctrineStorage::orm()
                            ->getRepository(__NAMESPACE__."\\Entity")
                            ->findOneBy(["id" => $id]);
                        $app->render(__NAMESPACE__."/Views/form.php", ["anuncio" => $registro]);
                    }
                }
            );

            $app->get(
                '/liberar/:id',
                function ($id) use ($app, $controller) {
					if ($app->request()->isGet()) {
						try {
							$controller->gravar($id, '1');
							$app->flash("info", "Status alterado com sucesso!");
						} catch (\Exception $e) {
							throw $e;
							$app->flash("info", "Erro ao alterar o status! <br>" . addslashes($e->getMessage()));
	
						}
						$app->redirect("/painel/{$controller->pagina}/");
					}
                }
            );
			
			$app->get(
                '/bloquear/:id',
                function ($id) use ($app, $controller) {
					if ($app->request()->isGet()) {
						try {
							$controller->gravar($id, '2');
							$app->flash("info", "Status alterado com sucesso!");
						} catch (\Exception $e) {
							throw $e;
							$app->flash("info", "Erro ao alterar o status! <br>" . addslashes($e->getMessage()));
	
						}
						$app->redirect("/painel/{$controller->pagina}/");
					}
                }
            );
			
			$app->get(
                '/deletar/:id/',
                function ($id) use ($app, $controller) {
                    try {
                        $controller->deletar($id);
                        $app->flash("info", "Excluído com sucesso!");
                    } catch (\Exception $e) {
                        $app->flash("info", "<span>Erro ao excluir!</span>");

                    }
                    $app->redirect("/painel/{$controller->pagina}/");
                }
            );
        });
    }
}
