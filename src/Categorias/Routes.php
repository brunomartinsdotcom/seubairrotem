<?php

namespace Categorias;

use Slim\Slim;

/**
 * Class Routes
 * @package Categorias
 */
class Routes
{
    /**
     * @var Controller
     */
    protected static $controller;

    /**
     * @param Slim $app
     */
    public static function getRoutes(Slim $app)
    {
        $controller = new Controller();

        $app->group("/{$controller->pagina}/", function () use ($app , $controller) {

            $app->map(
                '',
                function () use ($app, $controller) {
                    if ($app->request()->isGet()) {
                        $app->render(__NAMESPACE__."/Views/grid.php");
                    }
                    if ($app->request->isPost()) {
                        $controller->getDataTable();
                    }
                }
            )->via('GET', 'POST');

            $app->map(
                'novo/',
                function () use ($app, $controller) {
                    if ($app->request()->isGet()) {
                        $app->render(__NAMESPACE__."/Views/form.php");
                    }
                    if ($app->request()->isPost()) {

                        $app->flash("info", "Inserido com sucesso!");

                        if(!$controller->gravar()) {
                            $app->flash("info", "Erro ao inserir!");
                        }
                        $app->redirect("/painel/{$controller->pagina}/");
                    }
                }
            )->via('GET', 'POST');

            $app->map(
                'alterar/:id',
                function ($id) use ($app, $controller) {
                    if ($app->request()->isGet()) {
                        $registro = $app->orm->getRepository(Entity::FULL_NAME)->findOneBy(["id" => $id]);
                        $app->render(__NAMESPACE__ . "/Views/form.php", ["categoria" => $registro]);
                    }
                    if ($app->request()->isPost()) {
                        $app->flash("info", "Alterado com sucesso!");

                        if(!$controller->gravar()) {
                            $app->flash("info", "Erro ao alterar!");
                        }
                        $app->redirect("/painel/{$controller->pagina}/");
                    }
                }
            )->via('GET', 'POST');

            $app->get(
                'deletar/:id/',
                function ($id) use ($app, $controller) {
                    try {
                        $controller->deletar($id);
                        $app->flash("info", "Excluído com sucesso!");
                    } catch (\Exception $e) {
                        $app->flash("info", "Erro ao excluir!");
                    }
                    $app->redirect("/painel/{$controller->pagina}/");
                }
            );
        });
    }
}
