<?php

namespace Categorias;

use CoffeeCore\Core\AbstractController;
use CoffeeCore\Helper\JsonWriter;
use CoffeeCore\Helper\ResourceManager;
use CoffeeCore\Storage\DoctrineStorage;

/**
 * Class Controller
 * @package Categorias
 */
class Controller extends AbstractController
{
    /**
     * @var string
     */
    public $pagina = 'categorias';
    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Entity
     */
    protected $entity;

    /**
     * @return void
     */
    public function getDataTable()
    {
        $this->startConnection();
        $columns = ['id', 'titulo', 'tipo'];
        $noSearch = ['tipo'];

        $search = isset($_POST['search']['value']) ? $_POST['search']['value'] : ""; //$_POST['search']['value'];

        $limit = isset($_POST['length']) ? $_POST['length'] : 10;
        $offset = isset($_POST['start']) ? $_POST['start'] : 0;
        if(isset($_POST['order'])){
            $orderBy = [
                $_POST['order'][0]['column'],
                $_POST['order'][0]['dir']
            ];
        }else{
            $orderBy = [0, 'ASC'];
        }

        $dataTables = $this->model->getDataTableData($columns, $orderBy, $search, $limit, $offset, $noSearch);

        foreach ($dataTables['data'] as $k => &$row) {
            if (!is_array($row[2])) {
                if (($row[2] == 1)) {
                    $row[2] = 'Guia Comercial';
                } else {
                    $row[2] = 'Classificados';
                }
            }
            $row[] = "<span class='center'>
                        <button class='btn btn-primary' onclick='javascript:window.location = \"/painel/categorias/alterar/{$row[0]}\"'>
                            <span><i class='icon-pencil'></i></span>
                        </button>
                        </span>";
            $row[] = "<span class='center'>
                        <button data-url='/painel/categorias/deletar/{$row[0]}' id='deletar' class='btn btn-danger'>
                        <span><i class='icon-trash'></i></span>
                        </button>
                     </span>";
            unset($k);
        }

        //header('Content-Type: application/json');
        echo json_encode($dataTables);
    }

    /**
     *
     */
    public function startConnection()
    {
        $this->entity = new Entity();
        $this->model = new Model(new DoctrineStorage($this->entity));
    }

    /**
     *
     */
    public function gravar()
    {
        $this->startConnection();

        $this->entity->setTipo($_POST['tipo']);
        $this->entity->setTitulo($_POST['titulo']);
        $this->entity->setDescricao($_POST['descricao']);
        $this->entity->setPalavraschave($_POST['palavraschave']);
        try {
            if (!isset($_POST['id']) or empty($_POST['id'])) {
                $this->model->insert($this->entity);
            } else {
                $this->entity->setId($_POST['id']);
                $this->model->update($this->entity);
            }
            $this->model->flush();
        } catch (\Exception $e) {
            return false;
        }


        $tipos = [
            1 => "guia-comercial",
            2 => "classificados"
        ];

        foreach ($tipos as $id => $tipo) {
            $listaDeCategorias = $this->model->findBy(["tipo" => $id], ['titulo' => 'ASC']);

            $categorias = [];

            while ($listaDeCategorias->valid()) {
                $categorias[create_slug($listaDeCategorias->current()->getTitulo())] = [
                    "id" =>     $listaDeCategorias->current()->getId(),
                    "titulo" => $listaDeCategorias->current()->getTitulo(),
                    "descricao" => $listaDeCategorias->current()->getDescricao(),
                    "palavraschave" => $listaDeCategorias->current()->getPalavraschave(),
                    "tipo" =>   $listaDeCategorias->current()->getTipo()
                ];
                $listaDeCategorias->next();
            }

            ResourceManager::writeFile($tipo . ".json", json_encode($categorias));
        }

        return true;
    }

    /**
     * @param int|null $id
     */
    public function deletar($id = null)
    {
        $this->startConnection();
        if (!empty($id)) {
            $this->entity = $this->model->findOneBy(["id"=>$id]);
            $this->model->delete($this->entity);
            $this->model->flush();
			
			$tipos = [
				1 => "guia-comercial",
				2 => "classificados"
			];
	
			foreach ($tipos as $id => $tipo) {
				$listaDeCategorias = $this->model->findBy(["tipo" => $id], ['titulo' => 'ASC']);
	
				$categorias = [];
	
				while ($listaDeCategorias->valid()) {
					$categorias[create_slug($listaDeCategorias->current()->getTitulo())] = [
						"id" =>     $listaDeCategorias->current()->getId(),
						"titulo" => $listaDeCategorias->current()->getTitulo(),
                        "descricao" => $listaDeCategorias->current()->getDescricao(),
                        "palavraschave" => $listaDeCategorias->current()->getPalavraschave(),
						"tipo" =>   $listaDeCategorias->current()->getTipo()
					];
					$listaDeCategorias->next();
				}
	
				ResourceManager::writeFile($tipo . ".json", json_encode($categorias));
			}
        }
    }
}
