<?php

namespace Categorias;


/**
 * Entity
 *
 * @Entity
 * @Table(name="novo_categorias")
 */
class Entity implements \CoffeeCore\Core\Entity
{
    const FULL_NAME = "Categorias\\Entity";

    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @Column(name="tipo", type="integer", nullable=false)
     * @ManyToOne()
     */
    private $tipo;

    /**
     * @var string
     *
     * @Column(name="titulo", type="string", length=50, nullable=false)
     */
    private $titulo;

    /**
     * @var string
     *
     * @Column(name="descricao", type="string", length=200, nullable=false)
     */
    private $descricao;

    /**
     * @var string
     *
     * @Column(name="palavraschave", type="text", nullable=false)
     */
    private $palavraschave;

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    /**
     * @return int
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param string $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    /**
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param string $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param string $palavraschave
     */
    public function setPalavraschave($palavraschave)
    {
        $this->palavraschave = $palavraschave;
    }

    /**
     * @return string
     */
    public function getPalavraschave()
    {
        return $this->palavraschave;
    }
}
