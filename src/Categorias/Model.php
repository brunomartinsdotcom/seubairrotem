<?php


namespace Categorias;


use CoffeeCore\Core\AbstractDao;

/**
 * Class Model
 * @package Categorias
 */
class Model extends AbstractDao
{
    /**
     * @param array $criteria
     * @param array $orderBy
     * @param null $limit
     * @param null $offset
     * @return \arrayIterator
     */
    public function getGuiacomercial(array $criteria = [], array $orderBy = null, $limit = null, $offset = null)
    {
        $criteria['tipo'] = 1;

        return $this->storage->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * @param array $criteria
     * @param array $orderBy
     * @param null $limit
     * @param null $offset
     * @return \arrayIterator
     */
    public function getClassificados(array $criteria = [], array $orderBy = null, $limit = null, $offset = null)
    {
        $criteria['tipo'] = 2;

        return $this->storage->findBy($criteria, $orderBy, $limit, $offset);
    }
}
