<?php
include 'inc_head.php';

$action = "Adicionar";
if (isset($categoria)) {
    $action = "Editar";
    $dados = [
        'id' => $categoria->getId(),
        'tipo' => $categoria->getTipo(),
        'titulo' => $categoria->getTitulo(),
        'descricao' => $categoria->getDescricao(),
        'palavraschave' => $categoria->getPalavraschave(),
    ];
} else {
    $dados = [
        'id' => '',
        'tipo' => '',
        'titulo' => '',
        'descricao' => '',
        'palavraschave' => ''
    ];
}
?>
<script src="<?=$prefix;?>js/jquery.tagsinput.js"></script>
</head>
<body>
    <div class="layout">
    <?php
        include_once 'inc_header.php';
        include_once 'inc_usuario.php';
        include_once 'inc_sidebar.php';
    ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">Categorias</h3>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="<?=$prefix;?>" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li>
                            <a href="<?=$prefix;?>categorias">Categorias</a><span class="divider">
                                <i class="icon-angle-right"></i>
                            </span>
                        </li>
                        <li class="active"><?=$action;?></li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="content-widgets light-gray">
                    <div class="widget-head black">
                        <h3><?=$action;?> Categoria</h3>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function(){
                            // Script que coloca função "TAGS" na caixa de palavras chave
                            $('#palavraschave').tagsInput({
                                width: 'auto',
                                defaultText:'add mais'
                            });
                            // valida o formulário
                            $('#categoria').validate({
                                // define regras para os campos
                                rules: {
                                    tipo: {
                                        required: true
                                    },
                                    titulo: {
                                        required: true
                                    },
                                    descricao: {
                                        required: true
                                    },
                                    palavraschave: {
                                        required: true
                                    }
                                },
                                // define messages para cada campo
                                messages: {
                                    titulo: "Informe o título da categoria",
                                    tipo: "Informe o tipo da categoria",
                                    descricao: "Informe a descrição da categoria",
                                    palavraschave: "Informe as palavras-chave da categoria"
                                }
                            });
                        });
                    </script>
                    <div class="widget-container">
                        <form name="categoria" id="categoria" class="form-horizontal" action="<?=$_SERVER["REQUEST_URI"];?>" method="post">
                            <input name="id" id="id" type="hidden" value="<?=$dados['id'];?>">
                            <div class="control-group">
                                <label for="titulo" class="control-label">Título</label>
                                <div class="controls"><input name="titulo" id="titulo" type="text" maxlength="50" class="span7" value="<?=$dados['titulo'];?>"></div>
                            </div>
                            <div class="control-group">
                                <label for="tipo" class="control-label">Tipo</label>
                                <div class="controls">
                                    <select name="tipo">
                                        <option value="">Selecione</option>
                                        <?php
                                        $tipos = \CoffeeCore\Helper\ResourceManager::readFile('tiposCategoria.json');
                                        foreach ($tipos as $tipo) {
                                        ?>
                                            <option value="<?=$tipo['id'];?>"
                                                <?php if ($tipo['id'] == $dados['tipo'] and !empty($dados['tipo'])) {
                                                    echo " selected ";}
                                                ?>
                                            >
                                                <?=$tipo['description'];?>
                                            </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <!-- .control-group -->
                            <div class="control-group">
                                <label for="descricao" class="control-label"><strong>Informações para SEO</strong></label>
                            </div><!-- /.control-group -->
                            <!-- .control-group -->
                            <div class="control-group">
                                <label for="descricao" class="control-label">Descrição</label>
                                <div class="controls"><input name="descricao" id="descricao" type="text" maxlength="150" class="span8" value="<?=$dados['descricao'];?>"></div>
                            </div><!-- /.control-group -->
                            <!-- .control-group -->
                            <div class="control-group">
                                <label for="palavraschave" class="control-label">Palavras Chave</label>
                                <!-- .controls -->
                                <div class="controls">
                                    <textarea name="palavraschave" id="palavraschave" class="span8" style="height:150px;"><?=$dados["palavraschave"];?></textarea>
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                            <div class="form-actions">
                                <button type="submit" id="Bt" class="btn btn-primary">Salvar</button>
                                <button type="button" onclick="window.open('<?=$prefix;?>categorias', '_self');" class="btn pull-right">Voltar</button>
                            </div>
                        </form>
                    </div>
                </div>

<?php
include 'inc_footer.php';
