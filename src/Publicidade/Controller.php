<?php

namespace Publicidade;

use CoffeeCore\Core\AbstractController;
use CoffeeCore\Helper\ResourceManager;
use CoffeeCore\Storage\DoctrineStorage;

/**
 * Class Controller
 * @package Categorias
 */
class Controller extends AbstractController
{
    /**
     * @var string
     */
    public $pagina = 'publicidades';
    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Entity
     */
    protected $entity;

    /**
     * @return void
     */
    public function getDataTable()
    {
        $this->startConnection();
        $columns = ['id', 'titulo'];

        $search = $_POST['search']['value'];

        $limit = $_POST['length'];
        $offset = $_POST['start'];
        $orderBy = [
            $_POST['order'][0]['column'],
            $_POST['order'][0]['dir']
        ];

        $dataTables = $this->model->getDataTableData($columns, $orderBy, $search, $limit, $offset);



        foreach ($dataTables['data'] as $k => &$row) {
            $row[] = "<span class='center'>
                        <a class='btn btn-small' href='javascript:window.location = \"/painel/{$this->pagina}/alterar/{$row[0]}\"'>
                            <i class='icon-pencil'></i>
                        </a>
                     </span>";
        }

        header('Content-Type: application/json');
        echo json_encode($dataTables);
    }

    /**
     *
     */
    public function startConnection()
    {
        $this->entity = new \Publicidade\Entity();
        $this->model = new Model(new DoctrineStorage($this->entity));
    }

    /**
     *
     */
    public function gravar()
    {
        $this->startConnection();
        $this->entity = $this->model->findOneBy(['id' => $_POST['id']]);

        $this->entity->setTitulo($_POST['titulo']);
		$this->entity->setCodigohtml($_POST['codigohtml']);

        $this->model->update($this->entity);
        $this->model->flush();

        $lista = $this->model->findBy([]);
        
		$pubs = [];
        while ($lista->valid()) {
			$pubs[$lista->current()->getLocal()] = [
				"html" => $lista->current()->getCodigohtml()
			];
			$lista->next();
		}
		
        ResourceManager::writeFile("publicidade.json", json_encode($pubs));
    }

    /**
     * @param int|null $id
     */
    public function deletar($id = null)
    {
        $this->startConnection();
        if (!empty($id)) {
            $this->entity = $this->model->findOneBy(["id"=>$id]);
            $this->model->delete($this->entity);
            $this->model->flush();
        }
    }
}
