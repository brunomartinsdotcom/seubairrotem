<?php
namespace Publicidade;

/**
 * Entity
 *
 * @Table(name="publicidade")
 * @Entity
 */
class Entity implements \CoffeeCore\Core\Entity
{
    const FULL_NAME = 'Publicidade\\Entity';
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="titulo", type="string", length=65, nullable=false)
     */
    private $titulo = '';

    /**
     * @var string
     *
     * @Column(name="local", type="string", length=255, nullable=false)
     */
    private $local = '';

    /**
     * @var string
     *
     * @Column(name="codigohtml", type="text")
     */
    private $codigohtml = '';

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLocal()
    {
        return $this->local;
    }
	
	/**
     * @param string $local
     */
    public function setLocal($local)
    {
        $this->local = $local;
    }

    /**
     * @return string
     */
    public function getCodigohtml()
    {
        return $this->codigohtml;
    }

    /**
     * @param string $codigohtml
     */
    public function setCodigohtml($codigohtml)
    {
        $this->codigohtml = $codigohtml;
    }

    /**
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param string $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }
}
