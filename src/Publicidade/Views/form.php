<?php
include 'inc_head.php';

$action = "Adicionar";
if (isset($publicidade)) {
    $action = "Editar";
    $dados = [
        'id'            => $publicidade->getId(),
        'titulo'        => $publicidade->getTitulo(),
        'local'         => $publicidade->getLocal(),
        'codigohtml'    => $publicidade->getCodigohtml()
    ];
} else {
    $dados = [
        'id'            => '',
        'titulo'        => '',
        'local'         => '',
        'codigohtml'    => ''
    ];
}

?>
    <!-- styles -->
    <link href="<?=$prefix;?>css/bootstrap.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/jquery.gritter.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=$prefix;?>css/font-awesome.css">

    <!--[if IE 7]>
    <link rel="stylesheet" href="<?=$prefix;?>css/font-awesome-ie7.min.css">
    <![endif]-->
    <link href="<?=$prefix;?>css/tablecloth.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/styles.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie7.css" />
    <![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie8.css" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie9.css" />
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>

    <!--fav and touch icons -->
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="/favicon.png">

    <!--============ javascript ===========-->
    <script src="<?=$prefix;?>js/jquery.js"></script>
    <script src="<?=$prefix;?>js/bootstrap.js"></script>
    <script src="<?=$prefix;?>js/bootstrap-fileupload.js"></script>
    <script src="<?=$prefix;?>js/jquery.metadata.js"></script>
    <script src="<?=$prefix;?>js/jquery.tablesorter.min.js"></script>
    <script src="<?=$prefix;?>js/jquery.tablecloth.js"></script>
    <script src="<?=$prefix;?>js/excanvas.js"></script>
    <script src="<?=$prefix;?>js/jquery.collapsible.js"></script>
    <script src="<?=$prefix;?>js/accordion.nav.js"></script>
    <script src="<?=$prefix;?>js/custom.js"></script>
    <script src="<?=$prefix;?>js/respond.min.js"></script>
    <script src="<?=$prefix;?>js/ios-orientationchange-fix.js"></script>

    <script src="<?=$prefix;?>js/jquery.dataTables.js"></script>
    <script src="<?=$prefix;?>js/ZeroClipboard.js"></script>
    <script src="<?=$prefix;?>js/dataTables.bootstrap.js"></script>
    <script src="<?=$prefix;?>js/TableTools.js"></script>
    <script src="<?=$prefix;?>js/plugins/dataTables/date-uk.js"></script>

    <link href="<?=$prefix;?>css/tablecloth.css" rel="stylesheet">

    <script src="<?=$prefix;?>js/jquery.validate.js"></script>

    <script src="<?=$prefix;?>js/tiny_mce/tiny_mce.js"></script>
    <script src="<?=$prefix;?>js/tiny_mce/jquery.tinymce.js"></script>

</head>
<body>
    <div class="layout">
        <?php
        include_once 'inc_header.php';
        include_once 'inc_usuario.php';
        include_once 'inc_sidebar.php';
        ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">Publicidades</h3>
                        <span class="pull-right top-right-toolbar">
                        </span>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="<?=$prefix;?>" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li>
                            <a href="<?=$prefix;?>planos">Publicidades</a><span class="divider">
                                <i class="icon-angle-right"></i>
                            </span>
                        </li>
                        <li class="active">Formulário</li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="content-widgets light-gray">
                    <div class="widget-head black">
                        <h3><?=$action;?> Publicidade</h3>
                    </div>

                    <script type="text/javascript">
                        $(document).ready(function(){
                            // valida o formulário
                            $('#publicidade').validate({
                                // define regras para os campos
                                rules: {
                                    titulo: {
                                        required: true
                                    }
                                },
                                // define messages para cada campo
                                messages: {
                                    titulo: "Informe o título do banner"
                                }
                            });
                        });
                    </script>

                    <div class="widget-container">
                        <form name="publicidade" id="publicidade" class="form-horizontal" action="<?=$_SERVER["REQUEST_URI"];?>" method="post">
                            <div class="control-group">
                                <label for="nome" class="control-label">Título</label>
                                <div class="controls">

                                    <input name="id" id="id" type="hidden" value="<?=$dados['id'];?>">
                                    <input name="titulo" id="titulo" type="text" maxlength="50" class="span5" value="<?=$dados['titulo'];?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="local" class="control-label">Local</label>
                                <div class="controls">
                                    <input name="local" id="local" type="text" readonly class="span5" value="<?=$dados['local'];?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="codigohtml" class="control-label">Codigo Html</label>
                                <div class="controls">
                                    <textarea class="span8" rows="15" name="codigohtml" id="codigohtml"><?=$dados['codigohtml'];?></textarea>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" id="Bt" class="btn btn-success">Salvar</button>
                                <button type="button" onclick="window.open('<?=$prefix;?>publicidades', '_self');" class="btn right-float">Voltar</button>
                            </div>

                        </form>
                    </div>
                </div>
<?php
include 'inc_footer.php';
