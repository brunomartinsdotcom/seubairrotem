<?php
    include 'inc_head.php';

    $action = "Adicionar";
    if (isset($textoSite)) {
        $action = "Editar";
        $dados = [
            'id' => $textoSite->getId(),
            'titulo' => $textoSite->getTitulo(),
            'texto' => $textoSite->getTexto()
        ];
    } else {
        $dados = [
            'id' => '',
            'titulo' => '',
            'texto' => ''
        ];
    }
    ?>
</head>
<body>
    <div class="layout">
    <?php
        include_once 'inc_header.php';
        include_once 'inc_usuario.php';
        include_once 'inc_sidebar.php';
    ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">Textos do Site</h3>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="<?=$prefix;?>" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li>
                            <a href="<?=$prefix;?>textos-do-site">Textos do Site</a><span class="divider">
                                <i class="icon-angle-right"></i>
                            </span>
                        </li>
                        <li class="active"><?=$action;?></li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="content-widgets light-gray">
                    <div class="widget-head black">
                        <h3><?=$action;?> Texto do Site</h3>
                    </div>
                    <script type="text/javascript" src="<?=$prefix;?>editor/ckeditor/ckeditor.js"></script>
                    <script type="text/javascript">
                        $(document).ready(function(){
                            //INICIALIZA O EDITOR DE TEXTO
                            CKEDITOR.replace('texto');

                            // valida o formulário
                            $('#subtextos-do-site').validate({
                                // define regras para os campos
                                rules: {
                                    titulo: {
                                        required: true
                                    },
									texto: {
										required: function(){
                                            CKEDITOR.instances.texto.updateElement();
                                        }
									}
                                },
                                // define messages para cada campo
                                messages: {
                                    titulo:"Informe o título do texto",
									texto:"Informe o texto"
                                }
                            });
                        });
                    </script>
                    <div class="widget-container">
                        <form name="subtextos-do-site" id="subtextos-do-site" class="form-horizontal" action="<?=$_SERVER["REQUEST_URI"];?>" method="post">
                            <input name="id" id="id" type="hidden" value="<?=$dados['id'];?>">
                            <div class="control-group">
                                <label for="titulo" class="control-label">Título</label>
                                <div class="controls">
                                    <input name="titulo" id="titulo" type="text" maxlength="30" class="span8" value="<?=$dados['titulo'];?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="nome" class="control-label">Texto</label>
                                <div class="controls" style="width: 80%;">
                                    <textarea class="span8" rows="15" name="texto"><?=$dados['texto'];?></textarea>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" id="Bt" class="btn btn-primary">Salvar</button>
                                <button type="button" onclick="window.open('<?=$prefix;?>textos-do-site', '_self');" class="btn right-float">Voltar</button>
                            </div>
                        </form>
                    </div>
                </div>
<?php
include 'inc_footer.php';
