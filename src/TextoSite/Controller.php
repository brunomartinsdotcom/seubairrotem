<?php

namespace TextoSite;

use CoffeeCore\Core\AbstractController;
use CoffeeCore\Storage\DoctrineStorage;

/**
 * Class Controller
 * @package TextoSite
 */
class Controller extends AbstractController
{
    /**
     * @var string
     */
    public $pagina = 'textos-do-site';
    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Entity
     */
    protected $entity;

    /**
     * @return void
     */
    public function getDataTable()
    {
        $this->startConnection();
        $columns = ['id', 'titulo'];

        $search = isset($_POST['search']['value']) ? $_POST['search']['value'] : ""; //$_POST['search']['value'];

        $limit = isset($_POST['length']) ? $_POST['length'] : 10;
        $offset = isset($_POST['start']) ? $_POST['start'] : 0;
        if(isset($_POST['order'])){
            $orderBy = [
                $_POST['order'][0]['column'],
                $_POST['order'][0]['dir']
            ];
        }else{
            $orderBy = [0, 'ASC'];
        }

        $dataTables = $this->model->getDataTableData($columns, $orderBy, $search, $limit, $offset);



        foreach ($dataTables['data'] as $k => &$row) {
            $row[] = "<span class='center'>
                        <button class='btn btn-primary' onclick='window.location = \"/painel/{$this->pagina}/alterar/{$row[0]}\"'>
                            <span><i class='icon-pencil'></i></span>
                        </button>
                     </span>";
            unset($k);
        }

        //header('Content-Type: application/json');
        echo json_encode($dataTables);
    }

    /**
     *
     */
    public function startConnection()
    {
        $this->model = new Model(new DoctrineStorage(new Entity()));
    }

    /**
     *
     */
    public function gravar()
    {
        try {
            $this->startConnection();

            $entity = $this->model->findOneBy(["id" => $_POST['id']]);

            $entity->setTitulo($_POST['titulo']);
            $entity->setTexto($_POST['texto']);

            $this->model->update($entity);

            $this->model->flush();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param int|null $id
     */
    public function deletar($id = null)
    {
        $this->startConnection();
        if (!empty($id)) {
            $this->entity = $this->model->findOneBy(["id"=>$id]);
            $this->model->delete($this->entity);
            $this->model->flush();
        }
    }
}
