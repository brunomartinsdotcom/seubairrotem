<?php
include 'inc_head.php';

$action = "Adicionar";
if (isset($cidade)) {
    $action = "Editar";
    $dados = [
        'id' => $cidade->getId(),
        'id_uf' => $cidade->getIduf(),
        'nome' => $cidade->getNome()
    ];
} else {
    $dados = [
        'id' => '',
        'id_uf' => '',
        'nome' => ''
    ];
}
?>
</head>
<body>
    <div class="layout">
    <?php
        include_once 'inc_header.php';
        include_once 'inc_usuario.php';
        include_once 'inc_sidebar.php';
    ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">Cidades</h3>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="<?=$prefix;?>" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li>
                            <a href="<?=$prefix;?>cidades" title="Cidades">Cidades</a><span class="divider">
                                <i class="icon-angle-right"></i>
                            </span>
                        </li>
                        <li class="active"><?=$action;?></li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="content-widgets light-gray">
                    <div class="widget-head black">
                        <h3><?=$action;?> Cidade</h3>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function(){
                            // valida o formulário
                            $('#estados').validate({
                                // define regras para os campos
                                rules: {
                                    id_uf: {
                                        required: true
                                    },
                                    nome: {
                                        required: true
                                    }
                                },
                                // define messages para cada campo
                                messages: {
                                    id_uf:"Informe o Estado",
                                    nome:"Informe o nome da Cidade"
                                }
                            });

                            var interval = 0;
                            $('.chap').bind("keyup change", function(){
                                // começa a contar o tempo
                                clearInterval(interval);

                                // 500ms após o usuário parar de digitar a função é chamada
                                interval = window.setTimeout(function(){
                                    data_html = "nome=" + $('#nome').val() + "&id_uf=" + $('#id_uf').val()<?php if(isset($cidade)){ ?> + "&id=" + $('#id').val();<?php } ?>;
                                    $.ajax({
                                        type: 'POST',
                                        url: '<?=$prefix;?>cidades/verifica-existente',
                                        data: data_html,
                                        dataType: 'json',
                                        success: function (msg) {
                                            if (msg.error == 0) {
                                                $('#nome').css('border-color', 'green');
                                                $('#msg').html('');
                                                $("#Bt").removeAttr('disabled');
                                            } else {
                                                $('#nome').css('border-color', 'red');
                                                $('#msg').html('Cidade já cadastrada nesse Estado!');
                                                $('#Bt').attr('disabled', 'true');
                                            }
                                        }
                                    });
                                }, 500);
                            });
                        });
                    </script>
                    <div class="widget-container">
                        <form name="estados" id="estados" class="form-horizontal" action="<?=$_SERVER["REQUEST_URI"];?>" method="post">
                            <?php if(isset($cidade)){ ?>
                            <input name="id" id="id" type="hidden" value="<?=$dados['id'];?>">
                            <?php } ?>
                            <div class="control-group">
                                <label for="id_uf" class="control-label">Estado</label>
                                <div class="controls">
                                    <select name="id_uf" id="id_uf" class="chap">
                                        <option value="">Escolha...</option>
                                        <?php
                                            $estados = \CoffeeCore\Helper\ResourceManager::readFile("cadastro_de_estados.json");
                                            foreach($estados as $k => $estado):
                                        ?>
                                        <option value="<?=$estado['id'];?>"<?php if(isset($cidade) and $estado['id'] == $dados['id_uf']){ ?> selected<?php } ?>><?=$estado['nome'];?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="nome" class="control-label">Nome</label>
                                <div class="controls">
                                    <input name="nome" id="nome" type="text" maxlength="50" class="span7 chap" value="<?=$dados['nome'];?>">
                                    <em id="msg" class="error"></em>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" id="Bt" class="btn btn-primary">Salvar</button>
                                <button type="button" onclick="window.open('<?=$prefix;?>cidades', '_self');" class="btn right-float">Voltar</button>
                            </div>
                        </form>
                    </div>
                </div>
<?php
include 'inc_footer.php';
