<?php

namespace Noticia;


use CoffeeCore\Storage\DoctrineStorage;
use Slim\Slim;

/**
 * Class Routes
 * @package AreaAtuacao
 */
class Routes
{
    /**
     * @param Slim $app
     */
    public static function getRoutes(Slim $app)
    {
        $controller = new Controller();

        $app->group('/noticias', function () use ($app, $controller) {

            $app->map(
                '/',
                function () use ($app, $controller) {
                    if ($app->request()->isGet()) {
                        $app->render(__NAMESPACE__ . "/Views/grid.php");
                    }
                    if ($app->request()->isPost()) {
						$controller->getDataTable();
                    }
                }
            )->via("GET", "POST");

            $app->map(
                '/novo/',
                function () use ($app, $controller) {
                    if ($app->request()->isGet()) {
                        $app->render(__NAMESPACE__ . "/Views/form.php");
                    }
                    if ($app->request()->isPost()) {
                        try {
                            $controller->gravar();
                            $app->flash("info", "Inserido com sucesso!");
                        } catch (\Exception $e) {
                            $app->flash("info", "Erro ao inserir! <br>" . addslashes($e->getMessage()));

                        }
                        $app->redirect("/painel/{$controller->pagina}/");
                    }
                }
            )->via("GET", "POST");

            $app->map(
                '/alterar/:id',
                function ($id) use ($app, $controller) {
                    if ($app->request()->isGet()) {
                        $registro = DoctrineStorage::orm()
                            ->getRepository(__NAMESPACE__."\\Entity")->findOneBy(["id" => $id]);
                        $app->render(__NAMESPACE__."/Views/form.php", ["noticia" => $registro]);
                    }
                    if ($app->request()->isPost()) {
                        try {
                            $controller->gravar();
                            $app->flash("info", "Alterado com sucesso!");
                        } catch (\Exception $e) {
                            throw $e;
                            //$app->flash("info", "Erro ao alterar! <br>" . addslashes($e->getMessage()));

                        }
                        $app->redirect("/painel/{$controller->pagina}/");
                    }
                }
            )->via("GET", "POST");

            $app->get(
                '/deletar/:id/',
                function ($id) use ($app, $controller) {
                    try {
						$registro = DoctrineStorage::orm()->getRepository(__NAMESPACE__."\\Entity")->findOneBy(["id" => $id]);
							
                        $controller->deletar($id, $registro->getImagem());
                        $app->flash("info", "Excluido com sucesso!");
                    } catch (\Exception $e) {
                        $app->flash("info", "Erro ao excluir!");

                    }
                    $app->redirect("/painel/{$controller->pagina}/");
                }
            );
        });
    }
}
