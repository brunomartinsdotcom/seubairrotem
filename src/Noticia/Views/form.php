<?php
include 'inc_head.php';

$action = "Adicionar";
if (isset($noticia)) {
    $action = "Editar";
    $dados = [
        'id' => $noticia->getId(),
        'titulo' => $noticia->getTitulo(),
        'imagem' => $noticia->getImagem(),
        'texto' => $noticia->getTexto()
    ];
} else {
    $dados = [
        'id' => '',
        'titulo' => '',
        'imagem' => '',
        'texto' => ''
    ];
}
?>
    <!-- styles -->
    <link href="<?=$prefix;?>css/bootstrap.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/jquery.gritter.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=$prefix;?>css/font-awesome.css">

    <!--[if IE 7]>
    <link rel="stylesheet" href="<?=$prefix;?>css/font-awesome-ie7.min.css">
    <![endif]-->
    <link href="<?=$prefix;?>css/tablecloth.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/styles.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie7.css" />
    <![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie8.css" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie9.css" />
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>

    <!--fav and touch icons -->
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="/favicon.png">

    <!--============ javascript ===========-->
    <script src="<?=$prefix;?>js/jquery.js"></script>
    <script src="<?=$prefix;?>js/bootstrap.js"></script>
    <script src="<?=$prefix;?>js/bootstrap-fileupload.js"></script>
    <script src="<?=$prefix;?>js/jquery.metadata.js"></script>
    <script src="<?=$prefix;?>js/jquery.tablesorter.min.js"></script>
    <script src="<?=$prefix;?>js/jquery.tablecloth.js"></script>
    <script src="<?=$prefix;?>js/excanvas.js"></script>
    <script src="<?=$prefix;?>js/jquery.collapsible.js"></script>
    <script src="<?=$prefix;?>js/accordion.nav.js"></script>
    <script src="<?=$prefix;?>js/custom.js"></script>
    <script src="<?=$prefix;?>js/respond.min.js"></script>
    <script src="<?=$prefix;?>js/ios-orientationchange-fix.js"></script>

    <script src="<?=$prefix;?>js/jquery.dataTables.js"></script>
    <script src="<?=$prefix;?>js/ZeroClipboard.js"></script>
    <script src="<?=$prefix;?>js/dataTables.bootstrap.js"></script>
    <script src="<?=$prefix;?>js/TableTools.js"></script>
    <script src="<?=$prefix;?>js/plugins/dataTables/date-uk.js"></script>

    <link href="<?=$prefix;?>css/tablecloth.css" rel="stylesheet">

    <script src="<?=$prefix;?>js/jquery.validate.js"></script>

    <script src="<?=$prefix;?>js/tiny_mce/tiny_mce.js"></script>
    <script src="<?=$prefix;?>js/tiny_mce/jquery.tinymce.js"></script>

</head>
<body>
    <div class="layout">
    <?php
        include_once 'inc_header.php';
        include_once 'inc_usuario.php';
        include_once 'inc_sidebar.php';
    ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h1 class="page-header">Notícias</h1>
                        <span class="pull-right top-right-toolbar">
                            <a href="<?=$prefix;?>noticias/novo" class="btn btn-mini btn-success" title="Novo"><i class="icon-plus "></i> Adicionar Novo</a>
                        </span>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="<?=$prefix;?>" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li>
                            <a href="<?=$prefix;?>noticias">Notícias</a><span class="divider">
                                <i class="icon-angle-right"></i>
                            </span>
                        </li>
                        <li class="active">Formulário</li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="content-widgets light-gray">
                    <div class="widget-head black">
                        <h3><?=$action;?> Notícia</h3>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            tinyMCE.init({
                                mode : "textareas",
                                theme : "advanced"
                            });

                            // valida o formulário
                            $('#noticias').validate({
                                // define regras para os campos
                                rules: {
                                    titulo: {
                                        required: true,
                                        minlength: 4
                                    },
                                    texto: {
                                        required: true
                                    }<?php if (!isset($noticia)) { ?>,
									imagem: {
										required: true
									}<?php } ?>
                                },
                                // define messages para cada campo
                                messages: {
                                    titulo:"Informe o título da notícia",
                                    texto:"Informe o título da notícia"<?php if (!isset($noticia)) { ?>,
									imagem: "Informe a imagem da notícia"<?php } ?>
                                }
                            });
                        });
                    </script>
                    <div class="widget-container">
                        <form name="noticias" id="noticias" class="form-horizontal" enctype="multipart/form-data" action="<?=$_SERVER["REQUEST_URI"];?>" method="post">
                        	<?php if(isset($noticia)) { ?>
                            <input name="id" id="id" type="hidden" value="<?=$dados['id'];?>">
                            <input name="imagem_antiga" id="imagem_antiga" type="hidden" value="<?=$dados['imagem'];?>">
                            <?php } ?>
                            <div class="control-group">
                                <label for="titulo" class="control-label">Título</label>
                                <div class="controls">
                                    <input name="titulo" id="titulo" type="text" maxlength="60" class="span7" value="<?=$dados['titulo'];?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="texto" class="control-label">Texto</label>
                                <div class="controls">
                                    <textarea class="span7 tinymce-full" rows="15" name="texto"><?=$dados['texto'];?></textarea>
                                </div>
                            </div>
							<div class="control-group">
                                <label for="imagem" class="control-label">Imagem</label>
                                <div class="controls">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width:600px; height: 400px;">
                                        	<?php if (!isset($noticia)) { ?>
                                            <img src="http://www.placehold.it/600x400/EFEFEF/AAAAAA" alt="img" />
                                            <?php }else{ ?>
                                            <img src="<?="/_uploads/noticias/".$dados['imagem'];?>" alt="img" />
                                            <?php } ?>
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 400px; max-height: 250px; line-height: 20px;"> </div>
                                        <div> <span class="btn btn-file"><span class="fileupload-new">Selecionar Imagem</span><span class="fileupload-exists">Alterar</span>
                                        <input name="imagem" type="file"/>
                                        </span><a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remover</a> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" id="Bt" class="btn btn-success">Salvar</button>
                                <button type="button" onclick="window.open('<?=$prefix;?>noticias', '_self');" class="btn right-float">Voltar</button>
                            </div>
                        </form>
                    </div>
                </div>

<?php
include 'inc_footer.php';
