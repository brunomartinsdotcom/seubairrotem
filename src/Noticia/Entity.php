<?php

namespace Noticia;

use DateTime;

/**
 * Entity
 *
 * @Table(name="noticia")
 * @Entity
 */
class Entity implements \CoffeeCore\Core\Entity
{

    const FULL_NAME = "Noticia\\Entity";

    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="titulo", type="string", length=120, nullable=false)
     */
    private $titulo;

    /**
     * @var string
     *
     * @Column(name="imagem", type="string", length=255)
     */
    private $imagem;

    /**
     * @var string
     *
     * @Column(name="texto", type="text")
     */
    private $texto;

    /**
     * @var datetime
     *
     * @Column(name="datanoticia", type="datetime")
     */
    private $datanoticia;

    public function __construct()
    {
        $this->datanoticia = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \Datetime
     */
    public function getDatanoticia()
    {
        return $this->datanoticia;
    }

    /**
     * @param datetime $datanoticia
     */
    public function setDatanoticia($datanoticia)
    {
        $this->datanoticia = $datanoticia;
    }

    /**
     * @return string
     */
    public function getImagem()
    {
        return $this->imagem;
    }

    /**
     * @param string $imagem
     */
    public function setImagem($imagem)
    {
        $this->imagem = $imagem;
    }

    /**
     * @return string
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * @param string $texto
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;
    }

    /**
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param string $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }
}
