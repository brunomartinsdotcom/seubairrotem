<?php

namespace Noticia;

use CoffeeCore\Core\AbstractController;
use CoffeeCore\Helper\Canvas;
use CoffeeCore\Storage\DoctrineStorage;

/**
 * Class Controller
 * @package Categorias
 */
class Controller extends AbstractController
{
    /**
     * @var string
     */
    public $pagina = 'noticias';
    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Entity
     */
    protected $entity;

    /**
     * @return void
     */
    public function getDataTable()
    {
        $this->startConnection();
        $columns = ['id', 'datanoticia', 'titulo', 'imagem'];

        $search = $_POST['search']['value'];

        $limit = $_POST['length'];
        $offset = $_POST['start'];
        $orderBy = [
            //1, 'DESC'
			$_POST['order'][0]['column'],
            $_POST['order'][0]['dir']
        ];

        $dataTables = $this->model->getDataTableData($columns, $orderBy, $search, $limit, $offset);
		//print_r($dataTables); die;
        foreach ($dataTables['data'] as $k => &$row) {
            $row[1] =  $row[1]->format("d/m/Y H:i:s");
			$row[3] =  "<img src='" .url_base() . "/_uploads/noticias/tumb_{$row[3]}' />";
            $row[] = "<span class='center'>
                        <button class='btn' onclick='javascript:window.location = \"/painel/{$this->pagina}/alterar/{$row[0]}\"'>
                            <i class='icon-pencil'></i>
                        </button></span>";
			$row[] = "<span class='center'>					
                        <button data-url='/painel/{$this->pagina}/deletar/{$row[0]}' id='deletar' class='btn btn-primary'>
                            <i class='icon-trash'></i>
                        </button>
                     </span>";
        }
		
		/*header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Length: " . $dataTables);

		header('Content-Type: application/json');
		header("Content-Disposition: attachment; filename=data.json");*/
		
		header('Content-Type: application/json');
        echo json_encode($dataTables);
    }

    /**
     *
     */
    protected function startConnection()
    {
        $this->entity = new Entity();
        $this->model = new Model(new DoctrineStorage($this->entity));
    }

    /**
     *
     */
    public function gravar()
    {
        $this->startConnection();
        if (!empty($_POST['id'])) {
            $this->entity = $this->model->findOneBy(['id' => $_POST['id']]);
        }
		
        $this->entity->setTitulo($_POST['titulo']);
        $this->entity->setTexto($_POST['texto']);
		
        if (empty($this->entity->getId())) {
            $fileName = time() . $_FILES['imagem']['name'];
			$newFile = "../_uploads/noticias/" . $fileName;
			$newFileInt = "../_uploads/noticias/int_" .$fileName;
			$newFileTumb = "../_uploads/noticias/tumb_" . $fileName;
			move_uploaded_file($_FILES['imagem']['tmp_name'], $newFile);
			//echo ($newFile);
			$imagem = new Canvas();
			$imagem->load($newFile);
			$imagem->save($newFile);
			$imagem->resize(600, 400, "fill")->save($newFile);
			$imagem->resize(286, 190, "fill")->save($newFileInt);
			$imagem->resize(148, 111, "fill")->save($newFileTumb);
			$this->entity->setImagem($fileName);
			//INSERT
			$this->model->insert($this->entity);
        } else {
			if($_FILES['imagem']['name'] != ''){
				echo $_POST['imagem_antiga'];
				$output_dir = $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "_uploads/noticias/";
				if(is_file($output_dir.$_POST['imagem_antiga'])) {
					echo "<br>Deleta";
					unlink($output_dir.$_POST['imagem_antiga']);
				}
				if(is_file($output_dir.'int_'.$_POST['imagem_antiga'])) {
					unlink($output_dir.'int_'.$_POST['imagem_antiga']);
				}
				if(is_file($output_dir.'tumb_'.$_POST['imagem_antiga'])) {
					unlink($output_dir.'tumb_'.$_POST['imagem_antiga']);
				}
				
				$fileName = time() . $_FILES['imagem']['name'];
				$newFile = "../_uploads/noticias/" . $fileName;
				$newFileInt = "../_uploads/noticias/int_" .$fileName;
				$newFileTumb = "../_uploads/noticias/tumb_" . $fileName;
				move_uploaded_file($_FILES['imagem']['tmp_name'], $newFile);
				//echo ($newFile);
				$imagem = new Canvas();
				$imagem->load($newFile);
				$imagem->save($newFile);
				$imagem->resize(600, 400, "fill")->save($newFile);
				$imagem->resize(286, 190, "fill")->save($newFileInt);
				$imagem->resize(148, 111, "fill")->save($newFileTumb);
				$this->entity->setImagem($fileName);
			}
			//UPDATE
            $this->model->update($this->entity);
			//die;
        }

        $this->model->flush();
    }

    /**
     * @param int|null $id
     */
    public function deletar($id = null, $img = null)
    {
        $this->startConnection();
        if (!empty($id)) {
			$output_dir = $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "_uploads/noticias/";
			if(is_file($output_dir.$img)) {
				unlink($output_dir.$img);
			}
			if(is_file($output_dir.'int_'.$img)) {
				unlink($output_dir.'int_'.$img);
			}
			if(is_file($output_dir.'tumb_'.$img)) {
				unlink($output_dir.'tumb_'.$img);
			}
				
            $this->entity = $this->model->findOneBy(["id"=>$id]);
            $this->model->delete($this->entity);
            $this->model->flush();
        }
    }
}
