<?php

namespace AnuncioFoto;

/**
 * Entity
 *
 * @Table(name="anunciofoto")
 * @Entity
 */
class Entity implements \CoffeeCore\Core\Entity
{
    /**
     *
     */
    const FULL_NAME = "AnuncioFoto\\Entity";

    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @Column(name="anuncio", type="integer")
     */
    private $anuncio;

    /**
     * @var string
     *
     * @Column(name="foto", type="string", length=255)
     */
    private $foto;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getAnuncio()
    {
        return $this->anuncio;
    }

    /**
     * @param int $anuncio
     */
    public function setAnuncio($anuncio)
    {
        $this->anuncio = $anuncio;
    }

    /**
     * @return string
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * @param string $foto
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;
    }
}
