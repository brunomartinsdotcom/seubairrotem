<?php include 'inc_head.php'; ?>
</head>
<body>
<div class="layout">
    <?php
        include 'inc_header.php';
        include 'inc_usuario.php';
        include 'inc_sidebar.php';
    ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">Newsletter</h3>
                        <span class="pull-right top-right-toolbar">
                            <a href="<?=$prefix;?>newsletter/exportar-emails" target="_blank" class="btn btn-mini btn-info" title="Exportar E-mails">
                                <i class="icon-envelope-alt"></i> Exportar E-mails
                            </a>
                            <a href="<?=$prefix;?>newsletter/exportar-celulares" class="btn btn-mini btn-primary" style="margin-left: 30px;" title="Exportar Celulares">
                                <i class="icon-phone-sign"></i> Exportar Celulares
                            </a>
                        </span>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="<?=$prefix;?>" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li class="active">Newsletter</li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="widget-container">
                    <div id="data-table_wrapper" class=" dataTables_wrapper  form-inline" role="grid">
                        <table id="data-table" class="table table-bordered tabela">
                            <thead>
                            <tr>
                                <th style="width: 25px">ID</th>
                                <th>Nome</th>
                                <th>Celular</th>
                                <th>Email</th>
                                <th style="width: 16px" class="center">Ex</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <script type="application/javascript">
                    $(document).ready(function(){
                        $("#data-table").DataTable({
                            "processing": true,
                            "serverSide": true,
                            "ajax": {
                                "url": "<?=$prefix;?>newsletter",
                                "type": "POST"
                            },
                            "aoColumnDefs": [
                                { "bSortable": false, "aTargets": [4] }
                            ],
                            language : {
                                url: "<?=$prefix;?>js/plugins/dataTables/Portuguese-Brasil.lang"
                            }
                        })
                    })
                </script>
    <?php
include 'inc_footer.php';
