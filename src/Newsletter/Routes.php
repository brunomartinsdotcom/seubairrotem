<?php

namespace Newsletter;

use Slim\Slim;

/**
 * Class Routes
 * @package Newsletter
 */
class Routes
{
    /**
     * @param Slim $app
     */
    public static function getRoutes(Slim $app)
    {
        $controller = new Controller();

        $app->group("/{$controller->pagina}/", function () use ($app , $controller) {

            $app->map(
                '',
                function () use ($app, $controller) {
                    if ($app->request()->isGet()) {
						$app->render(__NAMESPACE__."/Views/grid.php");
                    }
                    if ($app->request->isPost()) {
                        $controller->getDataTable();
                    }
                }
            )->via('GET', 'POST');
			
			$app->get(
                'exportar-emails',
                function () use ($app, $controller) {
                    if ($app->request()->isGet()) {
						$app->render(__NAMESPACE__ . "/Views/exportar-emails.php");
                    }
                }
            );

            $app->get(
                'exportar-celulares',
                function () use ($app, $controller) {
                    if ($app->request()->isGet()) {
                        $app->render(__NAMESPACE__ . "/Views/exportar-celulares.php");
                    }
                }
            );
			
            $app->get(
                'deletar/:id/',
                function ($id) use ($app, $controller) {
                    try {
                        $controller->deletar($id);
                        $app->flash("info", "Excluido com sucesso!");
                    } catch (\Exception $e) {
                        $app->flash("info", "Erro ao excluir!");
                    }
                    $app->redirect("/painel/{$controller->pagina}/");
                }
            );
        });
    }
}
