<?php

namespace AnuncioDestaque;

use CoffeeCore\Storage\DoctrineStorage;

/**
 * Entity
 *
 * @Table(name="anunciodestaque")
 * @Entity
 */
class Entity implements \CoffeeCore\Core\Entity
{
    /**
     *
     */
    const FULL_NAME = "AnuncioDestaque\\Entity";

    /**
     * @var integer
     * @Id
     * @Column(name="id", type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * @Column(name="anuncio", type="integer", nullable=false)
     */
    private $anuncio;

    /**
     * @var integer
     * @Column(name="fatura", type="integer")
     */
    private $fatura;

    /**
     * @var integer
     * @Column(name="destaque", type="integer", nullable=false)
     */
    private $destaque;

    /**
     * @var integer
     *
     * @Column(name="diascontratados", type="integer")
     */
    private $diascontratados;

    /**
     * @var datetime
     * @Column(name="datavencimento", type="datetime")
     */
    private $datavencimento;

    /**
     * @var datetime
     * @Column(name="datainicio", type="datetime")
     */
    private $datainicio;

    public function __construct()
    {
        $this->datainicio = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getAnuncio()
    {
        return $this->anuncio;
    }

    /**
     * @param int $anuncio
     */
    public function setAnuncio($anuncio)
    {
        $this->anuncio = $anuncio;
    }

    /**
     * @return int
     */
    public function getDiascontratados()
    {
        return $this->diascontratados;
    }

    /**
     * @param int $diascontratados
     */
    public function setDiascontratados($diascontratados)
    {
        $this->diascontratados = $diascontratados;
    }

    /**
     * @return datetime
     */
    public function getDatainicio()
    {
        return $this->datainicio;
    }

    /**
     * @param datetime $datainicio
     */
    public function setDatainicio($datainicio)
    {
        $this->datainicio = $datainicio;
    }

    /**
     * @return datetime
     */
    public function getDatavencimento()
    {
        return $this->datavencimento;
    }

    /**
     * @param datetime $datavencimento
     */
    public function setDatavencimento($datavencimento)
    {
        $this->datavencimento = $datavencimento;
    }

    /**
     * @return int
     */
    public function getDestaque()
    {
        return $this->destaque;
    }

    /**
     * @param int $destaque
     */
    public function setDestaque($destaque)
    {
        $this->destaque = $destaque;
    }

    /**
     * @return int
     */
    public function getFatura()
    {
        return $this->fatura;
    }

    /**
     * @param int $fatura
     */
    public function setFatura($fatura)
    {
        $this->fatura = $fatura;
    }

    /**
     * @return mixed
     */
    public function getDestaqueTitulo()
    {
        return DoctrineStorage::orm()->getRepository(\Destaque\Entity::FULL_NAME)->findOneBy(['id' => $this->destaque])->getTitulo();
    }

    /**
     * @return mixed
     */
    public function getAnuncioTitulo()
    {
        return DoctrineStorage::orm()->getRepository(\Anuncio\Entity::FULL_NAME)->findOneBy(['id' => $this->anuncio])->getTitulo();
    }
}
