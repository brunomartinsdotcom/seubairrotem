<?php

namespace Paginas;

/**
 * Entity
 *
 * @Table(name="novo_paginas")
 * @Entity
 */
class Entity implements \CoffeeCore\Core\Entity
{
    /**
     *
     */
    const FULL_NAME = "Paginas\\Entity";

    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="titulo", type="string", length=100, nullable=false)
     */
    private $titulo;

    /**
     * @var string
     *
     * @Column(name="descricao", type="string", length=200, nullable=true)
     */
    private $descricao;

    /**
     * @var string
     *
     * @Column(name="palavraschave", type="string", nullable=true)
     */
    private $palavraschave;

    /**
     * @var string
     *
     * @Column(name="slug", type="string", length=100, nullable=false)
     */
    private $slug;

    /**
     * @param boolean $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return boolean
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    /**
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param string $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param string $palavraschave
     */
    public function setPalavraschave($palavraschave)
    {
        $this->palavraschave = $palavraschave;
    }

    /**
     * @return string
     */
    public function getPalavraschave()
    {
        return $this->palavraschave;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }
}
