<?php
include 'inc_head.php';

$action = "Editar";
if (isset($pagina)) {
    $dados = [
        'id' => $pagina->getId(),
        'titulo' => $pagina->getTitulo(),
        'descricao' => $pagina->getDescricao(),
        'palavraschave' => $pagina->getPalavraschave(),
        'slug' => $pagina->getSlug()
    ];
} else {
    echo "<script>alert('Nenhuma página encontrada!'); location='".$prefix."paginas';</script>";
}
?>
<script src="<?=$prefix;?>js/jquery.tagsinput.js"></script>
</head>
<body>
    <div class="layout">
    <?php
        include_once 'inc_header.php';
        include_once 'inc_usuario.php';
        include_once 'inc_sidebar.php';
    ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">Páginas</h3>

                    </div>
                    <ul class="breadcrumb">
                        <li><a href="<?=$prefix;?>" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li>
                            <a href="<?=$prefix;?>paginas" title="Páginas">Páginas</a><span class="divider">
                                <i class="icon-angle-right"></i>
                            </span>
                        </li>
                        <li class="active"><?=$action;?></li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="content-widgets light-gray">
                    <div class="widget-head black">
                        <h3><?=$action;?> Página</h3>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function(){
                            // Script que coloca função "TAGS" na caixa de palavras chave
                            $('#palavraschave').tagsInput({
                                width: 'auto'
                            });
                            // valida o formulário
                            $('#pagina').validate({
                                // define regras para os campos
                                rules: {
                                    titulo: {
                                        required: true
                                    }<?php if($dados['titulo'] != "Minha Conta"){ ?>,
                                    descricao: {
                                        required: true
                                    },
                                    palavraschave: {
                                        required: true
                                    }<?php } ?>
                                },
                                // define messages para cada campo
                                messages: {
                                    titulo			: "Informe o título da página"<?php if($dados['titulo'] != "Minha Conta"){ ?>,
                                    descricao		: "Informe a descrição da página",
                                    palavraschave	: "Informe as palavras-chave da página"<?php } ?>
                                }
                            });
                        });
                    </script>
                    <div class="widget-container">
                        <form name="pagina" id="pagina" class="form-horizontal" action="<?=$_SERVER["REQUEST_URI"];?>" method="post">
                            <input name="id" id="id" type="hidden" value="<?=$dados['id'];?>">
                            <div class="control-group">
                                <label for="titulo" class="control-label">Título</label>
                                <div class="controls">
                                    <input name="titulo" id="titulo" type="text" maxlength="100" class="span6" readonly="readonly" value="<?=$dados['titulo'];?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="descricao" class="control-label">Descrição</label>
                                <div class="controls">
                                    <input name="descricao" id="descricao" type="text" maxlength="200" class="span8" value="<?=$dados['descricao'];?>">
                                </div>
                            </div>
                            <!-- .control-group -->
                            <div class="control-group">
                                <label for="palavraschave" class="control-label">Palavras-Chave</label>
                                <!-- .controls -->
                                <div class="controls">
                                    <input name="palavraschave" id="palavraschave" type="text" maxlength="255" class="span10" value="<?=$dados["palavraschave"];?>" />
                                </div><!-- /.controls -->
                            </div><!-- /.control-group -->
                            <div class="form-actions">
                                <button type="submit" id="Bt" class="btn btn-primary">Salvar</button>
                                <button type="button" onclick="window.open('<?=$prefix;?>paginas', '_self');" class="btn right-float">Voltar</button>
                            </div>
                        </form>
                    </div>
                </div>

<?php
include 'inc_footer.php';
