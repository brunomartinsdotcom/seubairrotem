<?php
include 'inc_head.php';
?>
</head>
<body>
<div class="layout">
    <?php
        include 'inc_header.php';
        include 'inc_usuario.php';
        include 'inc_sidebar.php';
    ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">Páginas</h3>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="<?=$prefix;?>" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li class="active">Páginas</li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="widget-container">
                    <div id="data-table_wrapper" class=" dataTables_wrapper  form-inline" role="grid">
                        <table id="data-table" class="table table-bordered tabela">
                            <thead>
                            <tr>
                                <th style="width: 50px">ID</th>
                                <th>Título</th>
                                <th style="width: 16px" class="center">Ed</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <script type="text/javascript">
                    $(document).ready(function(){
                        $("#data-table").DataTable({
                            "processing": true,
                            "serverSide": true,
                            "ajax": {
                                "url": "<?=$prefix;?>paginas",
                                "type": "POST"
                            },
                            "aoColumnDefs": [
                                { "bSortable": false, "aTargets": [2] }
                            ],
                            language : {
                                url: "<?=$prefix;?>js/plugins/dataTables/Portuguese-Brasil.lang"
                            }
                        });
                    });
                </script>

    <?php
include 'inc_footer.php';
