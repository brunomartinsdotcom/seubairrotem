<?php

namespace Paginas;

use CoffeeCore\Core\AbstractController;
use CoffeeCore\Helper\ResourceManager;
use CoffeeCore\Storage\DoctrineStorage;

/**
 * Class Controller
 * @package Paginas
 */
class Controller extends AbstractController
{
    /**
     * @var string
     */
    public $pagina = 'paginas';
    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Entity
     */
    protected $entity;

    /**
     * @return void
     */
    public function getDataTable()
    {
        $this->startConnection();
        $columns = ['id', 'titulo'];

        $search = isset($_POST['search']['value']) ? $_POST['search']['value'] : ""; //$_POST['search']['value'];

        $limit = isset($_POST['length']) ? $_POST['length'] : 10;
        $offset = isset($_POST['start']) ? $_POST['start'] : 0;
        if(isset($_POST['order'])){
            $orderBy = [
                $_POST['order'][0]['column'],
                $_POST['order'][0]['dir']
            ];
        }else{
            $orderBy = [0, 'ASC'];
        }

        $dataTables = $this->model->getDataTableData($columns, $orderBy, $search, $limit, $offset);

        foreach ($dataTables['data'] as $k => &$row) {
            $row[] = "<span class='center'>
					     <button class='btn btn-primary' onclick='javascript:window.location = \"/painel/{$this->pagina}/alterar/{$row[0]}\"'>
                             <i class='icon-pencil'></i>
					     </button>
				     </span>";
            unset($k);
        }

        //header('Content-Type: application/json');
        echo json_encode($dataTables);
    }

    /**
     *
     */
    public function startConnection()
    {
        $this->entity = new Entity();
        $this->model = new Model(new DoctrineStorage($this->entity));
    }

    /**
     *
     */
    public function gravar()
    {
        try {
            $this->startConnection();

            $entity = $this->model->findOneBy(["id" => $_POST['id']]);

            $entity->setTitulo($_POST['titulo']);
            $entity->setDescricao($_POST['descricao']);
            $entity->setPalavraschave($_POST['palavraschave']);
            $slug = removeAcentos($_POST['titulo'], '-');
            $entity->setSlug($slug);

            $this->model->update($entity);

            $this->model->flush();

            //ESCREVE JSON
            $listaDePaginas = $this->model->findby([], ['titulo' => 'ASC']);

            $paginas = [];

            while ($listaDePaginas->valid()) {
                $paginas[create_slug($listaDePaginas->current()->getTitulo())] = [
                    "id" =>     $listaDePaginas->current()->getId(),
                    "titulo" => $listaDePaginas->current()->getTitulo(),
                    "descricao" => $listaDePaginas->current()->getDescricao(),
                    "palavraschave" => $listaDePaginas->current()->getPalavraschave(),
                    "slug" => $listaDePaginas->current()->getSlug()
                ];
                $listaDePaginas->next();
            }

            ResourceManager::writeFile("paginas.json", json_encode($paginas));

        } catch (\Exception $e) {
            throw $e;
        }
    }
}
