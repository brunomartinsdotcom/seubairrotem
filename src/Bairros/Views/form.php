<?php
include 'inc_head.php';

$action = "Adicionar";
if (isset($bairro)) {
    $action = "Editar";
    $dados = [
        'id' => $bairro->getId(),
        'id_cidade' => $bairro->getIdcidade(),
        'nome' => $bairro->getNome()
    ];
} else {
    $dados = [
        'id' => '',
        'id_cidade' => '',
        'nome' => ''
    ];
}
?>
</head>
<body>
    <div class="layout">
    <?php
        include_once 'inc_header.php';
        include_once 'inc_usuario.php';
        include_once 'inc_sidebar.php';
    ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">Bairros</h3>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="<?=$prefix;?>" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li>
                            <a href="<?=$prefix;?>bairros" title="Bairros">Bairros</a><span class="divider">
                                <i class="icon-angle-right"></i>
                            </span>
                        </li>
                        <li class="active"><?=$action;?></li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="content-widgets light-gray">
                    <div class="widget-head black">
                        <h3><?=$action;?> Bairro</h3>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function(){
                            // valida o formulário
                            $('#bairros').validate({
                                // define regras para os campos
                                rules: {
                                    id_cidade: {
                                        required: true
                                    },
                                    nome: {
                                        required: true
                                    }
                                },
                                // define messages para cada campo
                                messages: {
                                    id_cidade:"Informe a cidade",
                                    nome:"Informe o nome do bairro"
                                }
                            });

                            var interval = 0;
                            $('.chap').bind("keyup change", function(){
                                // começa a contar o tempo
                                clearInterval(interval);

                                // 500ms após o usuário parar de digitar a função é chamada
                                interval = window.setTimeout(function(){
                                    data_html = "nome=" + $('#nome').val() + "&id_cidade=" + $('#id_cidade').val()<?php if(isset($cidade)){ ?> + "&id=" + $('#id').val();<?php } ?>;
                                    $.ajax({
                                        type: 'POST',
                                        url: '<?=$prefix;?>bairros/verifica-existente',
                                        data: data_html,
                                        dataType: 'json',
                                        success: function (msg) {
                                            if (msg.error == 0) {
                                                $('#nome').css('border-color', 'green');
                                                $('#msg').html('');
                                                $("#Bt").removeAttr('disabled');
                                            } else {
                                                $('#nome').css('border-color', 'red');
                                                $('#msg').html('Bairro já cadastrado nessa cidade!');
                                                $('#Bt').attr('disabled', 'true');
                                            }
                                        }
                                    });
                                }, 500);
                            });
                        });
                    </script>
                    <div class="widget-container">
                        <form name="bairros" id="bairros" class="form-horizontal" action="<?=$_SERVER["REQUEST_URI"];?>" method="post">
                            <?php if(isset($bairro)){ ?>
                            <input name="id" id="id" type="hidden" value="<?=$dados['id'];?>">
                            <?php } ?>
                            <div class="control-group">
                                <label for="id_cidade" class="control-label">Cidade</label>
                                <div class="controls">
                                    <select name="id_cidade" id="id_cidade" class="chap">
                                        <option value="">Escolha...</option>
                                        <?php
                                            $cidades = \CoffeeCore\Helper\ResourceManager::readFile("cadastro_de_cidades.json");
                                            foreach($cidades as $k => $cidade):
                                        ?>
                                        <option value="<?=$cidade['id'];?>"<?php if(isset($bairro) and $cidade['id'] == $dados['id_cidade']){ ?> selected<?php } ?>><?=$cidade['nome'];?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="nome" class="control-label">Nome</label>
                                <div class="controls">
                                    <input name="nome" id="nome" type="text" maxlength="50" class="span7 chap" value="<?=$dados['nome'];?>">
                                    <em id="msg" class="error"></em>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" id="Bt" class="btn btn-primary">Salvar</button>
                                <button type="button" onclick="window.open('<?=$prefix;?>bairros', '_self');" class="btn right-float">Voltar</button>
                            </div>
                        </form>
                    </div>
                </div>
<?php
include 'inc_footer.php';
