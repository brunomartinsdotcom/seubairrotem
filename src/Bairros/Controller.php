<?php

namespace Bairros;

use CoffeeCore\Core\AbstractController;
use CoffeeCore\Helper\ResourceManager;
use CoffeeCore\Storage\DoctrineStorage;

/**
 * Class Controller
 * @package Bairros
 */
class Controller extends AbstractController
{
    /**
     * @var string
     */
    public $pagina = 'bairros';
    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Entity
     */
    protected $entity;

    /**
     * @return void
     */
    public function getDataTable()
    {
        $this->startConnection();
        $columns = ['id', 'nome', 'id_cidade'];
        $noSearch = ['id_cidade'];

        $search = isset($_POST['search']['value']) ? $_POST['search']['value'] : ""; //$_POST['search']['value'];

        $limit = isset($_POST['length']) ? $_POST['length'] : 10;
        $offset = isset($_POST['start']) ? $_POST['start'] : 0;
        if(isset($_POST['order'])){
            $orderBy = [
                $_POST['order'][0]['column'],
                $_POST['order'][0]['dir']
            ];
        }else{
            $orderBy = [0, 'ASC'];
        }

        $dataTables = $this->model->getDataTableData($columns, $orderBy, $search, $limit, $offset);

        foreach ($dataTables['data'] as $k => &$row) {
            $row[2] = $this->getNomeCidade($row[2]);
            $row[] = "<span class='center'>
                        <button class='btn btn-primary' onclick='javascript:window.location = \"/painel/{$this->pagina}/alterar/{$row[0]}\"'>
                            <span><i class='icon-pencil'></i></span>
                        </button>
                      </span>";
            $row[] = "<span class='center'>
                        <button data-url='/painel/{$this->pagina}/deletar/{$row[0]}' id='deletar' class='btn btn-danger'>
                            <span><i class='icon-trash'></i></span>
                        </button>
                     </span>";
            unset($k);
        }

        //header('Content-Type: application/json');
        echo json_encode($dataTables);
    }

    /**
     *
     */
    public function startConnection()
    {
        $this->entity = new Entity();
        $this->model = new Model(new DoctrineStorage($this->entity));
    }

    /**
     * @return string
     */
    public function getNomeCidade($idcidade)
    {
        $nomeCidade = DoctrineStorage::orm()->getRepository(\Cidades\Entity::FULL_NAME)->findOneBy(["id" => $idcidade])->getNome();
        return $nomeCidade;
    }

    /**
     *
     */
    public function gravar()
    {
        $this->startConnection();

        $this->entity->setIdcidade($_POST['id_cidade']);
        $this->entity->setNome($_POST['nome']);

        try {
            if (!isset($_POST['id']) or empty($_POST['id'])) {
                $this->model->insert($this->entity);
            } else {
                $this->entity->setId($_POST['id']);
                $this->model->update($this->entity);
            }
            $this->model->flush();
        } catch (\Exception $e) {
            return false;
        }

        //ESCREVE JSON
        $listaDeBairros = $this->model->findby([], ['nome' => 'ASC']);

        $bairros = [];

        while ($listaDeBairros->valid()) {
            $bairros[create_slug($listaDeBairros->current()->getNome())] = [
                "id" =>     $listaDeBairros->current()->getId(),
                "id_cidade" => $listaDeBairros->current()->getIdcidade(),
                "nome" => $listaDeBairros->current()->getNome()
            ];
            $listaDeBairros->next();
        }

        ResourceManager::writeFile("cadastro_de_bairros.json", json_encode($bairros));

    }

    /**
     * @param int|null $id
     */
    public function deletar($id = null)
    {
        $this->startConnection();
        if (!empty($id)) {
            $this->entity = $this->model->findOneBy(["id"=>$id]);
            $this->model->delete($this->entity);
            $this->model->flush();

            //ESCREVE JSON
            $listaDeBairros = $this->model->findby([], ['nome' => 'ASC']);

            $bairros = [];

            while ($listaDeBairros->valid()) {
                $bairros[create_slug($listaDeBairros->current()->getNome())] = [
                    "id" =>     $listaDeBairros->current()->getId(),
                    "id_cidade" => $listaDeBairros->current()->getIdcidade(),
                    "nome" => $listaDeBairros->current()->getNome()
                ];
                $listaDeBairros->next();
            }

            ResourceManager::writeFile("cadastro_de_bairros.json", json_encode($bairros));

        }
    }

    public function verificaexistente()
    {
        $id = isset($_POST['id']) ? $_POST['id'] : false;
        $idcidade = $_POST['id_cidade'];
        $nomebairro = $_POST['nome'];
        $this->startConnection();
        if (!empty($idcidade) and !empty($nomebairro)) {
            if(!empty($id)){
                $conn = \CoffeeCore\Storage\DoctrineStorage::orm()->getConnection();
                $this->entity = $conn->fetchAll("SELECT * FROM novo_cadastro_de_bairros WHERE id_cidade=".$idcidade." AND nome='".$nomebairro."' AND id!=".$id);
            }else{
                $this->entity = $this->model->findOneBy(["id_cidade" => $idcidade, "nome" => $nomebairro]);
            }

            if(!empty($this->entity)){
                $array = ['error' => 1];
            }else{
                $array = ['error' => 0];
            }
            header('Content-Type: application/json');
            echo json_encode($array);
        }
    }
}
