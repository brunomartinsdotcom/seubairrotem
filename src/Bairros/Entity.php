<?php

namespace Bairros;

/**
 * Entity
 *
 * @Table(name="novo_cadastro_de_bairros")
 * @Entity
 */
class Entity implements \CoffeeCore\Core\Entity
{
    const FULL_NAME = "Bairros\\Entity";

    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @Column(name="id_cidade", type="integer", nullable=false)
     */
    private $id_cidade;

    /**
     * @var string
     *
     * @Column(name="nome", type="string", length=50, nullable=false)
     */
    private $nome;

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $iduf
     */
    public function setIdcidade($id_cidade)
    {
        $this->id_cidade = $id_cidade;
    }

    /**
     * @return int
     */
    public function getIdcidade()
    {
        return $this->id_cidade;
    }

    /**
     * @param string $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }
}
