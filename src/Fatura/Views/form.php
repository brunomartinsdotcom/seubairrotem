<?php
include 'inc_head.php';
$jsonReader = new \CoffeeCore\Helper\JsonReader('paginas');

/** @var Fatura\Entity $fatura */

?>
    <!-- styles -->
    <link href="<?= url_base() . $prefix;?>css/bootstrap.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/jquery.gritter.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=$prefix;?>css/font-awesome.css">

    <!--[if IE 7]>
    <link rel="stylesheet" href="<?=$prefix;?>css/font-awesome-ie7.min.css">
    <![endif]-->
    <link href="<?=$prefix;?>css/tablecloth.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/styles.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie7.css" />
    <![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie8.css" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie9.css" />
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>

    <!--fav and touch icons -->
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="/favicon.png">

    <!--============ javascript ===========-->
    <script src="<?=$prefix;?>js/jquery.js"></script>
    <script src="<?=$prefix;?>js/bootstrap.js"></script>
    <script src="<?=$prefix;?>js/jquery.metadata.js"></script>
    <script src="<?=$prefix;?>js/excanvas.js"></script>
    <script src="<?=$prefix;?>js/jquery.collapsible.js"></script>
    <script src="<?=$prefix;?>js/accordion.nav.js"></script>
    <script src="<?=$prefix;?>js/custom.js"></script>
    <script src="<?=$prefix;?>js/respond.min.js"></script>
    <script src="<?=$prefix;?>js/ios-orientationchange-fix.js"></script>

    <script src="<?=$prefix;?>js/jquery.validate.js"></script>
    <link href="<?=$prefix;?>css/chosen.css" rel="stylesheet">
    <script src="<?=$prefix;?>js/chosen.jquery.js"></script>
    <script src="<?=$prefix;?>js/bootstrap-fileupload.js"></script>

</head>
<body>
    <div class="layout">
    <?php
        include_once 'inc_header.php';
        include_once 'inc_usuario.php';
        include_once 'inc_sidebar.php';
    ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">Faturas</h3>
                        <span class="pull-right top-right-toolbar">
                        </span>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="<?=$prefix;?>" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li>
                            <a href="<?=$prefix;?>faturas">Faturas</a><span class="divider">
                                <i class="icon-angle-right"></i>
                            </span>
                        </li>
                        <li class="active">Vizualização</li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="content-widgets light-gray">
                    <div class="widget-head black">
                        <h3>Dados da Fatura</h3>
                    </div>
                    <div class="widget-container">
                        <div class="tab-content">
                            <div class="tab-pane active" id="user">
                                <div class=" information-container">

                                    <form name="form1" id="form1" class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                                        <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Data da Compra:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <p></p><?= $fatura->getDatacompra()->format("d/m/Y H:i:s"); ?></p>
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                      <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Descrição:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <p></p><?= $fatura->getDescricao(); ?></p>
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Usuário:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <p></p><?= $fatura->getNomeUsuario($fatura->getUsuario()); ?></p>
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                       <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Valor Total:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <p></p><?= "R$ ". number_format($fatura->getValortotal(), 2, ',', '.'); ?></p>
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                         <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Status do Pag Seguro:</b></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <p></p><?= $fatura->getStatuspagseguro(); ?></p>
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                         <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Código CheckOut:</b></br><i>(PagSeguro)</i></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <p></p><?= $fatura->getCodigocheckout();?></p>
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->
                                        <!-- .control-group -->
                                        <div class="control-group">
                                            <label class="control-label"><b>Código da Transação:</b></br><i>(PagSeguro)</i></label>
                                            <!-- .controls -->
                                            <div class="controls">
                                                <p></p><?= $fatura->getCodigotransacao(); ?></p>
                                            </div><!-- /.controls -->
                                        </div><!-- /.control-group -->

                                </form>
                                    <ul class="profile-intro">
                                              <a href="/painel/faturas" class="btn btn-default right-float">Voltar</a>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

<?php
include 'inc_footer.php';
