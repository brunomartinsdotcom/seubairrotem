<?php

namespace Fatura;

use Fatura\Entity;
use CoffeeCore\Core\AbstractController;
use CoffeeCore\Storage\DoctrineStorage;

/**
 * Class Controller
 * @package Fatura
 */
class Controller extends AbstractController
{
    /**
     * @var string
     */
    public $pagina = 'faturas';
    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Entity
     */
    protected $entity;

    /**
     * @return void
     */
    public function getDataTable()
    {
        $this->startConnection();
        $columns = ['id', 'datacompra', 'descricao', 'usuario', 'valortotal', 'statuspagseguro'];

        $search = $_POST['search']['value'];

        $limit = $_POST['length'];
        $offset = $_POST['start'];
        $orderBy = [
            $_POST['order'][0]['column'],
            $_POST['order'][0]['dir']
        ];

        $dataTables = $this->model->getDataTableData($columns, $orderBy, $search, $limit, $offset);

        foreach ($dataTables['data'] as $k => &$row) {
            $row[1] = $row[1]->format("d/m/Y H:i:s");
			$row[3] = $this->getNomeUsuario($row[3]);
			$row[4] = "R$ ".number_format($row[4], 2, ',', '.');
			$row[5] = $this->getStatuspagseguro($row[5]);
            $row[] = "<span class='center'>
                        <button class='btn btn-info' onclick='javascript:window.location=\"/painel/{$this->pagina}/vizualizar/{$row[0]}\"'><i class='icon-search'></i></button>
                     </span>";
        }

        header('Content-Type: application/json');
        echo json_encode($dataTables);
    }

    /**
     *
     */
    public function startConnection()
    {
        $this->entity = new Entity();
        $this->model = new Model(new DoctrineStorage($this->entity));
    }
	
	/**
	 * @return string
	 */
	public function getNomeUsuario($id)
	{
		$tipoUsuario = DoctrineStorage::orm()
			->getRepository(\Usuario\Entity::FULL_NAME)
			->findOneBy(["id" => $id])
			->getTipopessoa();
		$nomeUsuario = '';
		if($tipoUsuario == 1) {
			$nomeUsuario = DoctrineStorage::orm()
				->getRepository(\Usuario\Entity::FULL_NAME)
				->findOneBy(["id" => $id])
				->getNome();
		}else if($tipoUsuario == 2){
			$nomeUsuario = DoctrineStorage::orm()
				->getRepository(\Usuario\Entity::FULL_NAME)
				->findOneBy(["id" => $id])
				->getNomefantasia();
		}
		return $nomeUsuario;
	}
	
	/**
     * @return int
     */
    public function getStatuspagseguro($id)
    {
        switch($id) {
            case "":
			    $status = "Fatura não gerada no Pagseguro.";
				break;
			case 1:
                $status = "Aguardando pagamento";
                break;

            case 2:
                $status = "Em análise";
                break;

            case 3:
                $status = "Paga";
                break;

            case 4:
                $status = "Disponível";
                break;

            case 5:
                $status = "Em disputa";
                break;

            case 6:
                $status = "Devolvida";
                break;

            case 7:
                $status = "Cancelada";
                break;
        }

        return $status;
    }

    /**
     * @param int|Entity|null $entity
     */
    public function deletar($entity = null)
    {
        $this->startConnection();

        $this->entity = $this->model->findOneBy(["id" => $entity]);
        $this->model->delete($this->entity);
        $this->model->flush();
    }
}
