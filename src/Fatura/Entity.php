<?php

namespace Fatura;

use CoffeeCore\Storage\DoctrineStorage;

/**
 * Class Entity
 * @package Anuncio
 *
 * @Table(name="fatura")
 * @Entity
 */
class Entity implements \CoffeeCore\Core\Entity
{
    /**
     *
     */
    const FULL_NAME = "Fatura\\Entity";

    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @Column(name="statuspagseguro", type="integer")
     */
    public $statuspagseguro = null;

    /**
     * @var integer
     *
     * @Column(name="usuario", type="integer", nullable=false)
     */
    private $usuario;

    /**
     * @var integer
     *
     * @Column(name="anuncio", type="integer", nullable=false)
     */
    private $anuncio;

    /**
     * @var string
     *
     * @Column(name="descricao", type="text")
     */
    private $descricao;

    /**
     * @var string
     *
     * @Column(name="codigocheckout", type="string", length=255)
     */
    private $codigocheckout = null;

    /**
     * @var string
     *
     * @Column(name="codigotransacao", type="string", length=255)
     */
    private $codigotransacao = null;

    /**
     * @var datetime
     *
     * @Column(name="datacompra", type="datetime")
     */
    private $datacompra;

    /**
     * @var datetime
     *
     * @Column(name="dataautorizacao", type="datetime")
     */
    private $dataautorizacao;

    /**
     * @var float
     *
     * @Column(name="valortotal", type="float", precision=10, scale=2, nullable=false)
     */
    private $valortotal = 0.0;

    public function __construct() {
        $this->datacompra = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getAnuncio()
    {
        return $this->anuncio;
    }

    /**
     * @param int $anuncio
     */
    public function setAnuncio($anuncio)
    {
        $this->anuncio = $anuncio;
    }

    /**
     * @return string
     */
    public function getCodigocheckout()
    {
        return $this->codigocheckout;
    }

    /**
     * @param string $codigocheckout
     */
    public function setCodigocheckout($codigocheckout)
    {
        $this->codigocheckout = $codigocheckout;
    }

    /**
     * @return string
     */
    public function getCodigotransacao()
    {
        return $this->codigotransacao;
    }

    /**
     * @param string $codigotransacao
     */
    public function setCodigotransacao($codigotransacao)
    {
        $this->codigotransacao = $codigotransacao;
    }

    /**
     * @return datetime
     */
    public function getDataautorizacao()
    {
        return $this->dataautorizacao;
    }

    /**
     * @param datetime $dataautorizacao
     */
    public function setDataautorizacao($dataautorizacao)
    {
        $this->dataautorizacao = $dataautorizacao;
    }

    /**
     * @return datetime
     */
    public function getDatacompra()
    {
        return $this->datacompra;
    }

    /**
     * @param datetime $datacompra
     */
    public function setDatacompra($datacompra)
    {
        $this->datacompra = $datacompra;
    }

    /**
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param string $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @return int
     */
    public function getStatuspagseguro()
    {
        $status = "Fatura não gerada no Pagseguro.";

        switch($this->statuspagseguro) {
            case 1:
                $status = "Aguardando pagamento";
                break;

            case 2:
                $status = "Em análise";
                break;

            case 3:
                $status = "Paga";
                break;

            case 4:
                $status = "Disponível";
                break;

            case 5:
                $status = "Em disputa";
                break;

            case 6:
                $status = "Devolvida";
                break;

            case 7:
                $status = "Cancelada";
                break;
        }

        return $status;
    }

    /**
     * @param string $statuspagseguro
     */
    public function setStatuspagseguro($statuspagseguro)
    {
        $this->statuspagseguro = $statuspagseguro;
    }

    /**
     * @return int
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param int $usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

    public function getNomeUsuario($id)
    {
        if ($id) {
            $f= [];
            $formas =  DoctrineStorage::orm()->getConnection()->fetchAll("SELECT * FROM usuario WHERE id={$id}");
            foreach ($formas as $forma) {
                if($forma['tipopessoa'] == 1){
                    $nome = $forma['nome'];
                }else{
                    $nome = $forma['nomefantasia'];
                }
            }
            return $nome;
        }
    }

    /**
     * @return float
     */
    public function getValortotal()
    {
        return $this->valortotal;
    }

    /**
     * @param float $valortotal
     */
    public function setValortotal($valortotal)
    {
        $this->valortotal = $valortotal;
    }
}
