<?php

namespace Fatura;

use CoffeeCore\Storage\DoctrineStorage;
use Slim\Slim;

/**
 * Class Routes
 * @package Fatura
 */
class Routes
{
    /**
     * @param Slim $app
     */
    public static function getRoutes(Slim $app)
    {
        $controller = new Controller();

        $app->group("/{$controller->pagina}", function () use ($app, $controller) {

            $app->map(
                '/',
                function () use ($app, $controller) {
                    if ($app->request()->isGet()) {
                        $app->render(__NAMESPACE__ . "/Views/grid.php");
                    }
                    if ($app->request()->isPost()) {
                        $controller->getDataTable();
                    }
                }
            )->via("GET", "POST");

            $app->map(
                '/vizualizar/:id',
                function ($id) use ($app, $controller) {
                    if ($app->request()->isGet()) {
                        $registro = DoctrineStorage::orm()
                            ->getRepository(__NAMESPACE__."\\Entity")
                            ->findOneBy(["id" => $id]);
                        $app->render(__NAMESPACE__."/Views/form.php", ["fatura" => $registro]);
                    }
                    if ($app->request()->isPost()) {
                        try {
                            $controller->gravar();
                            $app->flash("info", "Alterado com sucesso!");
                        } catch (\Exception $e) {
                            $app->flash("info", "Erro ao alterar!");
                        }
                        $app->redirect("/painel/{$controller->pagina}/");
                    }
                }
            )->via("GET", "POST");

            $app->get(
                '/verificacao/',
                function () use ($app, $controller) {

                    $conn = \CoffeeCore\Storage\DoctrineStorage::orm()->getConnection();
                    $sqlFatura = "SELECT * FROM fatura WHERE statuspagseguro != 3 AND statuspagseguro != 4 AND statuspagseguro != 0";
                    $resFaturas = $conn->fetchAll($sqlFatura);
                    //print_r($resFaturas); die;

                    \PagSeguroLibrary::init();
                    \PagSeguroConfig::init();
                    \PagSeguroResources::init();

                    /** @var \PagSeguroAccountCredentials $credenciais */
                    $credenciais = \PagSeguroConfig::getAccountCredentials();

                    foreach($resFaturas as $resFatura) {

                        $url = "https://ws.pagseguro.uol.com.br/v2/transactions/{$resFatura['codigotransacao']}?email={$credenciais->getEmail()}&token={$credenciais->getToken()}";

                        $curl = curl_init($url);
                        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        $transactionCurl = curl_exec($curl);
                        curl_close($curl);
                        $transaction = simplexml_load_string($transactionCurl);

                        /** @var \Fatura\Entity $fatura */
                        $fatura = DoctrineStorage::orm()->getRepository(\Fatura\Entity::FULL_NAME)->findOneBy(['id' => $transaction->reference]);
                        $fatura->setStatuspagseguro($transaction->status);
                        $fatura->setDataautorizacao(new \DateTime($transaction->lastEventDate));
                        DoctrineStorage::orm()->merge($fatura);
                        DoctrineStorage::orm()->flush();

                        if ($transaction->status == 3 or $transaction->status == 4) {
                            \Site\Controller::liberarPlano($transaction->reference);
                            \Site\Controller::liberarDestaque($transaction->reference);
                        }
                    }

                    $app->redirect("/painel/{$controller->pagina}/");

                }
            );

            $app->get(
                '/deletar/:id/',
                function ($id) use ($app, $controller) {
                    try {
                        $controller->deletar($id);
                        $app->flash("info", "Excluido com sucesso!");
                    } catch (\Exception $e) {
                        $app->flash("info", "<span>Erro ao excluir!</span>");

                    }
                    $app->redirect("/painel/{$controller->pagina}/");
                }
            );
        });
    }
}
