<?php

namespace Departamentos;


use CoffeeCore\Storage\DoctrineStorage;
use Slim\Slim;

/**
 * Class Routes
 * @package Departamentos
 */
class Routes
{
    /**
     * @param Slim $app
     */
    public static function getRoutes(Slim $app)
    {
        $controller = new Controller();

        $app->group("/{$controller->pagina}", function () use ($app, $controller) {

            $app->map(
                '/',
                function () use ($app, $controller) {
                    if ($app->request()->isGet()) {
                        $app->render(__NAMESPACE__ . "/Views/grid.php");
                    }
                    if ($app->request()->isPost()) {
                        $controller->getDataTable();
                    }
                }
            )->via("GET", "POST");

            $app->map(
                '/novo/',
                function () use ($app, $controller) {
                    if ($app->request()->isGet()) {
                        $app->render(__NAMESPACE__ . "/Views/form.php");
                    }
                    if ($app->request()->isPost()) {
                        try {
                            $controller->gravar();
                            $app->flash("info", "Inserido com sucesso!");
                        } catch (\Exception $e) {
                            $app->flash("info", "Erro ao inserir! <br> " . addslashes($e->getMessage()));

                        }
                        $app->redirect("/painel/{$controller->pagina}/");
                    }
                }
            )->via("GET", "POST");

            $app->map(
                '/alterar/:id',
                function ($id) use ($app, $controller) {
                    if ($app->request()->isGet()) {
                        $registro = DoctrineStorage::orm()
                            ->getRepository(__NAMESPACE__."\\Entity")
                            ->findOneBy(["id" => $id]);
                        $app->render(__NAMESPACE__."/Views/form.php", ["departamento" => $registro]);
                    }
                    if ($app->request()->isPost()) {
                        try {
                            $controller->gravar();
                            $app->flash("info", "Alterado com sucesso!");
                        } catch (\Exception $e) {
                            $app->flash("info", "Erro ao alterar!");

                        }
                        $app->redirect("/painel/{$controller->pagina}/");
                    }
                }
            )->via("GET", "POST");

            $app->map(
                '/ordem/',
                function () use ($app, $controller) {

                    if ($app->request()->isGet()) {
                        $repository = DoctrineStorage::orm()->getRepository(__NAMESPACE__."\\Entity");
                        $app->render(__NAMESPACE__."/Views/order.php", ["repository" => $repository]);
                    }
                    if ($app->request()->isPost()) {
                        $controller->reordenar();
                    }
                }
            )->via("GET", "POST");

            $app->get(
                '/deletar/:id/',
                function ($id) use ($app, $controller) {
                    try {
                        $controller->deletar($id);
                        $app->flash("info", "Excluido com sucesso!");
                    } catch (\Exception $e) {
                        $app->flash("info", "Erro ao excluir!");
                    }
                    $app->redirect("/painel/{$controller->pagina}/");
                }
            );
        });
    }
}