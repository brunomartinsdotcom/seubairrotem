<?php

namespace Departamentos;

use CoffeeCore\Core\AbstractController;
use CoffeeCore\Helper\ResourceManager;
use CoffeeCore\Storage\DoctrineStorage;

/**
 * Class Controller
 * @package Departamentos
 */
class Controller extends AbstractController
{
    /**
     * @var string
     */
    public $pagina = 'departamentos';
    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Entity
     */
    protected $entity;

    /**
     * @return void
     */
    public function getDataTable()
    {
        $this->startConnection();
        $columns = ['sortorder', 'id', 'titulo', 'email'];

        $search = isset($_POST['search']['value']) ? $_POST['search']['value'] : ""; //$_POST['search']['value'];

        $limit = isset($_POST['length']) ? $_POST['length'] : 10;
        $offset = isset($_POST['start']) ? $_POST['start'] : 0;
        if(isset($_POST['order'])){
            $orderBy = [
                $_POST['order'][0]['column'],
                $_POST['order'][0]['dir']
            ];
        }else{
            $orderBy = [0, 'ASC'];
        }

        $dataTables = $this->model->getDataTableData($columns, $orderBy, $search, $limit, $offset);

        foreach ($dataTables['data'] as $k => &$row) {
            $row[] = "<span class='center'>
                        <button class='btn btn-primary' onclick='javascript:window.location = \"/painel/{$this->pagina}/alterar/{$row[1]}\"'>
                            <span><i class='icon-pencil'></i></span>
                        </button>
                      </span>";
            $row[] .= "<span class='center'>
                        <button data-url='/painel/{$this->pagina}/deletar/{$row[1]}' id='deletar' class='btn btn-danger'>
                            <span><i class='icon-trash'></i></span>
                        </button>
                      </span>";
        }

        //header('Content-Type: application/json');
        echo json_encode($dataTables);
    }

    /**
     *
     */
    public function startConnection()
    {
        $this->entity = new Entity();
        $this->model = new Model(new DoctrineStorage($this->entity));
    }

    /**
     *
     */
    public function gravar()
    {
        $this->startConnection();

        if (!empty($_POST['id'])) {
            $this->entity = $this->model->findOneBy(['id' => $_POST['id']]);
        }
        $this->entity->setTitulo($_POST['titulo']);
        $this->entity->setEmail($_POST['email']);

        if ( empty($this->entity->getId())) {
            $this->entity->setSortorder($this->model->findBy([])->count() + 1);
            $this->model->insert($this->entity);
        } else {
            $this->model->update($this->entity);
        }
        $this->model->flush();

        //ESCREVE JSON
        $this->rewriteJson();
    }

    /**
     * @param int|null $id
     */
    public function deletar($id = null)
    {
        $this->startConnection();
        if (!empty($id)) {
            $this->entity = $this->model->findOneBy(["id"=>$id]);
            $this->model->delete($this->entity);
            $this->model->flush();

            //ESCREVE JSON
            $this->rewriteJson();
        }
    }

    public function reordenar()
    {
        $this->startConnection();

        /* on form submission */
        if (isset($_POST['do_submit'])) {
            /* split the value of the sortation */
            $ids = explode(',', $_POST['sortorder']);
            /* run the update query for each id */
            foreach ($ids as $index => $id) {
                $id = (int)$id;
                if ($id != '') {

                    $this->entity = $this->model->findOneBy(["id"=>$id]);
                    $this->entity->setSortorder($index + 1);

                    //DoctrineStorage::orm()->merge($this->entity);

                    $this->model->update($this->entity);

                    //$query = 'UPDATE ' . $_POST['bd'] . ' SET sortorder=' . ($index + 1) . ' WHERE id = ' . $id;
                    //$result = mysql_query($query) or die(mysql_error() . ': ' . $query);
                }
            }
            $this->model->flush();

            //ESCREVE JSON
            $this->rewriteJson();

            /* now what? */
            if ($_POST['byajax']) {
                die();
            } else {
                $message = $this->entity; //'Ordem Salva';
            }
        }
    }

    private function rewriteJson() {
        $lista = $this->model->findBy([], ["sortorder" => "ASC"]);
        $duvidas = [];
        while ($lista->valid()) {
            $duvidas[] = [
                "titulo" => $lista->current()->getTitulo(),
                "email" => $lista->current()->getEmail()
            ];
            $lista->next();
        }

        ResourceManager::writeFile("departamentos.json", json_encode($duvidas));
    }
}
