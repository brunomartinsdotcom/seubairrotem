<?php
include 'inc_head.php';

$action = "Adicionar";
if (isset($departamento)) {
    $action = "Editar";
    $dados = [
        'id'        => $departamento->getId(),
        'titulo'    => $departamento->getTitulo(),
        'email'     => $departamento->getEmail(),
        'sortorder' => $departamento->getSortorder()
    ];
} else {
    $dados = [
        'id'        => '',
        'titulo'    => '',
        'email'     => '',
        'sortorder' => ''
    ];
}
?>
</head>
<body>
    <div class="layout">
    <?php
        include_once 'inc_header.php';
        include_once 'inc_usuario.php';
        include_once 'inc_sidebar.php';
    ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">Departamentos</h3>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="<?=$prefix;?>" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li>
                            <a href="<?=$prefix;?>departamentos" title="Departamentos">Departamentos</a><span class="divider">
                                <i class="icon-angle-right"></i>
                            </span>
                        </li>
                        <li class="active"><?=$action;?></li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="content-widgets light-gray">
                    <div class="widget-head black">
                        <h3><?=$action;?> Departamento</h3>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function(){
                            // valida o formulário
                            $('#departamento').validate({
                                // define regras para os campos
                                rules: {
                                    titulo: {
                                        required: true
                                    },
                                    email: {
                                        required: true,
                                        email:true
                                    }
                                },
                                // define messages para cada campo
                                messages: {
                                    titulo:"Informe o título do departamento",
                                    email:"Informe o e-mail do departamento"
                                }
                            });
                        });
                    </script>
                    <div class="widget-container">
                        <form name="departamento" id="departamento" class="form-horizontal" action="<?=$_SERVER["REQUEST_URI"];?>" method="post">
                            <?php if(isset($departamento)) { ?>
                                <input name="id" id="id" type="hidden" value="<?=$dados['id'];?>">
                            <?php } ?>
                            <input name="sortorder" id="sortorder" type="hidden" value="<?=$dados['sortorder'];?>">
                            <div class="control-group">
                                <label for="pergunta" class="control-label">Título</label>
                                <div class="controls">
                                    <input name="titulo" id="titulo" type="text" maxlength="50" class="span8" value="<?=$dados['titulo'];?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="email" class="control-label">E-mail</label>
                                <div class="controls">
                                    <input name="email" id="email" type="text" maxlength="50" class="span8" value="<?=$dados['email'];?>">
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" id="Bt" class="btn btn-primary">Salvar</button>
                                <button type="button" onclick="window.open('<?=$prefix;?>departamentos', '_self');" class="btn right-float">Voltar</button>
                            </div>
                        </form>
                    </div>
                </div>
    <?php
include 'inc_footer.php';
