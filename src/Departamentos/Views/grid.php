<?php include 'inc_head.php';?>
</head>
<body>
<div class="layout">
    <?php
        include 'inc_header.php';
        include 'inc_usuario.php';
        include 'inc_sidebar.php';
    ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">Departamentos</h3>
                        <span class="pull-right top-right-toolbar">
                            <a href="<?=$prefix;?>departamentos/novo" class="btn btn-mini btn-success" title="Adicionar"><i class="icon-plus "></i> Adicionar</a>
                            <a href="<?=$prefix;?>departamentos/ordem" class="btn btn-mini btn-warning" style="margin-left:30px;" title="Ordenar"><i class="icon-list "></i> Ordenar</a>
                        </span>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="<?=$prefix;?>" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li class="active">Departamentos</li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="widget-container">
                    <div id="data-table_wrapper" class="dataTables_wrapper  form-inline" role="grid">
                        <table id="data-table" class="table table-bordered tabela">
                            <thead>
                            <tr>
                                <th style="width: 35px">Or</th>
                                <th style="width: 35px">ID</th>
                                <th>Título</th>
                                <th>E-mail</th>
                                <th style="width: 16px" class="center">Ed</th>
                                <th style="width: 16px" class="center">Ex</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <script type="text/javascript">
                    $(document).ready(function(){
                        $("#data-table").DataTable({
                            "processing": true,
                            "serverSide": true,
                            "ajax": {
                                "url": "<?=$prefix;?>departamentos",
                                "type": "POST"
                            },
                            "aoColumnDefs": [
                                { "bSortable": false, "aTargets": [4] },
                                { "bSortable": false, "aTargets": [5] }
                            ],
                            language : {
                                url: "<?=$prefix;?>js/plugins/dataTables/Portuguese-Brasil.lang"
                            }
                        })
                    })
                </script>
    <?php
include 'inc_footer.php';
