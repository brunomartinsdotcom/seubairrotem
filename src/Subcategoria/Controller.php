<?php

namespace Subcategoria;

use CoffeeCore\Core\AbstractController;
use CoffeeCore\Storage\DoctrineStorage;

/**
 * Class Controller
 * @package Subcategoria
 */
class Controller extends AbstractController
{
    /**
     * @var string
     */
    public $pagina = 'subcategorias';
    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Entity
     */
    protected $entity;

    /**
     * @return void
     */
    public function getDataTable()
    {
        $this->startConnection();
        $columns = ['id', 'titulo', 'categoria'];

        $search = $_POST['search']['value'];

        $limit = $_POST['length'];
        $offset = $_POST['start'];
        $orderBy = [
            $_POST['order'][0]['column'],
            $_POST['order'][0]['dir']
        ];

        $dataTables = $this->model->getDataTableData($columns, $orderBy, $search, $limit, $offset, ["categoria"]);



        foreach ($dataTables['data'] as $k => &$row) {
            if (!empty($row[2]) and !is_array($row[2])) {
                $categoria = DoctrineStorage::orm()
                    ->getRepository(\Categoria\Entity::FULL_NAME)->findOneBy(["id" => $row[2]]);
                $row[2] = $categoria->getTitulo();
            }
            $row[] = "<span class='center'>
                        <button class='btn' onclick='javascript:window.location = \"/painel/subcategorias/alterar/{$row[0]}\"'>
                            <span><i class='icon-pencil'></i></span>
                        </button>
                        <button data-url='/painel/subcategorias/deletar/{$row[0]}' id='deletar' class='btn btn-primary'>
                        <span><i class='icon-trash'></i></span>
                        </button>
                     </span>";
        }

        header('Content-Type: application/json');
        echo json_encode($dataTables);
    }

    /**
     *
     */
    public function startConnection()
    {
        $this->entity = new Entity();
        $this->model = new Model(new DoctrineStorage($this->entity));
    }

    /**
     *
     */
    public function gravar()
    {
        $this->startConnection();

        $this->entity->setCategoria($_POST['categoria']);
        $this->entity->setTitulo($_POST['titulo']);
		$this->entity->setSlug($_POST['titulo']);
        try {
            if (!isset($_POST['id']) or empty($_POST['id'])) {
                $this->model->insert($this->entity);
            } else {
                $this->entity->setId($_POST['id']);
                $this->model->update($this->entity);
            }
            $this->model->flush();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param int|null $id
     */
    public function deletar($id = null)
    {
        $this->startConnection();
        if (!empty($id)) {
            $this->entity = $this->model->findOneBy(["id"=>$id]);
            $this->model->delete($this->entity);
            $this->model->flush();
        }
    }
}
