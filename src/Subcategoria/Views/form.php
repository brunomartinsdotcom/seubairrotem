<?php
include 'inc_head.php';
$action = "Adicionar";
if (isset($subcategoria)) {
    $action = "Editar";
    $dados = [
        'id'        => $subcategoria->getId(),
        'titulo'    => $subcategoria->getTitulo(),
        'categoria' => $subcategoria->getCategoria()
    ];
} else {
    $dados = [
        'id'        => '',
        'titulo'    => '',
        'categoria' => ''
    ];
}
?>
    <!-- styles -->
    <link rel="stylesheet" href="<?=$prefix;?>css/bootstrap.css">
    <link rel="stylesheet" href="<?=$prefix;?>css/jquery.gritter.css">
    <link rel="stylesheet" href="<?=$prefix;?>css/bootstrap-responsive.css">
    <link rel="stylesheet" href="<?=$prefix;?>css/font-awesome.css">

    <!--[if IE 7]>
    <link rel="stylesheet" href="<?=$prefix;?>css/font-awesome-ie7.min.css">
    <![endif]-->
    <link href="<?=$prefix;?>css/tablecloth.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/styles.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie7.css" />
    <![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie8.css" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie9.css" />
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>

    <!--fav and touch icons -->
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="/favicon.png">

    <!--============ javascript ===========-->
    <script src="<?=$prefix;?>js/jquery.js"></script>
    <script src="<?=$prefix;?>js/bootstrap.js"></script>
    <script src="<?=$prefix;?>js/jquery.sparkline.js"></script>
    <script src="<?=$prefix;?>js/bootstrap-fileupload.js"></script>
    <script src="<?=$prefix;?>js/jquery.metadata.js"></script>
    <script src="<?=$prefix;?>js/jquery.tablesorter.min.js"></script>
    <script src="<?=$prefix;?>js/jquery.tablecloth.js"></script>
    <script src="<?=$prefix;?>js/excanvas.js"></script>
    <script src="<?=$prefix;?>js/jquery.collapsible.js"></script>
    <script src="<?=$prefix;?>js/accordion.nav.js"></script>
    <script src="<?=$prefix;?>js/custom.js"></script>
    <script src="<?=$prefix;?>js/respond.min.js"></script>
    <script src="<?=$prefix;?>js/ios-orientationchange-fix.js"></script>

    <script src="<?=$prefix;?>js/jquery.dataTables.js"></script>
    <script src="<?=$prefix;?>js/ZeroClipboard.js"></script>
    <script src="<?=$prefix;?>js/dataTables.bootstrap.js"></script>
    <script src="<?=$prefix;?>js/TableTools.js"></script>
    <script src="<?=$prefix;?>js/plugins/dataTables/date-uk.js"></script>

    <link href="<?=$prefix;?>css/tablecloth.css" rel="stylesheet">

    <script src="<?=$prefix;?>js/jquery.validate.js"></script>

</head>
<body>
    <div class="layout">
    <?php
        include_once 'inc_header.php';
        include_once 'inc_usuario.php';
        include_once 'inc_sidebar.php';
    ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">Subcategoria</h3>
                        <span class="pull-right top-right-toolbar">
                            <a href="/painel/subcategorias/novo" class="btn btn-mini btn-success" title="Novo"><i class="icon-plus "></i> Adicionar Novo</a>
                        </span>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="<?=$prefix;?>" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li>
                            <a href="<?=$prefix;?>subcategorias">Subcategoria</a><span class="divider">
                                <i class="icon-angle-right"></i>
                            </span>
                        </li>
                        <li class="active">Formulário</li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="content-widgets light-gray">
                    <div class="widget-head black">
                        <h3><?=$action;?> Subcategoria</h3>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function(){
                            // valida o formulário
                            $('#formaspagamento').validate({
                                // define regras para os campos
                                rules: {
                                    titulo: {
                                        required: true,
                                        minlength: 4
                                    },
                                    tipo: {
                                        required: true
                                    }
                                },
                                // define messages para cada campo
                                messages: {
                                    titulo: "Informe o título para a subcategoria",
                                    tipo: "Selecione um tipo"
                                }
                            });
                        });
                    </script>
                    <div class="widget-container">
                        <form name="subcategoria" id="subcategoria" class="form-horizontal" action="<?=$_SERVER["REQUEST_URI"];?>" method="post">
                            <div class="control-group">
                                <label for="nome" class="control-label">Título</label>
                                <div class="controls">

                                    <input name="id" id="id" type="hidden" value="<?=$dados['id'];?>">
                                    <input name="titulo" id="titulo" type="text" maxlength="50" class="span7" value="<?=$dados['titulo'];?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="target" class="control-label">Categoria</label>
                                <div class="controls">
                                    <select name="categoria">
                                        <option value="">Selecione</option>
                                        <?php
                                        $categorias = \CoffeeCore\Storage\DoctrineStorage::orm()->getRepository(\Categoria\Entity::FULL_NAME)
                                            ->findBy([]);
                                        foreach ($categorias as $categoria) {
                                        ?>
                                            <option value="<?=$categoria->getId();?>"
                                                <?php if ($categoria->getId() == $dados['categoria'] and !empty($dados['titulo'])) {
                                                    echo " selected ";}
                                                ?> >
                                                <?=$categoria->getTitulo();?>
                                            </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" id="Bt" class="btn btn-success">Salvar</button>
                                <button type="button" onclick="window.open('<?=$prefix;?>subcategorias', '_self');" class="btn right-float">Voltar</button>
                            </div>
                        </form>
                    </div>
                </div>

<?php
include 'inc_footer.php';
