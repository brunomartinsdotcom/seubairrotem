<?php
include 'inc_head.php';
?>
    <!-- styles -->
    <link href="<?=$prefix;?>css/bootstrap.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/custom.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/jquery.gritter.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=$prefix;?>css/font-awesome.css">

    <!--[if IE 7]>
    <link rel="stylesheet" href="<?=$prefix;?>css/font-awesome-ie7.min.css">
    <![endif]-->
    <link href="<?=$prefix;?>css/tablecloth.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/styles.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie7.css" />
    <![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie8.css" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie9.css" />
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>

    <!--fav and touch icons -->
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="/favicon.png">

    <!--============ javascript ===========-->
    <script src="<?=$prefix;?>js/jquery.js"></script>
    <script src="<?=$prefix;?>js/bootbox.js"></script>
    <script src="<?=$prefix;?>js/bootstrap.js"></script>
    <script src="<?=$prefix;?>js/bootstrap-fileupload.js"></script>
    <script src="<?=$prefix;?>js/jquery.metadata.js"></script>
    <script src="<?=$prefix;?>js/jquery.tablesorter.min.js"></script>
    <script src="<?=$prefix;?>js/jquery.tablecloth.js"></script>
    <script src="<?=$prefix;?>js/excanvas.js"></script>
    <script src="<?=$prefix;?>js/jquery.collapsible.js"></script>
    <script src="<?=$prefix;?>js/accordion.nav.js"></script>
    <script src="<?=$prefix;?>js/custom.js"></script>
    <script src="<?=$prefix;?>js/respond.min.js"></script>
    <script src="<?=$prefix;?>js/ios-orientationchange-fix.js"></script>

    <script src="<?=$prefix;?>js/jquery.dataTables.js"></script>
    <script src="<?=$prefix;?>js/ZeroClipboard.js"></script>
    <script src="<?=$prefix;?>js/dataTables.bootstrap.js"></script>
    <script src="<?=$prefix;?>js/TableTools.js"></script>
    <script src="<?=$prefix;?>js/plugins/dataTables/date-uk.js"></script>

    <link href="<?=$prefix;?>css/tablecloth.css" rel="stylesheet">

</head>
<body>
<div class="layout">
    <?php
        include 'inc_header.php';
        include 'inc_usuario.php';
        include 'inc_sidebar.php';
    ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">Subcategorias</h3>
                        <span class="pull-right top-right-toolbar">
                            <a href="/painel/subcategorias/novo" class="btn btn-mini btn-success" title="Novo"><i class="icon-plus "></i> Adicionar Novo</a>
                        </span>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="/painel" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li><a href="<?=$prefix;?>subcategorias">Subcategorias</a><span class="divider"><i class="icon-angle-right"></i></span></li>
                        <li class="active">Listagem</li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="widget-container">
                    <div id="data-table_wrapper" class="dataTables_wrapper form-inline" role="grid">

                        <table id="data-table" class="table tabela table-bordered">
                            <thead>
                            <tr>
                                <th style="width: 90px">ID</th>
                                <th>Titulo</th>
                                <th>Categoria</th>
                                <th style="width: 86px" class="no_sort center">Ações</th>
                            </tr>
                            </thead>
                        </table>

                    </div>
                </div>
                <script type="application/javascript">

                    <?php
                    if (isset($flash["info"]) and !empty($flash["info"])) {
                        echo "bootbox.alert('{$flash["info"]}');";
                    }
                    ?>

                    $(document).ready(function(){

                        $(document).on("click", "#deletar", function (e) {

                            var url = $(this).attr('data-url');

                            bootbox.confirm("Tem certeza que deseja excluir? <br>(Isto será um ato irreversível!)", function (result) {
                                if (result) {
                                    window.location = url;
                                }

                            });
                        });

                        $("#data-table").DataTable({
                            "processing": true,
                            "serverSide": true,
                            "ajax": {
                                "url": "<?=$prefix;?>subcategorias",
                                "type": "POST"
                            },
                            "aoColumnDefs": [
                                { "bSortable": false, "aTargets": [3] }
                            ],
                            language : {
                                url: "<?=$prefix;?>js/plugins/dataTables/Portuguese-Brasil.lang"
                            }
                        })
                    })
                </script>

    <?php
    include 'inc_footer.php';
