<?php

namespace Subcategoria;

/**
 * Entity
 *
 * @Table(name="subcategoria", indexes={@Index(name="fk_subcategoria_categoria_idx", columns={"categoria"})})
 * @Entity
 */
class Entity implements \CoffeeCore\Core\Entity
{
    const FULL_NAME = "Subcategoria\\Entity";

    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @Column(name="categoria", type="integer", nullable=false)
     */
    private $categoria;

    /**
     * @var string
     *
     * @Column(name="titulo", type="string", length=60)
     */
    private $titulo;

    /**
     * @var string
     *
     * @Column(name="slug", type="string", length=60)
     */
    private $slug;

    /**
     * @param int $categoria
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }

    /**
     * @return int
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    /**
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

	/**
     * @param string $slug
     */
    public function setSlug($titulo)
    {
        $this->slug = create_slug($titulo);
    }
	
    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
