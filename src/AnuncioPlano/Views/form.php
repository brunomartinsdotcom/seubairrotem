<?php
include 'inc_head.php';

$action = "Adicionar";
if (isset($plano)) {
    $action = "Editar";
    $dados = [
        'id'            => $plano->getId(),
        'titulo'        => $plano->getTitulo(),
        'diasanuncio'   => $plano->getDiasanuncio(),
        'valor'         => $plano->getValor()
    ];
} else {
    $dados = [
        'id'            => '',
        'titulo'        => '',
        'diasanuncio'   => '',
        'valor'         => ''
    ];
}
?>
<script type="text/javascript" src="/js/jquery.maskMoney.js"></script>
</head>
<body>
    <div class="layout">
        <?php
        include_once 'inc_header.php';
        include_once 'inc_usuario.php';
        include_once 'inc_sidebar.php';
        ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">Guia Comercial - Planos</h3>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="<?=$prefix;?>" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li>
                            <a href="<?=$prefix;?>guia-comercial-planos" title="Guia Comercial - Planos">Guia Comercial - Planos</a><span class="divider">
                                <i class="icon-angle-right"></i>
                            </span>
                        </li>
                        <li class="active"><?=$action;?></li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="content-widgets light-gray">
                    <div class="widget-head black">
                        <h3><?=$action;?> Plano</h3>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function(){
                            $("#valor").maskMoney({showSymbol:true, symbol:"R$ ", decimal:",", thousands:"."});
                            // valida o formulário
                            $('#planos').validate({
                                // define regras para os campos
                                rules: {
                                    titulo: {
                                        required: true
                                    },
                                    diasanuncio: {
                                        required: true
                                    }
                                },
                                // define messages para cada campo
                                messages: {
                                    titulo: "Informe o título para o plano",
                                    diasanuncio: "Informe a quantidade de dias de duração do plano"
                                }
                            });
                        });
                    </script>
                    <div class="widget-container">
                        <form name="planos" id="planos" class="form-horizontal" action="<?=$_SERVER["REQUEST_URI"];?>" method="post">
                            <input name="id" id="id" type="hidden" value="<?=$dados['id'];?>">
                            <div class="control-group">
                                <label for="nome" class="control-label">Título</label>
                                <div class="controls">
                                    <input name="titulo" id="titulo" type="text" maxlength="50" class="span5" value="<?=$dados['titulo'];?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="nome" class="control-label">Duração</label>
                                <div class="controls">
                                    <input name="diasanuncio" id="diasanuncio" type="number" class="span5" value="<?=$dados['diasanuncio'];?>"> dias
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="nome" class="control-label">Valor</label>
                                <div class="controls">
                                    <input name="valor" id="valor" type="text" class="span4" value="<?php if(!empty($dados['valor'])){ echo number_format($dados['valor'], 2, ',','.'); } ?>"> <em>Deixe em branco em caso de plano gratuito</em>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" id="Bt" class="btn btn-primary">Salvar</button>
                                <button type="button" onclick="window.open('<?=$prefix;?>guia-comercial-planos', '_self');" class="btn right-float">Voltar</button>
                            </div>
                        </form>
                    </div>
                </div>
<?php
include 'inc_footer.php';
