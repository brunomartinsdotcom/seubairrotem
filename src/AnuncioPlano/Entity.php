<?php
namespace AnuncioPlano;

/**
 * Plano
 *
 * @Table(name="novo_anuncio_plano")
 * @Entity
 */
class Entity implements \CoffeeCore\Core\Entity
{
    const FULL_NAME = 'AnuncioPlano\\Entity';
    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="titulo", type="string", length=100, nullable=false)
     */
    private $titulo;

    /**
     * @var integer
     *
     * @Column(name="diasanuncio", type="integer", nullable=false)
     */
    private $diasanuncio;

    /**
     * @var float
     *
     * @Column(name="valor", type="float", precision=10, scale=2, nullable=false)
     */
    private $valor;

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    /**
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param integer $diasanuncio
     */
    public function setDiasanuncio($diasanuncio)
    {
        $this->diasanuncio = $diasanuncio;
    }

    /**
     * @return integer
     */
    public function getDiasanuncio()
    {
        return $this->diasanuncio;
    }

    /**
     * @param float $valor
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    }

    /**
     * @return float
     */
    public function getValor($formated = false)
    {
        if ($formated) {
            return number_format($this->valor, 2, ',','.');
        }
        return (float)$this->valor;
    }

}
