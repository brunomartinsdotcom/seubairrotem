<?php
include 'inc_head.php';

$action = "Adicionar";
if (isset($estado)) {
    $action = "Editar";
    $dados = [
        'id' => $estado->getId(),
        'nome' => $estado->getNome()
    ];
} else {
    $dados = [
        'id' => '',
        'nome' => ''
    ];
}
?>
</head>
<body>
    <div class="layout">
    <?php
        include_once 'inc_header.php';
        include_once 'inc_usuario.php';
        include_once 'inc_sidebar.php';
    ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">Estados</h3>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="<?=$prefix;?>" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li>
                            <a href="<?=$prefix;?>estados" title="Estados">Estados</a><span class="divider">
                                <i class="icon-angle-right"></i>
                            </span>
                        </li>
                        <li class="active"><?=$action;?></li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="content-widgets light-gray">
                    <div class="widget-head black">
                        <h3><?=$action;?> Estado</h3>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function(){
                            // valida o formulário
                            $('#estados').validate({
                                // define regras para os campos
                                rules: {
                                    nome: {
                                        required: true
                                    }
                                },
                                // define messages para cada campo
                                messages: {
                                    nome:"Informe o nome do Estado",
                                }
                            });

                            var interval = 0;
                            $('#nome').keyup(function(){
                                // começa a contar o tempo
                                clearInterval(interval);

                                // 500ms após o usuário parar de digitar a função é chamada
                                interval = window.setTimeout(function(){
                                    data_html = "nome=" + $('#nome').val();
                                    $.ajax({
                                        type: 'POST',
                                        url: '<?=$prefix;?>estados/verifica-existente',
                                        data: data_html,
                                        dataType: 'json',
                                        success: function (msg) {
                                            if (msg.error == 0) {
                                                $('#nome').css('border-color', 'green');
                                                $('#msg').html('');
                                                $("#Bt").removeAttr('disabled');
                                            } else {
                                                $('#nome').css('border-color', 'red');
                                                $('#msg').html('Estado já cadastrado!');
                                                $('#Bt').attr('disabled', 'true');
                                            }
                                        }
                                    });
                                }, 500);
                            });
                        });
                    </script>
                    <div class="widget-container">
                        <form name="estados" id="estados" class="form-horizontal" action="<?=$_SERVER["REQUEST_URI"];?>" method="post">
                            <input name="id" id="id" type="hidden" value="<?=$dados['id'];?>">
                            <div class="control-group">
                                <label for="nome" class="control-label">Nome</label>
                                <div class="controls">
                                    <input name="nome" id="nome" type="text" maxlength="50" class="span7" value="<?=$dados['nome'];?>">
                                    <em id="msg" class="error"></em>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" id="Bt" class="btn btn-primary">Salvar</button>
                                <button type="button" onclick="window.open('<?=$prefix;?>estados', '_self');" class="btn right-float">Voltar</button>
                            </div>
                        </form>
                    </div>
                </div>
<?php
include 'inc_footer.php';
