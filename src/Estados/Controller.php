<?php

namespace Estados;

use CoffeeCore\Core\AbstractController;
use CoffeeCore\Helper\ResourceManager;
use CoffeeCore\Storage\DoctrineStorage;

/**
 * Class Controller
 * @package Estados
 */
class Controller extends AbstractController
{
    /**
     * @var string
     */
    public $pagina = 'estados';
    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Entity
     */
    protected $entity;

    /**
     * @return void
     */
    public function getDataTable()
    {
        $this->startConnection();
        $columns = ['id', 'nome'];

        $search = isset($_POST['search']['value']) ? $_POST['search']['value'] : ""; //$_POST['search']['value'];

        $limit = isset($_POST['length']) ? $_POST['length'] : 10;
        $offset = isset($_POST['start']) ? $_POST['start'] : 0;
        if(isset($_POST['order'])){
            $orderBy = [
                $_POST['order'][0]['column'],
                $_POST['order'][0]['dir']
            ];
        }else{
            $orderBy = [0, 'ASC'];
        }

        $dataTables = $this->model->getDataTableData($columns, $orderBy, $search, $limit, $offset);

        foreach ($dataTables['data'] as $k => &$row) {
            $row[] = "<span class='center'>
                        <button class='btn btn-primary' onclick='javascript:window.location = \"/painel/{$this->pagina}/alterar/{$row[0]}\"'>
                            <span><i class='icon-pencil'></i></span>
                        </button>
                      </span>";
            $row[] = "<span class='center'>
                        <button data-url='/painel/{$this->pagina}/deletar/{$row[0]}' id='deletar' class='btn btn-danger'>
                            <span><i class='icon-trash'></i></span>
                        </button>
                     </span>";
            unset($k);
        }

        //header('Content-Type: application/json');
        echo json_encode($dataTables);
    }

    /**
     *
     */
    public function startConnection()
    {
        $this->entity = new Entity();
        $this->model = new Model(new DoctrineStorage($this->entity));
    }

    /**
     *
     */
    public function gravar()
    {
        $this->startConnection();

        $this->entity->setNome($_POST['nome']);

        try {
            if (!isset($_POST['id']) or empty($_POST['id'])) {
                $this->model->insert($this->entity);
            } else {
                $this->entity->setId($_POST['id']);
                $this->model->update($this->entity);
            }
            $this->model->flush();
        } catch (\Exception $e) {
            return false;
        }

        //ESCREVE JSON
        $listaDeEstados = $this->model->findby([], ['nome' => 'ASC']);

        $estados = [];

        while ($listaDeEstados->valid()) {
            $estados[create_slug($listaDeEstados->current()->getNome())] = [
                "id" =>     $listaDeEstados->current()->getId(),
                "nome" => $listaDeEstados->current()->getNome()
            ];
            $listaDeEstados->next();
        }

        ResourceManager::writeFile("cadastro_de_estados.json", json_encode($estados));

    }

    /**
     * @param int|null $id
     */
    public function deletar($id = null)
    {
        $this->startConnection();
        if (!empty($id)) {
            $this->entity = $this->model->findOneBy(["id"=>$id]);
            $this->model->delete($this->entity);
            $this->model->flush();

            //ESCREVE JSON
            $listaDeEstados = $this->model->findby([], ['nome' => 'ASC']);

            $estados = [];

            while ($listaDeEstados->valid()) {
                $estados[create_slug($listaDeEstados->current()->getNome())] = [
                    "id" =>     $listaDeEstados->current()->getId(),
                    "nome" => $listaDeEstados->current()->getNome()
                ];
                $listaDeEstados->next();
            }

            ResourceManager::writeFile("cadastro_de_estados.json", json_encode($estados));

        }
    }

    public function verificaexistente()
    {
        $nomeestado = $_POST['nome'];
        $this->startConnection();
        if (!empty($nomeestado)) {
            $this->entity = $this->model->findOneBy(["nome"=>$nomeestado]);

            if(!empty($this->entity)){
                $array = ['error' => 1];
            }else{
                $array = ['error' => 0];
            }
            header('Content-Type: application/json');
            echo json_encode($array);

            //return $this->entity;
                //$this->entity->getNome();
        }
    }
}
