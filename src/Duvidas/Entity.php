<?php

namespace Duvidas;

/**
 * Entity
 *
 * @Table(name="novo_duvidas")
 * @Entity
 */
class Entity implements \CoffeeCore\Core\Entity
{
    const FULL_NAME = "Duvidas\\Entity";

    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="pergunta", type="string", length=100, nullable=false)
     */
    private $pergunta;

    /**
     * @var string
     *
     * @Column(name="resposta", type="text", nullable=false)
     */
    private $resposta;

    /**
     * @var integer
     *
     * @Column(name="sortorder", type="integer")
     */
    private $sortorder = 0;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPergunta()
    {
        return $this->pergunta;
    }

    /**
     * @param string $pergunta
     */
    public function setPergunta($pergunta)
    {
        $this->pergunta = $pergunta;
    }

    /**
     * @return string
     */
    public function getResposta()
    {
        return $this->resposta;
    }

    /**
     * @param string $resposta
     */
    public function setResposta($resposta)
    {
        $this->resposta = $resposta;
    }

    /**
     * @return int
     */
    public function getSortorder()
    {
        return $this->sortorder;
    }

    /**
     * @param int $sortorder
     */
    public function setSortorder($sortorder)
    {
        $this->sortorder = $sortorder;
    }
}
