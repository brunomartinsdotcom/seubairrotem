<?php
include 'inc_head.php';

$action = "Adicionar";
if (isset($duvida)) {
    $action = "Editar";
    $dados = [
        'id'        => $duvida->getId(),
        'pergunta'  => $duvida->getPergunta(),
        'resposta'  => $duvida->getResposta(),
        'sortorder' => $duvida->getSortorder()
    ];
} else {
    $dados = [
        'id'        => '',
        'pergunta'  => '',
        'resposta'  => '',
        'sortorder' => ''
    ];
}
?>
</head>
<body>
    <div class="layout">
    <?php
        include_once 'inc_header.php';
        include_once 'inc_usuario.php';
        include_once 'inc_sidebar.php';
    ?>
    </div>
    <div class="main-wrapper">
        <div class="container-fluid">
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">Dúvidas</h3>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="<?=$prefix;?>" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                        <li>
                            <a href="<?=$prefix;?>duvidas">Dúvidas</a><span class="divider">
                                <i class="icon-angle-right"></i>
                            </span>
                        </li>
                        <li class="active"><?=$action;?></li>
                    </ul>
                </div>
            </div>
            <div class="content-widgets">
                <div class="content-widgets light-gray">
                    <div class="widget-head black">
                        <h3><?=$action;?> Dúvida</h3>
                    </div>
                    <script type="text/javascript" src="<?=$prefix;?>editor/ckeditor/ckeditor.js"></script>
                    <script type="text/javascript">
                        $(document).ready(function(){
                            //INICIALIZA O EDITOR DE TEXTO
                            CKEDITOR.replace('resposta');

                            // valida o formulário
                            $('#duvida').validate({
                                // define regras para os campos
                                rules: {
                                    pergunta: {
                                        required: true
                                    },
									resposta: {
										required: function(){
                                            CKEDITOR.instances.resposta.updateElement();
                                        }
									}
                                },
                                // define messages para cada campo
                                messages: {
                                    titulo:"Informe o a pergunta",
									texto:"Informe a resposta"
                                }
                            });
                        });
                    </script>
                    <div class="widget-container">
                        <form name="duvida" id="duvida" class="form-horizontal" action="<?=$_SERVER["REQUEST_URI"];?>" method="post">
                            <?php if(isset($duvida)) { ?>
                                <input name="id" id="id" type="hidden" value="<?=$dados['id'];?>">
                            <?php } ?>
                            <input name="sortorder" id="sortorder" type="hidden" value="<?=$dados['sortorder'];?>">
                            <div class="control-group">
                                <label for="pergunta" class="control-label">Pergunta</label>
                                <div class="controls">
                                    <input name="pergunta" id="pergunta" type="text" maxlength="100" class="span8" value="<?=$dados['pergunta'];?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="resposta" class="control-label">Resposta</label>
                                <div class="controls" style="width: 80%;">
                                    <textarea class="span8" rows="15" name="resposta" id="resposta"><?=$dados['resposta'];?></textarea>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" id="Bt" class="btn btn-primary">Salvar</button>
                                <button type="button" onclick="window.open('<?=$prefix;?>perguntas-frequentes', '_self');" class="btn right-float">Voltar</button>
                            </div>
                        </form>
                    </div>
                </div>
    <?php
include 'inc_footer.php';
