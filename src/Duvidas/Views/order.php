<?php
    include 'inc_head.php';
?>
<script src="<?=$prefix;?>js/jquery-ui-1.10.1.custom.min.js"></script>
<script type="text/javascript">
    /* when the DOM is ready */
    jQuery(document).ready(function () {
        /* grab important elements */
        var sortInput = jQuery('#sort_order');
        var submit = jQuery('#autoSubmit');
        var messageBox = jQuery('#message-box');
        var list = jQuery('#sortable');
        /* create requesting function to avoid duplicate code */
        var request = function () {
            jQuery.ajax({
                beforeSend: function () {
                    messageBox.show();
                    messageBox.text('Salvando...');
                },
                complete: function () {
                    messageBox.show();
                    messageBox.text('Salvo!');
                },
                data: 'sortorder=' + sortInput[0].value + '&ajax=' + submit[0].checked + '&do_submit=1&byajax=1', //need [0]?
                type: 'POST',
                url: '<?=$prefix;?>duvidas/ordem'
            });
        };
        /* worker function */
        var fnSubmit = function (save) {
            var sortOrder = [];
            list.children('li').each(function () {
                sortOrder.push(jQuery(this).data('id'));
            });
            sortInput.val(sortOrder.join(','));
            if (save) {
                request();
            }
        };
        /* store values */
        list.children('li').each(function () {
            var li = jQuery(this);
            li.data('id', li.attr('title')).attr('title', '');
        });
        /* sortables */
        list.sortable({
            opacity: 0.7,
            update: function () {
                fnSubmit(submit[0].checked);
            }
        });
        list.disableSelection();
        /* ajax form submission */
        jQuery('#dd-form').bind('submit', function (e) {
            if (e) e.preventDefault();
            fnSubmit(true);
        });
    });
</script>
<style type="text/css">
    .demo {
        margin: 0 auto
    }
    #sortable {
        list-style-type: none;
        margin: 0;
        padding: 0;
        width: 100%;
    }
    #sortable li {
        margin: 0 3px 3px 3px;
        padding: 0.4em;
        padding-left: 1.5em;
        font-size: 1.4em;
        height: 18px;
    }
    #sortable li span {
        position: absolute;
        margin-left: -1.3em;
    }
    #sortable li.highlights {
        background: yellow
    }
</style>
</head>
<body>
<div class="layout">
    <?php
        include 'inc_header.php';
        include 'inc_usuario.php';
        include 'inc_sidebar.php';
    ?>
</div>
<div class="main-wrapper">
    <div class="container-fluid">
        <div class="row-fluid ">
            <div class="span12">
                <div class="primary-head">
                    <h3 class="page-header">Dúvidas</h3>
                </div>
                <ul class="breadcrumb">
                    <li><a href="<?=$prefix;?>" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>
                    <li><a href="<?=$prefix;?>duvidas" title="Dúvidas">Dúvidas</a><span class="divider"><i class="icon-angle-right"></i></span></li>
                    <li class="active">Ordenar</li>
                </ul>
            </div>
        </div>
        <div class="content-widgets">
            <div class="content-widgets light-gray">
                <div class="widget-head black">
                    <h3>Ordenar Dúvidas</h3>
                </div>
                <div class="widget-container">
                    <div class="span12">
                        <p>Os 5 primeiros farão parte do header do site em suas respectivas posições.</p>
                    </div>
                    <form id="dd-form" style="padding: 15px;" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
                        <p style="display:none;">
                            <input type="checkbox" value="1" name="autoSubmit" id="autoSubmit" checked="checked" <?php if(isset($_POST['autoSubmit'])) { echo 'checked="checked"'; } ?> />
                            <label for="autoSubmit">Salvar Automaticamente</label>
                        </p>
                        <ul id="sortable">
                            <?php
                                $order = array();
                                $bg = 0;
                                /** @var $controllerData \CoffeeCore\Core\AbstractDao */
                                $perguntas = $repository->findBy([], ['sortorder' => 'asc']);
                                foreach ($perguntas as $pergunta) {
                                    $bg = $bg + 1;
                                    if ($bg % 2 == 0) {
                                        $ex = "#FFF";
                                    } else {
                                        $ex = "#DDD";
                                    }
                            ?>
                                <li class="ui-state-default" title="<?=$pergunta->getId();?>" style="background:<?= $ex; ?>;"><?=$pergunta->getPergunta();?></li>
                            <?php } ?>
                        </ul>
                        <div id="message-box"></div>
                        <input type="hidden" name="sort_order" id="sort_order" value="<?php echo implode(',', $order); ?>"/>
                        <input type="submit" name="do_submit" value="Submit Sortation" class="button" style="display:none;"/>
                        <div class="clearfix"></div>
                        <div class="form-actions">
                            <button type="button" onclick="window.open('<?=$prefix;?>duvidas', '_self');" class="btn btn-primary">Concluído</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'inc_footer.php';
