<?php

function make_seed() {
    list($usec, $sec) = explode(' ', microtime());
    return (float) $sec + ((float) $usec * 100000);
}

function nome_arquivo($db){
    //Define Formatos
    preg_match("/\.(gif|GIF|png|PNG|jpg|JPG|jpeg|JPEG|pdf|PDF|doc|DOC|docx|DOCX|txt|TXT|rar|RAR|zip|ZIP|ai|AI|cdr|CDR){1}$/i", $db, $ext);
    $nome = md5(uniqid(time())).".".$ext[1];
    return $nome;
}

function create_slug($title) {
    return removeAcentos($title, '-');
}

function removeAcentos($string, $slug = false) {
    $string = strtolower($string);
    $acentuados = ["á","à","ã","ä","â",
        "é","è","ê","ë",
        "í","ì","ï","î",
        "ó","ò","õ","ô","ö",
        "ú","ù","ü","û",
        "ç",
        "Á","À","Ã","Ä","Â",
        "É","À","Ê","Ë",
        "Í","Ì","Ï","Î",
        "Ó","Ò","Õ","Ô","Ö",
        "Ú","Ù","Ü","Û",
        "Ç"];

    $normais = ["a","a","a","a","a",
        "e","e","e","e",
        "i","i","i","i",
        "o","o","o","o","o",
        "u","u","u","u",
        "c",
        "a","a","a","a","a",
        "e","e","e","e",
        "i","i","i","i",
        "o","o","o","o","o",
        "u","u","u","u",
        "c"];

    $string = str_replace($acentuados, $normais, $string);

    $chars = ['&','ã ','ã¡','ã¢','ã£','ã¤','ã¥','ã¦','ã§','ã¨','ã©',
        'ãª','ã«','ã¬','ã­','ã®','ã¯','ã°','ã±','ã²','ã³','ã´',
        'ãµ','ã¶','ã¸','ã¹','ãº','ã»','ã¼','ã½','ã¾','ã¿','ã€',
        'ã','ã‚','ãƒ','ã"','ã…','ã†','ã‡','ãˆ','ã‰','ãŠ','ã‹',
        'ãŒ','ã','ãŽ','ã','ã','ã\'','ã\'','ã"','ã"','ã•','ã–',
        'ã˜','ã™','ãš','ã›','ãœ','ã','ãž','â‚¬','"','ãŸ','â¢','â£','â¤','â¥','â¦','â§','â¨','â©','âª','â«',
        'â¬','â­','â®','â¯','â°','â±','â²','â³','â´','âµ','â¶',
        'â·','â¸','â¹','âº','â»','â¼','â½','â¾', ' ', ':',
        '!','@','#','$','%','â¨','&','*','(',')','_','+','[','{','}','\''];

    $entities = ['e','a','a','a','a','a','a',
        'ae','c','e','e','e','e','i',
        'i','i','i','o','n','o','o',
        'o','o','o','o','u','u','u',
        'u','y','','y','a','a','a',
        'a','a','a','ae','c','e','e',
        'e','e','e','e','e','e','e','n',
        'o','o','o','o','o','o','u',
        'u','u','u','y','-','e','-','b',
        '-','-','c','f','x','y','-','s','-',
        'c','a','-','-','-','r','-','-','-',
        '2','3','-','u','-','-','2','1',
        '-','-','-','-','-', '-', '-',
        '-','-','-','-','-','-','-','-','-','-','-','-',
        '-','-','-', ''];

    $string = str_replace($chars, $entities, $string);
    $string = preg_replace("/([^a-z0-9]+)/", "-", $string);
    $string = (substr($string,-1) == "-") ? substr($string,0,-1) : $string;

    if ($slug) {
        // Troca tudo que não for letra ou número por um caractere ($slug)
        $string = preg_replace('/[^a-z0-9]/i', $slug, $string);
        // Tira os caracteres ($slug) repetidos
        $string = preg_replace('/' . $slug . '{2,}/i', $slug, $string);
        $string = trim($string, $slug);
    }
    return $string;
}

function video_imagem($url){
    $url_imagem = parse_url($url);
    if($url_imagem['host'] == 'www_antigo.youtube.com' || $url_imagem['host'] == 'youtube.com'){
        $array = explode("&", $url_imagem['query']);
        return "http://img.youtube.com/vi/".substr($array[0], 2)."/0.jpg";
    }else if($url_imagem['host'] == 'www_antigo.youtu.be' || $url_imagem['host'] == 'youtu.be'){
        $array = explode(".be/", $url);
        return "http://img.youtube.com/vi/".substr($array[1],0,11)."/0.jpg";
    } else if($url_imagem['host'] == 'www_antigo.vimeo.com' || $url_imagem['host'] == 'vimeo.com'){
        $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/".substr($url_imagem['path'], 1).".php"));
        return $hash[0]["thumbnail_small"];
    }
}

function identificaVideo($url, $tamanho){
    $url_imagem = parse_url($url);
    if($url_imagem['host'] == 'www_antigo.youtube.com' || $url_imagem['host'] == 'youtube.com' || $url_imagem['host'] == 'youtu.be'){
        if($tamanho == 'maior'){
            return '<iframe width="640" height="480" src="http://www_antigo.youtube.com/embed/'.idYouTube($url).'" frameborder="0" allowfullscreen></iframe>';
        }else if($tamanho == 'menor'){
            return '<iframe width="300" height="225" src="http://www_antigo.youtube.com/embed/'.idYouTube($url).'" frameborder="0" allowfullscreen></iframe>';
        }
    } else if($url_imagem['host'] == 'www_antigo.vimeo.com' || $url_imagem['host'] == 'vimeo.com'){
        if($tamanho == 'maior'){
            return '<iframe src="http://player.vimeo.com/video/'.idVimeo($url).'?title=0&amp;byline=0&amp;portrait=0" width="640" height="480" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
        }else if($tamanho == 'menor'){
            return '<iframe src="http://player.vimeo.com/video/'.idVimeo($url).'?title=0&amp;byline=0&amp;portrait=0" width="300" height="225" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
        }
    }
}


function idVimeo($url){
    $var = explode(".com/", $url);
    $final = substr($var[1],0,8);
    return $final;
}

function idYouTube($url){
    $url_imagem = parse_url($url);
    if($url_imagem['host'] == 'www_antigo.youtube.com' || $url_imagem['host'] == 'youtube.com'){
        $var = explode("watch?v=", $url);
        $final = substr($var[1],0,11);
    }else if($url_imagem['host'] == 'youtu.be'){
        $var = explode(".be/", $url);
        $final = substr($var[1],0,11);
    }
    return $final;
}