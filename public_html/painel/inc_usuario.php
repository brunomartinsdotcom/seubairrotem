<div class="admin-info clearfix">
    <div class="admin-thumb">
        <?php
        if (!empty($usuarioLogado->getFoto())) {
        echo "<img src='_uploads/usuarios/". $usuarioLogado->getFoto() . " '>";
        } else {
        echo '<i class="icon-user"></i>';
        }
        ?>
    </div>
    <div class="admin-meta">
        <ul>
            <li class="admin-username"><?=$usuarioLogado->getNome()?></li>
            <li>
                <a href="<?=$prefix;?>meus-dados"><i class="icon-pencil"></i> Meus Dados</a>
            </li>
            <li>
                <i class="icon-desktop"></i> Acesso Número: <?=$usuarioLogado->getNumeroacesso()?>
            </li>
        </ul>
    </div>
</div>