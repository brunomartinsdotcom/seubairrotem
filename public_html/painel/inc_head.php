<?php
if (!isset($_SESSION)) {
    session_start();
}

$prefix = '/painel/';

$usuarioLogado = \CoffeeCore\ACL\Authentication::getFromSession('usuario');
if(empty($usuarioLogado)){
    header("location: {$prefix}login");
    exit();
}
?>
<!DOCTYPE HTML>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0; user-scalable=0;">
    <meta name="author" content="Agência Café com Ideias">

    <!-- styles -->
    <link href="<?= url_base() . $prefix;?>css/bootstrap.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/jquery.gritter.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=$prefix;?>css/font-awesome.css">
    <link rel="stylesheet" href="<?=$prefix;?>css/custom.css">

    <!--[if IE 7]>
    <link rel="stylesheet" href="<?=$prefix;?>css/font-awesome-ie7.min.css">
    <![endif]-->
    <link href="<?=$prefix;?>css/tablecloth.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/styles.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/jquery.Jcrop.min.css" rel="stylesheet">
    <link href="<?=$prefix;?>css/chosen.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie7.css" />
    <![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie8.css" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" type="text/css" href="<?=$prefix;?>css/ie/ie9.css" />
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>

    <!-- Favicon -->
    <link rel="icon" href="../favicon.png" type="image/x-icon" />
    <link rel="shortcut icon" href="../favicon.png" type="image/x-icon" />
    <link rel="apple-touch-icon" href="../images/apple-touch-icon.png" />

    <!--============ javascript ===========-->
    <script src="<?=$prefix;?>js/jquery.js"></script>
    <script src="<?=$prefix;?>js/bootstrap.js"></script>
    <script src="<?=$prefix;?>js/jquery.metadata.js"></script>
    <script src="<?=$prefix;?>js/excanvas.js"></script>
    <script src="<?=$prefix;?>js/jquery.collapsible.js"></script>
    <script src="<?=$prefix;?>js/accordion.nav.js"></script>
    <script src="<?=$prefix;?>js/custom.js"></script>
    <script src="<?=$prefix;?>js/respond.min.js"></script>
    <script src="<?=$prefix;?>js/ios-orientationchange-fix.js"></script>
    <script src="<?=$prefix;?>js/bootbox.js"></script>
    <script src="<?=$prefix;?>js/jquery.validate.js"></script>
    <script src="<?=$prefix;?>js/chosen.jquery.js"></script>
    <script src="<?=$prefix;?>js/bootstrap-fileupload.js"></script>
    <script src="<?=$prefix;?>js/jquery.dataTables.js"></script>
    <script src="<?=$prefix;?>js/ZeroClipboard.js"></script>
    <script src="<?=$prefix;?>js/dataTables.bootstrap.js"></script>
    <script src="<?=$prefix;?>js/jquery.tablecloth.js"></script>
    <script src="<?=$prefix;?>js/accordion.nav.js"></script>
    <script src="<?=$prefix;?>js/mascara.js"></script>
    <script src="<?=$prefix;?>js/jquery.Jcrop.js"></script>
    <script src="<?=$prefix;?>js/jquery.color.js"></script>
    <script src="<?=$prefix;?>js/script.js"></script>
    <script src="<?=$prefix;?>js/TableTools.js"></script>
    <script type="text/javascript">
        /**=========================
         LEFT NAV ICON ANIMATION
         ==============================**/
        $(function () {
            $(".left-primary-nav a").hover(function () {
                $(this).stop().animate({
                    fontSize: "30px"
                }, 200);
            }, function () {
                $(this).stop().animate({
                    fontSize: "24px"
                }, 100);
            });
        });
    </script>

    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="css/ie/ie7.css" />
    <![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="css/ie/ie8.css" />
    <![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" type="text/css" href="css/ie/ie9.css" />
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>

    <title>Painel Administrativo | Guia Seu Bairro Tem</title>