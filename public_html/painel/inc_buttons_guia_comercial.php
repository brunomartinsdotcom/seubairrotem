<div class="switch-board gray">
    <ul class="clearfix switch-item">
        <li><a href="<?=$prefix;?>guia-comercial-planos" class="orange"><i class="icon-tags"></i><span>Planos</span></a></li>
        <li><a href="<?=$prefix;?>guia-comercial" class="blue"><i class="icon-bullhorn"></i><span>Anúncios</span></a></li>
        <li><a href="<?=$prefix;?>guia-comercial-faturas" class="dark-yellow"><i class="icon-file-alt"></i><span>Faturas</span></a></li>
        <li><a href="<?=$prefix;?>guia-comercial-promocoes" class="green"><i class="icon-star"></i><span>Promoções</span></a></li>
        <li><a href="<?=$prefix;?>formas-de-pagamento" class="bondi-blue"><i class="icon-money"></i><span>Formas de Pagamento</span></a></li>
    </ul>
</div>