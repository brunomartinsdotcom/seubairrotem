/*$(document).ready(function() {
 if(pagina == "clientes"){
 $("#tipo_pessoa").change(function() {
 if(this.value == 'Pessoa Física'){
 $('#div_cpf').show();
 $('#div_cnpj').hide();
 }else if(this.value == 'Pessoa Jurídica'){
 $('#div_cpf').hide();
 $('#div_cnpj').show();
 }else{
 $('#div_cpf').hide();
 $('#div_cnpj').hide();
 }
 });
 }
 });*/

//CÓDIGO PARA O BOTÃO EXCLUIR
$(document).on("click", "#deletar", function (e) {
    var url = $(this).data('url');
    if($(this).data('msg') == null){
        var msg = "Tem certeza que deseja excluir?<br />Isto será um ato irreversível!";
    }else{
        var msg = $(this).data('msg');
    }
    bootbox.confirm(msg, function (result){
        if (result == true) {
            window.location = url;
        }
    });
});

//pergunta
function pergunta(frase, url){
    if (confirm(frase)){
        window.location = url;
    }
}

//SHOW
function DivShow(id){
    caixa = document.getElementById(id);
    caixa.style.display = "";
}
//HIDE
function DivHide(id){
    caixa = document.getElementById(id);
    caixa.style.display = "none";
}

/**
 *
 * HTML5 Image uploader with Jcrop
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 *
 * Copyright 2012, Script Tutorials
 * http://www.script-tutorials.com/
 */

// convert bytes into friendly format
function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB'];
    if (bytes == 0) return 'n/a';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
};

// check for selected crop region
function checkForm() {
    if (parseInt($('#w').val())) return true;
    $('.error').html('Por favor, selecione a região que deseja fazer o corte!').show();
    return false;
};

// update info by cropping (onChange and onSelect events handler)
function updateInfo(e) {
    $('#x1').val(e.x);
    $('#y1').val(e.y);
    $('#x2').val(e.x2);
    $('#y2').val(e.y2);
    $('#w').val(e.w);
    $('#h').val(e.h);
};

// clear info by cropping (onRelease event handler)
function clearInfo() {
    $('.info #w').val('');
    $('.info #h').val('');
};

function fileSelectHandler(nome_campo, tamW, tamH, aspectR) {

    //LARGURA
    var largura = $("body").width();

    // get selected file
    var oFile = $('#'+nome_campo)[0].files[0];

    // hide all errors
    $('.error').hide();

    // check for image type (jpg and png are allowed)
    var rFilter = /^(image\/jpeg|image\/gif|image\/png)$/i;
    if (! rFilter.test(oFile.type)) {
        $('.error').html('Por favor envie um formato de imagem válido. (Formatos suportados: jpg, gif e png)').show();
        return;
    }

    // check for file size
    if (oFile.size > 1024 * 4088) {
        $('.error').html('Imagem muito grande!').show();
        return;
    }

    // preview element
    var oImage = document.getElementById('preview');

    // prepare HTML5 FileReader
    var oReader = new FileReader();
    oReader.onload = function(e) {

        // e.target.result contains the DataURL which we can use as a source of the image
        oImage.src = e.target.result;

        // e.target.result contains the DataURL which we can use as a source of the image
        $('.jcrop-holder img').attr('src', e.target.result);
        //oImage.src = e.target.result;

        //oImage.width = '60%'; //e.target.result;
        oImage.onload = function () { // onload event handler

            // display step 2
            $('.step2').fadeIn(500);

            // display some basic image info
            var sResultFileSize = bytesToSize(oFile.size);
            $('#filesize').val(sResultFileSize);
            $('#filetype').val(oFile.type);
            $('#filedim').val(oImage.naturalWidth + 'x' + oImage.naturalHeight);

            // Create variables (in this scope) to hold the Jcrop API and image size
            var jcrop_api, boundx, boundy;

            // destroy Jcrop if it is existed
            if (typeof jcrop_api != 'undefined') {
                jcrop_api.destroy();
            }

            var larg_imagem = $("#preview").width();

            //if(larg_imagem > tamW && larg_imagem > 760){
            if(larg_imagem >= 760){
                $('#preview').css('max-width','760px');
                $('#preview').css('width','760px');
                $('#preview').css('height','auto');
                $('#preview').css('max-height','auto');
                $('.jcrop-holder img').css('max-width','760px');
                $('.jcrop-holder img').css('width','760px');
                $('.jcrop-holder img').css('height','auto');
                $('.jcrop-holder img').css('max-height','auto');
                $('img.jcrop-preview').css('max-width','760px');
                $('img.jcrop-preview').css('width','760px');
                $('img.jcrop-preview').css('height','auto');
                $('img.jcrop-preview').css('max-height','auto');
                $('input#val_resize').val('760');
            }
            var tamW_N = tamW;
            var tamH_N = tamH;

            // initialize Jcrop
            $('#preview').Jcrop({
                //minSize: [32, 32], // min crop size
                setSelect: [0, 0, tamW_N, tamH_N],
                minSize		: [ tamW_N, tamH_N ],
                //minSize		: [ tamMinW, tamMinH ],
                //maxSize		: [ tamMaxW, tamMaxH ],
                aspectRatio : aspectR, // keep aspect ratio 1:1
                bgFade: true, // use fade effect
                bgOpacity: .3, // fade opacity
                onChange: updateInfo,
                onSelect: updateInfo,
                onRelease: clearInfo
            }, function(){
                $('.btn-file').hide();
                //$('.btn-file .fileupload-exists').hide();
                //$('a#remove-btn').hide();
                //$('a#trocar-imagem').show();
                // use the Jcrop API to get the real image size
                var bounds = this.getBounds();
                boundx = bounds[0];
                boundy = bounds[1];

                // Store the Jcrop API in the jcrop_api variable
                jcrop_api = this;
            });

        };
    };

    // read selected file as DataURL
    oReader.readAsDataURL(oFile);
}



function fileSelectHandlerTopo(nome_campo, tamW, tamH, aspectR, tamanho_maximo) {
    //760

    tamW = tamanho_maximo;
    tamH = tamW/aspectR;

    console.log(tamW);
    console.log(tamH);
    //LARGURA
    var largura = $("body").width();

    // get selected file
    var oFile = $('#'+nome_campo)[0].files[0];

    // hide all errors
    $('.error').hide();

    // check for image type (jpg and png are allowed)
    var rFilter = /^(image\/jpeg|image\/gif|image\/png)$/i;
    if (! rFilter.test(oFile.type)) {
        $('.error').html('Por favor envie um formato de imagem válido. (Formatos suportados: jpg, gif e png)').show();
        return;
    }

    // check for file size
    if (oFile.size > 1024 * 4088) {
        $('.error').html(oFile.size+' Imagem muito grande!').show();
        return;
    }

    // preview element
    var oImage = document.getElementById('preview');

    // prepare HTML5 FileReader
    var oReader = new FileReader();
    oReader.onload = function(e) {

        // e.target.result contains the DataURL which we can use as a source of the image
        oImage.src = e.target.result;
        //oImage.width = '60%'; //e.target.result;
        oImage.onload = function () { // onload event handler

            // display step 2
            $('.step2').fadeIn(500);

            // display some basic image info
            var sResultFileSize = bytesToSize(oFile.size);
            $('#filesize').val(sResultFileSize);
            $('#filetype').val(oFile.type);
            $('#filedim').val(oImage.naturalWidth + 'x' + oImage.naturalHeight);

            // Create variables (in this scope) to hold the Jcrop API and image size
            var jcrop_api, boundx, boundy;

            // destroy Jcrop if it is existed
            if (typeof jcrop_api != 'undefined')
                jcrop_api.destroy();

            //$('#preview').hide();
            var larg_imagem = $("#preview").width();

            //if(larg_imagem > tamW && larg_imagem > 760){
            if(larg_imagem >= 760){
                $('#preview').css('max-width','760px');
                $('#preview').css('width','760px');
                $('#preview').css('height','auto');
                $('#preview').css('max-height','auto');
                $('.jcrop-holder img').css('max-width','760px');
                $('.jcrop-holder img').css('width','760px');
                $('.jcrop-holder img').css('height','auto');
                $('.jcrop-holder img').css('max-height','auto');
                $('img.jcrop-preview').css('max-width','760px');
                $('img.jcrop-preview').css('width','760px');
                $('img.jcrop-preview').css('height','auto');
                $('img.jcrop-preview').css('max-height','auto');
                $('input#val_resize').val('760');
            }
            /*alert (largura);
             if(largura <= 1024){
             var tamW_N = 500;
             var tamH_N = tamW / tamH * tamW_N;

             $('#resize').val(760);
             $('#preview').css('max-width','760px');
             $('#preview').css('width','760px');
             $('#preview').css('height','auto');
             $('#preview').css('max-height','auto');
             $('.jcrop-holder img').css('max-width','760px');
             $('.jcrop-holder img').css('width','760px');
             $('.jcrop-holder img').css('height','auto');
             $('.jcrop-holder img').css('max-height','auto');
             $('img.jcrop-preview').css('max-width','760px');
             $('img.jcrop-preview').css('width','760px');
             $('img.jcrop-preview').css('height','auto');
             $('img.jcrop-preview').css('max-height','auto');
             }else if(largura <= 1024 && largura >= 641){
             var tamW_N = 300;
             var tamH_N = tamW / tamH * tamW_N;

             $('#resize').val(400);
             $('#preview').css('max-width','400px');
             $('#preview').css('width','400px');
             $('#preview').css('height','auto');
             $('#preview').css('max-height','auto');
             $('.jcrop-holder img').css('max-width','400px');
             $('.jcrop-holder img').css('width','400px');
             $('.jcrop-holder img').css('height','auto');
             $('.jcrop-holder img').css('max-height','auto');
             $('img.jcrop-preview').css('max-width','400px');
             $('img.jcrop-preview').css('width','400px');
             $('img.jcrop-preview').css('height','auto');
             $('img.jcrop-preview').css('max-height','auto');
             }else if(largura <= 640){
             var tamW_N = 150;
             var tamH_N = tamW / tamH * tamW_N;

             $('#resize').val(250);
             $('#preview').css('max-width','250px');
             $('#preview').css('width','250px');
             $('#preview').css('height','auto');
             $('#preview').css('max-height','auto');
             $('.jcrop-holder img').css('max-width','250px');
             $('.jcrop-holder img').css('width','250px');
             $('.jcrop-holder img').css('height','auto');
             $('.jcrop-holder img').css('max-height','auto');
             $('img.jcrop-preview').css('max-width','250px');
             $('img.jcrop-preview').css('width','250px');
             $('img.jcrop-preview').css('height','auto');
             $('img.jcrop-preview').css('max-height','auto');
             }else{*/
            var tamW_N = tamW;
            var tamH_N = tamH;
            //}

            // initialize Jcrop
            $('#preview').Jcrop({
                //minSize: [32, 32], // min crop size
                setSelect: [0, 0, tamW_N, tamH_N],
                minSize		: [ tamW_N, tamH_N ],
                //minSize		: [ tamMinW, tamMinH ],
                //maxSize		: [ tamMaxW, tamMaxH ],
                aspectRatio : aspectR, // keep aspect ratio 1:1
                bgFade: true, // use fade effect
                bgOpacity: .3, // fade opacity
                onChange: updateInfo,
                onSelect: updateInfo,
                onRelease: clearInfo
            }, function(){
                $('.btn-file').hide();
                $('a#remove-btn').hide();
                $('a#trocar-imagem').show();
                // use the Jcrop API to get the real image size
                var bounds = this.getBounds();
                boundx = bounds[0];
                boundy = bounds[1];

                // Store the Jcrop API in the jcrop_api variable
                jcrop_api = this;
            });

        };
    };

    // read selected file as DataURL
    oReader.readAsDataURL(oFile);
}