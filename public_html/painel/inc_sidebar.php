<div class="left-nav clearfix">
    <div class="left-primary-nav">
        <ul id="myTab">
            <li class="active"><a href="#forms" class="icon-th-large" title="Forms"></a></li>
        </ul>
    </div>
    <div class="responsive-leftbar">
        <i class="icon-list"></i>
    </div>
    <div class="left-secondary-nav tab-content">
        <div class="tab-pane active" id="forms">
            <h4 class="side-head">Menu</h4>
            <ul id="nav" class="accordion-nav">
                <?php
                if ($usuarioLogado->getTipo() == 2) {
                    if (!is_array($usuarioLogado->getPaginas())) {
                        $usuarioLogado->setPaginas(explode('|', $usuarioLogado->getPaginas()));
                    }
                }

                $jsonReader= new \CoffeeCore\Helper\JsonReader('paginas');
                $paginas = $jsonReader->getFileContent(true);
                foreach ($paginas as $pagina) {
                    if ($usuarioLogado->getTipo() == 2) {

                        if (!in_array($pagina['slug'], $usuarioLogado->getPaginas())) {
                            continue;
                        }
                    }
                ?>
                        <li><a href="<?=$prefix;?><?=$pagina['slug'];?>"><i class="<?=$pagina['icon'];?>"></i> <?=$pagina['pagina'];?></a></li>
                <?php
                }
                ?>
            </ul>
        </div>
    </div>
</div>
