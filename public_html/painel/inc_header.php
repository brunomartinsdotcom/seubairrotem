    <!-- Navbar
    ================================================== -->
    <div class="navbar navbar-inverse top-nav">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand" href="<?=$prefix;?>">
                    <img style="width: 105px; height: 50px" src="<?=$prefix;?>images/logo.png" width="105" height="50" alt="Guia Seu Bairro Tem" />
                </a>
                <div class="btn-toolbar pull-right notification-nav">
                    <div class="btn-group">
                        <div class="dropdown">
                            <a href="<?=$prefix;?>/sair" class="btn btn-notification">
                                <i class="icon-remove"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="leftbar leftbar-close clearfix">