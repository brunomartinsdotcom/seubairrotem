$(window).load(function(){
	$(".btn-login").click(function() {
		var e = document.getElementById("login");
		var dis = $("#login").css("display");
		
		$("#login").toggle("fast");
		if(dis == "none"){
			$(".btn-login i").addClass('fi-x');
			$(".btn-login i").removeClass('fi-lock');
		}else{
			$(".btn-login i").addClass('fi-lock');
			$(".btn-login i").removeClass('fi-x');
		}
	});
	
	$("#menu-footer").change(function(){
		window.location = $(this).val();
	});
	
	$("a#esqueceu-senha").click(function() {
		var bt_back = $('a#esqueceu-senha-back');
		var form_login = $("form#form-login");
		var form_e_s = $("form#form-esqueceu-senha");
		
		$(this).hide('fade');
		bt_back.show('fade');
		form_login.hide('fade');
		form_e_s.show('fade');
	});
	$("a#esqueceu-senha-back").click(function() {
		var bt_back = $('a#esqueceu-senha');
		var form_login = $("form#form-login");
		var form_e_s = $("form#form-esqueceu-senha");
		
		$(this).hide('fade');
		bt_back.show('fade');
		form_login.show('fade');
		form_e_s.hide('fade');
	});
	
	// VALIDA FORM LOGIN
	$('#form-login').validate({
		// define regras para os campos
		rules: {
			login: {
				required: true
			},
			senha: {
				required: true
			}
		}
	});
	// VALIDA FORM ESQUECEU SENHA
	$('#form-esqueceu-senha').validate({
		// define regras para os campos
		rules: {
			cpf: {
				required: true,
                digits: true
			}
		},
		// define messages para cada campo
		messages: {
			cpf	: { required:"Informe o número de seu documento" }
		}
	});
});
function show_hide(id) {
   var e = document.getElementById(id);
   if(e.style.display == 'block')
	  e.style.display = 'none';
   else
	  e.style.display = 'block';
}