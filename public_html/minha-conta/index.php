<?php
require_once "../../config.php";

if (!isset($_SESSION)) {
    session_cache_expire(30);
    session_start();
}

$app = new \Intranet\Application($app);
