            <ul class="off-canvas-list">
			<li><h2>Minha Conta</h2><span class="arrow-menu"></span></li>
                <li><a href="/minha-conta/meus-dados/" title="Meus Dados">Meus Dados</a></li>
                <li><a href="/minha-conta/historico-de-faturas/" title="Histórico de Faturas">Histórico de Faturas</a></li>
            <li class="separacao"></li>
			<li><h2>Meus Anúncios</h2><span class="arrow-menu"></span></li>
                <li><a href="/minha-conta/adicionar-anuncio" title="Adicionar Anúncio">Adicionar Anúncio</a></li>
                <li><a href="/minha-conta/anuncios-ativos" title="Anúncios Ativos">Anúncios Ativos</a></li>
                <li><a href="/minha-conta/anuncios-pendentes" title="Anúncios Pendentes">Anúncios Pendentes</a></li>
                <li><a href="/minha-conta/anuncios-bloqueados" title="Anúncios Bloqueados">Anúncios Bloqueados</a></li>
                <li><a href="/minha-conta/anuncios-expirados" title="Anúncios Expirados">Anúncios Expirados</a></li>
                <li><a href="/minha-conta/anuncios-cancelados" title="Anúncios Cancelados">Anúncios Cancelados</a></li>
                <li><a href="/minha-conta/perguntas-anuncio" title="Perguntas/Anúncio">Perguntas/Anúncio</a></li>
            <li class="separacao"></li>
			<li><h2>Meus Destaques</h2><span class="arrow-menu"></span></li>
                <li><a href="/minha-conta/destaques-ativos" title="Destaques Ativos">Destaques Ativos</a></li>
                <li><a href="/minha-conta/destacar-anuncio" title="Destacar Anúncio">Destacar Anúncio</a></li>
                <li><a href="/minha-conta/historico-destaques" title="Histórico de Destaques">Histórico de Destaques</a></li>
            <li class="separacao"></li>
			<li><h2>Ajuda</h2><span class="arrow-menu"></span></li>
                <li><a href="<?=url_base();?>/duvidas" title="Dúvidas Frequentes">Dúvidas Frequentes</a></li>
                <li><a href="<?=url_base();?>/minha-conta/tipos-de-destaque" title="Tipos de Destaque">Tipos de Destaque</a></li>
		</ul>
		<!-- .banner1-1 -->
        <?php
        if (isset($publicidade['sidebar1']['html']) and (!empty($publicidade['sidebar1']['html']))) {
            echo "<div class='banner1-1'>{$publicidade['sidebar1']['html']}<small></small></div>";
        }
        ?>
            <?php if(!empty($siteConfig['facebook'])){ ?>
                <!-- .facebook -->
                <div class="facebook">
                    <header>
                        <h2>Facebook</h2>
                        <span class="arrow-facebook"></span>
                    </header>
                    <iframe src="//www.facebook.com/plugins/likebox.php?href=<?= (!empty($siteConfig['facebook'])) ? $siteConfig['facebook'] : "https://www_antigo.facebook.com/cafecomideias" ;?>&amp;width=220&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=180132602163487" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:220px; height:258px;" allowTransparency="true"></iframe>
                </div><!-- /.facebook -->
            <?php } ?>
            <!-- .newsletter -->
            <div class="newsletter">
                <header>
                    <h2>Informativos</h2>
                    <span class="arrow-newsletter"></span>
                </header>
                <div>
                    <p>Cadastre-se e receba nossos informativos em seu e-mail.</p>
                    <form name="newsletter" id="newsletter" action="/newsletter" method="post" enctype="multipart/form-data">
                        <input name="newsnome" id="newsnome" type="text" placeholder="Nome" required="true" />
                        <input name="newsemail" id="newsemail" type="text" placeholder="E-mail" required="true" />
                        <input name="bt-ok" id="bt-ok" type="submit" value="OK" />
                    </form>
                </div>
            </div><!-- /.newsletter -->
            <!-- banner -->
            <div>
                <?php
                echo (isset($publicidade['sidebar2']['html']) and (!empty($publicidade['sidebar2']['html']))) ? '<div class="bannerselo"><small></small>'.$publicidade['sidebar2']['html'].'</div>' : '';
                ?>
            </div><!-- banner -->

            <script>
                $(document).ready(function(){
                    $("form#newsletter").validate({
                        "rules": {
                            "newsnome": {
                                "required": true,
                                "minlength": 4
                            },
                            "newsemail": {
                                "required": true,
                                "email": true,
                                "minlength": 6
                            }
                        },
                        "messages":{
                            "newsnome":{required:"Campo Obrigatório."},
                            "newsemail":{
                                required:"Campo Obrigatório.",
                                email: "Precisa ser um email válido."
                            }
                        }
                    })
                })
            </script>