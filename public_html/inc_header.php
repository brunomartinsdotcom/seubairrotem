<div class="top">
    <div class="row">
        <div class="large-12 medium-12 small-12 columns">
            <?php
                $intra = Intranet\Authentication::getInstance();
                $userAutenticated = $intra->getFromSession("entity");
                if ($intra->isAuthenticated()) {
            ?>
            <!-- FORMULARIO SEM LOGIN -->
            <h3>Olá <strong><?=$intra->getFromSession('name');?></strong></h3>
            <p>
                <a href="<?= $prefixLink; ?>minha-conta" title="Minha Conta">Minha Conta</a>
                &nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="<?= $prefixLink; ?>minha-conta/meus-dados" title="Alterar Meus Dados">Alterar Meus Dados</a>
                &nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="<?= $prefixLink; ?>minha-conta/sair" title="Sair">Sair</a>
            </p>
            <?php }else{ ?>
            Olá visitante, seja bem vindo!<span> | Terça-feira, <?=date('d/m/Y H:i');?> h</span>
            <?php } ?>
        </div>
    </div>
</div>
<header id="header">
    <div class="row">
        <div class="large-3 medium-3 small-12 box-logo columns left">
            <h1 class="logo">Guia Seu Bairro Tem</h1>
        </div>
        <div class="large-9 medium-9 small-12 columns right text-right ">
            <div class="publicidade">
                <span>Publicidade</span>
                <div class="banner right text-right">
                    <a href="#"><img src="<?=$prefixLink;?>images/_temp/banner_pizzaria_650x90.jpg" alt="Pizzaria" /></a>
                </div>
            </div>
            <nav class="top-bar" data-topbar role="navigation">
                <ul class="title-area">
                    <li class="name"></li>
                    <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
                    <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
                </ul>
                <section class="top-bar-section">
                    <!-- Right Nav Section -->
                    <ul class="right">
                        <li<?php if($pagina == 'home'){ ?> class="active"<?php } ?>><a href="<?=$prefixLink;?>" title="Home">Home</a></li>
                        <li<?php if($pagina == 'guia-comercial'){ ?> class="active"<?php } ?>><a href="<?=$prefixLink;?>guia-comercial" title="Guia Comercial">Guia Comercial</a></li>
                        <li<?php if($pagina == 'cadastre-se'){ ?> class="active"<?php } ?>><a href="<?=$prefixLink;?>cadastre-se" title="Cadastre-se">Cadastre-se</a></li>
                        <li<?php if($pagina == 'minha-conta' or $pagina == 'login'){ ?> class="active"<?php } ?>><a href="<?=$prefixLink;?>minha-conta" title="Minha Conta">Minha Conta</a></li>
                        <li<?php if($pagina == 'anuncie'){ ?> class="active"<?php } ?>><a href="<?=$prefixLink;?>anuncie" title="Anuncie">Anuncie</a></li>
                        <li<?php if($pagina == 'duvidas'){ ?> class="active"<?php } ?>><a href="<?=$prefixLink;?>duvidas" title="Dúvidas?">Dúvidas?</a></li>
                        <li<?php if($pagina == 'o-guia'){ ?> class="active"<?php } ?>><a href="<?=$prefixLink;?>o-guia" title="O Guia">O Guia</a></li>
                        <li<?php if($pagina == 'contato'){ ?> class="active"<?php } ?>><a href="<?=$prefixLink;?>contato" title="Contato">Contato</a></li>
                    </ul>
                </section>
            </nav>
        </div>
        <div class="clearfix"></div>
        <div class="large-8 medium-6 small-12 left columns"><h2 class="encontre-sua-empresa">Encontre sua Empresa</h2></div>
        <div class="large-4 medium-6 hide-for-small right text-right redes-sociais columns">
            <p>Siga-nos</p>
            <ul>
                <?php if(isset($dadosDoSite['facebook']) and !empty($dadosDoSite['facebook'])){ ?>
                <li><a href="<?=$dadosDoSite['facebook'];?>" target="_blank" class="f" title="Facebook"></a></li>
                <?php } ?>
                <?php if(isset($dadosDoSite['twitter']) and !empty($dadosDoSite['twitter'])){ ?>
                <li><a href="<?=$dadosDoSite['twitter'];?>" target="_blank" class="t" title="Twitter"></a></li>
                <?php } ?>
                <?php if(isset($dadosDoSite['instagram']) and !empty($dadosDoSite['instagram'])){ ?>
                <li><a href="<?=$dadosDoSite['instagram'];?>" target="_blank" class="i" title="Instagram"></a></li>
                <?php } ?>
                <?php if(isset($dadosDoSite['youtube']) and !empty($dadosDoSite['youtube'])){ ?>
                <li><a href="<?=$dadosDoSite['youtube'];?>" target="_blank" class="y" title="Youtube"></a></li>
                <?php } ?>
            </ul>
        </div>
        <!-- FORM BUSCA -->
        <div class="large-12 medium-12 small-12 columns">
            <div class="busca-duvidas-intranet">
                <form name="formbusca" id="formbusca" action="" method="post" enctype="multipart/form-data">
                    <div class="large-7 medium-6 small-12 left">
                        <p class="large-7 medium-7 small-6 columns">
                            <label for="buscarapida">Busca Rápida</label>
                            <input name="buscarapida" id="buscarapida" type="text" placeholder="Nome da Empresa" />
                        </p>
                        <p class="large-5 medium-5 small-6 columns">
                            <label for="categoria">Categoria</label>
                            <select name="categoria" id="categoria">
                                <option value="">Escolha...</option>
                            </select>
                        </p>
                        <div class="clearfix"></div>
                        <p class="large-3 medium-3 small-3 left columns">
                            <label for="b-estado">Estado</label>
                            <select name="estado" id="b-estado">
                                <option value="">Escolha...</option>
                            </select>
                        </p>
                        <p class="large-5 medium-5 small-5 left columns">
                            <label for="b-cidade">Cidade</label>
                            <select name="cidade" id="b-cidade">
                                <option value="">Escolha...</option>
                            </select>
                        </p>
                        <p class="large-4 medium-4 small-4 left columns">
                            <label for="b-bairro">Bairro</label>
                            <select name="bairro" id="b-bairro">
                                <option value="">Escolha...</option>
                            </select>
                        </p>
                    </div>
                    <script type="application/javascript">
                        $(document).ready(function(){
                            $("#form-search").submit(function(e){
                                e.preventDefault;
                                window.location = "/buscar/" + $("#palavra_chave").val();
                                return false;
                            })
                        })
                    </script>
                    <?php
                        $duvidasHeader = \CoffeeCore\Helper\ResourceManager::readFile("duvidasHeader.json");
                    ?>
                    <div class="large-5 medium-6 small-12 left">
                        <p class="large-5 medium-5 small-12 left columns"><button name="btnbuscar" type="submit">Buscar</button></p>
                        <section class="large-7 medium-7 hide-for-small left text-right duvidas-intranet columns">
                            <?php
                                $abrevia = new \CoffeeCore\Helper\Abrevia();
                                if(isset($duvidasHeader) and is_array($duvidasHeader)):
                            ?>
                            <p>Dúvidas?</p>
                            <?php foreach ($duvidasHeader as $duvida): ?>
                            <a href="<?=$prefixLink;?>duvidas" title="<?=$duvida['pergunta'];?>"><?=$abrevia->abreviar($duvida['pergunta'], 35);?></a>
                            <?php endforeach; endif; ?>
                        </section>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</header>