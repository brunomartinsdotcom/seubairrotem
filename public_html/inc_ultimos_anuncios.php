<?php
$abreviador = new \CoffeeCore\Helper\Abrevia();

/*$ultimosAnuncios = \CoffeeCore\Storage\DoctrineStorage::orm()
    ->getRepository(\Anuncio\Entity::FULL_NAME)
    ->findBy(["status" => 1], ["id" => "DESC"], 20);*/

$conn = \CoffeeCore\Storage\DoctrineStorage::orm()->getConnection();

$sql = "SELECT a.* FROM anuncio a
            LEFT JOIN faturaplano fp ON a.id = fp.anuncio
            WHERE
            DATE_FORMAT(a.datavalidade,'%Y-%m-%d') = fp.datavencimento AND
            a.status = 1 AND
            a.datavalidade >= CURDATE() AND
            fp.fatura IN (SELECT id FROM fatura WHERE ((codigotransacao='GRATUITO' OR codigotransacao='') AND (statuspagseguro=0 OR statuspagseguro=null))
            OR (codigotransacao!='GRATUITO' AND codigotransacao!=null AND codigotransacao!='' AND (fp.valor=0 OR (fp.valor > 0 AND (statuspagseguro=3 OR statuspagseguro=4) ) ) ) )
            ORDER BY a.datainclusao DESC LIMIT 0,20";
//echo $sql;
$ultimosAnuncios = $conn->fetchAll($sql);
if (!empty($ultimosAnuncios)) {
?>
    <!-- .ultimos-anuncios -->
    <div class="large-12 medium-12 small-12 ultimos-anuncios-zera columns">
        <article class="ultimos-anuncios">
            <header>
                <h2>Últimos Anúncios</h2>
                <span class="arrow-cotacoes"></span>
            </header>
            <div class="slider1">
                <?php foreach ($ultimosAnuncios as $anuncio) { ?>
                    <div class="slide">
                        <a href="/anuncio/<?="{$anuncio['id']}/";?>" title="<?=$anuncio['titulo'];?>">
                            <img style="height: 90px; width: 120px" src="<?= $prefixLink; ?>_uploads/anuncios/<?=$anuncio['fotocapa'];?>" alt="<?=$anuncio['titulo'];?>"/>
                            <?= $abreviador->abreviar($anuncio['titulo'], 46);?><br/>
                            <em class="<?php if($anuncio['tipo'] == 1){ ?>red<?php }else{ ?>green<?php } ?>-price">Preço a vista: R$ <?=number_format($anuncio['precoavista'], 2, ',', '.');?>/<?php if($anuncio['unidade'] == ''){ echo $anuncio['outraunidade']; }else{ echo $anuncio['unidade']; };?>.</em>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </article>
    </div><!-- /.ultimos-anuncios -->
<?php
}
?>