<?php
$guiacomercial = \CoffeeCore\Helper\ResourceManager::readFile('guia-comercial.json');
if (!isset($getCategoria)) {
    $getCategoria = "";
}
?>
                <nav class="tab-bar show-for-medium-down">
                    <section class="left-small"><a class="left-off-canvas-toggle menu-icon" ><span></span></a></section>
                    <section class="middle tab-bar-section"></section>
                </nav>
                <aside class="left-off-canvas-menu">
                    <!-- .categorias -->
                    <div class="categorias-titulo">
                        <h2>Categorias</h2>
                    </div>
                    <ul class="off-canvas-list">
                        <?php foreach ($guiacomercial as $guia) { ?>
                        <li>
                            <a href="/guia-comercial/<?=create_slug($guia['titulo']);?>"
                               class="<?=($getCategoria == create_slug($guia['titulo']) ? "ativo" : "");?>"
                               title="<?=$guia['titulo'];?>"> <?=$guia['titulo'];?>
                            </a>
                        </li>
                        <?php } ?>
                    </ul><!-- /.categorias -->
                    <!-- .newsletter -->
                    <div class="newsletter">
                        <header><h3>Newsletter</h3></header>
                        <form id="newsletter" name="newsletter" action="<?=$prefixLink;?>newsletter" method="post" enctype="multipart/form-data">
                            <input name="current-page" type="hidden" value="<?=$_SERVER["REQUEST_URI"];?>" />
                            <p>Cadastre-se e receba nossos informativos em seu e-mail.</p>
                            <p><input name="n-nome" id="n-nome" type="text" placeholder="Nome" /></p>
                            <p><input name="n-celular" id="n-celular" type="text" placeholder="Celular" /></p>
                            <p><input name="n-email" id="n-email" type="text" placeholder="E-mail" /></p>

                            <p><button name="bt-ok" id="bt-ok" type="submit">Ok</button></p>
                        </form>
                    </div><!-- /.newsletter -->
                    <!-- .previsao-do-tempo -->
                    <div class="previsao-do-tempo">
                        <header><h3>Tempo</h3></header>
                        <div>
                            <!-- Widget Previs&atilde;o de Tempo CPTEC/INPE -->
                            <iframe allowtransparency="true" marginwidth="0" marginheight="0" hspace="0" vspace="0" frameborder="0" scrolling="no" src="http://www.cptec.inpe.br/widget/widget.php?p=5517&w=h&c=00bfff&f=ffffff" height="200px" width="215px"></iframe><noscript>Previs&atilde;o de <a href="http://www.cptec.inpe.br/cidades/tempo/5517">Uberlândia/MG</a> oferecido por <a href="http://www.cptec.inpe.br">CPTEC/INPE</a></noscript>
                            <!-- Widget Previs&atilde;o de Tempo CPTEC/INPE -->
                        </div>
                    </div><!-- .previsao-do-tempo -->
                </aside>
                <script type="text/javascript">
                    $(document).ready(function(){
                        $('#n-celular').mask('(00) 0000-00009');
                        $("form#newsletter").validate({
                            "rules": {
                                "n-nome": {
                                    "required": true,
                                    "minlength": 4
                                },
                                "n-email": {
                                    "required": true,
                                    "email": true,
                                    "minlength": 6
                                }
                            },
                            "messages":{
                                "n-nome":{required:"Campo Obrigatório."},
                                "n-email":{
                                    required:"Campo Obrigatório.",
                                    email: "Precisa ser um email válido."
                                }
                            }
                        });
                    });
                </script>