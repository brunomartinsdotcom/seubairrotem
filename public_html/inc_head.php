<?php
$prefixLink = url_base() . "/";
$dadosDoSite = \CoffeeCore\Helper\ResourceManager::readFile("dadosDoSite.json");
$paginas = \CoffeeCore\Helper\ResourceManager::readFile("paginas.json");
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="pt-br"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="pt-br"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="pt-br"> <![endif]-->
<!--[if IE 9]><html class="lt-ie10" lang="pt-br" > <![endif]-->
<html class="no-js" lang="pt-br">
<head lang="pt-br">
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="format-detection" content="telephone=no" />
<meta name="author" content="Agência Café com Ideias" />
<link rel="shortcut icon" type="image/png" href="<?=$prefixLink;?>favicon.png"/>
<link rel="shortcut icon" href="<?=$prefixLink;?>favicon.ico"/>
<link rel="apple-touch-icon" href="<?=$prefixLink;?>images/apple-touch-icon.png" />
<!-- CSS -->
<link rel="stylesheet" href="<?=$prefixLink;?>css/normalize.css" />
<link rel="stylesheet" href="<?=$prefixLink;?>css/foundation.css" />
<link rel="stylesheet" href="<?=$prefixLink;?>css/custom.css" />
<link rel="stylesheet" href="<?=$prefixLink;?>css/responsive.css" />
<script src="<?=$prefixLink;?>js/modernizr.js"></script>
<script src="<?=$prefixLink;?>js/jquery.js"></script>
<script src="<?=$prefixLink;?>js/jquery.validate.js"></script>
<script src="<?=$prefixLink;?>js/mask.js"></script>
<!-- ANALYTICS -->
<?=$dadosDoSite['analytics'];?>