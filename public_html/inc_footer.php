<!-- .menu-footer -->
<div class="menu-footer">
    <!-- .row -->
    <div class="row">
        <div class="menu-link hide-for-small">
            <a href="<?=$prefixLink;?>" title="Home"<?php if($pagina == 'home'){ ?> class="active"<?php } ?>>Home</a>
            <a href="<?=$prefixLink;?>guia-comercial" title="Guia Comercial"<?php if($pagina == 'guia-comercial'){ ?> class="active"<?php } ?>>Guia Comercial</a>
            <a href="<?=$prefixLink;?>cadastre-se" title="Cadastre-se"<?php if($pagina == 'cadastre-se'){ ?> class="active"<?php } ?>>Cadastre-se</a>
            <a href="<?=$prefixLink;?>anuncie" title="Anuncie"<?php if($pagina == 'anuncie'){ ?> class="active"<?php } ?>>Anuncie</a>
            <a href="<?=$prefixLink;?>minha-conta" title="Minha Conta"<?php if($pagina == 'minha-conta' or $pagina == 'login'){ ?> class="active"<?php } ?>>Minha Conta</a>
            <a href="<?=$prefixLink;?>duvidas" title="Dúvidas?"<?php if($pagina == 'duvidas'){ ?> class="active"<?php } ?>>Dúvidas?</a>
            <a href="<?=$prefixLink;?>o-guia" title="O Guia"<?php if($pagina == 'o-guia'){ ?> class="active"<?php } ?>>O Guia</a>
            <a href="<?=$prefixLink;?>contato" title="Contato"<?php if($pagina == 'contato'){ ?> class="active"<?php } ?>>Contato</a>
        </div>
        <div class="small-12 text-center show-for-small menu-small columns">
            <select name="select-menu-footer" id="select-menu-footer">
                <option value="">Menu</option>
                <option value="<?=$prefixLink;?>"<?php if($pagina == 'home'){ ?> selected<?php } ?>>Home</option>
                <option value="<?=$prefixLink;?>guia-comercial"<?php if($pagina == 'guia-comercial'){ ?> selected<?php } ?>>Guia Comercial</option>
                <option value="<?=$prefixLink;?>cadastre-se"<?php if($pagina == 'cadastre-se'){ ?> selected<?php } ?>>Cadastre-se</option>
                <option value="<?=$prefixLink;?>anuncie"<?php if($pagina == 'anuncie'){ ?> selected<?php } ?>>Anuncie</option>
                <option value="<?=$prefixLink;?>minha-conta"<?php if($pagina == 'minha-conta'){ ?> selected<?php } ?>>Minha Conta</option>
                <option value="<?=$prefixLink;?>duvidas"<?php if($pagina == 'duvidas'){ ?> selected<?php } ?>>Dúvidas?</option>
                <option value="<?=$prefixLink;?>o-guia"<?php if($pagina == 'o-guia'){ ?> selected<?php } ?>>O Guia</option>
                <option value="<?=$prefixLink;?>contato"<?php if($pagina == 'contato'){ ?> selected<?php } ?>>Contato</option>
            </select>
        </div>
    </div><!-- /.row -->
</div><!-- /.menu-footer -->
<!-- #footer -->
<footer id="footer">
    <!-- .row -->
    <div class="row">
        <div class="banner-footer">
            <a href="#"><img src="<?=$prefixLink;?>images/_temp/banner_apack_720x90.jpg" alt="APack" /></a>
        </div>
        <!-- .redes-sociais -->
        <div class="redes-sociais">
            <ul>
                <?php if(isset($dadosDoSite['facebook']) and !empty($dadosDoSite['facebook'])){ ?>
                <li><a href="<?=$dadosDoSite['facebook'];?>" target="_blank" class="f" title="Facebook"></a></li>
                <?php } ?>
                <?php if(isset($dadosDoSite['twitter']) and !empty($dadosDoSite['twitter'])){ ?>
                <li><a href="<?=$dadosDoSite['twitter'];?>" target="_blank" class="t" title="Twitter"></a></li>
                <?php } ?>
                <?php if(isset($dadosDoSite['instagram']) and !empty($dadosDoSite['instagram'])){ ?>
                <li><a href="<?=$dadosDoSite['instagram'];?>" target="_blank" class="i" title="Instagram"></a></li>
                <?php } ?>
                <?php if(isset($dadosDoSite['youtube']) and !empty($dadosDoSite['youtube'])){ ?>
                <li><a href="<?=$dadosDoSite['youtube'];?>" target="_blank" class="y" title="Youtube"></a></li>
                <?php } ?>
            </ul>
        </div><!-- /.redes-sociais -->
        <!-- .copy -->
        <div class="large-12 medium-12 small-12 copy columns">
            <span>© Copyright 2014 Guia Seu Bairro Tem - Todos os Direitos Reservados.</span>
            <a href="http://www.cafecomideias.com.br" target="_blank" class="logo-cci" title="Agência Café com Ideias"></a>
        </div><!-- /.copy -->
    </div><!-- /.row -->
</footer><!-- /#footer -->
<script src="<?=$prefixLink;?>js/script.js"></script>
<script src="<?=$prefixLink;?>js/foundation.min.js"></script>
<script>
$(document).foundation();
</script>
<?php if(isset($flash['info']) and !empty($flash['info'])){ ?>
<script type="text/javascript">
$(document).ready(function(){
    alert('<?=$flash["info"];?>');
});
</script>
<?php } ?>
<?php
\CoffeeCore\Storage\DoctrineStorage::orm()->close();