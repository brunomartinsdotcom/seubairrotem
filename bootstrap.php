<?php

session_save_path("/tmp");

define("ROOT", dirname($_SERVER["DOCUMENT_ROOT"]) . DIRECTORY_SEPARATOR);

date_default_timezone_set('America/Sao_Paulo');

require_once 'vendor/autoload.php';

/**
 * @return string
 */
function url_base()
{
    $url  =  "http://" . $_SERVER["SERVER_NAME"];
    $url .=  $_SERVER["SERVER_PORT"] == "80" ? "" : ":"  . $_SERVER["SERVER_PORT"];

    return $url;
}

$entityManager = \CoffeeCore\Storage\DoctrineStorage::orm();

